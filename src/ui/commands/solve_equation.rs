use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::errors::UIError;
use crate::entities::ui::{Command, SolveEquation};
use crate::proto::parser::IParser;
use crate::proto::ui::ICommandParser;
use crate::ui::utils::{parse_interval, CommandLineSeparator};

pub struct CParser {
    expr_parser: Rc<dyn IParser>,
}

impl CParser {
    pub fn new(expr_parser: Rc<dyn IParser>) -> CParser {
        CParser { expr_parser }
    }
}

impl ICommandParser for CParser {
    fn command(&self) -> String {
        "solve_equation".to_string()
    }

    fn help_head(&self) -> String {
        String::from("args: '`equation`' [-i `from`,`to`]")
    }

    fn help_body(&self) -> String {
        String::from(
            "
Equation is `expr = expr`
If equation can not be solved with direct rule, derivarive method will be used
Set `from` and `to` values if you want to use derivarive method
        ",
        )
    }

    fn parse(&self, args: &[String]) -> Result<Command, UIError> {
        let separated = CommandLineSeparator::from_line(args, &["-i"], &[""])?;
        if separated.free_args.is_empty() {
            return Err(UIError::ArgNotSet("expression".to_string()));
        }
        if separated.free_args.len() > 1 {
            return Err(UIError::ArgNotSet("expression must be one".to_string()));
        }
        let expr = match self.expr_parser.parse(&separated.free_args[0]) {
            Err(e) => return Err(UIError::InvalidExpression(e)),
            Ok(val) => val,
        };
        let config = Config::new();
        let interval = match separated.keys.get("-i") {
            None => None,
            Some(val) => Some(parse_interval(val)?),
        };
        let solve = SolveEquation {
            expression: expr,
            config: config.set_interval(interval),
        };
        Ok(Command::SolveEquation(solve))
    }
}

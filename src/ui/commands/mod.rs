mod simplify;
mod solve_equation;
mod solve_system;

use crate::proto::parser::IParser;
use crate::proto::ui::ICommandParser;
use std::rc::Rc;

pub fn commands(parser: Rc<dyn IParser>) -> Vec<Box<dyn ICommandParser>> {
    vec![
        Box::new(simplify::CParser::new(parser.clone())),
        Box::new(solve_equation::CParser::new(parser.clone())),
        Box::new(solve_system::CParser::new(parser)),
    ]
}

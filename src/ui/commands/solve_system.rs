use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::errors::{ParserError, UIError};
use crate::entities::expression::PExpression;
use crate::entities::ui::{Command, SolveSystem};
use crate::proto::parser::IParser;
use crate::proto::ui::ICommandParser;
use crate::ui::utils::{parse_interval, CommandLineSeparator};

const INTERVAL: &str = "-i";
const FIND_BEST: &str = "-b";
const USE_SQUAD: &str = "--squad";
const USE_ADDITIONAL: &str = "--add";

pub struct CParser {
    expr_parser: Rc<dyn IParser>,
}

impl CParser {
    pub fn new(expr_parser: Rc<dyn IParser>) -> CParser {
        CParser { expr_parser }
    }
}

impl ICommandParser for CParser {
    fn command(&self) -> String {
        "solve_system".to_string()
    }

    fn help_head(&self) -> String {
        format!(
            "args: '`equation1`' ['`equation2`' [...]] [{} `from`,`to`] [{}] [{}]",
            INTERVAL, FIND_BEST, USE_SQUAD,
        )
    }

    fn help_body(&self) -> String {
        format!(
            "
Equation is `expr = expr`
If equation can not be solved with direct rule, derivarive method will be used
Set `from` and `to` values if you want to use derivarive method

if {} is not set then algorithm will stop on first solution else it will try best solution
if {} is set then extraction variables with \"square equation\" will be available
if {} is set then addition extractor will be used
        ",
            FIND_BEST, USE_SQUAD, USE_ADDITIONAL
        )
    }

    fn parse(&self, args: &[String]) -> Result<Command, UIError> {
        let separated = CommandLineSeparator::from_line(
            args,
            &[INTERVAL],
            &[FIND_BEST, USE_SQUAD, USE_ADDITIONAL],
        )?;
        if separated.free_args.is_empty() {
            return Err(UIError::ArgNotSet("expression".to_string()));
        }
        let expressions_wrapped = separated
            .free_args
            .iter()
            .map(|src| self.expr_parser.parse(src))
            .collect::<Result<Vec<PExpression>, ParserError>>();
        let expressions = match expressions_wrapped {
            Err(e) => return Err(UIError::InvalidExpression(e)),
            Ok(val) => val,
        };
        let interval = match separated.keys.get(INTERVAL) {
            None => None,
            Some(val) => Some(parse_interval(val)?),
        };
        let find_best = separated.flags.contains(FIND_BEST);
        let use_squad = separated.flags.contains(USE_SQUAD);
        let use_additional = separated.flags.contains(USE_ADDITIONAL);
        let config = Config::new()
            .set_interval(interval)
            .set_stop_on_first(!find_best)
            .set_use_squad_rule(use_squad)
            .set_use_additional_extractor(use_additional);
        let solve = SolveSystem {
            expressions,
            config,
        };
        Ok(Command::SolveSystem(solve))
    }
}

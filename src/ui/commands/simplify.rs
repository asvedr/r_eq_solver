use std::rc::Rc;

use crate::entities::errors::UIError;
use crate::entities::ui::{Command, Simplify};
use crate::proto::parser::IParser;
use crate::proto::ui::ICommandParser;

pub struct CParser {
    expr_parser: Rc<dyn IParser>,
}

impl CParser {
    pub fn new(expr_parser: Rc<dyn IParser>) -> CParser {
        CParser { expr_parser }
    }
}

impl ICommandParser for CParser {
    fn command(&self) -> String {
        "simplify".to_string()
    }

    fn help_head(&self) -> String {
        String::from("args: `expr`")
    }

    fn help_body(&self) -> String {
        String::from("Expression will be simplified and printed")
    }

    fn parse(&self, args: &[String]) -> Result<Command, UIError> {
        if args.is_empty() {
            return Err(UIError::ArgNotSet("expression".to_string()));
        }
        match self.expr_parser.parse(&args[0]) {
            Err(e) => Err(UIError::InvalidExpression(e)),
            Ok(val) => Ok(Command::Simplify(Simplify { expression: val })),
        }
    }
}

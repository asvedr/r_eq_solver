mod command_parser;
mod commands;
mod utils;

use crate::parser;
pub use command_parser::CommandsParser;
use std::rc::Rc;

pub fn create() -> CommandsParser {
    let parsers = commands::commands(Rc::new(parser::create()));
    CommandsParser::new(parsers)
}

use std::env;
use std::fmt::Write;

use crate::entities::errors::UIError;
use crate::entities::ui::Command;
use crate::proto::ui::{ICommandParser, ICommandsParser};
use std::collections::HashMap;

pub struct CommandsParser {
    parsers: HashMap<String, Box<dyn ICommandParser>>,
}

impl CommandsParser {
    pub fn new(parsers_vec: Vec<Box<dyn ICommandParser>>) -> CommandsParser {
        let parsers = parsers_vec
            .into_iter()
            .map(|parser| {
                let key = parser.command();
                (key, parser)
            })
            .collect::<HashMap<String, Box<dyn ICommandParser>>>();
        CommandsParser { parsers }
    }

    fn total_help(&self) -> String {
        let mut result = String::from("commands:\n");
        writeln!(result, "help `command`: description of command");
        for (command, parser) in self.parsers.iter() {
            writeln!(result, "{} {}", command, parser.help_head());
        }
        result
    }

    fn help(&self, args: &[String]) -> Result<Command, UIError> {
        if args.is_empty() {
            return Ok(Command::Help(self.total_help()));
        }
        if let Some(parser) = self.parsers.get(&args[0]) {
            let msg = format!("{}\n{}", parser.help_head(), parser.help_body());
            Ok(Command::Help(msg))
        } else {
            Err(UIError::UnknownCommand(args[0].clone()))
        }
    }
}

impl ICommandsParser for CommandsParser {
    fn get_command(&self) -> Result<Command, UIError> {
        let args = env::args().collect::<Vec<String>>();
        if args.len() == 1 {
            return self.help(&[]);
        }
        let cmd: &str = &args[1];
        if matches!(cmd, "-h" | "--help") {
            return self.help(&[]);
        }
        if cmd == "help" {
            return self.help(&args[2..]);
        }
        if let Some(parser) = self.parsers.get(cmd) {
            parser.parse(&args[2..])
        } else {
            Err(UIError::UnknownCommand(cmd.to_string()))
        }
    }
}

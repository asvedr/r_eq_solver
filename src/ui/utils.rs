use crate::entities::errors::UIError;
use crate::number::Number;
use std::collections::{BTreeMap, BTreeSet};

pub struct CommandLineSeparator {
    pub free_args: Vec<String>,
    pub keys: BTreeMap<String, String>,
    pub flags: BTreeSet<String>,
}

impl CommandLineSeparator {
    pub fn from_line(
        line: &[String],
        available_keys: &[&str],
        available_flags: &[&str],
    ) -> Result<CommandLineSeparator, UIError> {
        let available_keys: BTreeSet<String> =
            available_keys.iter().map(|x| x.to_string()).collect();
        let available_flags: BTreeSet<String> =
            available_flags.iter().map(|x| x.to_string()).collect();
        let mut keys = BTreeMap::new();
        let mut flags = BTreeSet::new();
        let mut free_args = Vec::new();
        let mut i = 0;
        while i < line.len() {
            let param = line[i].clone();
            if available_keys.contains(&param) {
                if i + 1 >= line.len() {
                    return Err(UIError::InvalidParam(param));
                }
                let value = line[i + 1].clone();
                keys.insert(param, value);
                i += 2;
                continue;
            }
            if available_flags.contains(&param) {
                flags.insert(param);
            } else {
                free_args.push(param);
            }
            i += 1;
        }
        Ok(CommandLineSeparator {
            keys,
            flags,
            free_args,
        })
    }
}

fn parse_num(source: &str) -> Result<Number, UIError> {
    let split = source.split('/').collect::<Vec<_>>();
    if split.len() == 2 {
        let num = match split[0].parse::<isize>() {
            Ok(val) => val,
            Err(_) => {
                return Err(UIError::InvalidParam(format!("{} not a int", split[0])))
            }
        };
        let denom = match split[1].parse::<isize>() {
            Ok(val) => val,
            Err(_) => {
                return Err(UIError::InvalidParam(format!("{} not a int", split[1])))
            }
        };
        return Ok(Number::fraction(num, denom));
    } else if split.len() != 1 {
        return Err(UIError::InvalidParam(format!("{} not a number", source)));
    }
    if let Ok(val) = source.parse::<isize>() {
        return Ok(Number::integer(val));
    }
    if let Ok(val) = source.parse::<f64>() {
        return Ok(Number::rational(val));
    }
    Err(UIError::InvalidParam(format!("{} not a number", source)))
}

pub fn parse_interval(source: &str) -> Result<(Number, Number), UIError> {
    let split = source.split(',').collect::<Vec<_>>();
    if split.len() != 2 {
        return Err(UIError::InvalidParam(source.to_string()));
    }
    let from = parse_num(split[0])?;
    let to = parse_num(split[1])?;
    Ok((from, to))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_separate_line() {
        let line: Vec<String> = vec!["hi", "is", "-a", "1", "do", "-k", "-x"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        let separator =
            CommandLineSeparator::from_line(&line, &["-a"], &["-k", "-h"]).unwrap();
        let free = vec!["hi", "is", "do", "-x"]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>();
        assert_eq!(separator.free_args, free);
        let flags: BTreeSet<String> = vec!["-k"].iter().map(|x| x.to_string()).collect();
        assert_eq!(separator.flags, flags);
        let keys: BTreeMap<String, String> = vec![("-a", "1")]
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect();
        assert_eq!(separator.keys, keys);
    }

    #[test]
    fn test_parse_num() {
        assert_eq!(parse_num("12").unwrap(), Number::integer(12));
        assert_eq!(parse_num("1.2").unwrap(), Number::rational(1.2));
        assert_eq!(parse_num(".2").unwrap(), Number::rational(0.2));
        assert_eq!(parse_num("1/7").unwrap(), Number::fraction(1, 7));
        assert!(parse_num("1.2/7").is_err());
        assert!(parse_num("").is_err());
        assert!(parse_num("x").is_err());
        assert!(parse_num("1.2.3").is_err());
        assert!(parse_num("/2").is_err());
        assert!(parse_num("1/").is_err());
    }

    #[test]
    fn test_parse_interval() {
        let interval = parse_interval("1,3/2").unwrap();
        assert_eq!(interval, (Number::integer(1), Number::fraction(3, 2)));
        assert!(parse_interval("1").is_err());
        assert!(parse_interval("1,").is_err());
        assert!(parse_interval(",1").is_err());
        assert!(parse_interval("1,1,1").is_err());
    }
}

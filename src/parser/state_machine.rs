use crate::entities::parser::{State, TokenType};
use crate::proto::parser::IStateMachineRule;

pub struct StateMachine {
    pub token_type: TokenType,
    rule: Box<dyn IStateMachineRule>,
}

impl StateMachine {
    pub fn new(rule: Box<dyn IStateMachineRule>) -> StateMachine {
        let token_type = rule.token_type();
        StateMachine { token_type, rule }
    }

    pub fn get_lexem(&self, text: &[char], begin: usize) -> Option<String> {
        let mut result = String::new();
        let mut is_final = false;
        let mut state = State::NotFinal(0);
        for symbol in text.iter().skip(begin) {
            state = self.rule.apply(state, *symbol);
            if state.is_err() {
                break;
            }
            is_final = state.is_fin();
            result.push(*symbol);
        }
        if is_final {
            Some(result)
        } else {
            None
        }
    }
}

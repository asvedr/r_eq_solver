pub mod lexer;
pub mod parser;
pub mod rules;
pub mod state_machine;
#[cfg(test)]
mod tests;

pub use parser::Parser;

use crate::entities::expression::Operator;
use std::collections::BTreeSet;

const BINARY_OPERATORS: &[(&str, (usize, Operator))] = &[
    ("=", (0, Operator::Eql)),
    ("+", (1, Operator::Add)),
    ("-", (1, Operator::Sub)),
    ("*", (2, Operator::Mul)),
    ("/", (2, Operator::Div)),
    ("^", (3, Operator::Pow)),
];
const UNARY_OPERATORS: &[(&str, Operator)] = &[("-", Operator::Neg)];

pub fn create() -> Parser {
    let mut tree_operators: BTreeSet<&'static str> = BTreeSet::new();
    for (key, _) in BINARY_OPERATORS.iter() {
        tree_operators.insert(key);
    }
    for (key, _) in UNARY_OPERATORS.iter() {
        tree_operators.insert(key);
    }
    let operators: Vec<&'static str> = tree_operators.iter().cloned().collect();
    let mut machines = vec![];
    for rule in rules::rules(&operators) {
        machines.push(state_machine::StateMachine::new(rule));
    }
    let lexer = lexer::Lexer::new(machines);
    Parser::new(Box::new(lexer), UNARY_OPERATORS, BINARY_OPERATORS)
}

mod number_rule;
mod var_rule;
mod word_rule;

use crate::entities::parser::TokenType;
use crate::proto::parser::IStateMachineRule;

pub fn rules(operators: &[&str]) -> Vec<Box<dyn IStateMachineRule>> {
    let mut result: Vec<Box<dyn IStateMachineRule>> = vec![];
    for operator in operators.iter() {
        let rule = word_rule::WordRule::new(operator, TokenType::Operator);
        result.push(Box::new(rule));
    }
    let op = word_rule::WordRule::new("(", TokenType::OpenedBracket);
    result.push(Box::new(op));
    let cl = word_rule::WordRule::new(")", TokenType::ClosedBracket);
    result.push(Box::new(cl));
    result.push(Box::new(var_rule::VarRule::new()));
    result.push(Box::new(number_rule::NumberRule::new()));
    result
}

use std::collections::HashSet;

use crate::entities::parser::{State, TokenType};
use crate::proto::parser::IStateMachineRule;

pub struct NumberRule {
    symbols: HashSet<char>,
}

impl NumberRule {
    pub fn new() -> NumberRule {
        let mut symbols = HashSet::new();
        for i in b'0'..=b'9' {
            symbols.insert(i as char);
        }
        NumberRule { symbols }
    }
}

impl IStateMachineRule for NumberRule {
    fn token_type(&self) -> TokenType {
        TokenType::Number
    }

    fn apply(&self, state: State, symbol: char) -> State {
        match state.index() {
            0 if self.symbols.contains(&symbol) => State::Final(1),
            1 if self.symbols.contains(&symbol) => State::Final(1),
            1 if symbol == '.' => State::NotFinal(2),
            2 if self.symbols.contains(&symbol) => State::Final(2),
            _ => State::Error,
        }
    }
}

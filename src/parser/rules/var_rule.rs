use std::collections::HashSet;

use crate::entities::parser::{State, TokenType};
use crate::proto::parser::IStateMachineRule;

pub struct VarRule {
    first_symbol: HashSet<char>,
    other_symbols: HashSet<char>,
}

impl VarRule {
    pub fn new() -> VarRule {
        let mut other_symbols = HashSet::new();
        for i in b'a'..=b'z' {
            other_symbols.insert(i as char);
        }
        let first_symbol = other_symbols.clone();
        for i in b'0'..=b'9' {
            other_symbols.insert(i as char);
        }
        VarRule {
            first_symbol,
            other_symbols,
        }
    }
}

impl IStateMachineRule for VarRule {
    fn token_type(&self) -> TokenType {
        TokenType::Var
    }

    fn apply(&self, state: State, symbol: char) -> State {
        match state.index() {
            0 if self.first_symbol.contains(&symbol) => State::Final(1),
            1 if self.other_symbols.contains(&symbol) => State::Final(1),
            _ => State::Error,
        }
    }
}

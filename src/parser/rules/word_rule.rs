use crate::entities::parser::{State, TokenType};
use crate::proto::parser::IStateMachineRule;

pub struct WordRule {
    word: Vec<char>,
    tt: TokenType,
}

impl WordRule {
    pub fn new(word: &str, tt: TokenType) -> WordRule {
        WordRule {
            word: word.chars().collect(),
            tt,
        }
    }
}

impl IStateMachineRule for WordRule {
    fn token_type(&self) -> TokenType {
        self.tt
    }

    fn apply(&self, state: State, symbol: char) -> State {
        let n = state.index();
        if n >= self.word.len() || symbol != self.word[n] {
            return State::Error;
        }
        if n == self.word.len() - 1 {
            State::Final(n + 1)
        } else {
            State::NotFinal(n + 1)
        }
    }
}

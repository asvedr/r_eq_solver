use std::collections::BTreeSet;

use crate::entities::errors::ParserError;
use crate::entities::expression::Operator;
use crate::parser::Parser;
use crate::parser::{lexer, rules, state_machine};
use crate::proto::parser::IParser;

const BINARY_OPERATORS: &'static [(&'static str, (usize, Operator))] = &[
    ("=", (0, Operator::Eql)),
    ("+", (1, Operator::Add)),
    ("-", (1, Operator::Sub)),
    ("*", (2, Operator::Mul)),
    ("/", (2, Operator::Div)),
    ("^", (3, Operator::Pow)),
];
const UNARY_OPERATORS: &'static [(&'static str, Operator)] = &[("-", Operator::Neg)];

fn create() -> Parser {
    let mut tree_operators: BTreeSet<&'static str> = BTreeSet::new();
    for (key, _) in BINARY_OPERATORS.iter() {
        tree_operators.insert(key);
    }
    for (key, _) in UNARY_OPERATORS.iter() {
        tree_operators.insert(key);
    }
    let operators: Vec<&'static str> = tree_operators.iter().cloned().collect();
    let mut machines = vec![];
    for rule in rules::rules(&operators) {
        machines.push(state_machine::StateMachine::new(rule));
    }
    let lexer = lexer::Lexer::new(machines);
    Parser::new(Box::new(lexer), UNARY_OPERATORS, BINARY_OPERATORS)
}

#[test]
fn test_parse_expr() {
    let parser = create();
    for varinat in ["x + 2", " x + 2", "x + 2 ", "  x+2  "].iter() {
        let expr = parser.parse(varinat).unwrap();
        assert_eq!(expr.to_string(), "(1x^1 Add 2)")
    }
}

#[test]
fn test_parse_with_brackets() {
    let parser = create();
    let expr = parser.parse("1 * (x + (2))").unwrap();
    assert_eq!(expr.to_string(), "(1 Mul (1x^1 Add 2))");
}

#[test]
fn test_invalid_token() {
    let parser = create();
    match parser.parse("1 * (x # (2))") {
        Err(err) => assert_eq!(err, ParserError::UnexpectedToken(7, format!("# (2))"))),
        _ => panic!(),
    }
}

#[test]
fn test_empty_block() {
    let parser = create();
    match parser.parse("1 * ()") {
        Err(err) => assert_eq!(err, ParserError::EmptyBlockFound),
        _ => panic!(),
    }
}

#[test]
fn test_unexpected_operator() {
    let parser = create();
    match parser.parse("* 2") {
        Err(err) => assert_eq!(err, ParserError::UnexpectedOperator(format!("*"))),
        _ => panic!(),
    }
}

#[test]
fn test_unexpected_operand() {
    let parser = create();
    match parser.parse("x 2") {
        Err(err) => assert_eq!(err, ParserError::ExpectedOperator(format!("2"))),
        _ => panic!(),
    }
}

#[test]
fn test_bracket_not_closed() {
    let parser = create();
    match parser.parse("x * (") {
        Err(err) => {
            assert_eq!(err, ParserError::TokenNotFound(format!("ClosedBracket")),)
        }
        _ => panic!(),
    }
}

#[test]
fn test_expected_operand() {
    let parser = create();
    match parser.parse("x * (1 +)") {
        Err(err) => assert_eq!(err, ParserError::UnexpectedEndOfBlock,),
        _ => panic!(),
    }
}

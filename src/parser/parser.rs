use std::collections::HashMap;

use crate::entities::errors::ParserError;
use crate::entities::expression::{
    BinaryOperation, IExpression, Operator, PExpression, UnaryOperation, Var,
};
use crate::entities::parser::{Token, TokenType};
use crate::number::Number;
use crate::proto::parser::{ILexer, IParser};

pub struct Parser {
    lexer: Box<dyn ILexer>,
    unary_operators: HashMap<String, Operator>,
    binary_operators: HashMap<String, (usize, Operator)>,
}

enum BlockItem {
    Expr(PExpression),
    Token(Token),
}

enum ExprLineItem {
    Expr(PExpression),
    Operator(usize, Operator),
}

impl Parser {
    pub fn new(
        lexer: Box<dyn ILexer>,
        unary_operators: &[(&str, Operator)],
        binary_operators: &[(&str, (usize, Operator))],
    ) -> Parser {
        let mut parser = Parser {
            unary_operators: HashMap::new(),
            binary_operators: HashMap::new(),
            lexer,
        };
        for (key, val) in unary_operators.iter() {
            parser.unary_operators.insert(key.to_string(), *val);
        }
        for (key, val) in binary_operators.iter() {
            parser.binary_operators.insert(key.to_string(), *val);
        }
        parser
    }

    fn split_to_tokens(&self, text: &str) -> Result<Vec<Token>, ParserError> {
        let text: Vec<char> = text.chars().collect();
        let mut index = 0;
        let mut tokens = Vec::new();
        while index < text.len() {
            let opt_result = self.lexer.next_token(&text, index)?;
            let (new_index, token) = match opt_result {
                None => break,
                Some(val) => val,
            };
            index = new_index;
            tokens.push(token);
        }
        Ok(tokens)
    }

    fn expr_line_to_tree(&self, expr_line: &[ExprLineItem]) -> PExpression {
        if expr_line.len() == 1 {
            match expr_line[0] {
                ExprLineItem::Expr(ref e) => return e.clone(),
                _ => unreachable!(),
            }
        }
        let mut minimal_index = None;
        let mut minimal_priority = None;
        for (i, expr) in expr_line.iter().enumerate() {
            match (&expr, minimal_priority) {
                (ExprLineItem::Operator(ref p, _), None) => {
                    minimal_priority = Some(*p);
                    minimal_index = Some(i);
                }
                (ExprLineItem::Operator(ref p, _), Some(min_p)) => {
                    if *p <= min_p {
                        minimal_priority = Some(*p);
                        minimal_index = Some(i);
                    }
                }
                _ => (),
            }
        }
        let (_, operator, index) = if let Some(index) = minimal_index {
            match expr_line[index] {
                ExprLineItem::Operator(p, o) => (p, o, index),
                _ => unreachable!(),
            }
        } else {
            panic!("no operator found")
        };
        let left = self.expr_line_to_tree(&expr_line[..index]);
        let right = self.expr_line_to_tree(&expr_line[index + 1..]);
        let operation = BinaryOperation::new(operator, left, right);
        operation.as_expr()
    }

    fn build_var(&self, source: &str) -> PExpression {
        Var::new(1, Number::integer(1), source.to_string()).as_expr()
    }

    fn build_number(&self, source: &str) -> PExpression {
        if let Ok(val) = source.parse::<isize>() {
            return Number::integer(val).as_expr();
        } else if let Ok(val) = source.parse::<f64>() {
            return Number::rational(val).as_expr();
        }
        unreachable!()
    }

    fn get_operand(
        &self,
        block: &[BlockItem],
        begin: usize,
    ) -> Result<(usize, PExpression), ParserError> {
        if begin >= block.len() {
            return Err(ParserError::UnexpectedEndOfBlock);
        }
        let token: &Token = match &block[begin] {
            BlockItem::Expr(ref expr) => return Ok((begin + 1, expr.clone())),
            BlockItem::Token(ref token) => token,
        };
        let begin = begin + 1;
        match token.tp {
            TokenType::Var => return Ok((begin, self.build_var(&token.lexem))),
            TokenType::Number => return Ok((begin, self.build_number(&token.lexem))),
            TokenType::Operator => (),
            _ => unreachable!(),
        }
        // It's operator
        if let Some(operator) = self.unary_operators.get(&token.lexem) {
            let (begin, operand) = self.get_operand(block, begin)?;
            let operation = UnaryOperation::new(*operator, operand);
            Ok((begin, operation.as_expr()))
        } else {
            Err(ParserError::UnexpectedOperator(token.lexem.clone()))
        }
    }

    fn get_operator(
        &self,
        block: &[BlockItem],
        begin: usize,
    ) -> Result<(usize, (usize, Operator)), ParserError> {
        let token: &Token = match &block[begin] {
            BlockItem::Expr(ref expr) => {
                let err = ParserError::UnexpectedOperand(expr.to_string());
                return Err(err);
            }
            BlockItem::Token(ref token) => token,
        };
        if let Some(operator) = self.binary_operators.get(&token.lexem) {
            Ok((begin + 1, *operator))
        } else {
            Err(ParserError::ExpectedOperator(token.lexem.clone()))
        }
    }

    fn get_expr_line_item(
        &self,
        block: &[BlockItem],
        begin: usize,
        operand_expected: bool,
    ) -> Result<(usize, ExprLineItem), ParserError> {
        if operand_expected {
            let (end, value) = self.get_operand(block, begin)?;
            Ok((end, ExprLineItem::Expr(value)))
        } else {
            let (end, (priority, op)) = self.get_operator(block, begin)?;
            Ok((end, ExprLineItem::Operator(priority, op)))
        }
    }

    fn block_to_expression(
        &self,
        block: &[BlockItem],
    ) -> Result<PExpression, ParserError> {
        if block.is_empty() {
            return Err(ParserError::EmptyBlockFound);
        }
        let mut expr_line: Vec<ExprLineItem> = vec![];
        let mut operand_expected = true;
        let mut i = 0;
        while i < block.len() {
            let (new_i, item) = self.get_expr_line_item(block, i, operand_expected)?;
            i = new_i;
            expr_line.push(item);
            operand_expected = !operand_expected;
        }
        if operand_expected {
            return Err(ParserError::UnexpectedEndOfBlock);
        }
        Ok(self.expr_line_to_tree(&expr_line))
    }

    fn get_block(
        &self,
        tokens: &[Token],
        begin: usize,
        ending: Option<TokenType>,
    ) -> Result<(usize, Vec<BlockItem>), ParserError> {
        let mut result = Vec::new();
        let mut i = begin;
        let mut ending_found = false;
        while i < tokens.len() {
            let token = &tokens[i];
            if Some(token.tp) == ending {
                ending_found = true;
                break;
            }
            if token.tp == TokenType::OpenedBracket {
                let (next_i, expr) = self.build_subexpression(
                    tokens,
                    i + 1,
                    Some(TokenType::ClosedBracket),
                )?;
                result.push(BlockItem::Expr(expr));
                i = next_i;
            } else {
                result.push(BlockItem::Token(token.clone()));
                i += 1;
            }
        }
        if ending.is_some() && !ending_found {
            let token_name = format!("{:?}", ending.unwrap());
            return Err(ParserError::TokenNotFound(token_name));
        }
        Ok((i + 1, result))
    }

    fn build_subexpression(
        &self,
        tokens: &[Token],
        begin: usize,
        ending: Option<TokenType>,
    ) -> Result<(usize, PExpression), ParserError> {
        let (end, block) = self.get_block(tokens, begin, ending)?;
        let expression = self.block_to_expression(&block)?;
        Ok((end, expression))
    }

    fn build_expression(&self, tokens: Vec<Token>) -> Result<PExpression, ParserError> {
        let (_, expr) = self.build_subexpression(&tokens, 0, None)?;
        Ok(expr)
    }
}

impl IParser for Parser {
    fn parse(&self, text: &str) -> Result<PExpression, ParserError> {
        let tokens = self.split_to_tokens(text)?;
        self.build_expression(tokens)
    }
}

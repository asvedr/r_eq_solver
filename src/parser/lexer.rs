use std::collections::HashSet;

use crate::entities::errors::ParserError;
use crate::entities::parser::Token;
use crate::parser::state_machine::StateMachine;
use crate::proto::parser::ILexer;

const SPACE_SYMBOLS: &str = " \n\t\r";

pub struct Lexer {
    space_symbols: HashSet<char>,
    machines: Vec<StateMachine>,
}

impl Lexer {
    pub fn new(machines: Vec<StateMachine>) -> Lexer {
        let mut space_symbols = HashSet::new();
        for sym in SPACE_SYMBOLS.chars() {
            space_symbols.insert(sym);
        }
        Lexer {
            space_symbols,
            machines,
        }
    }

    fn skip_spaces(&self, text: &[char], begin: usize) -> usize {
        for (i, symbol) in text.iter().enumerate().skip(begin) {
            if !self.space_symbols.contains(symbol) {
                return i;
            }
        }
        text.len()
    }
}

impl ILexer for Lexer {
    fn next_token(
        &self,
        text: &[char],
        begin: usize,
    ) -> Result<Option<(usize, Token)>, ParserError> {
        let begin = self.skip_spaces(text, begin);
        if begin == text.len() {
            return Ok(None);
        }
        for machine in self.machines.iter() {
            if let Some(lexem) = machine.get_lexem(text, begin) {
                let end = begin + lexem.len();
                let tt = machine.token_type;
                return Ok(Some((end, Token::new(lexem, tt))));
            }
        }
        let mut rest = String::new();
        for symbol in text.iter().skip(begin) {
            rest.push(*symbol);
        }
        Err(ParserError::UnexpectedToken(begin, rest))
    }
}

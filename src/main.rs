#![allow(clippy::module_inception)]
#![allow(non_snake_case)]
#![allow(unused_must_use)]

extern crate num_bigint;
extern crate num_integer;
extern crate num_traits;

mod app;
#[macro_use]
mod entities;
mod fraction;
mod normalizer;
mod number;
mod parser;
mod proto;
mod single_var_solver;
mod system_solver;
mod ui;

fn main() {
    if let Err(err) = app::App::new().run() {
        eprintln!("{}", err);
        std::process::exit(1)
    }
}

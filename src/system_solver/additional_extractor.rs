use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet};
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, Mul, PNormalized, Sum, Var};
use crate::entities::solver::{EquationComplexity, ExtractedVar};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::{
    IAdditionalExtractor, IComplexityEstimator, IPolynomVarExtractor,
};
use crate::system_solver::utils::{has_only_one_var, iterate_over_tree};

pub struct AdditionalExtractor {
    estimator: Rc<dyn IComplexityEstimator>,
    simple_extractor: Rc<dyn IPolynomVarExtractor>,
    normalizer: Rc<dyn INormalizer>,
}

type Operands = BTreeMap<String, Number>;

struct Extracted {
    variants: Vec<PNormalized>,
    max_complexity: EquationComplexity,
}

fn to_opt<T>(val: SolverResult<Option<T>>) -> SolverResult<Option<T>> {
    Ok(try_convert_ret!(val, None))
}

impl AdditionalExtractor {
    pub fn new(
        estimator: Rc<dyn IComplexityEstimator>,
        simple_extractor: Rc<dyn IPolynomVarExtractor>,
        normalizer: Rc<dyn INormalizer>,
    ) -> AdditionalExtractor {
        AdditionalExtractor {
            estimator,
            simple_extractor,
            normalizer,
        }
    }

    fn to_key_coeff(&self, expr: &PNormalized) -> (String, Number) {
        if let Some(coeff) = expr.get_coefficient() {
            (expr.drop_coefficient().to_string(), coeff.clone())
        } else {
            (expr.to_string(), Number::integer(1))
        }
    }

    fn get_operands(&self, polynom: &PNormalized) -> Operands {
        let mut result = BTreeMap::new();
        if let Some(sum) = cast_norm::<Sum>(polynom) {
            for item in sum.items.iter() {
                let (key, coeff) = self.to_key_coeff(item);
                result.insert(key, coeff);
            }
        } else {
            let (key, coeff) = self.to_key_coeff(polynom);
            result.insert(key, coeff);
        }
        result
    }

    fn add_mul(
        &self,
        a: PNormalized,
        coeff: Number,
        b: PNormalized,
    ) -> SolverResult<Option<PNormalized>> {
        let mul = Mul::two(b, Rc::new(coeff));
        let new_b = self.normalizer.shortify(Rc::new(mul))?;
        let val = self
            .normalizer
            .shortify(Rc::new(Sum::new(vec![a, new_b])))?;
        Ok(Some(val))
    }

    fn polynom_to_extracted(&self, polynom: PNormalized) -> Extracted {
        let complexity = self.estimator.estimate(&polynom);
        Extracted {
            variants: vec![polynom],
            max_complexity: complexity,
        }
    }

    fn extract_from_sum(
        &self,
        var: &str,
        polynom: PNormalized,
    ) -> SolverResult<Option<Extracted>> {
        let variants = self.simple_extractor.extract(var, polynom)?;
        if variants.is_empty() {
            return Ok(None);
        }
        let mut max_vars = 0;
        let mut max_grade = 0;
        for variant in variants.iter() {
            let complexity = self.estimator.estimate(variant);
            max_vars = max_vars.max(complexity.vars);
            max_grade = max_grade.max(complexity.grade);
        }
        let max_complexity = EquationComplexity {
            vars: max_vars,
            grade: max_grade,
        };
        Ok(Some(Extracted {
            variants,
            max_complexity,
        }))
    }
}

fn intersect(a: &Operands, b: &Operands) -> Vec<String> {
    a.keys()
        .filter(|key| b.get(*key).is_some())
        .cloned()
        .collect()
}

fn get_multiplier(a: &Operands, b: &Operands, key: &str) -> Number {
    let num_a = a.get(key).unwrap();
    let num_b = b.get(key).unwrap();
    -(num_a / num_b)
}

fn find_the_best(variants: Vec<Extracted>) -> ExtractedVar<PNormalized> {
    let min_val = variants
        .iter()
        .min_by_key(|result| &result.max_complexity)
        .unwrap();
    if min_val.max_complexity.vars == 0 {
        assert_eq!(min_val.variants.len(), 1);
        ExtractedVar::Final(min_val.variants[0].clone(), min_val.max_complexity.grade)
    } else {
        ExtractedVar::NotFinal(min_val.variants.clone(), min_val.max_complexity.clone())
    }
}

fn all_vars_are_ok(
    extracted: &Extracted,
    var: &str,
    var_include: &HashMap<String, HashSet<String>>,
) -> bool {
    let aliases = match var_include.get(var) {
        Some(val) => val,
        _ => return true,
    };
    let mut vars = BTreeSet::new();
    for expr in extracted.variants.iter() {
        iterate_over_tree(&mut vars, expr, &|var: &Var| var.name.clone(), &None);
    }
    for expr_var in vars {
        if aliases.contains(&expr_var) {
            return false;
        }
    }
    true
}

impl IAdditionalExtractor for AdditionalExtractor {
    fn extract(
        &self,
        var: &str,
        a: &PNormalized,
        b: &PNormalized,
        var_include: &HashMap<String, HashSet<String>>,
    ) -> SolverResult<ExtractedVar<PNormalized>> {
        // WARNING
        // The algorithm that is used next expects that there is no
        // "Div" items in sum on toplevel. Because "Normalizer" always
        // turn "a + b/c" into "(ca + b) / c"
        //
        // how does it works?
        // Way 1:
        //   call for "x"
        //   / x^2 + x + y = -1
        //   \ -x^2 + x + y = 2
        //        VVV
        //      x + y = 3 => x = 3 - y
        //      return "3 - y"
        // Way 2:
        //   call for "x"
        //   / x + x^2 + y = 2
        //   \ x + x^2 - y = 3
        //        VVV
        //      x + x^2 = 5 => x^2 + x - 5 = 0
        //      return "x + x^2 - 5"
        // Way 2 in priority
        let operands_a = self.get_operands(a);
        let operands_b = self.get_operands(b);
        let keys = intersect(&operands_a, &operands_b);
        let mut variants = Vec::new();
        for key in keys {
            let m = get_multiplier(&operands_a, &operands_b, &key);
            let polynom = match to_opt(self.add_mul(a.clone(), m, b.clone()))? {
                Some(val) => val,
                _ => continue,
            };
            if has_only_one_var(var, &polynom) {
                let mut res = self.polynom_to_extracted(polynom);
                // downgrade complexity for correct sorting
                res.max_complexity.vars = 0;
                variants.push(res);
            } else if let Some(res) = to_opt(self.extract_from_sum(var, polynom))? {
                if all_vars_are_ok(&res, var, var_include) {
                    variants.push(res)
                };
            }
        }
        if variants.is_empty() {
            return Ok(ExtractedVar::NoSolution);
        }
        Ok(find_the_best(variants))
    }
}

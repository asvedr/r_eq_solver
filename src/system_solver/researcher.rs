use std::collections::{BTreeSet, HashMap, HashSet};
use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, IExpression, PNormalized, Var};
use crate::entities::solver::{
    Equation, EquationComplexity, ExtractedVar, PreparedEquation, PreparedEquations,
    SystemSolution, SystemSolutionVariant,
};
use crate::number::Number;
use crate::proto::single_var_solver::{IReplacer, ISolver};
use crate::proto::system_solver::{
    IAdditionalExtractor, IComplexityEstimator, IPolynomVarExtractor, IResearcher,
    ISystemValidator,
};
use crate::system_solver::utils::{has_only_one_var, iterate_over_tree};

pub struct Researcher {
    config: Rc<Config>,
    single_var_solver: Rc<dyn ISolver>,
    prime_extractor: Rc<dyn IPolynomVarExtractor>,
    add_extractor: Rc<dyn IAdditionalExtractor>,
    estimator: Rc<dyn IComplexityEstimator>,
    replacer: Rc<dyn IReplacer>,
    validator: Rc<dyn ISystemValidator>,
    zero: PNormalized,
}

#[derive(Clone)]
struct RecState {
    pre_solved_vars: HashMap<String, PNormalized>,
    used_equations: HashSet<usize>,
    var_include: HashMap<String, HashSet<String>>,
}

impl RecState {
    fn new() -> RecState {
        RecState {
            pre_solved_vars: HashMap::new(),
            used_equations: HashSet::new(),
            var_include: HashMap::new(),
        }
    }

    fn add_var_link(&mut self, var: &str, alias: String) {
        if let Some(aliases) = self.var_include.get_mut(var) {
            aliases.insert(alias);
        } else {
            let mut aliases = HashSet::new();
            aliases.insert(alias);
            self.var_include.insert(var.to_string(), aliases);
        }
    }

    fn add_var(
        &self,
        var: String,
        extracted: PNormalized,
        used: Option<usize>,
    ) -> RecState {
        let mut copy = self.clone();
        let mut childs = BTreeSet::new();
        iterate_over_tree(
            &mut childs,
            &extracted,
            &|var: &Var| -> String { var.name.clone() },
            &None,
        );
        for item in childs {
            copy.add_var_link(&item, var.clone())
        }
        copy.pre_solved_vars.insert(var, extracted);
        if let Some(index) = used {
            copy.used_equations.insert(index);
        }
        copy
    }

    fn var_contains(&self, var: &str, other: &str) -> bool {
        if let Some(aliases) = self.var_include.get(var) {
            aliases.contains(other)
        } else {
            false
        }
    }
}

fn cast_num_and_log(
    expr: &PNormalized,
    pre_result: &SystemSolution,
    var: &str,
) -> Option<Number> {
    if let Some(root) = cast_norm::<Number>(expr) {
        Some(root.clone())
    } else {
        eprintln!(
            "root not num \"{}\" for var {} when {:?}",
            expr.format(),
            var,
            pre_result.variants,
        );
        None
    }
}

impl Researcher {
    pub fn new(
        config: Rc<Config>,
        prime_extractor: Rc<dyn IPolynomVarExtractor>,
        add_extractor: Rc<dyn IAdditionalExtractor>,
        estimator: Rc<dyn IComplexityEstimator>,
        replacer: Rc<dyn IReplacer>,
        single_var_solver: Rc<dyn ISolver>,
        validator: Rc<dyn ISystemValidator>,
    ) -> Researcher {
        Researcher {
            config,
            single_var_solver,
            prime_extractor,
            add_extractor,
            estimator,
            replacer,
            validator,
            zero: Rc::new(Number::integer(0)),
        }
    }

    fn next_vars<'a>(
        &self,
        prepared: &'a PreparedEquations,
        used_vars: &HashMap<String, PNormalized>,
    ) -> Vec<&'a String> {
        let mut result = Vec::new();
        for var in prepared.all_vars.iter() {
            if !used_vars.contains_key(var) {
                result.push(var)
            }
        }
        result
    }

    fn var_in_equation(
        &self,
        state: &RecState,
        var: &str,
        eq: &PreparedEquation,
    ) -> bool {
        for item in eq.vars.iter() {
            if item == var || state.var_contains(item, var) {
                return true;
            }
        }
        false
    }

    // value conversion used
    fn convert_extracted(
        &self,
        extracted: ExtractedVar<PNormalized>,
    ) -> SolverResult<ExtractedVar<Vec<Number>>> {
        match extracted {
            ExtractedVar::Final(val, _) => self.solve_finalized(&val),
            ExtractedVar::NotFinal(vals, grade) => {
                Ok(ExtractedVar::NotFinal(vals, grade))
            }
            ExtractedVar::NoSolution => Ok(ExtractedVar::NoSolution),
        }
    }

    // value conversion used
    fn extract_var_prime(
        &self,
        prepared: PNormalized,
        var: &str,
    ) -> SolverResult<ExtractedVar<Vec<Number>>> {
        let prime_solution_res = self.prime_extractor.extract(var, prepared);
        let prime_solution =
            try_convert_ret!(prime_solution_res, ExtractedVar::NoSolution);
        if prime_solution.is_empty() {
            return Ok(ExtractedVar::NoSolution);
        }
        let max_grade = self.estimate_max(&prime_solution);
        Ok(ExtractedVar::NotFinal(prime_solution, max_grade))
    }

    // value conversion used
    fn extract_var_additional(
        &self,
        prepared: &PreparedEquations,
        current_index: usize,
        state: &RecState,
        var: &str,
    ) -> SolverResult<ExtractedVar<Vec<Number>>> {
        let mut result = ExtractedVar::NoSolution;
        let first_eq = &prepared.equations[current_index].normalized;
        for (index, second_eq) in prepared.equations.iter().enumerate() {
            if index == current_index || state.used_equations.contains(&index) {
                continue;
            }
            let extracted = self.add_extractor.extract(
                var,
                first_eq,
                &second_eq.normalized,
                &state.var_include,
            )?;
            let extracted = self.convert_extracted(extracted)?;
            if extracted < result {
                result = extracted;
            }
        }
        Ok(result)
    }

    // value conversion used
    fn solve_finalized(
        &self,
        expr: &PNormalized,
    ) -> SolverResult<ExtractedVar<Vec<Number>>> {
        let eq = Equation {
            left: expr.clone(),
            right: self.zero.clone(),
        };
        let solution = try_convert_ret!(
            self.single_var_solver.solve_norm(&eq),
            ExtractedVar::NoSolution
        );
        Ok(ExtractedVar::Final(solution.roots, solution.complexity))
    }

    // value conversion used
    fn extract_var(
        &self,
        var: &str,
        prepared: &PreparedEquations,
        index: usize,
        eq: &PreparedEquation,
        state: &RecState,
    ) -> SolverResult<(ExtractedVar<Vec<Number>>, bool)> {
        let open_eq = match self.replace_vars(state, eq)? {
            Some(val) => val,
            None => return Ok((ExtractedVar::NoSolution, true)),
        };
        if has_only_one_var(var, &open_eq) {
            let solution = self.solve_finalized(&open_eq)?;
            return if matches!(solution, ExtractedVar::Final(_, _)) {
                Ok((solution, true))
            } else {
                Ok((ExtractedVar::NoSolution, true))
            };
        }
        let prime_solution = self.extract_var_prime(open_eq, var)?;
        let complex_solution =
            self.extract_var_additional(prepared, index, state, var)?;
        if prime_solution < complex_solution {
            Ok((prime_solution, true))
        } else {
            Ok((complex_solution, false))
        }
    }

    // value conversion used
    fn replace_vars(
        &self,
        state: &RecState,
        eq: &PreparedEquation,
    ) -> SolverResult<Option<PNormalized>> {
        let wrapped = self
            .replacer
            .replace_vars_in_norm(&eq.normalized, &state.pre_solved_vars);
        let result = try_convert_ret!(wrapped, None);
        Ok(Some(result))
    }

    // value conversion used
    fn replace_vars_in_back(
        &self,
        fun: &PNormalized,
        pre_solution: &SystemSolution,
    ) -> SolverResult<Vec<PNormalized>> {
        let mut result = Vec::new();
        for variant in pre_solution.variants.iter() {
            let map = variant
                .iter()
                .map(|(key, val)| (key.clone(), val.as_norm()))
                .collect::<HashMap<String, PNormalized>>();
            let val_wrapped = self.replacer.replace_vars_in_norm(fun, &map);
            if let Some(val) = try_convert!(val_wrapped.map(Some), None) {
                result.push(val);
            }
        }
        Ok(result)
    }

    fn estimate(&self, normalized: &PNormalized) -> EquationComplexity {
        self.estimator.estimate(normalized)
    }

    fn estimate_max(&self, variants: &[PNormalized]) -> EquationComplexity {
        variants
            .iter()
            .map(|variant| self.estimate(variant))
            .max()
            .unwrap()
    }

    fn join_solutions(&self, solutions: Vec<SystemSolution>) -> SystemSolution {
        if solutions.is_empty() {
            return SystemSolution::empty();
        }
        let mut variants = Vec::new();
        let mut complexity = solutions[0].complexity;
        for mut solution in solutions {
            variants.append(&mut solution.variants);
            complexity = complexity.max(solution.complexity);
        }
        SystemSolution {
            variants,
            complexity,
        }
    }

    // value conversion used
    fn process_norm_variants(
        &self,
        prepared: &PreparedEquations,
        variants: Vec<PNormalized>,
        state: &RecState,
        var: &str,
        used: &Option<usize>,
    ) -> SolverResult<SystemSolution> {
        let mut result = Vec::new();
        for variant in variants {
            let new_state = state.add_var(var.to_string(), variant.clone(), *used);
            let pre_result = self.main_loop(prepared, &new_state)?;
            if pre_result.is_empty() {
                continue;
            }
            let opt_roots: Option<Vec<Number>> = self
                .replace_vars_in_back(&variant, &pre_result)?
                .iter()
                .map(|x| cast_num_and_log(x, &pre_result, var))
                .collect();
            match opt_roots {
                Some(roots) if !roots.is_empty() => {
                    result.push(pre_result.add_var(var.to_string(), roots, 0))
                }
                _ => (),
            }
        }
        Ok(self.join_solutions(result))
    }

    // value conversion used
    fn process_num_variants(
        &self,
        prepared: &PreparedEquations,
        nums: Vec<Number>,
        complexity: usize,
        state: &RecState,
        var: &str,
        used: &Option<usize>,
    ) -> SolverResult<SystemSolution> {
        let mut result = Vec::new();
        for num in nums {
            let new_state = state.add_var(var.to_string(), num.as_norm(), *used);
            let pre_result = self.main_loop(prepared, &new_state)?;
            if pre_result.is_empty() {
                continue;
            }
            result.push(pre_result.add_var(var.to_string(), vec![num], complexity));
        }
        Ok(self.join_solutions(result))
    }

    // value conversion used
    fn var_iteration(
        &self,
        prepared: &PreparedEquations,
        state: &RecState,
        var: &str,
    ) -> SolverResult<SystemSolution> {
        let mut result = SystemSolution::empty();
        for (index, eq) in prepared.equations.iter().enumerate() {
            if state.used_equations.contains(&index) {
                continue;
            }
            let (extracted, used_flag) =
                self.extract_var(var, prepared, index, eq, state)?;
            let used = if used_flag { Some(index) } else { None };
            let mut solution = match extracted {
                ExtractedVar::NoSolution => continue,
                ExtractedVar::Final(nums, complexity) => self.process_num_variants(
                    prepared, nums, complexity, state, var, &used,
                )?,
                ExtractedVar::NotFinal(variants, _) => {
                    self.process_norm_variants(prepared, variants, state, var, &used)?
                }
            };
            self.validate_pre_solution(prepared, &mut solution.variants)?;
            if self.can_stop_on_this(&solution) {
                return Ok(solution);
            } else if solution < result {
                result = solution
            }
        }
        Ok(result)
    }

    // value conversion used
    fn build_result(
        &self,
        prepared: &PreparedEquations,
        state: &RecState,
        var: &str,
    ) -> SolverResult<SystemSolution> {
        let mut result_solution = SystemSolution::empty();
        for (index, eq) in prepared.equations.iter().enumerate() {
            if state.used_equations.contains(&index) {
                continue;
            }
            if !self.var_in_equation(state, var, eq) {
                continue;
            }
            let replaced = match self.replace_vars(state, eq)? {
                Some(val) => val,
                None => continue,
            };
            let (values, complexity) = match self.solve_finalized(&replaced)? {
                ExtractedVar::NoSolution => continue,
                ExtractedVar::Final(values, complexity) => (values, complexity),
                ExtractedVar::NotFinal(_, _) => unreachable!(),
            };
            let variant = SystemSolution::one(var.to_string(), values, complexity);
            if self.can_stop_on_this(&variant) {
                return Ok(variant);
            }
            if variant <= result_solution {
                result_solution = variant;
            }
        }
        Ok(result_solution)
    }

    // value conversion used
    fn main_loop(
        &self,
        prepared: &PreparedEquations,
        state: &RecState,
    ) -> SolverResult<SystemSolution> {
        let vars = self.next_vars(prepared, &state.pre_solved_vars);
        if vars.is_empty() {
            return Ok(SystemSolution::empty());
        }
        if vars.len() == 1 {
            return self.build_result(prepared, state, vars[0]);
        }
        let mut result_solution = SystemSolution::empty();
        for var in vars {
            let solution = self.var_iteration(prepared, state, var)?;
            if self.can_stop_on_this(&solution) {
                return Ok(solution);
            }
            if solution <= result_solution {
                result_solution = solution
            }
        }
        Ok(result_solution)
    }

    fn can_stop_on_this(&self, solution: &SystemSolution) -> bool {
        self.config.stop_on_first && !solution.variants.is_empty()
    }

    fn validate_pre_solution(
        &self,
        prepared: &PreparedEquations,
        solutions: &mut Vec<SystemSolutionVariant>,
    ) -> SolverResult<()> {
        let mut i = 0;
        if solutions.is_empty() {
            return Ok(());
        }
        let use_full = solutions[0].len() == prepared.all_vars.len();
        while i < solutions.len() {
            let passed = if use_full {
                self.validator.validate_full(prepared, &solutions[i])?
            } else {
                self.validator.validate_partial(prepared, &solutions[i])?
            };
            if passed {
                i += 1
            } else {
                solutions.swap_remove(i);
            }
        }
        Ok(())
    }
}

impl IResearcher for Researcher {
    fn research(
        &self,
        prepared: &PreparedEquations,
    ) -> SolverResult<Vec<SystemSolutionVariant>> {
        let solution = self.main_loop(prepared, &RecState::new())?;
        Ok(solution.variants)
    }
}

use std::collections::BTreeSet;

use num_bigint::BigInt;
use num_integer::Integer;
use num_traits::ToPrimitive;

use crate::entities::expression::{PNormalized, Root, Var};
use crate::entities::solver::EquationComplexity;
use crate::proto::system_solver::IComplexityEstimator;
use crate::system_solver::utils::iterate_over_tree;

const ODD_ROOT: usize = 100;
const EVEN_ROOT: usize = 200;

pub struct ComplexityEstimator {}

impl ComplexityEstimator {
    pub fn new() -> ComplexityEstimator {
        ComplexityEstimator {}
    }

    fn count_vars(&self, expression: &PNormalized) -> usize {
        let mut accum = BTreeSet::new();
        let getter = |var: &Var| -> String { var.name.clone() };
        iterate_over_tree(&mut accum, &expression, &getter, &None);
        accum.len()
    }

    fn get_root_grade(&self, expression: &PNormalized) -> usize {
        let mut roots = BTreeSet::new();
        let getter = |root: &Root| -> BigInt { root.root.clone() };
        iterate_over_tree(&mut roots, &expression, &getter, &None);
        let mut odd_root_found = false;
        let mut result = 0;
        for root in roots.iter() {
            if root.is_even() {
                result += EVEN_ROOT;
            } else if !odd_root_found {
                result += ODD_ROOT;
                odd_root_found = true;
            }
        }
        result
    }

    fn get_var_grade(&self, expression: &PNormalized) -> usize {
        let mut exps = BTreeSet::new();
        let getter = |var: &Var| -> BigInt { var.pow.clone() };
        iterate_over_tree(&mut exps, &expression, &getter, &None);
        let mut result = 0;
        for exp in exps.iter() {
            let exp = exp.to_isize().unwrap();
            if exp != 1 {
                result += exp.abs() as usize;
            }
        }
        result
    }

    fn get_grade(&self, expr: &PNormalized) -> usize {
        self.get_root_grade(expr) + self.get_var_grade(expr)
    }
}

impl IComplexityEstimator for ComplexityEstimator {
    fn estimate(&self, expression: &PNormalized) -> EquationComplexity {
        EquationComplexity {
            vars: self.count_vars(expression),
            grade: self.get_grade(expression),
        }
    }

    fn estimate_vec(&self, expressions: &[PNormalized]) -> EquationComplexity {
        let mut result = EquationComplexity { vars: 0, grade: 0 };
        for item in expressions {
            let complexity = self.estimate(item);
            result.vars = result.vars.max(complexity.vars);
            result.grade = result.grade.max(complexity.grade);
        }
        result
    }
}

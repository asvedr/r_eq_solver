use std::collections::{HashMap, HashSet};
use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::expression::{IExpression, Mul, PNormalized, Sum, Var};
use crate::entities::solver::ExtractedVar;
use crate::normalizer;
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::IAdditionalExtractor;
use crate::system_solver::additional_extractor::AdditionalExtractor;
use crate::system_solver::complexity_estimator::ComplexityEstimator;
use crate::system_solver::create_extractor;

fn create_additional_extractor() -> AdditionalExtractor {
    let normalizer: Rc<dyn INormalizer> = Rc::new(normalizer::create(false));
    let config = Rc::new(Config::new());
    AdditionalExtractor::new(
        Rc::new(ComplexityEstimator::new()),
        Rc::new(create_extractor(config, normalizer.clone(), true)),
        normalizer,
    )
}

fn var(exp: isize, coeff: isize, name: &str) -> PNormalized {
    let var = Var::new(exp, Number::integer(coeff), name.to_string());
    var.as_norm()
}

fn num(val: isize) -> PNormalized {
    Number::integer(val).as_norm()
}

fn show_vec(vec: &[PNormalized]) -> Vec<String> {
    vec.iter().map(|x| x.format()).collect()
}

fn map() -> HashMap<String, HashSet<String>> {
    HashMap::new()
}

#[test]
fn test_extract_identical_sums() {
    let extractor = create_additional_extractor();
    let sum_a = Sum::new(vec![var(2, 1, "x"), var(2, 1, "y"), num(3)]);
    let sum_b = Sum::new(vec![var(2, 1, "x"), var(2, 1, "y"), num(4)]);
    let res = extractor
        .extract("x", &sum_a.as_norm(), &sum_b.as_norm(), &map())
        .unwrap();
    assert!(matches!(res, ExtractedVar::NoSolution));
}

#[test]
fn test_extract_sum_no_coeff_nan() {
    let extractor = create_additional_extractor();
    let sum_a = Sum::new(vec![var(2, 1, "x"), var(2, 1, "y"), num(3)]);
    let sum_b = Sum::new(vec![var(2, 1, "x"), var(2, -1, "y"), num(4)]);
    let res = extractor
        .extract("x", &sum_a.as_norm(), &sum_b.as_norm(), &map())
        .unwrap();
    match res {
        ExtractedVar::Final(val, _) => assert_eq!(val.format(), "(2x^2 + 7)"),
        _ => panic!(),
    }
}

#[test]
fn test_extract_sum_no_coeff_value() {
    let extractor = create_additional_extractor();
    let sum_a = Sum::new(vec![var(2, 1, "x"), var(2, 1, "y"), num(-8)]);
    let sum_b = Sum::new(vec![var(2, 1, "x"), var(2, -1, "y"), num(-10)]);
    let res = extractor
        .extract("x", &sum_a.as_norm(), &sum_b.as_norm(), &map())
        .unwrap();
    match res {
        ExtractedVar::Final(val, _) => assert_eq!(val.format(), "(2x^2 + -18)"),
        _ => panic!(),
    }
}

#[test]
fn test_extract_sum_different_ways_mul() {
    let extractor = create_additional_extractor();
    let mul = Mul::two(var(1, 1, "x"), var(2, 1, "y")).as_norm();
    let two = Number::integer(2);
    let sum_a = Sum::new(vec![
        var(1, 1, "x"),
        var(1, 1, "y"),
        mul.set_coefficient(two),
        num(-50),
    ]);
    let sum_b = Sum::new(vec![var(1, 1, "x"), var(1, 1, "y"), mul, num(60)]);
    let res = extractor
        .extract("x", &sum_a.as_norm(), &sum_b.as_norm(), &map())
        .unwrap();
    match res {
        ExtractedVar::NotFinal(res, _) => {
            assert_eq!(show_vec(&res), vec!["(-1y + -170)"])
        }
        _ => panic!(),
    }
}

#[test]
fn test_extract_sum_different_ways_exp() {
    let extractor = create_additional_extractor();
    let mul = Mul::two(var(1, 1, "x"), var(2, 1, "y")).as_norm();
    let two = Number::integer(2);
    let sum_a = Sum::new(vec![
        var(1, 1, "x"),
        var(3, 1, "y"),
        mul.set_coefficient(two),
        num(-50),
    ]);
    let sum_b = Sum::new(vec![var(1, 1, "x"), var(3, 1, "y"), mul, num(60)]);
    let res = extractor
        .extract("x", &sum_a.as_norm(), &sum_b.as_norm(), &map())
        .unwrap();
    match res {
        ExtractedVar::NotFinal(res, _) => assert_eq!(show_vec(&res), vec!["(110 / y^2)"]),
        _ => panic!(),
    }
}

use std::collections::{HashMap, HashSet};
use std::fmt::Write;

use crate::entities::expression::{Div, IExpression, Mul, PNormalized, Sum, Var};
use crate::entities::solver::{
    Equation, EquationComplexity, PreparedEquation, PreparedEquations,
};
use crate::number::Number;

pub fn int(x: isize) -> Number {
    Number::integer(x)
}

pub fn xyz() -> (PNormalized, PNormalized, PNormalized) {
    (
        Var::new(1, int(1), "x".to_string()).as_norm(),
        Var::new(1, int(1), "y".to_string()).as_norm(),
        Var::new(1, int(1), "z".to_string()).as_norm(),
    )
}

pub fn make_eq(sum: Vec<PNormalized>, vars: &[&str], grade: usize) -> PreparedEquation {
    let sum = Sum::new(sum);
    PreparedEquation {
        source: Equation {
            left: sum.as_expr(),
            right: int(0).as_expr(),
        },
        normalized: sum.as_norm(),
        vars: vars.iter().map(|x| x.to_string()).collect(),
        complexity: EquationComplexity {
            vars: vars.len(),
            grade,
        },
    }
}

pub fn make_eqs(equations: Vec<PreparedEquation>) -> PreparedEquations {
    let mut all_vars_set: HashSet<String> = HashSet::new();
    for eq in equations.iter() {
        all_vars_set.extend(eq.vars.iter().cloned());
    }
    let mut all_vars: Vec<String> = all_vars_set.into_iter().collect();
    all_vars.sort();
    PreparedEquations {
        equations,
        all_vars,
    }
}

pub fn unsolvable_complex_system() -> PreparedEquations {
    //  / x^8 + y^5 + z^7 = 1
    //  | x - y - z = 12
    // -| x + y * z = 10
    //  | x^11 / y^12 = 20
    //  \ x*y/z = 5
    // algo must ignore 1 and 4 equations use best grade
    let (x, y, z) = xyz();
    let eq1_src = vec![
        x.set_exponent((8).into()),
        y.set_exponent((5).into()),
        z.set_exponent((7).into()),
        int(-1).as_norm(),
    ];
    let eq1 = make_eq(eq1_src, &["x", "y", "z"], 8 + 5 + 7);
    let eq2_src = vec![
        x.clone(),
        y.set_coefficient(int(-1)),
        z.set_coefficient(int(-1)),
        int(-12).as_norm(),
    ];
    let eq2 = make_eq(eq2_src, &["x", "y", "z"], 3);
    let eq3_src = vec![x.clone(), Mul::two(y.clone(), z.clone()).as_norm()];
    let eq3 = make_eq(eq3_src, &["x", "y", "z"], 3);
    let eq4_src = vec![
        Div::new(x.set_exponent((11).into()), y.set_exponent((12).into())).as_norm(),
        int(-20).as_norm(),
    ];
    let eq4 = make_eq(eq4_src, &["x", "y"], 11 + 12);
    let eq5_src = vec![
        Div::new(Mul::two(x, y).as_norm(), z).as_norm(),
        int(-5).as_norm(),
    ];
    let eq5 = make_eq(eq5_src, &["x", "y", "z"], 3);
    let eqs = vec![eq1, eq2, eq3, eq4, eq5];
    make_eqs(eqs)
}

pub fn show_result(variants: Vec<HashMap<String, Number>>) -> String {
    let mut result = String::new();
    for variant in variants {
        let mut variant_as_vec = variant.into_iter().collect::<Vec<(String, Number)>>();
        variant_as_vec.sort_by_key(&|item: &(String, Number)| {
            let (ref key, _) = item;
            key.clone()
        });
        for (key, val) in variant_as_vec {
            write!(result, "{}: {}, ", key, val);
        }
        write!(result, "\n");
    }
    result
}

pub fn join_lines(lines: Vec<&str>) -> String {
    let mut result = String::new();
    for line in lines {
        write!(result, "{}\n", line);
    }
    result
}

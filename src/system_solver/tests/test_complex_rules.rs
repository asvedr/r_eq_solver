use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::expression::{Div, IExpression, Mul, PNormalized, Root, Sum, Var};
use crate::normalizer;
use crate::number::Number;
use crate::proto::system_solver::IPolynomVarExtractor;
use crate::system_solver::create_extractor;

fn show_vec(vec: &[PNormalized]) -> Vec<String> {
    vec.iter().map(|x| x.format()).collect()
}

fn xyzn() -> (PNormalized, PNormalized, PNormalized, PNormalized) {
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let y = Var::new(1, Number::integer(2), "y".to_string());
    let z = Var::new(1, Number::integer(1), "z".to_string());
    let n = Number::integer(3);
    (x.as_norm(), y.as_norm(), z.as_norm(), n.as_norm())
}

macro_rules! extractor {
    () => {
        create_extractor(
            Rc::new(Config::new()),
            Rc::new(normalizer::create(false)),
            true,
        )
    };
}

#[test]
fn test_extract_from_sum() {
    let extractor = extractor!();
    let (x, y, _, n) = xyzn();
    let x = x.set_coefficient(Number::integer(2));
    let val = Sum::new(vec![x, y, n]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    assert_eq!(show_vec(&result), vec!["(-1y + -1(1/2))"]);
}

#[test]
fn test_extract_from_sum_multipliers() {
    // 2x + xy = z -> x(2 + y) = z -> x = z/(2 + y)
    let extractor = extractor!();
    let (x, y, z, _) = xyzn();
    let y = y.set_coefficient(Number::integer(1));
    let xy = Mul::two(x.clone(), y).as_norm();
    let x = x.set_coefficient(Number::integer(2));
    let z = z.set_coefficient(Number::integer(-1));
    let val = Sum::new(vec![x, xy, z]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    assert_eq!(show_vec(&result), vec!["(z / (y + 2))"]);
}

#[test]
fn test_extract_from_var_positive_exps() {
    let extractor = extractor!();
    let (x, _, z, _) = xyzn();
    let x = x.set_exponent((2).into());
    let eq = Sum::new(vec![x.clone(), z.clone()]).as_norm();
    let result = extractor.extract("x", eq).unwrap();
    assert_eq!(show_vec(&result), vec!["[-1z]^(1/2)", "-1*[-1z]^(1/2)"]);

    let x = x.set_exponent((3).into());
    let eq = Sum::new(vec![x.clone(), z.clone()]).as_norm();
    let result = extractor.extract("x", eq).unwrap();
    assert_eq!(show_vec(&result), vec!["[-1z]^(1/3)"]);

    let x = x.set_exponent((4).into());
    let eq = Sum::new(vec![x, z]).as_norm();
    let result = extractor.extract("x", eq).unwrap();
    assert_eq!(show_vec(&result), vec!["[-1z]^(1/4)", "-1*[-1z]^(1/4)"]);
}

#[test]
fn test_extract_from_var_negative_exps() {
    // 1 / (x^2) + z = 0
    let extractor = extractor!();
    let (x, _, z, _) = xyzn();

    let x = x.set_exponent((-1).into());
    let eq = Sum::new(vec![x.clone(), z.clone()]).as_norm();
    let result = extractor.extract("x", eq).unwrap();
    assert_eq!(show_vec(&result), vec!["(1 / -1z)"]);

    let x = x.set_exponent((-2).into());
    let eq = Sum::new(vec![x, z]).as_norm();
    let result = extractor.extract("x", eq).unwrap();
    assert_eq!(
        show_vec(&result),
        vec!["(1 / [-1z]^(1/2))", "(-1 / [-1z]^(1/2))"]
    );
}

#[test]
fn test_extract_from_multi_exp() {
    // x^2 + x + y = 0
    let extractor = extractor!();
    let (x, y, _, _) = xyzn();
    let x2 = x.set_exponent((2).into());
    let val = Sum::new(vec![x2, x, y]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    let expected = vec![
        "(1/2*[(-8y + 1)]^(1/2) + -1/2)",
        "(-1/2*[(-8y + 1)]^(1/2) + -1/2)",
    ];
    assert_eq!(show_vec(&result), expected)
}

#[test]
fn test_extract_from_squad_2() {
    // x^4 + x^2 + y = 0
    let extractor = extractor!();
    let (x, y, _, _) = xyzn();
    let x4 = x.set_exponent((4).into());
    let x2 = x.set_exponent((2).into());
    let val = Sum::new(vec![x4, x2, y]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    let expected = vec![
        "[(1/2*[(-8y + 1)]^(1/2) + -1/2)]^(1/2)",
        "-1*[(1/2*[(-8y + 1)]^(1/2) + -1/2)]^(1/2)",
        "[(-1/2*[(-8y + 1)]^(1/2) + -1/2)]^(1/2)",
        "-1*[(-1/2*[(-8y + 1)]^(1/2) + -1/2)]^(1/2)",
    ];
    assert_eq!(show_vec(&result), expected)
}

#[test]
fn test_extract_from_squad_3() {
    // x^6 + x^3 + y = 0
    let extractor = extractor!();
    let (x, y, _, _) = xyzn();
    let x6 = x.set_exponent((6).into());
    let x3 = x.set_exponent((3).into());
    let val = Sum::new(vec![x6, x3, y]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    let expected = vec![
        "[(1/2*[(-8y + 1)]^(1/2) + -1/2)]^(1/3)",
        "[(-1/2*[(-8y + 1)]^(1/2) + -1/2)]^(1/3)",
    ];
    assert_eq!(show_vec(&result), expected)
}

#[test]
fn test_extract_from_root() {
    // (3*[x]^(1/2) + -2y + 3) = 0
    let extractor = extractor!();
    let (x, y, _, n) = xyzn();
    let root = Root::new(1, 2, Number::integer(3), x).as_norm();
    let y = y.set_coefficient(Number::integer(-2));
    let val = Sum::new(vec![root, y, n]).as_norm();
    let result = extractor.extract("x", val.clone()).unwrap();
    assert_eq!(show_vec(&result), vec!["(-1(1/3)y + 4/9y^2 + 1)"]);
    let result = extractor.extract("y", val.clone()).unwrap();
    assert_eq!(show_vec(&result), vec!["(1(1/2)*[x]^(1/2) + 1(1/2))"]);
    let result = extractor.extract("z", val.clone()).unwrap();
    assert_eq!(result.len(), 0);
}

#[test]
fn test_extract_from_root_of_sum() {
    // [x + 3]^(1/2) + -2y = 0 -> x + 3 = 4y^2 -> x = 4y^2 - 3
    let extractor = extractor!();
    let (x, y, _, n) = xyzn();
    let sum = Sum::new(vec![x, n]).as_norm();
    let root = Root::new(1, 2, Number::integer(1), sum).as_norm();
    let y = y.set_coefficient(Number::integer(-2));
    let val = Sum::new(vec![root, y]).as_norm();
    let result = extractor.extract("x", val.clone()).unwrap();
    assert_eq!(show_vec(&result), vec!["(4y^2 + -3)"]);
}

#[test]
fn test_extract_from_mul() {
    // (2 * x * y^2) + z + 3 = 0
    let extractor = extractor!();
    let (x, y, z, n) = xyzn();
    let x = x.set_coefficient(Number::integer(1));
    let y = y
        .set_coefficient(Number::integer(1))
        .set_exponent((2).into());
    let mul = Mul::new(Number::integer(2), vec![x, y]).as_norm();
    let val = Sum::new(vec![mul, z, n]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    assert_eq!(show_vec(&result), vec!["((-1z + -3) / 2y^2)"]);
}

#[test]
fn test_extract_from_mul_mix_var_root_even() {
    // x * root(x) - y = 0 -> no solutions
    let extractor = extractor!();
    let (x, y, _, _) = xyzn();
    let y = y.set_coefficient(Number::integer(-1));
    let root = Root::new(1, 2, Number::integer(1), x.clone()).as_norm();
    let mul = Mul::two(x, root).as_norm();
    let val = Sum::new(vec![mul, y]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    assert_eq!(show_vec(&result), vec!["[y^2]^(1/3)"])
}

#[test]
fn test_extract_from_mul_mix_var_root_odd() {
    // x * root[3](x) - y = 0 -> root[3](x^3 * x) = y -> x^4 = y^3 -> x = +- root[4](y^3)
    let extractor = extractor!();
    let (x, y, _, _) = xyzn();
    let y = y.set_coefficient(Number::integer(-1));
    let root = Root::new(1, 3, Number::integer(1), x.clone()).as_norm();
    let mul = Mul::two(x, root).as_norm();
    let val = Sum::new(vec![mul, y]).as_norm();
    let result = extractor.extract("x", val).unwrap();
    assert_eq!(show_vec(&result), vec!["[y]^(3/4)", "-1*[y]^(3/4)"]);
}

#[test]
fn test_extract_from_div_top() {
    let extractor = extractor!();
    // x / 2y + z = 0 -> x = -2yz
    let (x, y, z, _) = xyzn();
    let div = Div::new(x, y).as_norm();
    let val = Sum::new(vec![div, z]).as_norm();
    let result = extractor.extract("x", val.as_norm()).unwrap();
    assert_eq!(show_vec(&result), vec!["(-2 * y * z)"]);
}

#[test]
fn test_extract_from_div_bottom() {
    let extractor = extractor!();
    // 2y / x + z = 0 -> -zx = 2y -> x = -2y / z
    let (x, y, z, _) = xyzn();
    let div = Div::new(y, x).as_norm();
    let val = Sum::new(vec![div, z]).as_norm();
    let result = extractor.extract("x", val.as_norm()).unwrap();
    assert_eq!(show_vec(&result), vec!["(-2y / z)"]);
}

#[test]
fn test_extract_from_div_top_and_bottom_no_value() {
    let extractor = extractor!();
    // 2xy / root[3](x) - z = 0 -> -zx = 2y -> x = -2y / z
    let (x, y, z, _) = xyzn();
    let mul = Mul::new(
        Number::integer(2),
        vec![x.clone(), y.set_coefficient(Number::integer(1))],
    );
    let root = Root::new(1, 2, Number::integer(1), x);
    let div = Div::new(mul.as_norm(), root.as_norm()).as_norm();
    let val = Sum::new(vec![div, z]).as_norm();
    let result = extractor.extract("x", val.as_norm()).unwrap();
    assert_eq!(result.len(), 0);
}

#[test]
fn test_extract_from_div_top_and_bottom_value() {
    let extractor = extractor!();
    // (x + 3) / (x - 3) + 2y = 0
    let (x, y, _, n) = xyzn();
    let top = Sum::new(vec![x.clone(), n.clone()]).as_norm();
    let bottom = Sum::new(vec![x, Number::integer(-3).as_norm()]).as_norm();
    let div = Div::new(top, bottom).as_norm();
    let val = Sum::new(vec![div, y]).as_norm();
    let result = extractor.extract("x", val.as_norm()).unwrap();
    assert_eq!(show_vec(&result), vec!["((6y + -3) / (2y + 1))"]);
}

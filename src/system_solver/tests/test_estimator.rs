use crate::entities::expression::{Div, IExpression, Mul, PNormalized, Root, Sum, Var};
use crate::number::Number;
use crate::proto::system_solver::IComplexityEstimator;
use crate::system_solver::complexity_estimator::ComplexityEstimator;

fn xyn() -> (PNormalized, PNormalized, PNormalized) {
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let y = Var::new(1, Number::integer(1), "y".to_string());
    let n = Number::integer(1);
    (x.as_norm(), y.as_norm(), n.as_norm())
}

#[test]
fn test_grade_0() {
    let estimator = ComplexityEstimator::new();
    let (x, y, n) = xyn();
    let mul = Mul::two(x.clone(), y.clone()).as_norm();
    let div = Div::new(x, n).as_norm();
    let expr = Sum::new(vec![y, mul, div]).as_norm();
    let complexity = estimator.estimate(&expr);
    assert_eq!(complexity.vars, 2);
    assert_eq!(complexity.grade, 0);
}

#[test]
fn test_grade_even_root() {
    let estimator = ComplexityEstimator::new();
    let (x, y, _) = xyn();
    let n = Number::integer(1);
    let rx2 = Root::new(1, 2, n.clone(), x.clone()).as_norm();
    let ry2 = Root::new(1, 2, n.clone(), y).as_norm();

    let complexity = estimator.estimate(&rx2);
    assert_eq!(complexity.vars, 1);
    assert_eq!(complexity.grade, 200);

    let div = Div::new(rx2, ry2).as_norm();
    let complexity = estimator.estimate(&div);
    assert_eq!(complexity.vars, 2);
    assert_eq!(complexity.grade, 200);

    let rx4 = Root::new(1, 4, Number::integer(1), x).as_norm();
    let complexity = estimator.estimate(&rx4);
    assert_eq!(complexity.vars, 1);
    assert_eq!(complexity.grade, 200);

    let mul = Mul::two(div, rx4).as_norm();
    let complexity = estimator.estimate(&mul);
    assert_eq!(complexity.vars, 2);
    assert_eq!(complexity.grade, 400);
}

#[test]
fn test_grade_odd_root() {
    let estimator = ComplexityEstimator::new();
    let (x, y, _) = xyn();
    let n = Number::integer(1);
    let rx3 = Root::new(1, 3, n.clone(), x.clone()).as_norm();
    let ry3 = Root::new(1, 3, n.clone(), y).as_norm();

    let complexity = estimator.estimate(&rx3);
    assert_eq!(complexity.vars, 1);
    assert_eq!(complexity.grade, 100);

    let div = Div::new(rx3, ry3).as_norm();
    let complexity = estimator.estimate(&div);
    assert_eq!(complexity.vars, 2);
    assert_eq!(complexity.grade, 100);

    let rx5 = Root::new(1, 5, Number::integer(1), x).as_norm();
    let complexity = estimator.estimate(&rx5);
    assert_eq!(complexity.vars, 1);
    assert_eq!(complexity.grade, 100);

    let mul = Mul::two(div, rx5).as_norm();
    let complexity = estimator.estimate(&mul);
    assert_eq!(complexity.vars, 2);
    assert_eq!(complexity.grade, 100);
}

#[test]
fn test_grade_mixed_root() {
    let estimator = ComplexityEstimator::new();
    let (x, y, _) = xyn();
    let n = Number::integer(1);
    let rx2 = Root::new(1, 2, n.clone(), x.clone()).as_norm();
    let ry3 = Root::new(1, 3, n.clone(), y.clone()).as_norm();
    let rx4 = Root::new(1, 4, n.clone(), x).as_norm();
    let ry5 = Root::new(1, 5, n.clone(), y).as_norm();
    let sum = Sum::new(vec![rx2, ry3, rx4, ry5]).as_norm();
    let complexity = estimator.estimate(&sum);
    assert_eq!(complexity.vars, 2);
    assert_eq!(complexity.grade, 500);
}

#[test]
fn test_grade_var_and_root() {
    let estimator = ComplexityEstimator::new();
    let (x, _, _) = xyn();
    let x = x.set_exponent((2).into());
    let r = Root::new(1, 3, Number::integer(1), x.clone()).as_norm();
    let sum = Sum::new(vec![x, r]).as_norm();
    let complexity = estimator.estimate(&sum);
    assert_eq!(complexity.vars, 1);
    assert_eq!(complexity.grade, 102);
}

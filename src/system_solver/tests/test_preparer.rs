use std::cell::RefCell;
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{BinaryOperation, IExpression, Operator};
use crate::entities::solver::{
    EquationComplexity, PreparedEquations, SystemSolutionVariant,
};
use crate::normalizer;
use crate::number::Number;
use crate::proto::system_solver::{IComplexityEstimator, IResearcher, ISystemSolver};
use crate::system_solver::complexity_estimator::ComplexityEstimator;
use crate::system_solver::system_solver::SystemSolver;
use crate::system_solver::tests::fixtures::xyz;
use std::collections::HashSet;

type CallMocker = Rc<RefCell<Option<PreparedEquations>>>;

struct MockedResearcher {
    call: CallMocker,
}

impl MockedResearcher {
    fn new() -> (MockedResearcher, CallMocker) {
        let mocker = Rc::new(RefCell::new(None));
        (
            MockedResearcher {
                call: mocker.clone(),
            },
            mocker,
        )
    }
}

impl IResearcher for MockedResearcher {
    fn research(
        &self,
        prepared: &PreparedEquations,
    ) -> SolverResult<Vec<SystemSolutionVariant>> {
        *self.call.borrow_mut() = Some(prepared.clone());
        Ok(Vec::new())
    }
}

fn make_mocked_solver() -> (SystemSolver, CallMocker) {
    let estimator: Rc<dyn IComplexityEstimator> = Rc::new(ComplexityEstimator::new());
    let (researcher, mocker) = MockedResearcher::new();
    let solver = SystemSolver::new(
        Box::new(researcher),
        Rc::new(normalizer::create(false)),
        estimator,
    );
    (solver, mocker)
}

#[test]
fn test_preparer() {
    let (solver, mocker) = make_mocked_solver();
    let (x, y, z) = xyz();
    let left1 = BinaryOperation::new(Operator::Mul, x.as_expr(), y.as_expr()).as_expr();
    let right1 = Number::integer(10).as_expr();
    let eq1 =
        BinaryOperation::new(Operator::Eql, left1.clone(), right1.clone()).as_expr();
    let left2 = BinaryOperation::new(Operator::Sub, x.as_expr(), z.as_expr()).as_expr();
    let right2 = Number::integer(0).as_expr();
    let eq2 =
        BinaryOperation::new(Operator::Eql, left2.clone(), right2.clone()).as_expr();
    let equations = vec![eq1.clone(), eq2.clone()];
    assert!(solver.solve(equations).unwrap().is_empty());
    let mut call = mocker.borrow().clone().unwrap();
    call.all_vars.sort();
    assert_eq!(
        call.all_vars,
        vec!["x".to_string(), "y".to_string(), "z".to_string()]
    );
    assert_eq!(call.equations.len(), 2);

    assert!(call.equations[0].source.left.cmp_expr(&left1));
    assert!(call.equations[0].source.right.cmp_expr(&right1));
    assert_eq!(call.equations[0].normalized.format(), "((x * y) + -10)");
    let vars: HashSet<String> = vec!["x", "y"].iter().map(|x| x.to_string()).collect();
    assert_eq!(call.equations[0].vars, vars);
    assert_eq!(
        call.equations[0].complexity,
        EquationComplexity { vars: 2, grade: 0 }
    );

    assert!(call.equations[1].source.left.cmp_expr(&left2));
    assert!(call.equations[1].source.right.cmp_expr(&right2));
    assert_eq!(call.equations[1].normalized.format(), "(x + -1z)");
    let vars: HashSet<String> = vec!["x", "z"].iter().map(|x| x.to_string()).collect();
    assert_eq!(call.equations[1].vars, vars);
    assert_eq!(
        call.equations[1].complexity,
        EquationComplexity { vars: 2, grade: 0 }
    );
}

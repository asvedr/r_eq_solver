use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::expression::{Div, IExpression, Mul, Sum};
use crate::normalizer;
use crate::number::Number;
use crate::proto::system_solver::{IComplexityEstimator, IResearcher};
use crate::system_solver::complexity_estimator::ComplexityEstimator;
use crate::system_solver::create_researcher;
use crate::system_solver::researcher::Researcher;
use crate::system_solver::tests::fixtures::{
    int, join_lines, make_eq, make_eqs, show_result, xyz,
};

fn make_researcher(config: Config) -> Researcher {
    let estimator: Rc<dyn IComplexityEstimator> = Rc::new(ComplexityEstimator::new());
    let normalizer = normalizer::create(false);
    create_researcher(Rc::new(config), Rc::new(normalizer), estimator)
}

#[test]
fn test_3_system_234() {
    let (x, y, z) = xyz();
    let sum = Sum::new(vec![x.set_exponent((2).into()), z.set_coefficient(int(-1))]);
    let eq1 = make_eq(
        vec![Div::new(sum.as_norm(), y.clone()).as_norm()],
        &["x", "y", "z"],
        2,
    );
    let res = int(53).as_norm();
    let eq2 = make_eq(
        vec![
            x.clone(),
            y.set_exponent((2).into()),
            z.set_exponent((3).into()).set_coefficient(int(-1)),
            res,
        ],
        &["x", "y", "z"],
        3,
    );
    let xy = Mul::two(x.clone(), y.clone()).as_norm();
    let yz = Mul::new(int(-1), vec![y.clone(), z]).as_norm();
    let eq3 = make_eq(
        vec![
            y.set_exponent((2).into()),
            xy,
            yz,
            y.set_coefficient(int(-1)),
        ],
        &["x", "y", "z"],
        2,
    );
    let eqs = make_eqs(vec![eq1, eq2, eq3]);
    let config = Config::new()
        .set_interval(Some((int(0), Number::fraction(4 * 2 + 1, 2))))
        .set_use_squad_rule(false);
    let researcher = make_researcher(config);
    let solution = researcher.research(&eqs).unwrap();
    let lines = vec!["x: 2, y: 3, z: 4, "];
    assert_eq!(show_result(solution), join_lines(lines));
}

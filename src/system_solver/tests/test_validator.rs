use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    BinaryOperation, Div, IExpression, Operator, PExpression, PNormalized, Sum, Var,
};
use crate::entities::solver::{
    Equation, EquationComplexity, PreparedEquation, PreparedEquations,
};
use crate::normalizer;
use crate::number::Number;
use crate::proto::single_var_solver::{IReplacer, IValidator, VarExprMap, VarNormMap};
use crate::proto::system_solver::ISystemValidator;
use crate::single_var_solver::{create_replacer, create_validator};
use crate::system_solver::tests::fixtures::unsolvable_complex_system;
use crate::system_solver::validator::SystemValidator;

struct MockedValidator {
    results: RefCell<Vec<bool>>,
}

impl IValidator for MockedValidator {
    fn validate_norm_eq(
        &self,
        _: &Equation<PNormalized>,
        _: &VarNormMap,
    ) -> SolverResult<bool> {
        unreachable!()
    }

    fn validate_expr_eq(
        &self,
        _: &Equation<PExpression>,
        _: &VarExprMap,
    ) -> SolverResult<bool> {
        let val = self.results.borrow_mut().pop().unwrap();
        Ok(val)
    }
}

fn prepared_equations() -> PreparedEquations {
    let one = Number::integer(1);
    let equations = vec![
        PreparedEquation {
            source: Equation {
                left: one.as_expr(),
                right: one.as_expr(),
            },
            normalized: one.as_norm(),
            vars: HashSet::new(),
            complexity: EquationComplexity { vars: 0, grade: 1 },
        },
        PreparedEquation {
            source: Equation {
                left: one.as_expr(),
                right: one.as_expr(),
            },
            normalized: one.as_norm(),
            vars: HashSet::new(),
            complexity: EquationComplexity { vars: 0, grade: 1 },
        },
    ];
    PreparedEquations {
        equations,
        all_vars: vec![],
    }
}

fn replacer() -> Rc<dyn IReplacer> {
    Rc::new(create_replacer(Rc::new(normalizer::create(false))))
}

#[test]
fn test_system_validator_all_ok() {
    let single_validator = MockedValidator {
        results: RefCell::new(vec![true, true]),
    };
    let target = SystemValidator::new(Rc::new(single_validator), replacer());
    let result = target
        .validate_full(&prepared_equations(), &HashMap::new())
        .unwrap();
    assert!(result)
}

#[test]
fn test_system_validator_one_out() {
    let single_validator = MockedValidator {
        results: RefCell::new(vec![false, true]),
    };
    let target = SystemValidator::new(Rc::new(single_validator), replacer());
    let result = target
        .validate_full(&prepared_equations(), &HashMap::new())
        .unwrap();
    assert!(!result)
}

#[test]
fn test_system_validator_nan_found() {
    let single_validator = create_validator(Rc::new(normalizer::create(false)));
    let validator = SystemValidator::new(Rc::new(single_validator), replacer());
    let left = BinaryOperation::new(
        Operator::Div,
        Number::integer(1).as_expr(),
        Var::new(1, Number::integer(1), "x".to_string()).as_expr(),
    )
    .as_expr();
    let equation = PreparedEquation {
        source: Equation {
            left,
            right: Number::integer(3).as_expr(),
        },
        normalized: Number::integer(2).as_norm(),
        vars: HashSet::new(),
        complexity: EquationComplexity { vars: 0, grade: 0 },
    };
    let prepared = PreparedEquations {
        equations: vec![equation],
        all_vars: vec!["x".to_string()],
    };
    let mut vars = HashMap::new();
    vars.insert("x".to_string(), Number::integer(0));
    let result = validator.validate_full(&prepared, &vars).unwrap();
    assert!(!result)
}

fn equations_for_partial() -> PreparedEquations {
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let div = Div::new(Number::integer(2).as_norm(), x).as_norm();
    let num = Number::integer(3).as_norm();
    let normalized = Sum::new(vec![div, num]).as_norm();
    let e = Number::integer(3).as_expr();
    let equation = PreparedEquation {
        source: Equation {
            left: e.clone(),
            right: e,
        },
        normalized,
        vars: HashSet::new(),
        complexity: EquationComplexity { vars: 0, grade: 0 },
    };
    PreparedEquations {
        equations: vec![equation],
        all_vars: vec!["x".to_string()],
    }
}

#[test]
fn test_system_validator_partial_ok() {
    let single_validator = create_validator(Rc::new(normalizer::create(false)));
    let validator = SystemValidator::new(Rc::new(single_validator), replacer());
    let equations = equations_for_partial();
    let mut vars = HashMap::new();
    vars.insert("x".to_string(), Number::integer(1));
    let result = validator.validate_partial(&equations, &vars).unwrap();
    assert!(result)
}

#[test]
fn test_system_validator_partial_fail() {
    let single_validator = create_validator(Rc::new(normalizer::create(false)));
    let validator = SystemValidator::new(Rc::new(single_validator), replacer());
    let equations = equations_for_partial();
    let mut vars = HashMap::new();
    vars.insert("x".to_string(), Number::integer(0));
    let result = validator.validate_partial(&equations, &vars).unwrap();
    assert!(!result)
}

#[test]
fn test_validate_complex_system_bug() {
    let equations = unsolvable_complex_system();
    let single_validator = create_validator(Rc::new(normalizer::create(false)));
    let validator = SystemValidator::new(Rc::new(single_validator), replacer());
    let mut variant = HashMap::new();
    variant.insert("x".to_string(), Number::integer(0));
    variant.insert("y".to_string(), Number::integer(-12));
    variant.insert("z".to_string(), Number::integer(0));
    let result = validator.validate_full(&equations, &variant).unwrap();
    assert!(!result)
}

use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::expression::IExpression;
use crate::normalizer;
use crate::proto::system_solver::IResearcher;
use crate::system_solver::complexity_estimator::ComplexityEstimator;
use crate::system_solver::create_researcher;
use crate::system_solver::researcher::Researcher;
use crate::system_solver::tests::fixtures::{
    int, join_lines, make_eq, make_eqs, show_result, unsolvable_complex_system, xyz,
};

fn create_researcher_mocked_additional(config: Config) -> Researcher {
    create_researcher(
        Rc::new(config.set_use_additional_extractor(false)),
        Rc::new(normalizer::create(false)),
        Rc::new(ComplexityEstimator::new()),
    )
}

#[test]
fn test_pre_solve_simple_system_2() {
    // x - y = 1
    // x + y = 5
    let (x, y, _) = xyz();
    let first = make_eq(
        vec![x.clone(), y.set_coefficient(int(-1)), int(-1).as_norm()],
        &["x", "y"],
        0,
    );
    let second = make_eq(vec![x, y, int(-5).as_norm()], &["x", "y"], 0);
    let eqs = make_eqs(vec![first, second]);
    let solver = create_researcher_mocked_additional(Config::new());
    let result = solver.research(&eqs).unwrap();
    assert_eq!(show_result(result), "x: 3, y: 2, \n");
}

#[test]
fn test_pre_solve_simple_system_3() {
    // x - y + z = 2
    // x + y - z = 3
    // x - y - z = 1
    let (x, y, z) = xyz();
    let first = make_eq(
        vec![
            x.clone(),
            y.set_coefficient(int(-1)),
            z.clone(),
            int(-2).as_norm(),
        ],
        &["x", "y", "z"],
        0,
    );
    let second = make_eq(
        vec![
            x.clone(),
            y.clone(),
            z.set_coefficient(int(-1)),
            int(-3).as_norm(),
        ],
        &["x", "y", "z"],
        0,
    );
    let third = make_eq(
        vec![
            x,
            y.set_coefficient(int(-1)),
            z.set_coefficient(int(-1)),
            int(-1).as_norm(),
        ],
        &["x", "y", "z"],
        0,
    );
    let eqs = make_eqs(vec![first, second, third]);
    let solver = create_researcher_mocked_additional(Config::new());
    let result = solver.research(&eqs).unwrap();
    let lines = vec!["x: 2(1/2), y: 1, z: 1/2, "];
    assert_eq!(show_result(result), join_lines(lines));
}

#[test]
fn test_pre_solve_simple_system_2_1() {
    // x - y = 2
    // x + y = 3
    // z - x = 1
    let (x, y, z) = xyz();
    let first = make_eq(
        vec![x.clone(), y.set_coefficient(int(-1)), int(-2).as_norm()],
        &["x", "y"],
        0,
    );
    let second = make_eq(vec![x.clone(), y, int(-3).as_norm()], &["x", "y"], 0);
    let third = make_eq(
        vec![x.set_coefficient(int(-1)), z, int(-1).as_norm()],
        &["x", "z"],
        0,
    );
    let eqs = make_eqs(vec![first, second, third]);
    let pre_solver = create_researcher_mocked_additional(Config::new());
    let result = pre_solver.research(&eqs).unwrap();
    let lines = vec!["x: 2(1/2), y: 1/2, z: 3(1/2), "];
    assert_eq!(show_result(result), join_lines(lines));
}

#[test]
fn test_pre_solve_complex_system() {
    let system = unsolvable_complex_system();
    let pre_solver =
        create_researcher_mocked_additional(Config::new().set_use_squad_rule(false));
    let result = pre_solver.research(&system).unwrap();
    assert!(result.is_empty());
}

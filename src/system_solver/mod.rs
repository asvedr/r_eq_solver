mod additional_extractor;
mod complexity_estimator;
mod extractor_rules;
mod polynom_var_extractor;
mod researcher;
mod system_solver;
#[cfg(test)]
mod tests;
mod utils;
mod validator;

pub use polynom_var_extractor::PolynomVarExtractor;

use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::errors::SolverResult;
use crate::entities::expression::PNormalized;
use crate::entities::solver::ExtractedVar;
use crate::normalizer;
use crate::proto::normalizer::{IEventLogger, INormalizer};
use crate::proto::single_var_solver::IReplacer;
use crate::proto::system_solver::{
    IAdditionalExtractor, IComplexityEstimator, IPolynomVarExtractor,
};
use crate::single_var_solver::{self, create_replacer, create_validator};
use crate::system_solver::additional_extractor::AdditionalExtractor;
use crate::system_solver::complexity_estimator::ComplexityEstimator;
use crate::system_solver::researcher::Researcher;
pub use crate::system_solver::system_solver::SystemSolver;
use crate::system_solver::validator::SystemValidator;
use std::collections::{HashMap, HashSet};

struct FakeAdditionalExtractor {}

impl IAdditionalExtractor for FakeAdditionalExtractor {
    fn extract(
        &self,
        _: &str,
        _: &PNormalized,
        _: &PNormalized,
        _: &HashMap<String, HashSet<String>>,
    ) -> SolverResult<ExtractedVar<PNormalized>> {
        Ok(ExtractedVar::NoSolution)
    }
}

fn create_extractor(
    config: Rc<Config>,
    normalizer: Rc<dyn INormalizer>,
    use_logger: bool,
) -> PolynomVarExtractor {
    let rules = extractor_rules::create(config, normalizer.clone());
    let logger: Option<Box<dyn IEventLogger>> = if use_logger {
        Some(Box::new(normalizer::EventLogger::new()))
    } else {
        None
    };
    PolynomVarExtractor::new(normalizer, rules, logger)
}

fn create_additional_extractor(
    config: Rc<Config>,
    estimator: Rc<dyn IComplexityEstimator>,
    prime_extractor: Rc<dyn IPolynomVarExtractor>,
    normalizer: Rc<dyn INormalizer>,
) -> Rc<dyn IAdditionalExtractor> {
    if config.use_additional_extractor {
        let val = AdditionalExtractor::new(
            estimator.clone(),
            prime_extractor.clone(),
            normalizer.clone(),
        );
        Rc::new(val)
    } else {
        Rc::new(FakeAdditionalExtractor {})
    }
}

pub fn create_researcher(
    config: Rc<Config>,
    normalizer: Rc<dyn INormalizer>,
    estimator: Rc<dyn IComplexityEstimator>,
) -> Researcher {
    let prime_extractor: Rc<dyn IPolynomVarExtractor> =
        Rc::new(create_extractor(config.clone(), normalizer.clone(), false));
    let additional_extractor = create_additional_extractor(
        config.clone(),
        estimator.clone(),
        prime_extractor.clone(),
        normalizer.clone(),
    );
    let single_var_solver = single_var_solver::create(config.clone(), normalizer.clone());
    let replacer: Rc<dyn IReplacer> = Rc::new(create_replacer(normalizer.clone()));
    let validator =
        SystemValidator::new(Rc::new(create_validator(normalizer)), replacer.clone());
    Researcher::new(
        config,
        prime_extractor,
        additional_extractor,
        estimator,
        replacer,
        Rc::new(single_var_solver),
        Rc::new(validator),
    )
}

pub fn create_system_solver(
    config: Rc<Config>,
    normalizer: Rc<dyn INormalizer>,
) -> SystemSolver {
    let estimator: Rc<dyn IComplexityEstimator> = Rc::new(ComplexityEstimator::new());
    let researcher = create_researcher(config, normalizer.clone(), estimator.clone());
    SystemSolver::new(Box::new(researcher), normalizer, estimator)
}

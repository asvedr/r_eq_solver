mod div;
mod mul;
mod root;
mod sum;
mod sum_rules;
mod var;

use std::rc::Rc;

use crate::entities::config::Config;
use crate::normalizer::multiply_controller::MultiplyController;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::IEquationVarExtractorRule;

pub fn create(
    config: Rc<Config>,
    norm: Rc<dyn INormalizer>,
) -> Vec<Box<dyn IEquationVarExtractorRule>> {
    let m_controller = Rc::new(MultiplyController::new());
    let sum_subrules = sum_rules::rules(config, norm.clone(), m_controller);
    vec![
        Box::new(var::Rule::new(norm.clone())),
        Box::new(mul::Rule::new(norm.clone())),
        Box::new(div::Rule::new(norm.clone())),
        Box::new(root::Rule::new(norm.clone())),
        Box::new(sum::Rule::new(norm, sum_subrules)),
    ]
}

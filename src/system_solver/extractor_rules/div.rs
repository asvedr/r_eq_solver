use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, contain_var, Div, ExpressionType, IExpression, Mul, PNormalized, Sum,
};
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::NEquation;
use crate::proto::system_solver::{IEquationVarExtractorRule, IPolynomVarExtractor};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }

    fn var_in_denominator(
        &self,
        var: &str,
        div: &Div,
        right: PNormalized,
    ) -> SolverResult<NEquation> {
        let left = div.top.clone();
        let mul = Mul::two(div.bottom.clone(), right);
        let right = self.normalizer.shortify(Rc::new(mul))?;
        if contain_var(var, &left) {
            let sum = Sum::new(vec![left, right.apply_neg()]);
            let total_left = self.normalizer.shortify(Rc::new(sum))?;
            let total_right = Number::integer(0).as_norm();
            Ok(Equation {
                left: total_left,
                right: total_right,
            })
        } else {
            Ok(Equation {
                left: right,
                right: left,
            })
        }
    }

    fn var_in_numerator(&self, div: &Div, right: PNormalized) -> SolverResult<NEquation> {
        let left = div.top.clone();
        let mul = Mul::two(right, div.bottom.clone());
        let right = self.normalizer.shortify(Rc::new(mul))?;
        Ok(Equation { left, right })
    }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Div
    }

    fn apply(
        &self,
        _: &dyn IPolynomVarExtractor,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let div = cast_norm::<Div>(&equation.left).unwrap();
        let right = equation.right.clone();
        let new_eq;
        if contain_var(main_var, &div.bottom) {
            new_eq = self.var_in_denominator(main_var, div, right)?;
        } else {
            new_eq = self.var_in_numerator(div, right)?;
        }
        Ok(EquationExtractorRuleResult::NotFinal(new_eq))
    }
}

use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, contain_var, ExpressionType, Sum};
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::NEquation;
use crate::proto::system_solver::{IEquationVarExtractorRule, IPolynomVarExtractor};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
    subrules: Vec<Box<dyn IEquationVarExtractorRule>>,
}

impl Rule {
    pub fn new(
        normalizer: Rc<dyn INormalizer>,
        subrules: Vec<Box<dyn IEquationVarExtractorRule>>,
    ) -> Rule {
        Rule {
            normalizer,
            subrules,
        }
    }

    // Shift values without target var to right using "+-" operations
    fn separate_and_sub(
        &self,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<NEquation> {
        let sum = cast_norm::<Sum>(&equation.left).unwrap();
        let mut to_left = vec![];
        let mut to_right = vec![];
        for item in sum.items.iter() {
            if contain_var(main_var, &item) {
                to_left.push(item.clone())
            } else {
                to_right.push(item.apply_neg())
            }
        }
        if to_right.is_empty() {
            return Ok(equation.clone());
        }
        assert!(!to_left.is_empty());
        to_right.push(equation.right.clone());
        let right = self.normalizer.shortify(Rc::new(Sum::new(to_right)))?;
        let left = self.normalizer.shortify(Rc::new(Sum::new(to_left)))?;
        Ok(Equation { left, right })
    }

    // fn separate_and_div(
    //     &self,
    //     main_var: &str,
    //     equation: NEquation,
    // ) -> SolverResult<EquationExtractorRuleResult> {
    //     let mut mults = self.m_controller.get_multipliers(&equation.left);
    //     let left = match mults.get_mult_with_var(main_var) {
    //         Some(val) => val,
    //         _ => return Ok(EquationExtractorRuleResult::NoSolution),
    //     };
    //     mults.only_mult_with_var(main_var);
    //     let sum_without_var =
    //         self.m_controller
    //             .shortify(&*self.normalizer, &equation.left, mults)?;
    //     if contain_var(main_var, &sum_without_var) {
    //         // Can not exctract var
    //         return Ok(EquationExtractorRuleResult::NoSolution);
    //     }
    //     let right = Div::new(equation.right.clone(), sum_without_var.clone());
    //     let right = self.normalizer.shortify(Rc::new(right))?;
    //     let new_equation = Equation { left, right };
    //     Ok(EquationExtractorRuleResult::NotFinal(new_equation))
    // }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Sum
    }

    fn apply(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let equation = self.separate_and_sub(main_var, equation)?;
        if equation.left.get_type() != ExpressionType::Sum {
            return Ok(EquationExtractorRuleResult::NotFinal(equation));
        }
        // self.separate_and_div(main_var, equation)
        for rule in self.subrules.iter() {
            let result = rule.apply(extractor, main_var, equation.clone())?;
            if !matches!(result, EquationExtractorRuleResult::NoSolution) {
                return Ok(result);
            }
        }
        Ok(EquationExtractorRuleResult::NoSolution)
    }
}

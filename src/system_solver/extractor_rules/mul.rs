use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, contain_var, get_lcm_of_roots, Div, ExpressionType, IExpression, Mul,
};
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::NEquation;
use crate::proto::system_solver::{IEquationVarExtractorRule, IPolynomVarExtractor};
use crate::single_var_solver::utils::pow_norm;

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }

    fn move_multipliers_to_right(
        &self,
        var: &str,
        equation: NEquation,
    ) -> SolverResult<NEquation> {
        let mul = cast_norm::<Mul>(&equation.left).unwrap();
        let mut to_left = vec![];
        let mut to_right = vec![];
        for item in mul.items.iter() {
            if contain_var(var, &item) {
                to_left.push(item.clone());
            } else {
                to_right.push(item.clone());
            }
        }
        if to_right.is_empty() {
            return Ok(equation);
        }
        let mut right = Mul::new(mul.coefficient.clone(), to_right).as_norm();
        right = self.normalizer.shortify(right)?;
        right = Rc::new(Div::new(equation.right.clone(), right));
        right = self.normalizer.shortify(right)?;
        let left_mul = Mul::new(Number::integer(1), to_left);
        let left = self.normalizer.shortify(Rc::new(left_mul))?;
        Ok(Equation { left, right })
    }

    fn join_roots(&self, equation: NEquation) -> SolverResult<NEquation> {
        let mul = cast_norm::<Mul>(&equation.left).unwrap();
        let exp = get_lcm_of_roots(&mul.items);
        let left = pow_norm(&*self.normalizer, equation.left, exp.clone())?;
        let right = pow_norm(&*self.normalizer, equation.right, exp)?;
        Ok(Equation { left, right })
    }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Mul
    }

    fn apply(
        &self,
        _: &dyn IPolynomVarExtractor,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        // There are 3 available situations
        // 1) var: x, Mults[y,z,...] => no solitions
        // 2) var: x, Mults[x,y,...] || Mults[root(x),y,...] => item with x to left, other to right
        // 3) var: x, Mults[x,root1(x),root2(x),...] => try join them to one root
        let new_eq = self.move_multipliers_to_right(main_var, equation)?;
        if new_eq.left.get_type() != ExpressionType::Mul {
            return Ok(EquationExtractorRuleResult::NotFinal(new_eq));
        }
        // multiple roots or roots with var found
        let result = self.join_roots(new_eq)?;
        Ok(EquationExtractorRuleResult::NotFinal(result))
    }
}

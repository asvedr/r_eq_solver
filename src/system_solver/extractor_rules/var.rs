use std::rc::Rc;

use crate::num_traits::One;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Root, Var,
};
use crate::entities::solver::EquationExtractorRuleResult;
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::NEquation;
use crate::proto::system_solver::{IEquationVarExtractorRule, IPolynomVarExtractor};
use num_traits::ToPrimitive;

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Var
    }

    fn apply(
        &self,
        _: &dyn IPolynomVarExtractor,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let mut result = equation.right.clone();
        let var = cast_norm::<Var>(&equation.left).unwrap();
        if var.name != main_var {
            return Ok(EquationExtractorRuleResult::NoSolution);
        }
        if !var.coefficient.eq_int(1) {
            let div = Div::new(result, var.coefficient.as_norm());
            result = self.normalizer.shortify(div.as_norm())?;
        }
        if var.pow.is_one() {
            return Ok(EquationExtractorRuleResult::Final(vec![result]));
        }
        let mut exp = var.pow.to_isize().unwrap();
        if exp < 0 {
            exp = -exp;
            let one = self.normalizer.constants().norm_one.clone();
            let div = Div::new(one, result);
            result = self.normalizer.shortify(div.as_norm())?;
        }
        result = Root::new(1, exp, Number::integer(1), result).as_norm();
        result = self.normalizer.shortify(result)?;
        if exp % 2 == 0 {
            let negated = result.apply_mul_num(&Number::integer(-1));
            let variants = vec![result, negated];
            Ok(EquationExtractorRuleResult::Final(variants))
        } else {
            Ok(EquationExtractorRuleResult::Final(vec![result]))
        }
    }
}

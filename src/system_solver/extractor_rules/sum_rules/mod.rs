mod multiplier;
mod square;

use std::rc::Rc;

use crate::entities::config::Config;
use crate::proto::normalizer::{IMultiplyController, INormalizer};
use crate::proto::system_solver::IEquationVarExtractorRule;

pub fn rules(
    config: Rc<Config>,
    normalizer: Rc<dyn INormalizer>,
    m_controller: Rc<dyn IMultiplyController>,
) -> Vec<Box<dyn IEquationVarExtractorRule>> {
    let mut rules: Vec<Box<dyn IEquationVarExtractorRule>> = vec![Box::new(
        multiplier::Rule::new(normalizer.clone(), m_controller.clone()),
    )];
    if config.use_squad_rule_in_system_solver {
        rules.push(Box::new(square::Rule::new(normalizer, m_controller)))
    }
    rules
}

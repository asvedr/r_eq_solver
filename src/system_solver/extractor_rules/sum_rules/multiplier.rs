use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{contain_var, Div, ExpressionType, PNormalized};
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::proto::normalizer::{IMultiplyController, INormalizer};
use crate::proto::system_solver::{
    IEquationVarExtractorRule, IPolynomVarExtractor, NEquation,
};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
    m_controller: Rc<dyn IMultiplyController>,
}

impl Rule {
    pub fn new(
        normalizer: Rc<dyn INormalizer>,
        m_controller: Rc<dyn IMultiplyController>,
    ) -> Rule {
        Rule {
            normalizer,
            m_controller,
        }
    }

    fn solution_eq_0(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        var: &str,
        left: PNormalized,
    ) -> SolverResult<Vec<PNormalized>> {
        let right = self.normalizer.constants().norm_zero.clone();
        let equation = Equation { left, right };
        extractor.extract_equation(var, equation)
    }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        unreachable!()
    }

    fn apply(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let mut mults = self.m_controller.get_multipliers(&equation.left);
        let left = match mults.get_mult_with_var(main_var) {
            Some(val) => val,
            _ => return Ok(EquationExtractorRuleResult::NoSolution),
        };
        mults.only_mult_with_var(main_var);
        let sum_without_var =
            self.m_controller
                .shortify(&*self.normalizer, &equation.left, mults)?;
        let var_still_in_sum = contain_var(main_var, &sum_without_var);
        let zero = &self.normalizer.constants().norm_zero;
        if var_still_in_sum && equation.right.cmp_norm(zero) {
            let way1 = left;
            let way2 = sum_without_var;
            let mut solution1 = self.solution_eq_0(extractor, main_var, way1)?;
            let mut solution2 = self.solution_eq_0(extractor, main_var, way2)?;
            solution1.append(&mut solution2);
            if solution1.is_empty() {
                return Ok(EquationExtractorRuleResult::NoSolution);
            }
            Ok(EquationExtractorRuleResult::Final(solution1))
        } else if var_still_in_sum {
            // Can not exctract var
            Ok(EquationExtractorRuleResult::NoSolution)
        } else {
            let right = Div::new(equation.right.clone(), sum_without_var.clone());
            let right = self.normalizer.shortify(Rc::new(right))?;
            let new_equation = Equation { left, right };
            Ok(EquationExtractorRuleResult::NotFinal(new_equation))
        }
    }
}

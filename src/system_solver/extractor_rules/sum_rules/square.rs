use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Mul, PNormalized, Root, Sum, Var,
};
use crate::entities::normalizer::Multipliers;
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::number::Number;
use crate::proto::normalizer::{IMultiplyController, INormalizer};
use crate::proto::system_solver::{
    IEquationVarExtractorRule, IPolynomVarExtractor, NEquation,
};
use num_bigint::BigInt;
use num_traits::One;
use std::collections::BTreeSet;

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
    m_controller: Rc<dyn IMultiplyController>,
}

impl Rule {
    pub fn new(
        normalizer: Rc<dyn INormalizer>,
        m_controller: Rc<dyn IMultiplyController>,
    ) -> Rule {
        Rule {
            normalizer,
            m_controller,
        }
    }

    fn get_var_exp(&self, var: &str, expr: &PNormalized) -> Option<BigInt> {
        // We can be sure that expr _has_ target var inside
        match expr.get_type() {
            // This var is totally target var
            ExpressionType::Var => return Some(expr.get_exponent().unwrap().clone()),
            // We will check it later
            ExpressionType::Mul => (),
            // Bad case. No solution here
            _ => return None,
        };
        let mults = self.m_controller.get_multipliers(expr);
        match mults.get_mult_with_var(var) {
            Some(val) if val.get_type() == ExpressionType::Var => {
                Some(val.get_exponent().unwrap().clone())
            }
            _ => None,
        }
    }

    fn get_square_exps(&self, var: &str, expr: &PNormalized) -> Option<(BigInt, BigInt)> {
        let sum = cast_norm::<Sum>(expr).unwrap();
        let mut exps = BTreeSet::new();
        for item in sum.items.iter() {
            if let Some(exp) = self.get_var_exp(var, item) {
                exps.insert(exp);
            } else {
                return None;
            }
        }
        let mut exps_vec = exps.into_iter().collect::<Vec<_>>();
        if exps_vec.len() != 2 || (exps_vec[1] != (&exps_vec[0] * 2)) {
            return None;
        }
        let two = exps_vec.pop().unwrap();
        let one = exps_vec.pop().unwrap();
        Some((one, two))
    }

    fn build_square(
        &self,
        main_var: &str,
        equation: NEquation,
        one: BigInt,
        two: BigInt,
    ) -> SolverResult<(PNormalized, PNormalized, PNormalized)> {
        let mut m1 = Multipliers::new();
        let mut m2 = Multipliers::new();
        let var =
            Var::new(one.clone(), Number::integer(1), main_var.to_string()).as_norm();
        m2.add_multiplier(var.set_exponent(two.clone()));
        m1.add_multiplier(var);
        let mut items_of_two = Vec::new();
        let mut items_of_one = Vec::new();
        let left = cast_norm::<Sum>(&equation.left).unwrap();
        for item in left.items.iter() {
            let exp = self.get_var_exp(main_var, item).unwrap();
            if exp == one {
                items_of_one.push(item.clone())
            } else if exp == two {
                items_of_two.push(item.clone())
            } else {
                unreachable!()
            }
        }
        let coeff_of_two = self.m_controller.shortify(
            &*self.normalizer,
            &Sum::new(items_of_two).as_norm(),
            m2,
        )?;
        let coeff_of_one = self.m_controller.shortify(
            &*self.normalizer,
            &Sum::new(items_of_one).as_norm(),
            m1,
        )?;
        let coeff_of_zero = equation.right.apply_neg();
        Ok((coeff_of_two, coeff_of_one, coeff_of_zero))
    }

    fn mul(
        &self,
        n: Option<Number>,
        a: PNormalized,
        b: PNormalized,
    ) -> SolverResult<PNormalized> {
        let mul = match n {
            Some(val) => Mul::new(val, vec![a, b]),
            None => Mul::two(a, b),
        };
        self.normalizer.shortify(Rc::new(mul))
    }

    fn add(&self, a: PNormalized, b: PNormalized) -> SolverResult<PNormalized> {
        let sum = Sum::new(vec![a, b]);
        self.normalizer.shortify(Rc::new(sum))
    }

    fn div(&self, a: PNormalized, b: PNormalized) -> SolverResult<PNormalized> {
        let div = Div::new(a, b);
        self.normalizer.shortify(Rc::new(div))
    }

    fn finalize(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        var_name: &str,
        var: PNormalized,
        value: PNormalized,
    ) -> SolverResult<Vec<PNormalized>> {
        if var.get_exponent().unwrap().is_one() {
            return Ok(vec![value]);
        }
        let equation = Equation {
            left: var,
            right: value,
        };
        extractor.extract_equation(var_name, equation)
    }

    fn solve_square(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        var: &str,
        one_exp: BigInt,
        a: PNormalized,
        b: PNormalized,
        c: PNormalized,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let two = Number::integer(2);
        let four = Number::integer(4);
        let d = self.add(
            self.mul(None, b.clone(), b.clone())?,
            self.mul(Some(four), a.clone(), c.clone())?.apply_neg(),
        )?;
        let sqrt_d =
            self.normalizer
                .shortify(Rc::new(Root::new(1, 2, Number::integer(1), d)))?;
        let two_a = self.mul(None, two.as_norm(), a.clone())?;
        let x1 = self.div(self.add(sqrt_d.clone(), b.apply_neg())?, two_a.clone())?;
        let x2 = self.div(self.add(b, sqrt_d)?.apply_neg(), two_a)?;
        let one_var = Var::new(one_exp, Number::integer(1), var.to_string()).as_norm();
        let mut roots1 = self.finalize(extractor, var, one_var.clone(), x1)?;
        let mut roots2 = self.finalize(extractor, var, one_var, x2)?;
        roots1.append(&mut roots2);
        if roots1.is_empty() {
            return Ok(EquationExtractorRuleResult::NoSolution);
        }
        Ok(EquationExtractorRuleResult::Final(roots1))
    }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        unreachable!()
    }

    fn apply(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        main_var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        match self.get_square_exps(main_var, &equation.left) {
            None => Ok(EquationExtractorRuleResult::NoSolution),
            Some((one, two)) => {
                let (a, b, c) =
                    self.build_square(main_var, equation, one.clone(), two)?;
                self.solve_square(extractor, main_var, one, a, b, c)
            }
        }
    }
}

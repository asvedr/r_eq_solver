use std::rc::Rc;

use num_bigint::BigInt;
use num_traits::One;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, ExpressionType, Operator, PNormalized, Root,
};
use crate::entities::normalizer::NormalizedBin;
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::NEquation;
use crate::proto::system_solver::{IEquationVarExtractorRule, IPolynomVarExtractor};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }

    fn remove_coefficient(&self, root: &Root, right: PNormalized) -> PNormalized {
        let coeff = &Number::integer(1) / &root.coefficient;
        right.apply_mul_num(&coeff)
    }

    fn pow(&self, expr: PNormalized, exp: BigInt) -> SolverResult<PNormalized> {
        let bin_op =
            NormalizedBin::new(Operator::Pow, expr, Rc::new(Number::integer(exp)));
        self.normalizer.normalize_n_binary(&bin_op)
    }
}

impl IEquationVarExtractorRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Root
    }

    fn apply(
        &self,
        _: &dyn IPolynomVarExtractor,
        _: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let root = cast_norm::<Root>(&equation.left).unwrap();
        let mut right = equation.right.clone();
        if !root.coefficient.eq_int(1) {
            right = self.remove_coefficient(&root, right);
        }
        right = self.pow(right, root.root.clone())?;
        let mut left = root.expr.clone();
        if !root.exp.is_one() {
            left = self.pow(left, root.exp.clone())?;
        }
        let equation = Equation { left, right };
        Ok(EquationExtractorRuleResult::NotFinal(equation))
    }
}

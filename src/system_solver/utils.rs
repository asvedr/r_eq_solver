use crate::entities::expression::{cast_norm, INormalized, PNormalized, Var};
use std::collections::BTreeSet;

pub fn iterate_over_tree<T: 'static + INormalized, K: Clone + Ord + Eq>(
    accum: &mut BTreeSet<K>,
    tree: &PNormalized,
    get_key: &dyn Fn(&T) -> K,
    stop_on: &Option<Box<dyn Fn(&BTreeSet<K>) -> bool>>,
) -> bool {
    if let Some(item) = cast_norm::<T>(tree) {
        accum.insert(get_key(&item));
        matches!(stop_on, Some(fun) if fun(&accum))
    } else {
        for child in tree.norm_children() {
            if iterate_over_tree(accum, &child, get_key, stop_on) {
                return true;
            }
        }
        false
    }
}

pub fn has_only_one_var(var: &str, tree: &PNormalized) -> bool {
    fn rec_down(var_name: &str, tree: &PNormalized) -> (bool, bool) {
        if let Some(var_node) = cast_norm::<Var>(tree) {
            let eq = var_node.name == var_name;
            return (eq, !eq);
        }
        let mut has_target = false;
        let mut has_others = false;
        for child in tree.norm_children() {
            let (child_has_target, child_has_others) = rec_down(var_name, &child);
            if child_has_others {
                return (false, true);
            }
            has_target = has_target || child_has_target;
            has_others = has_others || child_has_others;
        }
        (has_target, has_others)
    }
    let (has_target, has_others) = rec_down(var, tree);
    has_target && !has_others
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entities::expression::{IExpression, Sum};
    use crate::number::Number;

    #[test]
    fn test_has_only_one_var_yes() {
        let sum = Sum::new(vec![
            Var::new(2, Number::integer(2), "x".to_string()).as_norm(),
            Number::integer(-18).as_norm(),
        ]);
        assert!(has_only_one_var("x", &sum.as_norm()));
        assert!(!has_only_one_var("y", &sum.as_norm()));
    }

    #[test]
    fn test_has_only_one_var_too_many() {
        let sum = Sum::new(vec![
            Var::new(2, Number::integer(2), "x".to_string()).as_norm(),
            Var::new(1, Number::integer(1), "y".to_string()).as_norm(),
            Number::integer(-18).as_norm(),
        ]);
        assert!(!has_only_one_var("x", &sum.as_norm()));
        assert!(!has_only_one_var("y", &sum.as_norm()));
    }
}

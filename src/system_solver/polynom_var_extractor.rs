use std::collections::HashMap;
use std::fmt::Write;
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, contain_var, Div, ExpressionType, PNormalized,
};
use crate::entities::solver::{Equation, EquationExtractorRuleResult};
use crate::proto::normalizer::{IEventLogger, INormalizer};
use crate::proto::system_solver::{
    IEquationVarExtractorRule, IPolynomVarExtractor, NEquation,
};

pub struct PolynomVarExtractor {
    normalizer: Rc<dyn INormalizer>,
    rules: HashMap<ExpressionType, Box<dyn IEquationVarExtractorRule>>,
    pub logger: Option<Box<dyn IEventLogger>>,
}

impl PolynomVarExtractor {
    pub fn new(
        normalizer: Rc<dyn INormalizer>,
        rules_vec: Vec<Box<dyn IEquationVarExtractorRule>>,
        logger: Option<Box<dyn IEventLogger>>,
    ) -> PolynomVarExtractor {
        let mut rules = HashMap::new();
        for rule in rules_vec {
            let key = rule.get_type();
            rules.insert(key, rule);
        }
        PolynomVarExtractor {
            rules,
            normalizer,
            logger,
        }
    }

    fn log_applied_equation(
        &self,
        orig_type: ExpressionType,
        result: &EquationExtractorRuleResult,
    ) {
        let logger = match self.logger {
            Some(ref val) => val,
            _ => return,
        };
        let mut event = format!("{:?}: ", orig_type);
        match result {
            EquationExtractorRuleResult::Final(vals) => {
                write!(event, "FIN ");
                for val in vals {
                    write!(event, "{}, ", val.format());
                }
            }
            EquationExtractorRuleResult::NotFinal(eq) => {
                write!(event, "EQL {} = {}", eq.left.format(), eq.right.format());
            }
            EquationExtractorRuleResult::NoSolution => {
                write!(event, "NO SOLUTION");
            }
        };
        logger.log_event(event);
    }

    fn log_init_equation(&self, eq: &NEquation) {
        let logger = match self.logger {
            Some(ref val) => val,
            _ => return,
        };
        let ev = format!("INIT: {} = {}", eq.left.format(), eq.right.format());
        logger.log_event(ev);
    }

    fn extraction_step(
        &self,
        var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult> {
        let expr_type = equation.left.get_type();
        let rule = match self.rules.get(&expr_type) {
            Some(rule) => rule,
            _ => return Ok(EquationExtractorRuleResult::NoSolution),
        };
        let result = rule.apply(self, var, equation)?;
        self.log_applied_equation(expr_type, &result);
        Ok(result)
    }

    fn extract_with_reraise(
        &self,
        var: &str,
        mut left: PNormalized,
    ) -> SolverResult<Vec<PNormalized>> {
        if left.get_type() == ExpressionType::Div {
            let top = cast_norm::<Div>(&left).unwrap().top.clone();
            left = top;
        }
        if !contain_var(var, &left) {
            return Ok(vec![]);
        }
        let right = self.normalizer.constants().norm_zero.clone();
        let target_equation = Equation { left, right };
        self.log_init_equation(&target_equation);
        self.extract_equation(var, target_equation)
    }
}

impl IPolynomVarExtractor for PolynomVarExtractor {
    fn extract(&self, var: &str, left: PNormalized) -> SolverResult<Vec<PNormalized>> {
        match self.extract_with_reraise(var, left) {
            Ok(val) => Ok(val),
            Err(err) if err.can_be_converted_to_no_solution() => Ok(Vec::new()),
            Err(err) => Err(err),
        }
    }

    fn extract_equation(
        &self,
        var: &str,
        mut equation: Equation<PNormalized>,
    ) -> SolverResult<Vec<PNormalized>> {
        loop {
            match self.extraction_step(var, equation.clone())? {
                EquationExtractorRuleResult::NoSolution => return Ok(vec![]),
                EquationExtractorRuleResult::NotFinal(new_eq) => equation = new_eq,
                EquationExtractorRuleResult::Final(vals) => return Ok(vals),
            }
        }
    }
}

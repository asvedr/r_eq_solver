use std::collections::HashMap;
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{IExpression, PExpression, PNormalized};
use crate::entities::solver::PreparedEquations;
use crate::number::Number;
use crate::proto::single_var_solver::{IReplacer, IValidator};
use crate::proto::system_solver::ISystemValidator;

pub struct SystemValidator {
    single_validator: Rc<dyn IValidator>,
    replacer: Rc<dyn IReplacer>,
}

impl SystemValidator {
    pub fn new(
        single_validator: Rc<dyn IValidator>,
        replacer: Rc<dyn IReplacer>,
    ) -> SystemValidator {
        SystemValidator {
            single_validator,
            replacer,
        }
    }
}

impl SystemValidator {
    fn one_partial_check(
        &self,
        expr: &PNormalized,
        vars: &HashMap<String, PNormalized>,
    ) -> SolverResult<bool> {
        try_convert_ret!(self.replacer.replace_vars_in_norm(expr, vars), false);
        Ok(true)
    }
}

impl ISystemValidator for SystemValidator {
    fn validate_full(
        &self,
        prepared: &PreparedEquations,
        vars: &HashMap<String, Number>,
    ) -> SolverResult<bool> {
        let var_map = vars
            .iter()
            .map(|(key, val)| (key.clone(), val.as_expr()))
            .collect::<HashMap<String, PExpression>>();
        for equation in prepared.equations.iter() {
            let result = self
                .single_validator
                .validate_expr_eq(&equation.source, &var_map)?;
            if !result {
                return Ok(false);
            }
        }
        Ok(true)
    }
    fn validate_partial(
        &self,
        prepared: &PreparedEquations,
        vars: &HashMap<String, Number>,
    ) -> SolverResult<bool> {
        let var_map = vars
            .iter()
            .map(|(key, val)| (key.clone(), val.as_norm()))
            .collect::<HashMap<String, PNormalized>>();
        for equation in prepared.equations.iter() {
            let result = self.one_partial_check(&equation.normalized, &var_map)?;
            if !result {
                return Ok(false);
            }
        }
        Ok(true)
    }
}

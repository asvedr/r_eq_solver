use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    expr_to_equation, BinaryOperation, IExpression, Operator, PExpression, PNormalized,
    Var,
};
use crate::entities::solver::{
    Equation, PreparedEquation, PreparedEquations, SystemSolutionVariant,
};
use crate::proto::normalizer::INormalizer;
use crate::proto::system_solver::{IComplexityEstimator, IResearcher, ISystemSolver};
use crate::system_solver::utils::iterate_over_tree;
use std::collections::{BTreeSet, HashSet};
use std::rc::Rc;

pub struct SystemSolver {
    researcher: Box<dyn IResearcher>,
    normalizer: Rc<dyn INormalizer>,
    estimator: Rc<dyn IComplexityEstimator>,
}

impl SystemSolver {
    pub fn new(
        researcher: Box<dyn IResearcher>,
        normalizer: Rc<dyn INormalizer>,
        estimator: Rc<dyn IComplexityEstimator>,
    ) -> SystemSolver {
        SystemSolver {
            researcher,
            normalizer,
            estimator,
        }
    }

    fn normalize_equation(
        &self,
        source: &Equation<PExpression>,
    ) -> SolverResult<PNormalized> {
        let call = BinaryOperation::new(
            Operator::Sub,
            source.left.clone(),
            source.right.clone(),
        );
        self.normalizer.normalize(&call.as_expr())
    }

    fn collect_vars(&self, expr: &PNormalized) -> HashSet<String> {
        let mut accum = BTreeSet::new();
        let getter = |var: &Var| -> String { var.name.clone() };
        iterate_over_tree(&mut accum, expr, &getter, &None);
        accum.into_iter().collect()
    }

    fn prepare(
        &self,
        sources: Vec<Equation<PExpression>>,
    ) -> SolverResult<PreparedEquations> {
        let mut equations = Vec::new();
        let mut all_vars = HashSet::new();
        for source in sources {
            let normalized = self.normalize_equation(&source)?;
            let vars = self.collect_vars(&normalized);
            all_vars.extend(vars.iter().cloned());
            let complexity = self.estimator.estimate(&normalized);
            let prepared = PreparedEquation {
                source,
                normalized,
                vars,
                complexity,
            };
            equations.push(prepared);
        }
        let result = PreparedEquations {
            equations,
            all_vars: all_vars.into_iter().collect(),
        };
        Ok(result)
    }
}

impl ISystemSolver for SystemSolver {
    fn solve(
        &self,
        equations: Vec<PExpression>,
    ) -> SolverResult<Vec<SystemSolutionVariant>> {
        let mut sources = Vec::new();
        for equation in equations {
            sources.push(expr_to_equation(&equation)?);
        }
        let prepared = self.prepare(sources)?;
        self.researcher.research(&prepared)
    }
}

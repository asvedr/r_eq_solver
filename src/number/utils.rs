use num_bigint::BigInt;

use crate::entities::constants::STEPS_TO_UPGRADE_F64;
use crate::fraction::Fraction;

pub fn upgrate_f64_to_fraction(mut source: f64) -> Box<Fraction> {
    let ten: BigInt = (10).into();
    let mut numerator: BigInt = (source as isize).into();
    let mut denominator: BigInt = (1).into();
    // source -= source.floor();
    for _ in 0..STEPS_TO_UPGRADE_F64 {
        numerator *= &ten;
        denominator *= &ten;
        source *= 10.0;
        numerator += (source as isize) % 10;
        // source -= source.floor();
    }
    Box::new(Fraction::new(numerator, denominator))
}

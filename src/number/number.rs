use std::cmp::Ordering;
use std::fmt;
use std::ops::{Add, Div, Mul, Neg, Sub};

use num_bigint::BigInt;
use num_integer::Integer;

use crate::entities::constants::ROUND_FRACTION_TO_INT_THRESHOLD;
use crate::fraction::Fraction;
use crate::number::ops::{add, div, mul, neg, pow, sub};
use crate::number::raw_number::RawNumber;

#[derive(Clone, Debug)]
pub struct Number {
    data: RawNumber,
}

macro_rules! wraps {
    ($data:expr) => {
        Number {
            data: $data.optimize(),
        }
    };
}

impl Number {
    pub fn integer<T: Into<BigInt>>(value: T) -> Number {
        wraps!(RawNumber::Integer(value.into()))
    }

    pub fn fraction<A: Into<BigInt>, B: Into<BigInt>>(
        numerator: A,
        denominator: B,
    ) -> Number {
        let data = Box::new(Fraction::new(numerator, denominator));
        wraps!(RawNumber::Fraction(data).optimize())
    }

    pub fn rational(value: f64) -> Number {
        wraps!(RawNumber::Rational(value).optimize())
    }

    pub fn inf(sign: isize) -> Number {
        wraps!(RawNumber::Inf(sign))
    }

    pub fn nan() -> Number {
        wraps!(RawNumber::NaN)
    }

    pub fn pow(&self, other: &Number) -> Number {
        wraps!(pow::apply(&self.data, &other.data))
    }

    pub fn eq_int<T: Into<BigInt>>(&self, value: T) -> bool {
        matches!(
            self.data,
            RawNumber::Integer(ref x) if *x == value.into()
        )
    }

    pub fn as_int(&self) -> Option<BigInt> {
        match self.data {
            RawNumber::Integer(ref x) => Some(x.clone()),
            _ => None,
        }
    }

    pub fn is_int(&self) -> bool {
        matches!(self.data, RawNumber::Integer(_))
    }

    pub fn is_neg(&self) -> bool {
        self.data.signum() < 0
    }

    pub fn is_even_div(&self) -> bool {
        matches!(self.data, RawNumber::Fraction(ref val) if val.denominator.is_even())
    }

    pub fn is_nan(&self) -> bool {
        matches!(self.data, RawNumber::NaN)
    }

    pub fn is_inf(&self) -> bool {
        matches!(self.data, RawNumber::Inf(_))
    }

    pub fn signum(&self) -> Number {
        if self.is_nan() {
            return Number::nan();
        }
        Number::integer(self.data.signum())
    }

    pub fn abs(&self) -> Number {
        Number {
            data: self.data.abs(),
        }
    }

    #[inline(always)]
    pub fn is_same_type(&self, other: &Number) -> bool {
        self.data.is_same_type(&other.data)
    }

    pub fn to_same_type(&self, destination: &Number) -> Option<Number> {
        let val = self.data.to_same_type(&destination.data)?;
        Some(Number { data: val })
    }

    pub fn round(&self) -> Number {
        let fraction: Box<Fraction> = match self.data.clone().optimize() {
            RawNumber::Fraction(fraction) => fraction,
            other => return Number { data: other },
        };
        let int = fraction.integer_part();
        let a = Number::integer(int.clone());
        let b = Number::integer(int + self.data.signum());
        let fraction = Number {
            data: RawNumber::Fraction(fraction),
        };
        let ethalon = Number::fraction(1, ROUND_FRACTION_TO_INT_THRESHOLD);
        if (&a - &fraction).abs() < ethalon {
            return a;
        }
        if (&b - &fraction).abs() < ethalon {
            return b;
        }
        fraction
    }

    pub fn is_difficult(&self) -> bool {
        match self.data {
            RawNumber::Rational(_) => true,
            RawNumber::Fraction(ref value) => value.denominator_is_big(),
            _ => false,
        }
    }
}

impl fmt::Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.data {
            RawNumber::Integer(ref value) => write!(f, "{}", value),
            RawNumber::Fraction(ref value) => write!(f, "{}", value),
            RawNumber::Rational(ref value) => write!(f, "{}", value),
            RawNumber::Inf(ref signum) => {
                if *signum < 0 {
                    write!(f, "-inf")
                } else {
                    write!(f, "+inf")
                }
            }
            RawNumber::NaN => write!(f, "NaN"),
        }
    }
}

macro_rules! impl_op_trait {
    ($trait:ident, $trait_method:ident) => {
        impl $trait for Number {
            type Output = Number;

            fn $trait_method(self, other: Number) -> Number {
                (&self).$trait_method(&other)
            }
        }

        impl $trait for &Number {
            type Output = Number;

            fn $trait_method(self, other: &Number) -> Number {
                if self.is_nan() || other.is_nan() {
                    return Number::nan();
                }
                if self.is_inf() || other.is_inf() {
                    let raw = $trait_method::apply_inf(&self.data, &other.data);
                    return wraps!(raw);
                }
                if self.is_same_type(other) {
                    let raw = $trait_method::apply_normal(&self.data, &other.data);
                    return wraps!(raw);
                }
                if let Some(converted) = other.to_same_type(self) {
                    let raw = $trait_method::apply_normal(&self.data, &converted.data);
                    return wraps!(raw);
                }
                if let Some(converted) = self.to_same_type(other) {
                    let raw = $trait_method::apply_normal(&converted.data, &other.data);
                    return wraps!(raw);
                }
                unreachable!()
            }
        }
    };
}

impl_op_trait!(Add, add);
impl_op_trait!(Sub, sub);
impl_op_trait!(Mul, mul);
impl_op_trait!(Div, div);

impl Neg for &Number {
    type Output = Number;

    fn neg(self) -> Number {
        wraps!(neg::apply(&self.data))
    }
}

impl Neg for Number {
    type Output = Number;

    fn neg(self) -> Number {
        wraps!(neg::apply(&self.data))
    }
}

impl PartialOrd for Number {
    fn partial_cmp(&self, other: &Number) -> Option<Ordering> {
        self.data.partial_cmp(&other.data)
    }
}

impl PartialEq for Number {
    fn eq(&self, other: &Number) -> bool {
        self.data.eq(&other.data)
    }
}

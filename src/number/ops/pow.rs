use num_bigint::BigInt;
use num_traits::identities::{One, Zero};

use crate::fraction::{Fraction, MathResult};
use crate::number::raw_number::RawNumber;
use num_traits::ToPrimitive;

fn pow_i32(base: &RawNumber, exp: i32) -> RawNumber {
    if exp == 0 {
        return RawNumber::Integer(BigInt::one());
    }
    match base {
        RawNumber::Integer(ref val) if exp > 0 => RawNumber::Integer(val.pow(exp as u32)),
        RawNumber::Integer(ref val) => {
            let exp = -exp as u32;
            let fraction = Fraction::new(BigInt::one(), val.pow(exp));
            RawNumber::Fraction(Box::new(fraction))
        }
        RawNumber::Fraction(ref val) => RawNumber::Fraction(Box::new(val.pow(exp))),
        RawNumber::Rational(ref val) => RawNumber::Rational(val.powf(exp as f64)),
        RawNumber::Inf(_) if exp < 0 => RawNumber::Integer(BigInt::zero()),
        RawNumber::Inf(_) if exp % 2 == 0 => RawNumber::Inf(1),
        RawNumber::Inf(ref signum) => RawNumber::Inf(*signum),
        RawNumber::NaN => unreachable!(),
    }
}

fn pow_int(base: &RawNumber, exp: &BigInt) -> RawNumber {
    if let Some(exp) = exp.to_i32() {
        return pow_i32(base, exp);
    }
    if base.is_zero() {
        return RawNumber::zero();
    }
    if base.signum() < 0 {
        return RawNumber::NaN;
    }
    if base.is_one() {
        return RawNumber::one();
    }
    RawNumber::Inf(1)
}

fn pow_int_float(base: &BigInt, exp: f64) -> RawNumber {
    if let Some(val) = base.to_f64() {
        RawNumber::Rational(val.powf(exp))
    } else {
        RawNumber::NaN
    }
}

fn pow_int_fract(base: &BigInt, exp: &Fraction) -> Option<RawNumber> {
    let numerator = exp.numerator.to_u32()?;
    let denominator = exp.denominator.to_u32()?;
    let powed = base.pow(numerator);
    let result = powed.nth_root(denominator);
    if result.pow(denominator) == powed {
        Some(RawNumber::Integer(result))
    } else {
        None
    }
}

fn pow_float(base: &RawNumber, exp: f64) -> RawNumber {
    match base {
        RawNumber::Integer(ref val) => pow_int_float(val, exp),
        RawNumber::Fraction(ref val) => match val.powf(exp) {
            MathResult::Fraction(frac) => RawNumber::Fraction(Box::new(frac)),
            MathResult::Float(rat) => RawNumber::Rational(rat),
            MathResult::NaN => RawNumber::NaN,
        },
        RawNumber::Rational(ref val) => RawNumber::Rational(val.powf(exp)),
        RawNumber::Inf(1) => RawNumber::Inf(1),
        RawNumber::Inf(-1) => RawNumber::NaN,
        _ => RawNumber::NaN,
    }
}

fn pow_inf(base: &RawNumber, inf_sign: isize) -> RawNumber {
    if base.is_zero() {
        return RawNumber::zero();
    }
    if base.is_one() {
        return RawNumber::one();
    }
    if inf_sign < 0 || base.signum() < 0 {
        return RawNumber::NaN;
    }
    if base > &RawNumber::one() {
        RawNumber::Inf(1)
    } else {
        RawNumber::zero()
    }
}

fn pow_frac(base: &RawNumber, exp: &Fraction) -> RawNumber {
    if let RawNumber::Integer(ref int) = base {
        if let Some(result) = pow_int_fract(int, exp) {
            return result;
        }
    }
    if let Some(val) = exp.as_f64() {
        pow_float(base, val)
    } else {
        RawNumber::NaN
    }
}

pub fn apply(base: &RawNumber, exp: &RawNumber) -> RawNumber {
    if matches!(base, RawNumber::NaN) || matches!(exp, RawNumber::NaN) {
        return RawNumber::NaN;
    }
    match exp {
        RawNumber::Integer(ref val) => pow_int(base, val),
        RawNumber::Fraction(ref val) => pow_frac(base, val),
        RawNumber::Rational(ref val) => pow_float(base, *val),
        RawNumber::Inf(ref sign) => pow_inf(base, *sign),
        RawNumber::NaN => unreachable!(),
    }
}

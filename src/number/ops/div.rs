use num_bigint::{BigInt, Sign};
use num_traits::identities::Zero;

use crate::fraction::Fraction;
use crate::number::raw_number::RawNumber;

#[inline(always)]
pub fn apply_normal(a: &RawNumber, b: &RawNumber) -> RawNumber {
    match (a, b) {
        (RawNumber::Integer(a), RawNumber::Integer(b)) => {
            let fraction = match b.sign() {
                Sign::Minus => Fraction::new(-a, -b),
                _ => Fraction::new(a.clone(), b.clone()),
            };
            RawNumber::Fraction(Box::new(fraction))
        }
        (RawNumber::Fraction(a), RawNumber::Fraction(b)) => {
            let a: &Fraction = a;
            let b: &Fraction = b;
            RawNumber::Fraction(Box::new(a / b))
        }
        (RawNumber::Rational(a), RawNumber::Rational(b)) => RawNumber::Rational(a / b),
        _ => unreachable!(),
    }
}

#[inline(always)]
pub fn apply_inf(a: &RawNumber, b: &RawNumber) -> RawNumber {
    match (a, b) {
        (_, RawNumber::Integer(n)) if n.is_zero() => RawNumber::NaN,
        (RawNumber::Inf(_), RawNumber::Inf(_)) => RawNumber::NaN,
        (RawNumber::Inf(sign), _) => RawNumber::Inf(*sign * b.signum()),
        (_, RawNumber::Inf(_)) => RawNumber::Integer(BigInt::zero()),
        _ => unreachable!(),
    }
}

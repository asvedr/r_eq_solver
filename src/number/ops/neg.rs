use crate::fraction::Fraction;
use crate::number::raw_number::RawNumber;

pub fn apply(value: &RawNumber) -> RawNumber {
    match value {
        RawNumber::Integer(ref x) => RawNumber::Integer(-x),
        RawNumber::Fraction(ref x) => {
            let copy: Fraction = (**x).clone();
            RawNumber::Fraction(Box::new(-copy))
        }
        RawNumber::Rational(ref x) => RawNumber::Rational(-x),
        RawNumber::Inf(ref signum) => RawNumber::Inf(-signum),
        RawNumber::NaN => RawNumber::NaN,
    }
}

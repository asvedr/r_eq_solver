use num_bigint::BigInt;
use num_traits::identities::Zero;

use crate::fraction::Fraction;
use crate::number::raw_number::RawNumber;

#[inline]
pub fn apply_normal(a: &RawNumber, b: &RawNumber) -> RawNumber {
    match (a, b) {
        (RawNumber::Integer(a), RawNumber::Integer(b)) => RawNumber::Integer(a * b),
        (RawNumber::Fraction(a), RawNumber::Fraction(b)) => {
            let a: &Fraction = a;
            let b: &Fraction = b;
            RawNumber::Fraction(Box::new(a * b))
        }
        (RawNumber::Rational(a), RawNumber::Rational(b)) => RawNumber::Rational(a * b),
        _ => unreachable!(),
    }
}

#[inline]
pub fn apply_inf(a: &RawNumber, b: &RawNumber) -> RawNumber {
    let signum = a.signum() * b.signum();
    if signum == 0 {
        RawNumber::Integer(BigInt::zero())
    } else {
        RawNumber::Inf(signum)
    }
}

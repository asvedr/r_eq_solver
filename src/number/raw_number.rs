use std::cmp::Ordering;

use num_bigint::{BigInt, Sign};
use num_traits::identities::{One, Zero};

use crate::entities::constants::MIN_F64_DIGIT;
use crate::fraction::Fraction;
use crate::number::utils::upgrate_f64_to_fraction;
use num_traits::ToPrimitive;

#[derive(Clone, Debug)]
pub enum RawNumber {
    Integer(BigInt),
    Fraction(Box<Fraction>),
    Rational(f64),
    // value of inf is the sign
    Inf(isize),
    NaN,
}

impl RawNumber {
    pub fn zero() -> RawNumber {
        RawNumber::Integer(BigInt::zero())
    }

    pub fn one() -> RawNumber {
        RawNumber::Integer(BigInt::one())
    }

    pub fn signum(&self) -> isize {
        match self {
            RawNumber::Integer(ref val) => match val.sign() {
                Sign::Plus => 1,
                Sign::Minus => -1,
                _ => 0,
            },
            RawNumber::Fraction(ref val) => val.signum(),
            RawNumber::Rational(ref val) => val.signum() as isize,
            RawNumber::Inf(ref val) => *val,
            RawNumber::NaN => panic!("can not get signum of NaN"),
        }
    }

    pub fn abs(&self) -> RawNumber {
        match self {
            RawNumber::Integer(ref val) => {
                if matches!(val.sign(), Sign::Minus) {
                    RawNumber::Integer(-val)
                } else {
                    RawNumber::Integer(val.clone())
                }
            }
            RawNumber::Fraction(ref val) => RawNumber::Fraction(Box::new(val.abs())),
            RawNumber::Rational(ref val) => RawNumber::Rational(val.abs()),
            RawNumber::Inf(ref val) => RawNumber::Inf(val.abs()),
            RawNumber::NaN => RawNumber::NaN,
        }
    }

    pub fn optimize(self) -> RawNumber {
        match self {
            RawNumber::Integer(_) => self,
            RawNumber::Fraction(ref value) => {
                if value.denominator.is_one() {
                    RawNumber::Integer(value.numerator.clone())
                } else {
                    self
                }
            }
            RawNumber::Rational(ref value) => {
                if value.is_nan() {
                    RawNumber::NaN
                } else if (value - value.floor()).abs() < MIN_F64_DIGIT {
                    RawNumber::Integer((*value as i64).into())
                } else {
                    RawNumber::Fraction(upgrate_f64_to_fraction(*value))
                }
            }
            other => other,
        }
    }

    pub fn is_same_type(&self, other: &RawNumber) -> bool {
        match (self, other) {
            (RawNumber::Integer(_), RawNumber::Integer(_)) => true,
            (RawNumber::Fraction(_), RawNumber::Fraction(_)) => true,
            (RawNumber::Rational(_), RawNumber::Rational(_)) => true,
            _ => false,
        }
    }

    pub fn to_same_type(&self, destination: &RawNumber) -> Option<RawNumber> {
        match (self, destination) {
            (RawNumber::Integer(ref val), RawNumber::Rational(_)) => {
                let rational = val.to_f64()?;
                Some(RawNumber::Rational(rational))
            }
            (RawNumber::Integer(ref val), RawNumber::Fraction(_)) => {
                let data = Fraction::new(val.clone(), BigInt::one());
                Some(RawNumber::Fraction(Box::new(data)))
            }
            (RawNumber::Fraction(ref val), RawNumber::Rational(_)) => {
                let rational = val.as_f64()?;
                Some(RawNumber::Rational(rational))
            }
            _ => None,
        }
    }

    pub fn is_zero(&self) -> bool {
        matches!(self, RawNumber::Integer(x) if x.is_zero())
    }

    pub fn is_one(&self) -> bool {
        matches!(self, RawNumber::Integer(x) if x.is_one())
    }

    #[inline(always)]
    fn _signum_cmp(&self, other: &RawNumber) -> Option<Ordering> {
        let a = self.signum();
        let b = other.signum();
        if a != b {
            return a.partial_cmp(&b);
        }
        None
    }

    #[inline(always)]
    fn _partial_cmp(&self, other: &RawNumber) -> Option<Ordering> {
        match (self, other) {
            (RawNumber::Integer(a), RawNumber::Integer(b)) => a.partial_cmp(b),
            (RawNumber::Fraction(a), RawNumber::Fraction(b)) => a.partial_cmp(b),
            (RawNumber::Rational(a), RawNumber::Rational(b)) => a.partial_cmp(b),
            _ => unreachable!(),
        }
    }

    #[inline(always)]
    fn _partial_cmp_inf(&self, other: &RawNumber) -> Option<Ordering> {
        match (self, other) {
            (RawNumber::Inf(_), RawNumber::Inf(_)) => None,
            (RawNumber::Inf(signum), _) => signum.partial_cmp(&0),
            (_, RawNumber::Inf(signum)) => (0).partial_cmp(signum),
            _ => unreachable!(),
        }
    }

    #[inline(always)]
    fn _eq(&self, other: &RawNumber) -> bool {
        match (self, other) {
            (RawNumber::Integer(a), RawNumber::Integer(b)) => a.eq(b),
            (RawNumber::Fraction(a), RawNumber::Fraction(b)) => a.eq(b),
            (RawNumber::Rational(a), RawNumber::Rational(b)) => a.eq(b),
            _ => unreachable!(),
        }
    }
}

impl PartialOrd for RawNumber {
    fn partial_cmp(&self, other: &RawNumber) -> Option<Ordering> {
        if matches!(self, RawNumber::NaN) || matches!(other, RawNumber::NaN) {
            return None;
        }
        if let Some(value) = self._signum_cmp(other) {
            return Some(value);
        }
        if matches!(self, RawNumber::Inf(_)) || matches!(other, RawNumber::Inf(_)) {
            return self._partial_cmp_inf(other);
        }
        if self.is_same_type(other) {
            return self._partial_cmp(other);
        }
        if let Some(converted) = other.to_same_type(self) {
            return self._partial_cmp(&converted);
        }
        if let Some(converted) = self.to_same_type(other) {
            return converted._partial_cmp(other);
        }
        None
    }
}

impl PartialEq for RawNumber {
    fn eq(&self, other: &RawNumber) -> bool {
        if self.is_same_type(other) {
            return self._eq(other);
        }
        if let Some(converted) = other.to_same_type(self) {
            return self._eq(&converted);
        }
        if let Some(converted) = self.to_same_type(other) {
            return converted._eq(other);
        }
        false
    }
}

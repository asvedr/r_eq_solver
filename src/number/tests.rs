use crate::number::number::Number;

#[test]
fn test_number_add_ints() {
    let x = Number::integer(1) + Number::integer(2);
    assert_eq!(x.to_string(), "3");
}

#[test]
fn test_number_add_fracts() {
    let a = Number::fraction(1, 2);
    let b = Number::fraction(1, 4);
    assert_eq!((&a + &b).to_string(), "3/4");
    assert_eq!((&a + &a).to_string(), "1");
    assert_eq!((b + Number::integer(1)).to_string(), "1(1/4)");
}

#[test]
fn test_number_add_ratio() {
    let one = Number::integer(1);
    let rat = Number::rational(0.2);
    let frac = Number::fraction(1, 2);
    assert_eq!((&one + &rat).to_string(), "1(1/5)");
    assert_eq!((&rat + &frac).to_string(), "7/10");
    assert_eq!(&(&frac - &frac) * &frac, Number::integer(0));
    let result = rat.clone();
    assert_eq!((&frac / &frac) * rat, result);
}

#[test]
fn test_number_sub() {
    assert_eq!(
        Number::fraction(3, 4) - Number::integer(1),
        Number::fraction(-1, 4),
    );
    assert_eq!(
        Number::fraction(1, 2) - Number::rational(0.1),
        Number::rational(0.4)
    );
    assert_eq!(
        Number::rational(0.5) - Number::rational(0.5),
        Number::integer(0),
    );
}

#[test]
fn test_number_mul() {
    assert_eq!(
        Number::fraction(3, 4) * Number::integer(2),
        Number::fraction(6, 4),
    );
    assert_eq!(
        Number::fraction(1, 2) * Number::integer(2),
        Number::integer(1),
    );
    assert_eq!(
        Number::rational(0.5) * Number::fraction(3, 2),
        Number::rational(0.75),
    );
}

#[test]
fn test_number_div() {
    assert_eq!(
        Number::fraction(8, 3) / Number::integer(2),
        Number::fraction(4, 3),
    );
    assert_eq!(
        Number::integer(3) / Number::fraction(1, 2),
        Number::integer(6),
    );
}

#[test]
fn test_number_neg() {
    assert_eq!(-Number::fraction(3, 2), Number::fraction(-3, 2));
    assert_eq!(-Number::integer(2), Number::integer(-2));
    assert_eq!(-Number::rational(0.5), Number::rational(-0.5));
}

#[test]
fn test_number_pow_int() {
    assert_eq!(
        Number::integer(2).pow(&Number::integer(3)),
        Number::integer(8),
    );
    assert_eq!(
        Number::integer(2).pow(&Number::integer(-3)),
        Number::fraction(1, 8),
    );
    assert_eq!(
        Number::fraction(2, 3).pow(&Number::integer(2)),
        Number::fraction(4, 9),
    );
    assert_eq!(
        Number::fraction(2, 3).pow(&Number::integer(-2)),
        Number::fraction(9, 4),
    );
    assert_eq!(
        Number::rational(1.5).pow(&Number::integer(2)),
        Number::rational(2.25),
    );
}

#[test]
fn test_number_pow_float() {
    assert_eq!(
        Number::integer(4).pow(&Number::rational(0.5)),
        Number::integer(2),
    );
    assert_eq!(
        Number::integer(9).pow(&Number::fraction(1, 2)),
        Number::integer(3),
    );
    assert_eq!(
        Number::fraction(1, 4).pow(&Number::rational(0.5)),
        Number::fraction(1, 2),
    );
    assert_eq!(
        Number::rational(1.1).pow(&Number::rational(1.1)),
        Number::rational(1.1105342410545758),
    );
}

#[test]
fn test_number_compare() {
    assert!(Number::integer(2) < Number::integer(3));
    assert!(Number::integer(2) > Number::fraction(3, 8));
    assert!(Number::integer(2) < Number::rational(2.1));
    assert!(Number::fraction(1, 2) > Number::rational(0.2));

    assert!(!(Number::fraction(1, 2) > Number::rational(0.5)));
    assert!(!(Number::fraction(1, 2) < Number::rational(0.5)));
    assert!(Number::fraction(1, 2) >= Number::rational(0.5));

    assert!(Number::inf(1) != Number::inf(1));
    assert!(!(Number::inf(1) > Number::inf(1)));
    assert!(!(Number::inf(1) < Number::inf(1)));
    assert!(Number::inf(1) != Number::inf(1));
    assert!(Number::inf(1) > Number::inf(-1));
    assert!(Number::inf(-1) < Number::inf(1));
    assert!(Number::inf(1) > Number::integer(10));
    assert!(Number::inf(-1) < Number::integer(0));
    assert!(Number::inf(-1) < Number::integer(-2));

    assert!(!(Number::nan() > Number::nan()));
    assert!(!(Number::nan() < Number::nan()));
    assert!(!(Number::nan() == Number::nan()));
    assert!(Number::nan() != Number::nan());

    assert!(!(Number::nan() > Number::integer(1)));
    assert!(!(Number::integer(1) > Number::nan()));
    assert!(!(Number::nan() > Number::inf(1)));
}

#[test]
fn test_nan_operation() {
    let vals = vec![Number::integer(1), Number::inf(1), Number::rational(0.5)];
    for x in vals.iter() {
        assert!((x + &Number::nan()).is_nan());
        assert!((&Number::nan() + x).is_nan());
    }
}

// #[test]
// fn test_get_inf_by_integer_overflow() {
//     let res = Number::integer(9999).pow(&Number::integer(9999));
//     assert_eq!(res.to_string(), "+inf");
//     let res = Number::integer(-9999).pow(&Number::integer(9999));
//     assert_eq!(res.to_string(), "-inf");
//     let res = Number::integer(-9999).pow(&Number::integer(10000));
//     assert_eq!(res.to_string(), "+inf");
//     let x = Number::integer(99999999999_i64);
//     assert_eq!((&x * &x).to_string(), "+inf");
//     let nx = - &x;
//     assert_eq!((&nx * &nx).to_string(), "+inf");
//     assert_eq!((x * nx).to_string(), "-inf");
//     let x = Number::integer(9223372036854775807_i64);
//     let nx = - &x;
//     assert_eq!((&x + &x).to_string(), "+inf");
//     assert_eq!((&nx + &nx).to_string(), "-inf");
//     assert_eq!((&x - &nx).to_string(), "+inf");
//     assert_eq!((nx - x).to_string(), "-inf")
// }

#[test]
fn test_inf_positive_operations() {
    let vals = vec![
        Number::integer(1),
        Number::rational(0.5),
        Number::fraction(1, 2),
    ];
    let inf = Number::inf(1);
    for x in vals.iter() {
        assert_eq!((x + &inf).to_string(), "+inf");
        assert_eq!((&inf + x).to_string(), "+inf");
        assert_eq!((x * &inf).to_string(), "+inf");
        assert_eq!((&inf * x).to_string(), "+inf");
        assert_eq!((&inf - x).to_string(), "+inf");
        assert_eq!((&inf / x).to_string(), "+inf");
        assert_eq!((x - &inf).to_string(), "-inf");
        assert_eq!((x / &inf).to_string(), "0");
    }
    let inf = Number::inf(-1);
    for x in vals.iter() {
        assert_eq!((x + &inf).to_string(), "-inf");
        assert_eq!((&inf + x).to_string(), "-inf");
        assert_eq!((x * &inf).to_string(), "-inf");
        assert_eq!((&inf * x).to_string(), "-inf");
        assert_eq!((&inf - x).to_string(), "-inf");
        assert_eq!((&inf / x).to_string(), "-inf");
        assert_eq!((x - &inf).to_string(), "+inf");
        assert_eq!((x / &inf).to_string(), "0");
    }
}

#[test]
fn test_inf_negative_operations() {
    let vals = vec![
        Number::integer(-1),
        Number::rational(-0.5),
        Number::fraction(-1, 2),
    ];
    let inf = Number::inf(1);
    for x in vals.iter() {
        assert_eq!((x + &inf).to_string(), "+inf");
        assert_eq!((&inf + x).to_string(), "+inf");
        assert_eq!((x * &inf).to_string(), "-inf");
        assert_eq!((&inf * x).to_string(), "-inf");
        assert_eq!((&inf - x).to_string(), "+inf");
        assert_eq!((&inf / x).to_string(), "-inf");
        assert_eq!((x - &inf).to_string(), "-inf");
        assert_eq!((x / &inf).to_string(), "0");
    }
    let inf = Number::inf(-1);
    for x in vals.iter() {
        assert_eq!((x + &inf).to_string(), "-inf");
        assert_eq!((&inf + x).to_string(), "-inf");
        assert_eq!((x * &inf).to_string(), "+inf");
        assert_eq!((&inf * x).to_string(), "+inf");
        assert_eq!((&inf - x).to_string(), "-inf");
        assert_eq!((&inf / x).to_string(), "+inf");
        assert_eq!((x - &inf).to_string(), "+inf");
        assert_eq!((x / &inf).to_string(), "0");
    }
}

#[test]
fn test_inf_to_nan_operations() {
    assert_eq!((Number::inf(1) / Number::inf(-1)).to_string(), "NaN");
    assert_eq!((Number::inf(1) + Number::nan()).to_string(), "NaN");
    assert_eq!((Number::nan() * Number::inf(1)).to_string(), "NaN");
    assert_eq!((Number::nan() / Number::inf(1)).to_string(), "NaN");
    assert_eq!((Number::nan() - Number::inf(1)).to_string(), "NaN");
    assert_eq!((Number::nan().pow(&Number::inf(1))).to_string(), "NaN");
    assert_eq!((Number::inf(1).pow(&Number::nan())).to_string(), "NaN");
}

#[test]
fn test_inf_pow() {
    assert_eq!(Number::inf(1).pow(&Number::integer(2)).to_string(), "+inf");
    assert_eq!(Number::inf(1).pow(&Number::integer(3)).to_string(), "+inf");
    assert_eq!(Number::inf(1).pow(&Number::integer(0)).to_string(), "1");
    assert_eq!(Number::inf(1).pow(&Number::integer(-1)).to_string(), "0");
    assert_eq!(Number::inf(-1).pow(&Number::integer(2)).to_string(), "+inf");
    assert_eq!(Number::inf(-1).pow(&Number::integer(3)).to_string(), "-inf");
    assert_eq!(Number::inf(-1).pow(&Number::integer(0)).to_string(), "1");

    assert_eq!(
        Number::inf(-1).pow(&Number::rational(0.1)).to_string(),
        "NaN"
    );
    assert_eq!(Number::integer(1).pow(&Number::inf(1)).to_string(), "1");
    assert_eq!(Number::rational(0.1).pow(&Number::inf(1)).to_string(), "0");
    assert_eq!(Number::integer(2).pow(&Number::inf(1)).to_string(), "+inf");
    assert_eq!(Number::integer(-2).pow(&Number::inf(1)).to_string(), "NaN");
}

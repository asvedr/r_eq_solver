use std::error::Error;
use std::fmt::Write;
use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::ui::{Command, Simplify, SolveEquation, SolveSystem};
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::ISolver;
use crate::proto::system_solver::ISystemSolver;
use crate::proto::ui::ICommandsParser;
use crate::{normalizer, single_var_solver, system_solver, ui};

pub struct App {
    command_parser: ui::CommandsParser,
    normalizer: Rc<dyn INormalizer>,
    // single_solver: Rc<dyn ISolver>,
    // system_solver: Rc<dyn ISystemSolver>,
}

macro_rules! tryd {
    ($value:expr) => {
        match $value {
            Ok(val) => val,
            Err(err) => return Err(Box::new(err)),
        }
    };
}

impl App {
    pub fn new() -> App {
        let norm = Rc::new(normalizer::create(false));
        // let single_solver: Rc<dyn ISolver> =
        //     Rc::new();
        App {
            command_parser: ui::create(),
            normalizer: norm,
            // single_solver,
            // system_solver: Rc::new(system_solver::create_system_solver(norm)),
        }
    }

    fn make_single_solver(&self, config: Config) -> single_var_solver::Solver {
        single_var_solver::create(Rc::new(config), self.normalizer.clone())
    }

    fn make_system_solver(&self, config: Config) -> system_solver::SystemSolver {
        system_solver::create_system_solver(Rc::new(config), self.normalizer.clone())
    }

    fn solve_equation(&self, solve: SolveEquation) -> Result<(), Box<dyn Error>> {
        let solution = self
            .make_single_solver(solve.config)
            .solve_expr(&solve.expression);
        let solution = tryd!(solution);
        let mut roots = String::new();
        if solution.roots.is_empty() {
            roots.push_str("[]")
        } else {
            roots = format!("[{}", solution.roots[0]);
            for root in solution.roots.iter().skip(1) {
                roots.push_str(&format!(",{}", root.to_string()));
            }
            roots.push(']')
        }
        println!(
            "equation: {} = {}\nmethods: {:?}\nroots: {}",
            solution.normalized_equation.left.format(),
            solution.normalized_equation.right.format(),
            solution.used_methods,
            roots,
        );
        Ok(())
    }

    fn solve_system(&self, solve: SolveSystem) -> Result<(), Box<dyn Error>> {
        let result = self
            .make_system_solver(solve.config)
            .solve(solve.expressions)?;
        if result.is_empty() {
            println!("no solutions");
            return Ok(());
        }
        for solution in result {
            let mut vec = solution
                .into_iter()
                .map(|(key, val)| (key, val))
                .collect::<Vec<_>>();
            vec.sort_by_key(|(key, _)| key.clone());
            let (ref var, ref val) = vec[0];
            let mut line = format!("Solution: {}={}", var, val);
            for (var, val) in vec.iter().skip(1) {
                write!(line, ", {}={}", var, val);
            }
            println!("{}", line)
        }
        Ok(())
    }

    fn simplify(&self, simplify: Simplify) -> Result<(), Box<dyn Error>> {
        let expr = tryd!(self.normalizer.normalize(&simplify.expression));
        println!("{}", expr.format());
        Ok(())
    }

    fn help(&self, msg: String) -> Result<(), Box<dyn Error>> {
        println!("{}", msg);
        Ok(())
    }

    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        let command = tryd!(self.command_parser.get_command());
        match command {
            Command::Help(msg) => self.help(msg),
            Command::SolveEquation(solve) => self.solve_equation(solve),
            Command::SolveSystem(solve) => self.solve_system(solve),
            Command::Simplify(simplify) => self.simplify(simplify),
        }
    }
}

use std::cmp::Ordering;
use std::fmt;
use std::mem;
use std::ops::{Add, Div, Mul, Neg, Sub};

use num_bigint::{BigInt, Sign};
use num_integer::Integer;
use num_traits::identities::{One, Zero};

use crate::entities::constants::{MAX_INACCURACY_DENOMINATOR, MIN_F64_DIGIT};
use num_traits::{Signed, ToPrimitive};

type IntegerType = BigInt;

#[derive(Clone, PartialEq, Debug)]
pub struct Fraction {
    pub numerator: IntegerType,
    pub denominator: IntegerType,
}

#[derive(Clone, PartialEq, Debug)]
pub enum MathResult {
    Fraction(Fraction),
    Float(f64),
    NaN,
}

impl fmt::Display for Fraction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let int = &self.numerator / &self.denominator;
        if !int.is_zero() {
            let numerator = &self.numerator - &(&int * &self.denominator);
            write!(
                f,
                "{}({}/{})",
                int,
                if numerator < BigInt::zero() {
                    -numerator
                } else {
                    numerator
                },
                self.denominator
            )
        } else {
            write!(f, "{}/{}", self.numerator, self.denominator)
        }
    }
}

fn powf_to_int(source: &IntegerType, pow: f64) -> Option<IntegerType> {
    let num = source.to_f64()?;
    let num = num.powf(pow);
    if (num.floor() - num).abs() < MIN_F64_DIGIT {
        return Some(IntegerType::from(num as i64));
    }
    None
}

impl Fraction {
    pub fn new<A: Into<IntegerType>, B: Into<IntegerType>>(
        numerator: A,
        denominator: B,
    ) -> Fraction {
        let mut fraction = Fraction {
            numerator: numerator.into(),
            denominator: denominator.into(),
        };
        fraction.shortify();
        fraction
    }

    pub fn signum(&self) -> isize {
        match self.numerator.sign() {
            Sign::Plus => 1,
            Sign::Minus => -1,
            _ => 0,
        }
    }

    pub fn abs(&self) -> Fraction {
        if matches!(self.numerator.sign(), Sign::Minus) {
            Fraction::new(-&self.numerator, self.denominator.clone())
        } else {
            self.clone()
        }
    }

    pub fn as_f64(&self) -> Option<f64> {
        let denom = self.denominator.to_f64()?;
        if let Some(num) = self.numerator.to_f64() {
            return Some(num / denom);
        }
        let int = (&self.numerator / &self.denominator).to_f64()?;
        let rest = (&self.numerator % &self.denominator).to_f64()?;
        Some(int / rest)
    }

    pub fn pow(&self, pow: i32) -> Fraction {
        if pow < 0 {
            let pow = (-pow) as u32;
            Fraction::new(self.denominator.pow(pow), self.numerator.pow(pow))
        } else {
            let pow = pow as u32;
            Fraction::new(self.numerator.pow(pow), self.denominator.pow(pow))
        }
    }

    pub fn powf(&self, pow: f64) -> MathResult {
        if pow < 0.0 {
            if let Some(mut fraction) = self.powf_positive(-pow) {
                fraction.reverse();
                MathResult::Fraction(fraction)
            } else if let Some(float) = self.as_f64() {
                MathResult::Float(float.powf(pow))
            } else {
                MathResult::NaN
            }
        } else if let Some(fraction) = self.powf_positive(pow) {
            MathResult::Fraction(fraction)
        } else if let Some(float) = self.as_f64() {
            MathResult::Float(float.powf(pow))
        } else {
            MathResult::NaN
        }
    }

    fn powf_positive(&self, pow: f64) -> Option<Fraction> {
        let num = powf_to_int(&self.numerator, pow)?;
        let denom = powf_to_int(&self.denominator, pow)?;
        Some(Fraction::new(num, denom))
    }

    #[inline(always)]
    fn shortify(&mut self) {
        if self.denominator.is_negative() {
            self.numerator = -&self.numerator;
            self.denominator = -&self.denominator;
        }
        if self.numerator.is_zero() {
            self.denominator.set_one();
            return;
        }
        let gcd = self.get_gcd();
        if !gcd.is_one() {
            self.numerator /= &gcd;
            self.denominator /= gcd;
        }
    }

    // There is no "abs" method for BigInt
    fn get_gcd(&self) -> IntegerType {
        if self.numerator.sign() == Sign::Minus {
            (-&self.numerator).gcd(&self.denominator)
        } else {
            self.numerator.gcd(&self.denominator)
        }
    }

    #[inline(always)]
    fn reverse(&mut self) {
        let numerator = mem::replace(&mut self.numerator, self.denominator.clone());
        self.denominator = numerator;
    }

    #[inline]
    fn add_same_denom(&self, other: &Fraction) -> Fraction {
        Fraction::new(&self.numerator + &other.numerator, self.denominator.clone())
    }

    #[inline]
    fn add_diff_denom(&self, other: &Fraction) -> Fraction {
        let lcm = self.denominator.lcm(&other.denominator);
        let mul_a = &lcm / &self.denominator;
        let mul_b = &lcm / &other.denominator;
        let numerator = &self.numerator * &mul_a + &other.numerator * &mul_b;
        Fraction::new(numerator, lcm)
    }

    #[inline]
    fn cmp_diff_denom(&self, other: &Fraction) -> Option<Ordering> {
        let lcm = self.denominator.lcm(&other.denominator);
        let mul_a = &lcm / &self.denominator;
        let mul_b = &lcm / &other.denominator;
        let num_a = &self.numerator * mul_a;
        let num_b = &other.numerator * mul_b;
        num_a.partial_cmp(&num_b)
    }

    pub fn integer_part(&self) -> IntegerType {
        &self.numerator / &self.denominator
    }

    pub fn denominator_is_big(&self) -> bool {
        self.denominator > (MAX_INACCURACY_DENOMINATOR).into()
    }
}

impl Add for Fraction {
    type Output = Fraction;

    fn add(self, other: Fraction) -> Fraction {
        if self.denominator == other.denominator {
            self.add_same_denom(&other)
        } else {
            self.add_diff_denom(&other)
        }
    }
}

impl Add for &Fraction {
    type Output = Fraction;

    fn add(self, other: &Fraction) -> Fraction {
        if self.denominator == other.denominator {
            self.add_same_denom(other)
        } else {
            self.add_diff_denom(other)
        }
    }
}

impl Neg for Fraction {
    type Output = Fraction;

    fn neg(self) -> Fraction {
        Fraction::new(-self.numerator, self.denominator)
    }
}

impl Neg for &Fraction {
    type Output = Fraction;

    fn neg(self) -> Fraction {
        Fraction::new(-&self.numerator, self.denominator.clone())
    }
}

impl Sub for Fraction {
    type Output = Fraction;

    fn sub(self, other: Fraction) -> Fraction {
        self + (-other)
    }
}

impl Sub for &Fraction {
    type Output = Fraction;

    fn sub(self, other: &Fraction) -> Fraction {
        self + &(-other)
    }
}

impl Mul for Fraction {
    type Output = Fraction;

    fn mul(self, other: Fraction) -> Fraction {
        Fraction::new(
            self.numerator * other.numerator,
            self.denominator * other.denominator,
        )
    }
}

impl Mul for &Fraction {
    type Output = Fraction;

    fn mul(self, other: &Fraction) -> Fraction {
        Fraction::new(
            &self.numerator * &other.numerator,
            &self.denominator * &other.denominator,
        )
    }
}

impl Div for Fraction {
    type Output = Fraction;

    fn div(self, other: Fraction) -> Fraction {
        Fraction::new(
            self.numerator * other.denominator,
            self.denominator * other.numerator,
        )
    }
}

impl Div for &Fraction {
    type Output = Fraction;

    fn div(self, other: &Fraction) -> Fraction {
        Fraction::new(
            &self.numerator * &other.denominator,
            &self.denominator * &other.numerator,
        )
    }
}

impl PartialOrd for Fraction {
    fn partial_cmp(&self, other: &Fraction) -> Option<Ordering> {
        if self.denominator == other.denominator
            || self.numerator.sign() != other.numerator.sign()
        {
            self.numerator.partial_cmp(&other.numerator)
        } else {
            self.cmp_diff_denom(other)
        }
    }
}

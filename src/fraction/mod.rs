mod fraction;
#[cfg(test)]
mod tests;

pub use fraction::{Fraction, MathResult};

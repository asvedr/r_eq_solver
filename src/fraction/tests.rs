use crate::fraction::fraction::{Fraction, MathResult};

macro_rules! f {
    ($a:expr, $b:expr) => {
        Fraction::new($a, $b)
    };
}

#[test]
fn test_fraction_str_convert() {
    assert_eq!(format!("{}", f!(1, 2)), "1/2");
    assert_eq!(format!("{}", f!(3, 4)), "3/4");
    assert_eq!(format!("{}", f!(-3, 4)), "-3/4");
    assert_eq!(format!("{}", f!(11, 7)), "1(4/7)");
    assert_eq!(format!("{}", f!(-11, 7)), "-1(4/7)");
}

#[test]
fn test_fraction_shortify() {
    assert_eq!(format!("{}", f!(4, 8)), "1/2");
    assert_eq!(format!("{}", f!(8, 12)), "2/3");
}

#[test]
fn test_fraction_eq() {
    assert!(f!(2, 4) == f!(1, 2));
    assert!(f!(3, 4) != f!(1, 2));
}

#[test]
fn test_fraction_add_and_sub() {
    let a = f!(3, 4);
    let b = f!(1, 4);
    assert_eq!(&a + &b, f!(1, 1));
    assert_eq!(&b + &a, f!(1, 1));
    assert_eq!(&a - &a, f!(0, 1));
    assert_eq!(&b - &a, f!(-1, 2));
    assert_eq!(f!(3, 8) + f!(1, 6), f!(13, 24));
    assert_eq!(f!(3, 8) + f!(1, 6) + f!(1, 24), f!(7, 12));
}

#[test]
fn test_fraction_mul_and_div() {
    assert_eq!(f!(3, 4) * f!(2, 1), f!(3, 2));
    assert_eq!(f!(3, 4) / f!(2, 1), f!(3, 8));
    assert_eq!(f!(3, 4) * f!(-2, 3), f!(-1, 2));
    assert_eq!(f!(3, 4) * f!(-2, 3), f!(-1, 2));
    assert_eq!(f!(3, 4) * f!(1, 1), f!(3, 4));
    assert_eq!(f!(3, 4) * f!(0, 1), f!(0, 1));
}

#[test]
fn test_fraction_pow_int() {
    assert_eq!(f!(3, 2).pow(3), f!(27, 8));
    assert_eq!(f!(3, 2).pow(-3), f!(8, 27));
    assert_eq!(f!(3, 2).pow(1), f!(3, 2));
    assert_eq!(f!(3, 2).pow(0), f!(1, 1));
}

#[test]
fn test_fraction_pow_float() {
    assert_eq!(f!(4, 9).powf(0.5), MathResult::Fraction(f!(2, 3)));
    assert_eq!(f!(3, 2).powf(-3.0), MathResult::Fraction(f!(8, 27)));
    match f!(3, 2).powf(1.5) {
        MathResult::Float(f) => assert!(f > 1.8 && f < 1.9),
        _ => panic!(),
    }
}

#[test]
fn test_fraction_compare() {
    assert!(f!(4, 9) < f!(2, 3));
    assert!(f!(4, 9) <= f!(2, 3));
    assert!(!(f!(4, 9) > f!(2, 3)));
    assert!(!(f!(4, 9) >= f!(2, 3)));
    assert!(f!(2, 3) > f!(1, 3));
    assert!(f!(-1, 3) < f!(1, 3));
    assert!(f!(-1, 3) < f!(1, 1000000000000_i128));

    assert!(!(f!(1, 3) < f!(1, 3)));
    assert!(!(f!(1, 3) > f!(1, 3)));
    assert!(f!(1, 3) >= f!(1, 3));
}

#[test]
fn test_big_numbers() {
    let numerator: i128 = 99999999999999999999;
    let denominator: i128 = 100000000000000000000;
    let x = f!(numerator, denominator);
    assert_eq!(
        (&x * &x).to_string(),
        "9999999999999999999800000000000000000001/10000000000000000000000000000000000000000"
    );
}

#[test]
fn test_neg_denom() {
    assert_eq!(f!(-1, -1).to_string(), "1(0/1)")
}

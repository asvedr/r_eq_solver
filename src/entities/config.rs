use crate::number::Number;

#[derive(Debug)]
pub struct Config {
    pub interval: Option<(Number, Number)>,
    pub stop_on_first: bool,
    pub use_squad_rule_in_system_solver: bool,
    pub use_additional_extractor: bool,
}

impl Config {
    pub fn new() -> Config {
        Config {
            interval: None,
            stop_on_first: true,
            use_squad_rule_in_system_solver: true,
            use_additional_extractor: true,
        }
    }

    pub fn set_interval(mut self, val: Option<(Number, Number)>) -> Config {
        self.interval = val;
        self
    }

    pub fn set_stop_on_first(mut self, val: bool) -> Config {
        self.stop_on_first = val;
        self
    }

    pub fn set_use_squad_rule(mut self, val: bool) -> Config {
        self.use_squad_rule_in_system_solver = val;
        self
    }

    pub fn set_use_additional_extractor(mut self, val: bool) -> Config {
        self.use_additional_extractor = val;
        self
    }
}

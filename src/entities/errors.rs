use std::error::Error;
use std::fmt;

pub type SolverResult<T> = Result<T, SolverError>;

#[derive(Debug, PartialEq, Eq)]
pub enum SolverError {
    NaNFound(String),
    CanNotFindRule(String),
    InvalidPow(String),
    ExpressionIsNotEquation,
    SolutionNotFound,
    TooManyVars(String),
    VarNotFound,
    ValidatorValueIsNotANumber(String),
    EquationContainsRoot(String),
    ExpressionIsVeryComplex(String),
}

#[derive(Debug, PartialEq, Eq)]
pub enum ParserError {
    UnexpectedToken(usize, String),
    TokenNotFound(String),
    EmptyBlockFound,
    UnexpectedEndOfBlock,
    UnexpectedOperator(String),
    UnexpectedOperand(String),
    ExpectedOperator(String),
}

#[derive(Debug, PartialEq, Eq)]
pub enum UIError {
    UnknownCommand(String),
    // UnknownOption(String),
    ArgNotSet(String),
    InvalidParam(String),
    InvalidExpression(ParserError),
}

impl fmt::Display for SolverError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SolverError::NaNFound(ref msg) => write!(f, "Found NaN: {}", msg),
            SolverError::CanNotFindRule(ref msg) => {
                write!(f, "Can not find rule: {}", msg)
            }
            SolverError::InvalidPow(ref msg) => write!(f, "Invalid pow: {}", msg),
            SolverError::ExpressionIsNotEquation => {
                write!(f, "Expression is not equation")
            }
            SolverError::SolutionNotFound => write!(f, "Solution not found"),
            SolverError::TooManyVars(ref vars) => write!(f, "Too many vars: {}", vars),
            SolverError::VarNotFound => write!(f, "Var not found"),
            SolverError::ValidatorValueIsNotANumber(ref msg) => {
                write!(f, "Validator value is not a num: {}", msg)
            }
            SolverError::EquationContainsRoot(ref msg) => {
                write!(f, "Equation contains root: {}", msg)
            }
            SolverError::ExpressionIsVeryComplex(ref msg) => {
                write!(f, "Expression is very complex: {}", msg)
            }
        }
    }
}

impl Error for SolverError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParserError::UnexpectedToken(ref line, ref rest) => {
                write!(f, "ParserError: unexpected token on {}: '{}'", line, rest)
            }
            ParserError::TokenNotFound(ref msg) => {
                write!(f, "ParserError: token not found: '{}'", msg)
            }
            ParserError::EmptyBlockFound => write!(f, "ParserError: empty block found",),
            ParserError::UnexpectedEndOfBlock => {
                write!(f, "ParserError: unexpected end of block",)
            }
            ParserError::UnexpectedOperator(ref msg) => {
                write!(f, "ParserError: unexpected operator: '{}'", msg)
            }
            ParserError::UnexpectedOperand(ref msg) => {
                write!(f, "ParserError: unexpected operand: '{}'", msg)
            }
            ParserError::ExpectedOperator(ref msg) => {
                write!(f, "ParserError: expected operator: '{}'", msg)
            }
        }
    }
}

impl Error for ParserError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl fmt::Display for UIError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            UIError::UnknownCommand(ref msg) => write!(f, "Unknown command: {}", msg),
            // UIError::UnknownOption(ref msg) =>
            //     write!(f, "Unknown option: {}", msg),
            UIError::ArgNotSet(ref msg) => write!(f, "Argument not set: {}", msg),
            UIError::InvalidParam(ref msg) => write!(f, "Unknown command: {}", msg),
            UIError::InvalidExpression(ref err) => {
                write!(f, "Invalid expression: {}", err)
            }
        }
    }
}

impl Error for UIError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            UIError::InvalidExpression(err) => Some(err),
            _ => None,
        }
    }
}

impl SolverError {
    pub fn can_be_converted_to_no_solution(&self) -> bool {
        matches!(
            self,
            SolverError::NaNFound(_) |
            SolverError::InvalidPow(_) |
            SolverError::SolutionNotFound |
            SolverError::EquationContainsRoot(_) |
            SolverError::VarNotFound |
            SolverError::ExpressionIsVeryComplex(_)
        )
    }
}

#[macro_export]
macro_rules! try_convert_ret {
    ($value:expr, $otherwise:expr) => {
        // value is Result<_, SolverError>
        match $value {
            Ok(val) => val,
            Err(err) if err.can_be_converted_to_no_solution() => return Ok($otherwise),
            Err(err) => return Err(err),
        }
    };
}

#[macro_export]
macro_rules! try_convert {
    ($value:expr, $otherwise:expr) => {
        // value is Result<_, SolverError>
        match $value {
            Ok(val) => val,
            Err(err) if err.can_be_converted_to_no_solution() => $otherwise,
            Err(err) => return Err(err),
        }
    };
}

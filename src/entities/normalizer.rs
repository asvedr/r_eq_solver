use std::collections::BTreeMap;
use std::fmt;
use std::mem;
use std::rc::Rc;

use num_bigint::BigInt;
use num_traits::identities::Zero;

use crate::entities::expression::{
    contain_var, BinaryOperation, ExpressionType, IExpression, Mul, Operator, PNormalized,
};
use crate::number::Number;

pub struct NormalizedBin {
    pub operator: Operator,
    pub left: PNormalized,
    pub right: PNormalized,
}

#[derive(Clone)]
pub struct Multipliers {
    pub data: BTreeMap<String, PNormalized>,
}

impl NormalizedBin {
    pub fn new(
        operator: Operator,
        left: PNormalized,
        right: PNormalized,
    ) -> NormalizedBin {
        NormalizedBin {
            operator,
            left,
            right,
        }
    }
}

impl fmt::Display for NormalizedBin {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = BinaryOperation {
            operator: self.operator,
            left: self.left.as_expr(),
            right: self.right.as_expr(),
        }
        .to_string();
        write!(f, "{}", text)
    }
}

impl Multipliers {
    pub fn new() -> Multipliers {
        Multipliers {
            data: BTreeMap::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    fn add_multiplier_with_key(&mut self, key: String, mut multiplier: PNormalized) {
        if let Some(exists) = self.data.remove(&key) {
            let a = exists.get_exponent().unwrap();
            let b = multiplier.get_exponent().unwrap();
            multiplier = multiplier.set_exponent(a + b)
        }
        self.data.insert(key, multiplier);
    }

    pub fn add_multiplier(&mut self, multiplier: PNormalized) {
        let key = multiplier.get_key().unwrap();
        self.add_multiplier_with_key(key, multiplier)
    }

    pub fn append_multipliers(&mut self, source: Multipliers) {
        for (key, expr) in source.data.iter() {
            self.add_multiplier_with_key(key.clone(), expr.clone())
        }
    }

    pub fn cross_multipliers(&self, other: &Multipliers) -> Multipliers {
        let mut result = Multipliers::new();
        for (key, expr_a) in self.data.iter() {
            if let Some(expr_b) = other.data.get(key) {
                let a = expr_a.get_exponent().unwrap();
                let b = expr_b.get_exponent().unwrap();
                let min_exp: BigInt = a.min(&b).clone();
                result
                    .data
                    .insert(key.clone(), expr_a.set_exponent(min_exp));
            }
        }
        result
    }

    pub fn min_multiplier_exp(&self, multiplier: &PNormalized) -> Option<BigInt> {
        let expr = self.data.get(&multiplier.get_key().unwrap())?;
        let exp = multiplier.get_exponent().unwrap();
        Some(expr.get_exponent().unwrap().min(&exp).clone())
    }

    fn get_key_for_mul_with_var(&self, var: &str) -> Option<String> {
        for (key, val) in self.data.iter() {
            if contain_var(var, val) {
                return Some(key.clone());
            }
        }
        None
    }

    pub fn only_mult_with_var(&mut self, var: &str) {
        if let Some(key) = self.get_key_for_mul_with_var(var) {
            let expr = self.data.remove(&key).unwrap();
            let mut new_data = BTreeMap::new();
            new_data.insert(key, expr);
            self.data = new_data;
        } else {
            self.data = BTreeMap::new();
        }
    }

    pub fn only_roots(&mut self) {
        let old_data = mem::replace(&mut self.data, BTreeMap::new());
        for (key, val) in old_data {
            if val.get_type() == ExpressionType::Root {
                self.data.insert(key, val);
            }
        }
    }

    pub fn get_mult_with_var(&self, var: &str) -> Option<PNormalized> {
        let key = self.get_key_for_mul_with_var(var)?;
        let val = self.data.get(&key)?;
        Some(val.clone())
    }

    pub fn down_multiplier_exp(&mut self, multiplier: &PNormalized) {
        let key = multiplier.get_key().unwrap();
        let exp = multiplier.get_exponent().unwrap();
        let expr: PNormalized = self.data.get(&key).unwrap().clone();
        let result = expr.get_exponent().unwrap() - exp;
        if result.is_zero() {
            self.data.remove(&key);
        } else {
            self.data.insert(key, expr.set_exponent(result));
        }
    }

    pub fn as_mul(&self) -> PNormalized {
        let items = self.data.values().cloned().collect();
        Rc::new(Mul::new(Number::integer(1), items))
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::entities::expression::Var;
    use crate::number::Number;

    #[test]
    fn test_multipliers_add() {
        let mut multipliers = Multipliers::new();
        let x = Var::new(2, Number::integer(1), "x".to_string()).as_norm();
        multipliers.add_multiplier(x.clone());
        let two: BigInt = (2).into();
        let four: BigInt = (4).into();
        let exp = multipliers.data.get("x").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, two);
        multipliers.add_multiplier(x);
        let exp = multipliers.data.get("x").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, four);
    }
    #[test]
    fn test_multipliers_append() {
        let mut multipliers_a = Multipliers::new();
        let mut multipliers_b = Multipliers::new();
        let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
        let y = Var::new(1, Number::integer(1), "y".to_string()).as_norm();
        let z = Var::new(1, Number::integer(1), "z".to_string()).as_norm();
        multipliers_a.add_multiplier(x.clone());
        multipliers_a.add_multiplier(y);
        multipliers_b.add_multiplier(x);
        multipliers_b.add_multiplier(z);
        let one: BigInt = (1).into();
        let two: BigInt = (2).into();
        multipliers_a.append_multipliers(multipliers_b);
        let exp = multipliers_a.data.get("x").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, two);
        let exp = multipliers_a.data.get("y").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, one);
        let exp = multipliers_a.data.get("z").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, one);
    }
    #[test]
    fn test_multipliers_cross() {
        let mut multipliers_a = Multipliers::new();
        let mut multipliers_b = Multipliers::new();
        let a = Var::new(1, Number::integer(1), "a".to_string()).as_norm();
        let b = Var::new(1, Number::integer(1), "b".to_string()).as_norm();
        let c = Var::new(1, Number::integer(1), "c".to_string()).as_norm();
        let d = Var::new(1, Number::integer(1), "d".to_string()).as_norm();
        multipliers_a.add_multiplier(a.set_exponent((3).into()));
        multipliers_b.add_multiplier(a.set_exponent((2).into()));
        multipliers_a.add_multiplier(b.clone());
        multipliers_b.add_multiplier(b);
        multipliers_a.add_multiplier(c);
        multipliers_b.add_multiplier(d);
        assert_eq!(multipliers_a.data.len(), 3);
        let cross = multipliers_a.cross_multipliers(&multipliers_b);
        let other_cross = multipliers_b.cross_multipliers(&multipliers_a);
        assert_eq!(cross.data.len(), other_cross.data.len());
        for ((_, a), (_, b)) in cross.data.iter().zip(other_cross.data.iter()) {
            assert!(a.cmp_norm(b))
        }
        assert_eq!(cross.data.len(), 2);
        let exp = cross.data.get("a").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, (2).into());
        let exp = cross.data.get("b").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, (1).into());
    }
    #[test]
    fn test_multipliers_min() {
        let mut multipliers = Multipliers::new();
        let a = Var::new(1, Number::integer(1), "a".to_string()).as_norm();
        let b = Var::new(1, Number::integer(1), "b".to_string()).as_norm();
        let c = Var::new(1, Number::integer(1), "c".to_string()).as_norm();
        multipliers.add_multiplier(a.set_exponent((3).into()));
        multipliers.add_multiplier(b.set_exponent((2).into()));

        assert_eq!(multipliers.min_multiplier_exp(&a), Some((1).into()));
        let exp = multipliers.min_multiplier_exp(&b.set_exponent((5).into()));
        assert_eq!(exp, Some((2).into()));
        assert_eq!(multipliers.min_multiplier_exp(&c), None);
    }
    #[test]
    fn test_multipliers_down() {
        let mut multipliers = Multipliers::new();
        let a = Var::new(1, Number::integer(1), "a".to_string()).as_norm();
        let b = Var::new(1, Number::integer(1), "b".to_string()).as_norm();
        multipliers.add_multiplier(a.set_exponent((3).into()));
        multipliers.add_multiplier(b.set_exponent((2).into()));

        multipliers.down_multiplier_exp(&a.set_exponent((2).into()));
        multipliers.down_multiplier_exp(&b.set_exponent((2).into()));
        let exp = multipliers.data.get("a").unwrap().get_exponent().unwrap();
        assert_eq!(*exp, (1).into());
        assert!(multipliers.data.get("b").is_none());
    }
}

use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};

use crate::entities::expression::{PExpression, PNormalized};
use crate::number::Number;

#[derive(Clone)]
pub struct Equation<E> {
    pub left: E,
    pub right: E,
}

pub struct Solution {
    pub source_equation: Equation<PExpression>,
    pub normalized_equation: Equation<PNormalized>,
    pub used_methods: Vec<String>,
    pub complexity: usize,
    pub roots: Vec<Number>,
}

pub struct SolutionOnlyResult {
    pub used_methods: Vec<String>,
    pub complexity: usize,
    pub roots: Vec<Number>,
}

pub enum EquationExtractorRuleResult {
    NoSolution,
    Final(Vec<PNormalized>),
    NotFinal(Equation<PNormalized>),
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
pub struct EquationComplexity {
    pub vars: usize,
    pub grade: usize,
}

pub enum ExtractedVar<FinalValue> {
    NoSolution,
    Final(FinalValue, usize),
    NotFinal(Vec<PNormalized>, EquationComplexity),
}

impl<A> PartialEq for ExtractedVar<A> {
    fn eq(&self, other: &ExtractedVar<A>) -> bool {
        use ExtractedVar::*;
        match (self, other) {
            (NoSolution, NoSolution) => true,
            (Final(_, a), Final(_, b)) => a == b,
            (NotFinal(_, a), NotFinal(_, b)) => a == b,
            _ => false,
        }
    }
}

impl<A> PartialOrd for ExtractedVar<A> {
    fn partial_cmp(&self, other: &ExtractedVar<A>) -> Option<Ordering> {
        use ExtractedVar::*;

        if self.eq(other) {
            return Some(Ordering::Equal);
        }
        match (self, other) {
            (NoSolution, _) => Some(Ordering::Greater),
            (_, NoSolution) => Some(Ordering::Less),
            (NotFinal(_, _), Final(_, _)) => Some(Ordering::Greater),
            (Final(_, _), NotFinal(_, _)) => Some(Ordering::Less),
            (NotFinal(_, a), NotFinal(_, b)) => a.partial_cmp(b),
            (Final(_, a), Final(_, b)) => a.partial_cmp(b),
        }
    }
}

#[derive(Clone)]
pub struct PreparedEquation {
    pub source: Equation<PExpression>,
    pub normalized: PNormalized,
    pub vars: HashSet<String>,
    pub complexity: EquationComplexity,
}

#[derive(Clone)]
pub struct PreparedEquations {
    pub equations: Vec<PreparedEquation>,
    pub all_vars: Vec<String>,
}

pub type SystemSolutionVariant = HashMap<String, Number>;

pub struct SystemSolution {
    pub variants: Vec<SystemSolutionVariant>,
    pub complexity: usize,
}

impl SystemSolution {
    pub fn one(var: String, values: Vec<Number>, complexity: usize) -> SystemSolution {
        let variants = values
            .into_iter()
            .map(|x| {
                let mut map = HashMap::new();
                map.insert(var.clone(), x);
                map
            })
            .collect::<Vec<SystemSolutionVariant>>();
        SystemSolution {
            variants,
            complexity,
        }
    }

    pub fn empty() -> SystemSolution {
        SystemSolution {
            variants: Vec::new(),
            complexity: 0,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.variants.is_empty()
    }

    pub fn add_var(
        &self,
        var: String,
        values: Vec<Number>,
        complexity: usize,
    ) -> SystemSolution {
        let mut variants = Vec::new();
        for map in self.variants.iter() {
            for value in values.iter() {
                let mut map_copy = map.clone();
                map_copy.insert(var.clone(), value.clone());
                variants.push(map_copy);
            }
        }
        SystemSolution {
            variants,
            complexity: complexity + self.complexity,
        }
    }
}

impl PartialEq for SystemSolution {
    fn eq(&self, other: &SystemSolution) -> bool {
        self.variants.is_empty() == other.variants.is_empty()
            && self.complexity == other.complexity
    }
}

impl PartialOrd for SystemSolution {
    fn partial_cmp(&self, other: &SystemSolution) -> Option<Ordering> {
        if self == other {
            return Some(Ordering::Equal);
        }
        if self.is_empty() {
            return Some(Ordering::Greater);
        }
        if other.is_empty() {
            return Some(Ordering::Less);
        }
        self.complexity.partial_cmp(&other.complexity)
    }
}

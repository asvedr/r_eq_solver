use std::any::Any;
use std::rc::Rc;

use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::operator::Operator;
use crate::entities::expression::traits::{cast_expr, IExpression, PExpression};

#[derive(Clone)]
pub struct UnaryOperation {
    pub operator: Operator,
    pub arg: PExpression,
}

impl UnaryOperation {
    pub fn new(operator: Operator, arg: PExpression) -> UnaryOperation {
        UnaryOperation { operator, arg }
    }
}

impl IExpression for UnaryOperation {
    fn to_string(&self) -> String {
        format!("({:?} {})", self.operator, self.arg.to_string())
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::UnaryOperation
    }
    fn children(&self) -> Vec<PExpression> {
        vec![self.arg.clone()]
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<UnaryOperation>(other).unwrap();
        self.operator == other.operator && self.arg.cmp_expr(&other.arg)
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
}

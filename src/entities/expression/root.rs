use std::any::Any;
use std::fmt::Write;
use std::rc::Rc;

use num_bigint::{BigInt, Sign};

use crate::entities::errors::SolverResult;
use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::traits::{
    cast_expr, cast_norm, Constants, ICoefficiented, IExponented, IExpression,
    IMultiplier, INormalized, PExpression, PNormalized,
};
use crate::number::Number;

#[derive(Clone)]
pub struct Root {
    pub coefficient: Number,
    pub expr: PNormalized,
    pub exp: BigInt,
    pub root: BigInt,
}

impl Root {
    pub fn new<A: Into<BigInt>, B: Into<BigInt>>(
        exp: A,
        root: B,
        coefficient: Number,
        expr: PNormalized,
    ) -> Root {
        Root {
            expr,
            exp: exp.into(),
            coefficient,
            root: root.into(),
        }
    }
}

impl PartialEq for Root {
    fn eq(&self, other: &Root) -> bool {
        self.exp == other.exp
            && self.coefficient == other.coefficient
            && self.expr.cmp_norm(&other.expr)
    }
}

impl IExpression for Root {
    fn to_string(&self) -> String {
        format!(
            "{}ROOT({}/{})[{}]",
            self.coefficient,
            self.exp,
            self.root,
            self.expr.to_string()
        )
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Root
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<Root>(other).unwrap();
        self == other
    }
    fn as_norm(&self) -> PNormalized {
        Rc::new(self.clone())
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
    fn children(&self) -> Vec<PExpression> {
        vec![self.expr.as_expr()]
    }
}

impl ICoefficiented for Root {
    fn get_coefficient(&self) -> Option<&Number> {
        Some(&self.coefficient)
    }
    fn set_coefficient(&self, coefficient: Number) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = coefficient;
        Rc::new(copy)
    }
}

impl IExponented for Root {
    fn get_exponent(&self) -> Option<&BigInt> {
        Some(&self.exp)
    }
    fn set_exponent(&self, exponent: BigInt) -> PNormalized {
        let mut copy = self.clone();
        copy.exp = exponent;
        Rc::new(copy)
    }
}

impl IMultiplier for Root {
    fn get_key(&self) -> Option<String> {
        Some(format!("ROOT({})[{}]", self.root, self.expr.to_string()))
    }
}

impl INormalized for Root {
    fn apply_neg(&self) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = -copy.coefficient;
        Rc::new(copy)
    }
    fn apply_div(&self, _: &Constants) -> SolverResult<PNormalized> {
        let mut copy = self.clone();
        copy.coefficient = &Number::integer(1) / &self.coefficient;
        copy.exp = -&self.exp;
        Ok(Rc::new(copy))
    }
    fn apply_mul_num(&self, number: &Number) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = &self.coefficient * number;
        Rc::new(copy)
    }
    fn format(&self) -> String {
        let mut result = String::new();
        if !self.coefficient.eq_int(1) {
            write!(result, "{}*", self.coefficient);
        }
        write!(
            result,
            "[{}]^({}/{})",
            self.expr.format(),
            self.exp,
            self.root,
        );
        result
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool {
        let other = cast_norm::<Root>(other).unwrap();
        self == other
    }
    fn as_div(&self) -> Option<(PNormalized, PNormalized)> {
        if self.exp.sign() != Sign::Minus {
            return None;
        }
        let top = self.coefficient.as_norm();
        let mut bottom = self.clone();
        bottom.exp = -&self.exp;
        bottom.coefficient = Number::integer(1);
        Some((top, Rc::new(bottom)))
    }
    fn norm_children(&self) -> Vec<PNormalized> {
        vec![self.expr.clone()]
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_stringify_root() {
        let expr = Number::integer(2).as_norm();
        let root = Root::new(1, 2, Number::integer(1), expr.clone());
        assert_eq!(root.to_string(), "1ROOT(1/2)[2]");
        assert_eq!(root.format(), "[2]^(1/2)");
        let root = Root::new(1, 2, Number::integer(2), expr.clone());
        assert_eq!(root.to_string(), "2ROOT(1/2)[2]");
        assert_eq!(root.format(), "2*[2]^(1/2)");
    }
}

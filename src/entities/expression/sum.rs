use std::any::Any;
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::div::Div;
use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::traits::{
    cast_expr, cast_norm, Constants, ICoefficiented, IExponented, IExpression,
    IMultiplier, INormalized, PExpression, PNormalized,
};
use crate::number::Number;

#[derive(Clone)]
pub struct Sum {
    pub items: Vec<PNormalized>,
}

impl Sum {
    pub fn new(items: Vec<PNormalized>) -> Sum {
        Sum { items }
    }
}

impl PartialEq for Sum {
    fn eq(&self, other: &Sum) -> bool {
        if self.items.len() != other.items.len() {
            return false;
        }
        for i in 0..self.items.len() {
            if !self.items[i].cmp_norm(&other.items[i]) {
                return false;
            }
        }
        true
    }
}

impl IExpression for Sum {
    fn to_string(&self) -> String {
        let mut result = "SUM[".to_string();
        for item in self.items.iter() {
            result.push_str(&item.to_string());
            result.push(',');
        }
        result.push_str("]");
        result
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Sum
    }
    fn children(&self) -> Vec<PExpression> {
        self.items.iter().map(|e| e.as_expr()).collect()
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<Sum>(other).unwrap();
        self == other
    }
    fn as_norm(&self) -> PNormalized {
        Rc::new(self.clone())
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
}

impl ICoefficiented for Sum {}
impl IExponented for Sum {}
impl IMultiplier for Sum {}

impl INormalized for Sum {
    fn apply_neg(&self) -> PNormalized {
        let mut items = vec![];
        for item in self.items.iter() {
            items.push(item.apply_neg())
        }
        Rc::new(Sum::new(items))
    }
    fn apply_div(&self, cons: &Constants) -> SolverResult<PNormalized> {
        let val = Rc::new(Div {
            top: cons.norm_one.clone(),
            bottom: Rc::new(self.clone()),
        });
        Ok(val)
    }
    fn apply_mul_num(&self, number: &Number) -> PNormalized {
        let mut items = vec![];
        for item in self.items.iter() {
            items.push(item.apply_mul_num(number))
        }
        Rc::new(Sum::new(items))
    }
    fn format(&self) -> String {
        if self.items.is_empty() {
            return "(0)".to_string();
        }
        let mut result = format!("({}", self.items[0].format());
        for i in 1..self.items.len() {
            result.push_str(" + ");
            result.push_str(&self.items[i].format())
        }
        result.push(')');
        result
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool {
        let other = cast_norm::<Sum>(other).unwrap();
        self == other
    }
    fn norm_children(&self) -> Vec<PNormalized> {
        self.items.clone()
    }
}

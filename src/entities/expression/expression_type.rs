#[derive(Clone, PartialEq, Eq, Debug, Copy, Hash)]
pub enum ExpressionType {
    BinaryOperation,
    UnaryOperation,
    Number,
    Var,
    Sum,
    Div,
    Mul,
    Root,
}

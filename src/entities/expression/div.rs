use std::any::Any;
use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::traits::{
    cast_expr, cast_norm, Constants, ICoefficiented, IExponented, IExpression,
    IMultiplier, INormalized, PExpression, PNormalized,
};
use crate::number::Number;

#[derive(Clone)]
pub struct Div {
    pub top: PNormalized,
    pub bottom: PNormalized,
}

impl Div {
    pub fn new(top: PNormalized, bottom: PNormalized) -> Div {
        Div { top, bottom }
    }
}

impl PartialEq for Div {
    fn eq(&self, other: &Div) -> bool {
        self.top.cmp_norm(&other.top) && self.bottom.cmp_norm(&other.bottom)
    }
}

impl IExpression for Div {
    fn to_string(&self) -> String {
        format!("DIV[{},{}]", self.top.to_string(), self.bottom.to_string())
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Div
    }
    fn children(&self) -> Vec<PExpression> {
        vec![self.top.as_expr(), self.bottom.as_expr()]
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<Div>(other).unwrap();
        self == other
    }
    fn as_norm(&self) -> PNormalized {
        Rc::new(self.clone())
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
}

impl ICoefficiented for Div {}
impl IExponented for Div {}
impl IMultiplier for Div {}

impl INormalized for Div {
    fn apply_neg(&self) -> PNormalized {
        Rc::new(Div {
            top: self.top.apply_neg(),
            bottom: self.bottom.clone(),
        })
    }
    fn apply_div(&self, cons: &Constants) -> SolverResult<PNormalized> {
        if self.top.cmp_expr(&cons.expr_one) {
            return Ok(self.bottom.clone());
        }
        let val = Rc::new(Div {
            top: self.bottom.clone(),
            bottom: self.top.clone(),
        });
        Ok(val)
    }
    fn apply_mul_num(&self, number: &Number) -> PNormalized {
        Rc::new(Div {
            top: self.top.apply_mul_num(number),
            bottom: self.bottom.clone(),
        })
    }
    fn format(&self) -> String {
        format!("({} / {})", self.top.format(), self.bottom.format())
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool {
        let other = cast_norm::<Div>(other).unwrap();
        self == other
    }
    fn norm_children(&self) -> Vec<PNormalized> {
        vec![self.top.clone(), self.bottom.clone()]
    }
}

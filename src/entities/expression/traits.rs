use std::any::Any;
use std::rc::Rc;

use num_bigint::BigInt;
use num_traits::identities::One;

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::expression_type::ExpressionType;
use crate::number::Number;

pub struct Constants {
    pub expr_zero: PExpression,
    pub expr_one: PExpression,
    pub norm_zero: PNormalized,
    pub norm_one: PNormalized,
}

pub type PExpression = Rc<dyn IExpression>;
pub type PNormalized = Rc<dyn INormalized>;

pub trait IExpression {
    fn to_string(&self) -> String;
    fn as_any(&self) -> &dyn Any;
    fn get_type(&self) -> ExpressionType;
    fn cmp_expr(&self, other: &PExpression) -> bool {
        self.get_type() == other.get_type() && self._is_eql_expr(other)
    }
    fn children(&self) -> Vec<PExpression> {
        vec![]
    }
    fn as_expr(&self) -> PExpression;
    fn as_norm(&self) -> PNormalized {
        panic!(
            "Can not cast {}({:?}) to normalized",
            self.to_string(),
            self.get_type()
        )
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool;
}

pub trait ICoefficiented: IExpression {
    fn get_coefficient(&self) -> Option<&Number> {
        None
    }
    fn set_coefficient(&self, _coefficient: Number) -> PNormalized {
        panic!("set_coefficient not implemented for {:?}", self.get_type())
    }
    fn drop_coefficient(&self) -> PNormalized {
        self.set_coefficient(Number::integer(1))
    }
}

pub trait IExponented: IExpression {
    fn get_exponent(&self) -> Option<&BigInt> {
        None
    }
    fn set_exponent(&self, _exponent: BigInt) -> PNormalized {
        panic!("set_exponent not implemented for {:?}", self.get_type())
    }
    fn drop_exponent(&self) -> PNormalized {
        self.set_exponent(BigInt::one())
    }
}

pub trait IMultiplier: IExpression {
    fn get_key(&self) -> Option<String> {
        None
    }
}

pub trait INormalized: IExpression + ICoefficiented + IExponented + IMultiplier {
    fn apply_neg(&self) -> PNormalized;
    fn apply_div(&self, cons: &Constants) -> SolverResult<PNormalized>;
    fn apply_mul_num(&self, number: &Number) -> PNormalized;
    fn format(&self) -> String;
    fn cmp_norm(&self, other: &PNormalized) -> bool {
        self.get_type() == other.get_type() && self._is_eql_norm(other)
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool;
    // (Numerator, Denominator)
    fn as_div(&self) -> Option<(PNormalized, PNormalized)> {
        None
    }
    fn norm_children(&self) -> Vec<PNormalized> {
        self.children().iter().map(|x| x.as_norm()).collect()
    }
}

pub fn cast_expr<T: IExpression + 'static>(ptr: &PExpression) -> Option<&T> {
    ptr.as_any().downcast_ref::<T>()
}

pub fn cast_norm<T: INormalized + 'static>(ptr: &PNormalized) -> Option<&T> {
    ptr.as_any().downcast_ref::<T>()
}

impl IExpression for Number {
    fn to_string(&self) -> String {
        format!("{}", self)
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Number
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<Number>(other).unwrap();
        self == other
    }
    fn as_norm(&self) -> PNormalized {
        Rc::new(self.clone())
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
}

impl ICoefficiented for Number {}
impl IExponented for Number {}
impl IMultiplier for Number {}

impl INormalized for Number {
    fn apply_neg(&self) -> PNormalized {
        Rc::new(-self)
    }
    fn apply_div(&self, _: &Constants) -> SolverResult<PNormalized> {
        if self.eq_int(0) {
            Err(SolverError::NaNFound("1/0".to_string()))
        } else {
            Ok(Rc::new(&Number::integer(1) / self))
        }
    }
    fn apply_mul_num(&self, number: &Number) -> PNormalized {
        Rc::new(self * number)
    }
    fn format(&self) -> String {
        format!("{}", self)
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool {
        let other = cast_norm::<Number>(other).unwrap();
        self == other
    }
}

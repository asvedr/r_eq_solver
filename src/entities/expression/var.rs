use std::any::Any;
use std::rc::Rc;

use num_bigint::{BigInt, Sign};
use num_traits::identities::One;

use crate::entities::errors::SolverResult;
use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::traits::{
    cast_expr, cast_norm, Constants, ICoefficiented, IExponented, IExpression,
    IMultiplier, INormalized, PExpression, PNormalized,
};
use crate::number::Number;

#[derive(Clone)]
pub struct Var {
    pub pow: BigInt,
    pub coefficient: Number,
    pub name: String,
}

impl Var {
    pub fn new<T: Into<BigInt>>(pow: T, coefficient: Number, name: String) -> Var {
        Var {
            pow: pow.into(),
            coefficient,
            name,
        }
    }
}

impl PartialEq for Var {
    fn eq(&self, other: &Var) -> bool {
        self.name == other.name
            && self.pow == other.pow
            && self.coefficient == other.coefficient
    }
}

impl IExpression for Var {
    fn to_string(&self) -> String {
        format!("{}{}^{}", self.coefficient, self.name, self.pow)
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Var
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<Var>(other).unwrap();
        self == other
    }
    fn as_norm(&self) -> PNormalized {
        Rc::new(self.clone())
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
}

impl ICoefficiented for Var {
    fn get_coefficient(&self) -> Option<&Number> {
        Some(&self.coefficient)
    }
    fn set_coefficient(&self, coefficient: Number) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = coefficient;
        Rc::new(copy)
    }
}

impl IExponented for Var {
    fn get_exponent(&self) -> Option<&BigInt> {
        Some(&self.pow)
    }
    fn set_exponent(&self, exponent: BigInt) -> PNormalized {
        let mut copy = self.clone();
        copy.pow = exponent;
        Rc::new(copy)
    }
}

impl IMultiplier for Var {
    fn get_key(&self) -> Option<String> {
        Some(self.name.clone())
    }
}

impl INormalized for Var {
    fn apply_neg(&self) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = -copy.coefficient;
        Rc::new(copy)
    }
    fn apply_div(&self, _: &Constants) -> SolverResult<PNormalized> {
        let copy = Var::new(
            -&self.pow,
            &Number::integer(1) / &self.coefficient,
            self.name.clone(),
        );
        Ok(Rc::new(copy))
    }
    fn apply_mul_num(&self, number: &Number) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = &self.coefficient * number;
        Rc::new(copy)
    }
    fn format(&self) -> String {
        let mut result = String::new();
        if !self.coefficient.eq_int(1) {
            result = format!("{}", self.coefficient)
        }
        result.push_str(&self.name);
        if !self.pow.is_one() {
            result = format!("{}^{}", result, self.pow)
        }
        result
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool {
        let other = cast_norm::<Var>(other).unwrap();
        self == other
    }
    fn as_div(&self) -> Option<(PNormalized, PNormalized)> {
        if self.pow.sign() != Sign::Minus {
            return None;
        }
        let top = self.coefficient.as_norm();
        let bottom = Var::new(-&self.pow, Number::integer(1), self.name.clone());
        Some((top, Rc::new(bottom)))
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Hash, Copy)]
pub enum Operator {
    Add,
    Sub,
    Mul,
    Div,
    Neg,
    Pow,
    Eql,
}

impl Operator {
    pub fn all_unary() -> Vec<Operator> {
        vec![Operator::Neg]
    }

    pub fn all_binary() -> Vec<Operator> {
        vec![
            Operator::Add,
            Operator::Sub,
            Operator::Mul,
            Operator::Div,
            Operator::Pow,
        ]
    }
}

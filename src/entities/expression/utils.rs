use crate::num_integer::Integer;
use num_bigint::BigInt;

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::traits::{cast_norm, PNormalized};
use crate::entities::expression::var::Var;
use crate::entities::expression::{
    cast_expr, BinaryOperation, ExpressionType, Operator, PExpression, Root,
};
use crate::entities::solver::Equation;

pub fn contain_var(var_name: &str, expr: &PNormalized) -> bool {
    if let Some(var) = cast_norm::<Var>(expr) {
        return var.name == var_name;
    }
    for child in expr.norm_children() {
        if contain_var(var_name, &child) {
            return true;
        }
    }
    false
}

pub fn contain_root(expr: &PNormalized) -> bool {
    if expr.get_type() == ExpressionType::Root {
        return true;
    }
    for child in expr.norm_children() {
        if contain_root(&child) {
            return true;
        }
    }
    false
}

pub fn get_lcm_of_roots(items: &[PNormalized]) -> BigInt {
    let mut result: BigInt = (1).into();
    for item in items.iter() {
        let tp = item.get_type();
        assert!(matches!(tp, ExpressionType::Var | ExpressionType::Root));
        if let Some(root) = cast_norm::<Root>(item) {
            result = root.root.lcm(&result);
        }
    }
    result
}

pub fn expr_to_equation(expr: &PExpression) -> SolverResult<Equation<PExpression>> {
    let call = match cast_expr::<BinaryOperation>(expr) {
        Some(val) => val,
        _ => return Err(SolverError::ExpressionIsNotEquation),
    };
    if call.operator != Operator::Eql {
        return Err(SolverError::ExpressionIsNotEquation);
    }
    Ok(Equation {
        left: call.left.clone(),
        right: call.right.clone(),
    })
}

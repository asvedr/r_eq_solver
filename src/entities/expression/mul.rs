use std::any::Any;
use std::fmt::Write;
use std::rc::Rc;

use num_bigint::Sign;

use crate::entities::errors::SolverResult;
use crate::entities::expression::div::Div;
use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::traits::{
    cast_expr, cast_norm, Constants, ICoefficiented, IExponented, IExpression,
    IMultiplier, INormalized, PExpression, PNormalized,
};
use crate::number::Number;

#[derive(Clone)]
pub struct Mul {
    pub coefficient: Number,
    pub items: Vec<PNormalized>,
}

impl Mul {
    pub fn new(coefficient: Number, items: Vec<PNormalized>) -> Mul {
        Mul { coefficient, items }
    }
    pub fn two(a: PNormalized, b: PNormalized) -> Mul {
        Mul {
            coefficient: Number::integer(1),
            items: vec![a, b],
        }
    }
}

impl PartialEq for Mul {
    fn eq(&self, other: &Mul) -> bool {
        if self.coefficient != other.coefficient || self.items.len() != other.items.len()
        {
            return false;
        }
        for i in 0..self.items.len() {
            if !self.items[i].cmp_norm(&other.items[i]) {
                return false;
            }
        }
        true
    }
}

impl IExpression for Mul {
    fn to_string(&self) -> String {
        let mut result = format!("MUL({})[", self.coefficient);
        for item in self.items.iter() {
            write!(result, "{},", item.to_string());
        }
        result.push(']');
        result
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Mul
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<Mul>(other).unwrap();
        self == other
    }
    fn as_norm(&self) -> PNormalized {
        Rc::new(self.clone())
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
    fn children(&self) -> Vec<PExpression> {
        self.items.iter().map(|x| x.as_expr()).collect()
    }
}

impl ICoefficiented for Mul {
    fn get_coefficient(&self) -> Option<&Number> {
        Some(&self.coefficient)
    }
    fn set_coefficient(&self, coefficient: Number) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = coefficient;
        Rc::new(copy)
    }
}

impl IExponented for Mul {}
impl IMultiplier for Mul {}

impl INormalized for Mul {
    fn apply_neg(&self) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = -copy.coefficient;
        Rc::new(copy)
    }
    fn apply_div(&self, cons: &Constants) -> SolverResult<PNormalized> {
        let div = Div::new(cons.norm_one.clone(), self.as_norm());
        Ok(Rc::new(div))
    }
    fn apply_mul_num(&self, number: &Number) -> PNormalized {
        let mut copy = self.clone();
        copy.coefficient = &self.coefficient * number;
        Rc::new(copy)
    }
    fn format(&self) -> String {
        if self.items.is_empty() {
            return format!("MUL({})", self.coefficient);
        }
        let mut result = "(".to_string();
        if !self.coefficient.eq_int(1) {
            write!(result, "{} * ", self.coefficient);
        }
        write!(result, "{}", self.items[0].format());
        for item in self.items.iter().skip(1) {
            write!(result, " * {}", item.format());
        }
        result.push(')');
        result
    }
    fn _is_eql_norm(&self, other: &PNormalized) -> bool {
        let other = cast_norm::<Mul>(other).unwrap();
        self == other
    }
    fn as_div(&self) -> Option<(PNormalized, PNormalized)> {
        let mut top = Vec::new();
        let mut bottom = Vec::new();
        for item in self.items.iter() {
            match item.get_exponent() {
                Some(exp) if matches!(exp.sign(), Sign::Minus) => {
                    bottom.push(item.set_exponent(-exp))
                }
                _ => top.push(item.clone()),
            }
        }
        if bottom.is_empty() {
            return None;
        }
        let mul_top = Rc::new(Mul::new(self.coefficient.clone(), top));
        let mul_bottom = Rc::new(Mul::new(Number::integer(1), bottom));
        Some((mul_top, mul_bottom))
    }
    fn norm_children(&self) -> Vec<PNormalized> {
        self.items.clone()
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::entities::expression::var::Var;

    #[test]
    fn test_stringify_mul() {
        let a = Var::new(1, Number::integer(1), "a".to_string());
        let b = Var::new(2, Number::integer(3), "b".to_string());
        let mul = Mul::new(Number::integer(1), vec![]);
        assert_eq!(mul.to_string(), "MUL(1)[]");
        assert_eq!(mul.format(), "MUL(1)");
        let mul = Mul::new(Number::integer(2), vec![a.as_norm(), b.as_norm()]);
        assert_eq!(mul.to_string(), "MUL(2)[1a^1,3b^2,]");
        assert_eq!(mul.format(), "(2 * a * 3b^2)");
    }
}

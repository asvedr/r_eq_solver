use std::any::Any;
use std::rc::Rc;

use crate::entities::expression::expression_type::ExpressionType;
use crate::entities::expression::operator::Operator;
use crate::entities::expression::traits::{cast_expr, IExpression, PExpression};

#[derive(Clone)]
pub struct BinaryOperation {
    pub operator: Operator,
    pub left: PExpression,
    pub right: PExpression,
}

impl BinaryOperation {
    pub fn new(
        operator: Operator,
        left: PExpression,
        right: PExpression,
    ) -> BinaryOperation {
        BinaryOperation {
            operator,
            left,
            right,
        }
    }
}

impl IExpression for BinaryOperation {
    fn to_string(&self) -> String {
        format!(
            "({} {:?} {})",
            self.left.to_string(),
            self.operator,
            self.right.to_string(),
        )
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_type(&self) -> ExpressionType {
        ExpressionType::BinaryOperation
    }
    fn children(&self) -> Vec<PExpression> {
        vec![self.left.clone(), self.right.clone()]
    }
    fn _is_eql_expr(&self, other: &PExpression) -> bool {
        let other = cast_expr::<BinaryOperation>(&other).unwrap();
        self.operator == other.operator
            && self.left.cmp_expr(&other.left)
            && self.right.cmp_expr(&other.right)
    }
    fn as_expr(&self) -> PExpression {
        Rc::new(self.clone())
    }
}

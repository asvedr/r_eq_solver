use std::fmt;

use crate::entities::config::Config;
use crate::entities::expression::PExpression;

#[derive(Debug)]
pub enum Command {
    Help(String),
    Simplify(Simplify),
    SolveEquation(SolveEquation),
    SolveSystem(SolveSystem),
}

pub struct SolveEquation {
    pub expression: PExpression,
    pub config: Config,
}

pub struct SolveSystem {
    pub expressions: Vec<PExpression>,
    pub config: Config,
}

pub struct Simplify {
    pub expression: PExpression,
}

impl fmt::Debug for SolveEquation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Solve(\"{}\", {:?})",
            self.expression.to_string(),
            self.config,
        )
    }
}

impl fmt::Debug for SolveSystem {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let expressions = self
            .expressions
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<_>>();
        write!(f, "SolveSystem({:?}, {:?})", expressions, self.config)
    }
}

impl fmt::Debug for Simplify {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Simplify(\"{}\")", self.expression.to_string(),)
    }
}

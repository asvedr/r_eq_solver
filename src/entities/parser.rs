#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TokenType {
    Var,
    Number,
    Operator,
    OpenedBracket,
    ClosedBracket,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum State {
    Final(usize),
    NotFinal(usize),
    Error,
}

#[derive(Debug, Clone)]
pub struct Token {
    pub lexem: String,
    pub tp: TokenType,
}

impl Token {
    pub fn new(lexem: String, tp: TokenType) -> Token {
        Token { lexem, tp }
    }
}

impl State {
    pub fn is_fin(&self) -> bool {
        matches!(self, State::Final(_))
    }
    pub fn is_err(&self) -> bool {
        matches!(self, State::Error)
    }
    pub fn index(&self) -> usize {
        match self {
            State::Final(n) => *n,
            State::NotFinal(n) => *n,
            _ => unreachable!(),
        }
    }
}

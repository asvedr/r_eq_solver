use std::cell::RefCell;

use crate::proto::normalizer::IEventLogger;

struct State {
    level: usize,
    log: Vec<(usize, String)>,
}

pub struct EventLogger {
    state: RefCell<State>,
}

impl EventLogger {
    pub fn new() -> EventLogger {
        EventLogger {
            state: RefCell::new(State {
                level: 0,
                log: vec![],
            }),
        }
    }
}

fn make_space(count: usize) -> String {
    let mut space = String::new();
    for _ in 0..count {
        space.push_str("  ");
    }
    space
}

impl IEventLogger for EventLogger {
    fn inc(&self) {
        self.state.borrow_mut().level += 1;
    }
    fn dec(&self) {
        self.state.borrow_mut().level -= 1;
    }
    fn log_event(&self, event: String) {
        let mut state = self.state.borrow_mut();
        let level = state.level;
        state.log.push((level, event));
    }
    fn get_log(&self) -> Vec<(usize, String)> {
        self.state.borrow().log.clone()
    }
    fn format_log(&self) -> String {
        let mut result = String::new();
        for (level, event) in self.state.borrow().log.iter() {
            result.push_str(&make_space(*level));
            result.push_str(event);
            result.push('\n');
        }
        result
    }
    fn last_level(&self) -> Option<usize> {
        if let Some((ref val, _)) = self.state.borrow().log.last() {
            Some(*val)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_logger() {
        let logger = EventLogger::new();
        logger.log_event(format!("aaa"));
        logger.inc();
        logger.log_event(format!("bbb"));
        logger.dec();
        logger.log_event(format!("ccc"));
        assert_eq!(logger.format_log(), "aaa\n  bbb\nccc\n");
    }
}

use crate::entities::expression::{BinaryOperation, IExpression, Operator, Root, Var};
use crate::normalizer::{
    binary_reverser, event_logger, normalizer::Normalizer, rules, shortifiers,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

fn create() -> Normalizer {
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        Some(Box::new(event_logger::EventLogger::new())),
    )
}

#[test]
fn test_pow_zero() {
    /*
        (1/(x - 1)) ^ (x - x) => 1
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let one = Number::integer(1).as_expr();
    let expr = BinaryOperation::new(
        Operator::Pow,
        BinaryOperation::new(
            Operator::Div,
            one.clone(),
            BinaryOperation::new(Operator::Sub, x.clone(), one.clone()).as_expr(),
        )
        .as_expr(),
        BinaryOperation::new(Operator::Sub, x.clone(), x.clone()).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "1");
}

#[test]
fn test_pow_one() {
    /*
        (1/(x - 1)) ^ (x / x) => (1/(x - 1))
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let one = Number::integer(1).as_expr();
    let expr = BinaryOperation::new(
        Operator::Pow,
        BinaryOperation::new(
            Operator::Div,
            one.clone(),
            BinaryOperation::new(Operator::Sub, x.clone(), one.clone()).as_expr(),
        )
        .as_expr(),
        BinaryOperation::new(Operator::Div, x.clone(), x.clone()).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "DIV[1,SUM[1x^1,-1,]]");
}

#[test]
fn test_pow_two() {
    /*
        (x + 3)^(2x / x) => x^2 + 6x + 9
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let thr = Number::integer(3).as_expr();
    let expr = BinaryOperation::new(
        Operator::Pow,
        BinaryOperation::new(Operator::Add, x.clone(), thr).as_expr(),
        BinaryOperation::new(
            Operator::Div,
            BinaryOperation::new(Operator::Add, x.clone(), x.clone()).as_expr(),
            x.clone(),
        )
        .as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "SUM[6x^1,1x^2,9,]");
}

#[test]
fn zero_pow_x() {
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let expr =
        BinaryOperation::new(Operator::Pow, Number::integer(0).as_expr(), x).as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "0");
}

#[test]
fn one_pow_x() {
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let expr =
        BinaryOperation::new(Operator::Pow, Number::integer(1).as_expr(), x).as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "1");
}

#[test]
fn test_pow_num_num() {
    // 0.5
    let right = BinaryOperation::new(
        Operator::Div,
        Var::new(1, Number::fraction(1, 2), format!("x")).as_expr(),
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
    )
    .as_expr();
    // 9
    let left = BinaryOperation::new(
        Operator::Sub,
        Number::integer(10).as_expr(),
        Number::integer(1).as_expr(),
    )
    .as_expr();
    let expr = BinaryOperation::new(Operator::Pow, left, right).as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "3");
}

#[test]
fn test_pow_mul_num() {
    // ((x/2) * (4y)) ^ 3 => 8 * x^3 * y^3
    let left = BinaryOperation::new(
        Operator::Mul,
        Var::new(1, Number::fraction(1, 2), "x".to_string()).as_expr(),
        Var::new(1, Number::integer(4), "y".to_string()).as_expr(),
    );
    let right = Number::integer(3);
    let expr = BinaryOperation::new(Operator::Pow, left.as_expr(), right.as_expr());
    let normalizer = create();
    let result = normalizer.normalize(&expr.as_expr()).unwrap();
    assert_eq!(result.format(), "(8 * x^3 * y^3)");
}

#[test]
fn test_pow_root_num() {
    // (2*sqrt(x)) ^ 3 => 8 * x^(3/2)
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let expr = BinaryOperation::new(
        Operator::Pow,
        Root::new(1, 2, Number::integer(2), x.as_norm()).as_expr(),
        Number::integer(3).as_expr(),
    );
    let normalizer = create();
    let result = normalizer.normalize(&expr.as_expr()).unwrap();
    assert_eq!(result.format(), "8*[x]^(3/2)");
}

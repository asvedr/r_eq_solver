use crate::entities::expression::{
    BinaryOperation, IExpression, Operator, Sum, UnaryOperation, Var,
};
use crate::normalizer::{
    binary_reverser, event_logger, normalizer::Normalizer, rules, shortifiers,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

fn create() -> Normalizer {
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        Some(Box::new(event_logger::EventLogger::new())),
    )
}

#[test]
fn test_var_plus_num() {
    // x^2 + 2
    let var = Var::new(2, Number::integer(1), format!("x"));
    let num = Number::integer(2);
    let expr =
        BinaryOperation::new(Operator::Add, var.as_expr(), num.as_expr()).as_expr();
    let normalizer = create();
    let value = normalizer.normalize(&expr).unwrap();
    let expected = Sum {
        items: vec![var.as_norm(), num.as_norm()],
    }
    .as_expr();
    assert!(value.cmp_expr(&expected));
}

#[test]
fn test_shortify_div_of_sums_to_one() {
    /*
        (x^2 + 2 - 1) / (1 + 2x^2 - x^2) => (x^2 + 1) / (x^2 + 1) => 1
    */
    let expr = BinaryOperation::new(
        Operator::Div,
        // (x^2 + 2 - 1)
        BinaryOperation::new(
            Operator::Sub,
            // x^2 + 2
            BinaryOperation::new(
                Operator::Add,
                Var::new(2, Number::integer(1), format!("x")).as_expr(),
                Number::integer(2).as_expr(),
            )
            .as_expr(),
            Number::integer(1).as_expr(),
        )
        .as_expr(),
        // (1 + 2x^2 - x^2)
        BinaryOperation::new(
            Operator::Sub,
            // 1 + 2x^2
            BinaryOperation::new(
                Operator::Add,
                Number::integer(1).as_expr(),
                Var::new(2, Number::integer(2), format!("x")).as_expr(),
            )
            .as_expr(),
            Var::new(2, Number::integer(1), format!("x")).as_expr(),
        )
        .as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert!(result.cmp_expr(&normalizer.constants().expr_one));
}

#[test]
fn test_sum_of_same_divs() {
    /*
        x + 2     x       2x + 2
        ----- + ----- => --------
        x - 2   x - 2      x - 2
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let two = Number::integer(2).as_expr();
    let expr = BinaryOperation::new(
        Operator::Add,
        // (x + 2) / (x - 2)
        BinaryOperation::new(
            Operator::Div,
            BinaryOperation::new(Operator::Add, x.clone(), two.clone()).as_expr(),
            BinaryOperation::new(Operator::Sub, x.clone(), two.clone()).as_expr(),
        )
        .as_expr(),
        // x / (x - 2)
        BinaryOperation::new(
            Operator::Div,
            // 1 + 2x^2
            x.clone(),
            BinaryOperation::new(Operator::Sub, x.clone(), two.clone()).as_expr(),
        )
        .as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "DIV[SUM[2x^1,2,],SUM[1x^1,-2,]]")
}

#[test]
fn test_divs_plus_inv_var() {
    /*
         x + 2      1           x^3 + 2x^2 + x - 2          2x^3 + x - 2
        ------- + ----- + 1 => -------------------- + 1 => --------------
         x - 2     x^2              x^3 - 2x^2               x^3 - 2x^2
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let x2 = Var::new(2, Number::integer(1), format!("x")).as_expr();
    let one = Number::integer(1).as_expr();
    let two = Number::integer(2).as_expr();
    let first_div = BinaryOperation::new(
        Operator::Div,
        BinaryOperation::new(Operator::Add, x.clone(), two.clone()).as_expr(),
        BinaryOperation::new(Operator::Sub, x.clone(), two.clone()).as_expr(),
    )
    .as_expr();
    let second_div = BinaryOperation::new(Operator::Div, one.clone(), x2).as_expr();
    let expr = BinaryOperation::new(
        Operator::Add,
        BinaryOperation::new(Operator::Add, first_div, second_div).as_expr(),
        one,
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(
        result.to_string(),
        "DIV[SUM[1x^1,2x^3,-2,],SUM[-2x^2,1x^3,]]"
    );
}

#[test]
fn test_sum_of_different_divs() {
    /*
         x + 2     x - 2      2x^2 + 8
        ------- + ------- => ----------
         x - 2     x + 2      x^2 - 4
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let two = Number::integer(2).as_expr();
    let sum = BinaryOperation::new(Operator::Add, x.clone(), two.clone()).as_expr();
    let sub = BinaryOperation::new(Operator::Sub, x.clone(), two.clone()).as_expr();
    let expr = BinaryOperation::new(
        Operator::Add,
        BinaryOperation::new(Operator::Div, sum.clone(), sub.clone()).as_expr(),
        BinaryOperation::new(Operator::Div, sub.clone(), sum.clone()).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "DIV[SUM[2x^2,8,],SUM[1x^2,-4,]]");
}

#[test]
fn test_plus_minus_one() {
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let two = Number::integer(2).as_expr();
    let expr = BinaryOperation::new(
        Operator::Add,
        BinaryOperation::new(Operator::Add, x, two.clone()).as_expr(),
        UnaryOperation::new(Operator::Neg, two.clone()).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "1x^1");
}

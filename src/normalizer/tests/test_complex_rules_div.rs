use crate::entities::expression::{
    BinaryOperation, ICoefficiented, IExpression, Mul, Operator, Var,
};
use crate::normalizer::{
    binary_reverser, event_logger, normalizer::Normalizer, rules, shortifiers,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

fn create() -> Normalizer {
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        Some(Box::new(event_logger::EventLogger::new())),
    )
}

#[test]
fn test_div_sum_to_num() {
    let expr = BinaryOperation::new(
        Operator::Div,
        BinaryOperation::new(
            Operator::Add,
            Var::new(1, Number::integer(4), format!("x")).as_expr(),
            Number::integer(4).as_expr(),
        )
        .as_expr(),
        Number::integer(2).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "SUM[2x^1,2,]");
}

#[test]
fn test_div_div_to_div() {
    let sum = BinaryOperation::new(
        Operator::Add,
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
        Number::integer(1).as_expr(),
    )
    .as_expr();
    let sub = BinaryOperation::new(
        Operator::Sub,
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
        Number::integer(1).as_expr(),
    )
    .as_expr();
    let expr = BinaryOperation::new(
        Operator::Div,
        BinaryOperation::new(Operator::Div, sum.clone(), sub.clone()).as_expr(),
        BinaryOperation::new(Operator::Div, sub.clone(), sum.clone()).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(
        result.to_string(),
        "DIV[SUM[2x^1,1x^2,1,],SUM[-2x^1,1x^2,1,]]"
    );
}

#[test]
fn test_div_div_to_var_cut() {
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let sum =
        BinaryOperation::new(Operator::Add, x.clone(), Number::integer(1).as_expr())
            .as_expr();
    let expr = BinaryOperation::new(
        Operator::Div,
        BinaryOperation::new(Operator::Div, x.clone(), sum).as_expr(),
        x.clone(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "DIV[1,SUM[1x^1,1,]]");
}

#[test]
fn test_div_div_to_var_no_cut() {
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let sum =
        BinaryOperation::new(Operator::Add, x.clone(), Number::integer(1).as_expr())
            .as_expr();
    let expr = BinaryOperation::new(
        Operator::Div,
        BinaryOperation::new(Operator::Div, Number::integer(1).as_expr(), sum).as_expr(),
        x.clone(),
    )
    .as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.to_string(), "DIV[1,SUM[1x^1,1x^2,]]");
}

#[test]
fn test_div_shortifibly_sums() {
    /*
        2xy + 3y      2x + 3
        --------  => --------
         xy - y        x - 1
    */
    let x = Var::new(1, Number::integer(1), format!("x")).as_expr();
    let y = Var::new(1, Number::integer(1), format!("y")).as_expr();
    let mul = Mul::two(x.as_norm(), y.as_norm());
    let mul2 = mul.set_coefficient(Number::integer(2));
    let e3y =
        BinaryOperation::new(Operator::Mul, Number::integer(3).as_expr(), y.clone())
            .as_expr();
    let top = BinaryOperation::new(Operator::Add, mul2.as_expr(), e3y).as_expr();
    let bottom = BinaryOperation::new(Operator::Sub, mul.as_expr(), y).as_expr();
    let expr = BinaryOperation::new(Operator::Div, top, bottom).as_expr();
    let normalizer = create();
    let result = normalizer.normalize(&expr).unwrap();
    assert_eq!(result.format(), "((2x + 3) / (x + -1))");
}

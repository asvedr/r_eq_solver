use crate::entities::expression::{
    BinaryOperation, Div, ICoefficiented, IExponented, IExpression, Mul, Operator, Var,
};
use crate::normalizer::{
    binary_reverser, event_logger, normalizer::Normalizer, rules, shortifiers,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

fn create() -> Normalizer {
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        Some(Box::new(event_logger::EventLogger::new())),
    )
}

#[test]
fn test_mul_vars() {
    let normalizer = create();
    let x = Var::new(1, Number::integer(2), "x".to_string());
    let y = Var::new(1, Number::integer(3), "y".to_string());
    let mul1 = BinaryOperation::new(Operator::Mul, x.as_expr(), y.as_expr());
    let mul2 = BinaryOperation::new(
        Operator::Mul,
        x.set_exponent((-1).into())
            .set_coefficient(Number::integer(1))
            .as_expr(),
        y.set_coefficient(Number::integer(2)).as_expr(),
    );
    let res_mul = BinaryOperation::new(Operator::Mul, mul1.as_expr(), mul2.as_expr());
    let val = normalizer.normalize(&mul1.as_expr()).unwrap();
    assert_eq!(val.format(), "(6 * x * y)");
    let val = normalizer.normalize(&mul2.as_expr()).unwrap();
    assert_eq!(val.format(), "(2 * x^-1 * y)");
    let val = normalizer.normalize(&res_mul.as_expr()).unwrap();
    assert_eq!(val.format(), "12y^2");
}

#[test]
fn test_shortify_mul_with_div_and_sum() {
    // 2x * 3y * (x + 1) => 6(x^2)y + 6xy
    let normalizer = create();
    let x = Var::new(1, Number::integer(2), "x".to_string());
    let y = Var::new(1, Number::integer(3), "y".to_string());
    let mul = BinaryOperation::new(Operator::Mul, x.as_expr(), y.as_expr());
    let sum = BinaryOperation::new(
        Operator::Add,
        x.set_coefficient(Number::integer(1)).as_expr(),
        Number::integer(1).as_expr(),
    );
    let res = BinaryOperation::new(Operator::Mul, mul.as_expr(), sum.as_expr());
    let val = normalizer.normalize(&res.as_expr()).unwrap();
    assert_eq!(val.format(), "((6 * x * y) + (6 * x^2 * y))");
}

#[test]
fn test_shortify_div_of_muls() {
    /*
                 2
        3 * x * --- => 6
                 x
    */
    let normalizer = create();
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let div = Div::new(Number::integer(2).as_norm(), x.as_norm());
    let mul = Mul::new(Number::integer(3), vec![x.as_norm(), div.as_norm()]);
    let val = normalizer.shortify(mul.as_norm()).unwrap();
    assert_eq!(val.format(), "6");
}

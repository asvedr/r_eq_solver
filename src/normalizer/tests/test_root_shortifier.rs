use crate::entities::expression::{Div, IExpression, Mul, Root, Sum, Var};
use crate::normalizer::{
    binary_reverser, event_logger, normalizer::Normalizer, rules, shortifiers,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

fn create() -> Normalizer {
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        Some(Box::new(event_logger::EventLogger::new())),
    )
}

#[test]
fn test_shortify_root_num() {
    let normalizer = create();
    let root = Root::new(2, 3, Number::integer(1), Number::integer(8).as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "4");
}

#[test]
fn test_shortify_var_odd_exp_no_short() {
    let normalizer = create();
    let var = Var::new(3, Number::integer(4), "x".to_string());
    let root = Root::new(1, 2, Number::integer(3), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "6*[x]^(3/2)");
}

#[test]
fn test_shortify_var_odd_exp_short() {
    let normalizer = create();
    let var = Var::new(6, Number::integer(8), "x".to_string());
    let root = Root::new(1, 3, Number::integer(3), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "6x^2");
}

#[test]
fn test_shortify_var_even_exp_no_short() {
    let normalizer = create();

    let var = Var::new(2, Number::integer(4), "x".to_string());
    let root = Root::new(1, 2, Number::integer(1), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "2*[x^2]^(1/2)");

    let var = Var::new(1, Number::integer(4), "x".to_string());
    let root = Root::new(1, 2, Number::integer(1), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "2*[x]^(1/2)");

    let var = Var::new(6, Number::integer(4), "x".to_string());
    let root = Root::new(1, 2, Number::integer(1), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "2*[x^2]^(3/2)");
}

#[test]
fn test_shortify_var_even_exp_short() {
    let normalizer = create();
    let var = Var::new(4, Number::integer(4), "x".to_string());
    let root = Root::new(1, 2, Number::integer(1), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "2x^2");
}

#[test]
fn test_shortify_inv_var() {
    let normalizer = create();
    let var = Var::new(-1, Number::integer(4), "x".to_string());
    let root = Root::new(1, 2, Number::integer(1), var.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "(2 / [x]^(1/2))");
}

#[test]
fn test_shortify_div() {
    let normalizer = create();
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let y = Var::new(1, Number::integer(1), "y".to_string());
    let one = Number::integer(-1);
    let sum = Sum::new(vec![y.as_norm(), one.as_norm()]);
    let div = Div::new(x.as_norm(), sum.as_norm());
    let root = Root::new(1, 2, Number::integer(1), div.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "([x]^(1/2) / [(y + -1)]^(1/2))");
}

#[test]
fn test_shortify_sum() {
    let normalizer = create();
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let y = Var::new(1, Number::integer(1), "y".to_string());
    let sum = Sum::new(vec![x.as_norm(), y.as_norm()]);
    let root = Root::new(1, 2, Number::integer(1), sum.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "[(x + y)]^(1/2)");
}

#[test]
fn test_shortify_mul() {
    let normalizer = create();
    let x = Var::new(3, Number::integer(1), "x".to_string());
    let y = Var::new(1, Number::integer(1), "y".to_string());
    let mul = Mul::new(Number::integer(8), vec![x.as_norm(), y.as_norm()]);
    let root = Root::new(1, 3, Number::integer(3), mul.as_norm());
    let result = normalizer.shortify(root.as_norm()).unwrap();
    assert_eq!(result.format(), "(6 * [y]^(1/3) * x)");
}

#[test]
fn test_shortify_sub_root() {
    let normalizer = create();
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let sub_root = Root::new(1, 3, Number::integer(4), x.as_norm());
    let super_root = Root::new(1, 2, Number::integer(1), sub_root.as_norm());
    let result = normalizer.shortify(super_root.as_norm()).unwrap();
    assert_eq!(result.format(), "2*[x]^(1/6)");
}

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_norm, BinaryOperation, Constants, Div, ExpressionType, IExpression, Operator,
    PNormalized, Sum, UnaryOperation, Var,
};
use crate::normalizer::binary_reverser::BinaryReverser;
use crate::normalizer::normalizer::Normalizer;
use crate::normalizer::rules::num_ops::AddNumNum;
use crate::normalizer::rules::unary_neg::NormNeg;
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

struct MockAddSumNum {}
struct MockAddNumSum {}

impl INormalizerRuleBinary for MockAddSumNum {
    fn slug(&self) -> String {
        panic!()
    }

    fn priority(&self) -> usize {
        1
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        a == ExpressionType::Sum && b == ExpressionType::Number
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(Number::integer(1).as_norm())
    }
}

impl INormalizerRuleBinary for MockAddNumSum {
    fn slug(&self) -> String {
        panic!()
    }

    fn priority(&self) -> usize {
        2
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        a == ExpressionType::Number && b == ExpressionType::Sum
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        panic!("this should not be called")
    }
}

#[test]
fn test_normalize_primitive() {
    let prims = vec![
        Var::new(0, Number::integer(1), "x".to_string()).as_expr(),
        Sum::new(vec![]).as_expr(),
        Div::new(Number::integer(1).as_norm(), Number::integer(2).as_norm()).as_expr(),
    ];
    let normalizer = Normalizer::new(
        vec![Box::new(NormNeg {})],
        vec![Box::new(AddNumNum {})],
        Box::new(BinaryReverser {}),
        vec![],
        None,
    );
    for prim in prims {
        let value = normalizer.normalize(&prim).unwrap();
        assert!(value.cmp_expr(&prim));
    }
}

#[test]
fn test_normalize_num_sum() {
    let normalizer = Normalizer::new(
        vec![Box::new(NormNeg {})],
        vec![Box::new(AddNumNum {})],
        Box::new(BinaryReverser {}),
        vec![],
        None,
    );
    let expr = BinaryOperation::new(
        Operator::Add,
        UnaryOperation::new(Operator::Neg, Number::integer(1).as_expr()).as_expr(),
        BinaryOperation::new(
            Operator::Add,
            Number::integer(2).as_expr(),
            Number::integer(3).as_expr(),
        )
        .as_expr(),
    )
    .as_expr();
    let value = normalizer.normalize(&expr).unwrap();
    let num = cast_norm::<Number>(&value).unwrap();
    assert!(num.eq_int(4));
    // assert!(*num > Number::rational(3.9) && *num < Number::rational(4.1));
}

#[test]
fn test_reverse_operation() {
    let normalizer = Normalizer::new(
        vec![Box::new(NormNeg {})],
        vec![Box::new(MockAddNumSum {}), Box::new(MockAddSumNum {})],
        Box::new(BinaryReverser {}),
        vec![],
        None,
    );
    let a = BinaryOperation::new(
        Operator::Add,
        Number::integer(2).as_expr(),
        Sum { items: vec![] }.as_expr(),
    )
    .as_expr();
    let b = BinaryOperation::new(
        Operator::Add,
        Sum { items: vec![] }.as_expr(),
        Number::integer(2).as_expr(),
    )
    .as_expr();
    // Didn't panic on both functions
    let value_a = normalizer.normalize(&a).unwrap();
    let value_b = normalizer.normalize(&b).unwrap();
    assert!(value_a.cmp_norm(&value_b));
    let num = cast_norm::<Number>(&value_a).unwrap();
    assert!(num.eq_int(1));
}

#[test]
fn test_no_rule_found() {
    let normalizer = Normalizer::new(
        vec![Box::new(NormNeg {})],
        vec![Box::new(MockAddNumSum {}), Box::new(MockAddSumNum {})],
        Box::new(BinaryReverser {}),
        vec![],
        None,
    );
    let expr = BinaryOperation::new(
        Operator::Pow,
        Sum { items: vec![] }.as_expr(),
        Sum { items: vec![] }.as_expr(),
    )
    .as_expr();
    match normalizer.normalize(&expr) {
        Err(SolverError::CanNotFindRule(_)) => (),
        Err(err) => panic!("invalid err: {:?}", err),
        Ok(val) => panic!("unexpected value: {}", val.to_string()),
    }
}

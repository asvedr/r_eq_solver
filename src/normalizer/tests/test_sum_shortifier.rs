use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, BinaryOperation, Constants, Div, IExpression, PExpression, PNormalized,
    Sum, UnaryOperation, Var,
};
use crate::entities::normalizer::NormalizedBin;
use crate::normalizer::shortifiers::sum_shortifier::SumShortifier;
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, IShortifier};

struct MockNormalizer {
    c: Constants,
}

impl MockNormalizer {
    fn new() -> MockNormalizer {
        MockNormalizer {
            c: Constants {
                expr_one: Number::integer(1).as_expr(),
                expr_zero: Number::integer(0).as_expr(),
                norm_one: Number::integer(1).as_norm(),
                norm_zero: Number::integer(0).as_norm(),
            },
        }
    }
}

impl INormalizer for MockNormalizer {
    fn constants(&self) -> &Constants {
        &self.c
    }

    fn normalize(&self, _: &PExpression) -> SolverResult<PNormalized> {
        panic!()
    }

    fn normalize_n_binary(&self, a: &NormalizedBin) -> SolverResult<PNormalized> {
        Ok(Var::new(1, Number::integer(1), a.to_string()).as_norm())
    }

    fn normalize_binary(&self, _: &BinaryOperation) -> SolverResult<PNormalized> {
        panic!()
    }

    fn normalize_unary(&self, _: &UnaryOperation) -> SolverResult<PNormalized> {
        panic!()
    }

    fn shortify(&self, x: PNormalized) -> SolverResult<PNormalized> {
        Ok(x)
    }
}

#[test]
fn shortify_null() {
    let shortifier = SumShortifier::new();
    let normalizer = MockNormalizer::new();
    let sum = Sum::new(vec![]);
    let val = match shortifier.shortify(&normalizer, sum.as_norm()) {
        Ok(val) => val,
        Err(_) => panic!(),
    };
    let result = cast_norm::<Number>(&val).unwrap();
    assert!(result.eq_int(0));
}

#[test]
fn shortify_one() {
    let shortifier = SumShortifier::new();
    let normalizer = MockNormalizer::new();
    let x = Var::new(2, Number::integer(1), format!("x")).as_norm();
    let sum = Sum::new(vec![x.clone()]);
    let val = match shortifier.shortify(&normalizer, sum.as_norm()) {
        Ok(val) => val,
        Err(_) => panic!(),
    };
    assert!(val.cmp_norm(&x));

    let supersum = Sum {
        items: vec![sum.as_norm()],
    };
    let val = shortifier
        .shortify(&normalizer, supersum.as_norm())
        .unwrap();
    assert!(val.cmp_norm(&x));
}

#[test]
fn shortify_vars_and_num() {
    let shortifier = SumShortifier::new();
    let normalizer = MockNormalizer::new();
    let sum = Sum::new(vec![
        Var::new(2, Number::integer(1), format!("x")).as_norm(),
        Var::new(1, Number::integer(1), format!("x")).as_norm(),
        Number::integer(3).as_norm(),
        Var::new(2, Number::fraction(-3, 2), format!("x")).as_norm(),
        Var::new(1, Number::integer(2), format!("x")).as_norm(),
        Number::integer(1).as_norm(),
    ]);
    let expected = Sum::new(vec![
        Var::new(1, Number::integer(3), format!("x")).as_norm(),
        Var::new(2, Number::fraction(-1, 2), format!("x")).as_norm(),
        Number::integer(4).as_norm(),
    ])
    .as_expr();
    match shortifier.shortify(&normalizer, sum.as_norm()) {
        Ok(val) => {
            assert!(val.cmp_expr(&expected));
        }
        Err(_) => panic!(),
    }
}

#[test]
fn shortify_null_after_calculation() {
    let shortifier = SumShortifier::new();
    let normalizer = MockNormalizer::new();
    let sum = Sum {
        items: vec![
            Var::new(2, Number::integer(1), format!("x")).as_norm(),
            Var::new(2, Number::integer(-1), format!("x")).as_norm(),
            Number::integer(3).as_norm(),
            Number::integer(-3).as_norm(),
        ],
    };
    let val = match shortifier.shortify(&normalizer, sum.as_norm()) {
        Ok(val) => val,
        Err(_) => panic!(),
    };
    let result = cast_norm::<Number>(&val).unwrap();
    assert!(result.eq_int(0));
}

#[test]
fn shortify_sum_with_div() {
    let shortifier = SumShortifier::new();
    let normalizer = MockNormalizer::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let two = Number::integer(2).as_norm();
    let one = Number::integer(1).as_norm();
    let xp2 = Sum::new(vec![x.clone(), two.clone()]).as_norm();
    let xm2 = Sum::new(vec![x.clone(), two.apply_neg()]).as_norm();
    let div = Div::new(xp2, xm2).as_norm();
    let sum = Sum::new(vec![div, one]).as_norm();
    let result = shortifier.shortify(&normalizer, sum).unwrap();
    assert_eq!(result.format(), "(DIV[SUM[1x^1,2,],SUM[1x^1,-2,]] Add 1)");
}

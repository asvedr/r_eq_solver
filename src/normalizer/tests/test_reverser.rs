use crate::entities::expression::{IExpression, Operator};
use crate::entities::normalizer::NormalizedBin;
use crate::normalizer::binary_reverser::BinaryReverser;
use crate::number::Number;
use crate::proto::normalizer::IBinaryReverser;

#[test]
fn test_reverse_add() {
    let expr = NormalizedBin {
        operator: Operator::Add,
        left: Number::integer(1).as_norm(),
        right: Number::integer(2).as_norm(),
    };
    let reverser = BinaryReverser {};
    let reversed = reverser.reverse(&expr).unwrap();
    assert_eq!(reversed.operator, Operator::Add);
    assert!(reversed.left.cmp_norm(&expr.right));
    assert!(reversed.right.cmp_norm(&expr.left));
}

#[test]
fn test_reverse_sub() {
    let expr = NormalizedBin {
        operator: Operator::Sub,
        left: Number::integer(1).as_norm(),
        right: Number::integer(2).as_norm(),
    };
    let reverser = BinaryReverser {};
    assert!(reverser.reverse(&expr).is_none());
}

#[test]
fn test_reverse_mul() {
    let expr = NormalizedBin {
        operator: Operator::Mul,
        left: Number::integer(1).as_norm(),
        right: Number::integer(2).as_norm(),
    };
    let reverser = BinaryReverser {};
    let reversed = reverser.reverse(&expr).unwrap();
    assert_eq!(reversed.operator, Operator::Mul);
    assert!(reversed.left.cmp_norm(&expr.right));
    assert!(reversed.right.cmp_norm(&expr.left));
}

#[test]
fn test_reverse_pow() {
    let expr = NormalizedBin {
        operator: Operator::Pow,
        left: Number::integer(1).as_norm(),
        right: Number::integer(2).as_norm(),
    };
    let reverser = BinaryReverser {};
    assert!(reverser.reverse(&expr).is_none());
}

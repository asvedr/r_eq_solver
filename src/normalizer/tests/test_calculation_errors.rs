use crate::entities::errors::SolverError;
use crate::entities::expression::{BinaryOperation, IExpression, Operator, Var};
use crate::normalizer::{
    binary_reverser, event_logger, normalizer::Normalizer, rules, shortifiers,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

fn create() -> Normalizer {
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        Some(Box::new(event_logger::EventLogger::new())),
    )
}

#[test]
fn test_pow_var_to_frac() {
    let expr = BinaryOperation::new(
        Operator::Pow,
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
        Number::rational(0.5).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    match normalizer.normalize(&expr) {
        Err(SolverError::InvalidPow(_)) => (),
        _ => panic!(),
    }
}

#[test]
fn test_pow_num_to_var() {
    let expr = BinaryOperation::new(
        Operator::Pow,
        Number::integer(2).as_expr(),
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    match normalizer.normalize(&expr) {
        Err(SolverError::CanNotFindRule(_)) => (),
        Err(e) => panic!("invalid error: {:?}", e),
        Ok(val) => panic!("value: {}", val.to_string()),
    }
}

#[test]
fn test_pow_zero_min_one() {
    let expr = BinaryOperation::new(
        Operator::Pow,
        Number::integer(0).as_expr(),
        Number::integer(-1).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    match normalizer.normalize(&expr) {
        Err(SolverError::InvalidPow(_)) => (),
        Err(e) => panic!("invalid error: {:?}", e),
        Ok(val) => {
            println!("{}", normalizer.event_logger.unwrap().format_log());
            panic!("value: {}", val.to_string())
        }
    }
}

#[test]
fn test_div_var_zero() {
    let expr = BinaryOperation::new(
        Operator::Div,
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
        Number::integer(0).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    match normalizer.normalize(&expr) {
        Err(SolverError::NaNFound(_)) => (),
        Err(e) => panic!("invalid error: {:?}", e),
        Ok(val) => {
            println!("{}", normalizer.event_logger.unwrap().format_log());
            panic!("value: {}", val.to_string())
        }
    }
}

#[test]
fn test_div_num_zero() {
    let expr = BinaryOperation::new(
        Operator::Div,
        Number::integer(1).as_expr(),
        Number::integer(0).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    match normalizer.normalize(&expr) {
        Err(SolverError::NaNFound(_)) => (),
        Err(e) => panic!("invalid error: {:?}", e),
        Ok(val) => {
            println!("{}", normalizer.event_logger.unwrap().format_log());
            panic!("value: {}", val.to_string())
        }
    }
}

#[test]
fn test_div_zero_zero() {
    let expr = BinaryOperation::new(
        Operator::Div,
        Number::integer(0).as_expr(),
        Number::integer(0).as_expr(),
    )
    .as_expr();
    let normalizer = create();
    match normalizer.normalize(&expr) {
        Err(SolverError::NaNFound(_)) => (),
        Err(e) => panic!("invalid error: {:?}", e),
        Ok(val) => {
            println!("{}", normalizer.event_logger.unwrap().format_log());
            panic!("value: {}", val.to_string())
        }
    }
}

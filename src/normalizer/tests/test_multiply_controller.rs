use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    BinaryOperation, Constants, Div, IExpression, Mul, PExpression, PNormalized, Root,
    Sum, UnaryOperation, Var,
};
use crate::entities::normalizer::NormalizedBin;
use crate::normalizer::multiply_controller::MultiplyController;
use crate::number::Number;
use crate::proto::normalizer::{IMultiplyController, INormalizer};

struct MockNormalizer {
    c: Constants,
}

impl MockNormalizer {
    fn new() -> MockNormalizer {
        MockNormalizer {
            c: Constants {
                expr_one: Number::integer(1).as_expr(),
                expr_zero: Number::integer(0).as_expr(),
                norm_one: Number::integer(1).as_norm(),
                norm_zero: Number::integer(0).as_norm(),
            },
        }
    }
}

impl INormalizer for MockNormalizer {
    fn constants(&self) -> &Constants {
        &self.c
    }

    fn normalize(&self, _: &PExpression) -> SolverResult<PNormalized> {
        panic!()
    }

    fn normalize_n_binary(&self, expr: &NormalizedBin) -> SolverResult<PNormalized> {
        let sum = Sum {
            items: vec![expr.left.clone(), expr.right.clone()],
        };
        Ok(sum.as_norm())
    }

    fn normalize_binary(&self, _: &BinaryOperation) -> SolverResult<PNormalized> {
        panic!()
    }

    fn normalize_unary(&self, _: &UnaryOperation) -> SolverResult<PNormalized> {
        panic!()
    }

    fn shortify(&self, x: PNormalized) -> SolverResult<PNormalized> {
        Ok(x)
    }
}

#[test]
fn test_get_num_multipliers() {
    let ctrl = MultiplyController::new();
    let mults = ctrl.get_multipliers(&Number::integer(2).as_norm());
    assert!(mults.is_empty());
}

#[test]
fn test_get_var_multipliers() {
    let ctrl = MultiplyController::new();
    let var = Var::new(2, Number::integer(3), "x".to_string());
    let mults = ctrl.get_multipliers(&var.as_norm());
    assert_eq!(mults.data.len(), 1);
    let exp = mults.data.get("x").unwrap().get_exponent().unwrap();
    assert_eq!(*exp, (2).into());
}

#[test]
fn test_get_root_multipliers() {
    let ctrl = MultiplyController::new();
    let n = Number::integer(3).as_norm();
    let x = Var::new(2, Number::integer(3), "x".to_string()).as_norm();
    let y = Var::new(2, Number::integer(3), "x".to_string()).as_norm();

    let root_n_2 = Root::new(1, 2, Number::integer(1), n.clone()).as_norm();
    let root_n_3 = Root::new(1, 3, Number::integer(1), n.clone()).as_norm();
    let mul = Mul::two(x, y).as_norm();
    let root_v = Root::new(1, 2, Number::integer(1), mul).as_norm();

    let mults = ctrl.get_multipliers(&root_n_2);
    assert_eq!(mults.data.len(), 1);
    let exp = mults
        .data
        .get("ROOT(2)[3]")
        .unwrap()
        .get_exponent()
        .unwrap();
    assert_eq!(*exp, (1).into());
    assert_eq!(mults.min_multiplier_exp(&root_n_2), Some((1).into()));
    assert_eq!(mults.min_multiplier_exp(&root_n_3), None);

    let mults = ctrl.get_multipliers(&root_v);
    assert_eq!(mults.data.len(), 1);
    let exp = mults
        .data
        .get("ROOT(2)[MUL(1)[3x^2,3x^2,]]")
        .unwrap()
        .get_exponent()
        .unwrap();
    assert_eq!(*exp, (1).into());

    assert_eq!(mults.min_multiplier_exp(&root_v), Some((1).into()));
    assert_eq!(mults.min_multiplier_exp(&root_n_2), None);
}

#[test]
fn test_get_div_multipliers() {
    let ctrl = MultiplyController::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let y = Var::new(1, Number::integer(1), "y".to_string()).as_norm();
    let div = Div::new(x.clone(), y.clone()).as_norm();
    let mults = ctrl.get_multipliers(&div);
    assert_eq!(mults.min_multiplier_exp(&x), Some((1).into()));
    assert_eq!(mults.min_multiplier_exp(&y), None);
}

#[test]
fn test_get_mul_multipliers() {
    let ctrl = MultiplyController::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let y = Var::new(1, Number::integer(1), "y".to_string()).as_norm();
    let mul = Mul::two(x.clone(), y.clone()).as_norm();
    let mults = ctrl.get_multipliers(&mul);
    assert_eq!(mults.data.len(), 2);
    assert_eq!(mults.min_multiplier_exp(&x), Some((1).into()));
    assert_eq!(mults.min_multiplier_exp(&y), Some((1).into()));
}

#[test]
fn test_get_sum_multipliers_ok() {
    let ctrl = MultiplyController::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let y = Var::new(1, Number::integer(1), "y".to_string()).as_norm();
    let mul = Mul::two(x.clone(), y.clone()).as_norm();
    let sum = Sum::new(vec![mul, y.clone()]).as_norm();
    let mults = ctrl.get_multipliers(&sum);
    assert_eq!(mults.data.len(), 1);
    assert_eq!(mults.min_multiplier_exp(&x), None);
    assert_eq!(mults.min_multiplier_exp(&y), Some((1).into()));
}

#[test]
fn test_get_sum_multipliers_none() {
    let ctrl = MultiplyController::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let y = Var::new(1, Number::integer(1), "y".to_string()).as_norm();
    let mul = Mul::two(x.clone(), y.clone()).as_norm();
    let sum = Sum::new(vec![mul, y.clone(), Number::integer(1).as_norm()]).as_norm();
    let mults = ctrl.get_multipliers(&sum);
    assert_eq!(mults.data.len(), 0);
}

#[test]
fn test_shortify_multipliers_var() {
    let ctrl = MultiplyController::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let y = Var::new(1, Number::integer(1), "y".to_string()).as_norm();
    let mul = Mul::two(x.clone(), y.clone()).as_norm();

    let sum = Sum::new(vec![mul.clone(), y.clone()]).as_norm();
    let mults = ctrl.get_multipliers(&sum);
    let normalizer = MockNormalizer::new();
    let expr = ctrl.shortify(&normalizer, &sum, mults).unwrap();
    assert_eq!(expr.format(), "((x * 1) + 1)");

    let sum = Sum::new(vec![mul, y.set_exponent((3).into())]).as_norm();
    let mults = ctrl.get_multipliers(&sum);
    let normalizer = MockNormalizer::new();
    let expr = ctrl.shortify(&normalizer, &sum, mults).unwrap();
    assert_eq!(expr.format(), "((x * 1) + y^2)");
}

#[test]
fn test_shortify_multipliers_root() {
    let ctrl = MultiplyController::new();
    let x = Var::new(1, Number::integer(1), "x".to_string()).as_norm();
    let r = Root::new(1, 2, Number::integer(1), x.clone()).as_norm();
    let mul = Mul::two(r.clone(), r.clone()).as_norm();
    let div = Div::new(r.clone(), r.clone()).as_norm();
    let sum = Sum::new(vec![mul, div]).as_norm();

    let mults = ctrl.get_multipliers(&sum);
    let normalizer = MockNormalizer::new();
    let expr = ctrl.shortify(&normalizer, &sum, mults).unwrap();
    assert_eq!(expr.format(), "((1 * [x]^(1/2)) + (1 / [x]^(1/2)))");
}

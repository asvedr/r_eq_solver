use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    BinaryOperation, Constants, Div, IExpression, INormalized, PExpression, PNormalized,
    Sum, UnaryOperation, Var,
};
use crate::entities::normalizer::{Multipliers, NormalizedBin};
use crate::normalizer::shortifiers::div_shortifier::DivShortifier;
use crate::number::Number;
use crate::proto::normalizer::{IMultiplyController, INormalizer, IShortifier};

struct MockNormalizer {
    c: Constants,
}

struct MockMultiplyController {}

impl MockNormalizer {
    fn new() -> MockNormalizer {
        MockNormalizer {
            c: Constants {
                expr_one: Number::integer(1).as_expr(),
                expr_zero: Number::integer(0).as_expr(),
                norm_one: Number::integer(1).as_norm(),
                norm_zero: Number::integer(0).as_norm(),
            },
        }
    }
}

impl INormalizer for MockNormalizer {
    fn constants(&self) -> &Constants {
        &self.c
    }

    fn normalize(&self, _: &PExpression) -> SolverResult<PNormalized> {
        panic!()
    }

    fn normalize_n_binary(&self, expr: &NormalizedBin) -> SolverResult<PNormalized> {
        let sum = Sum {
            items: vec![expr.left.clone(), expr.right.clone()],
        };
        Ok(sum.as_norm())
    }

    fn normalize_binary(&self, _: &BinaryOperation) -> SolverResult<PNormalized> {
        panic!()
    }

    fn normalize_unary(&self, _: &UnaryOperation) -> SolverResult<PNormalized> {
        panic!()
    }

    fn shortify(&self, x: PNormalized) -> SolverResult<PNormalized> {
        Ok(x)
    }
}

impl IMultiplyController for MockMultiplyController {
    fn get_multipliers(&self, _: &PNormalized) -> Multipliers {
        Multipliers::new()
    }
    fn shortify(
        &self,
        _: &dyn INormalizer,
        expr: &PNormalized,
        _: Multipliers,
    ) -> SolverResult<PNormalized> {
        Ok(expr.clone())
    }
}

fn create() -> DivShortifier {
    DivShortifier::new(Box::new(MockMultiplyController {}))
}

#[test]
fn test_no_changes() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let div = Div {
        top: Var::new(5, Number::integer(1), format!("x")).as_norm(),
        bottom: Sum::new(vec![]).as_norm(),
    };
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    assert!(div.cmp_norm(&result));
}

#[test]
fn test_bottom_num() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let div = Div::new(
        Var::new(5, Number::integer(4), format!("x")).as_norm(),
        Number::integer(2).as_norm(),
    );
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    let expect = Var::new(5, Number::integer(2), format!("x")).as_expr();
    assert!(result.cmp_expr(&expect));
}

#[test]
fn test_bottom_var() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let var = Var::new(1, Number::integer(2), format!("x")).as_norm();
    let div = Div::new(Number::integer(1).as_norm(), var.clone());
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    assert_eq!(result.to_string(), "DIV[1,2x^1]");
}

#[test]
fn test_top_div() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let sum = Sum { items: vec![] }.as_norm();
    let div = Div::new(
        Div::new(Number::integer(1).as_norm(), Number::integer(2).as_norm()).as_norm(),
        sum.clone(),
    );
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    let expect = Div::new(
        Number::integer(1).as_norm(),
        Sum::new(vec![Number::integer(2).as_norm(), sum.clone()]).as_norm(),
    )
    .as_expr();
    assert!(result.cmp_expr(&expect));
}

#[test]
fn test_bottom_div() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let sum = Sum::new(vec![]).as_norm();
    let div = Div::new(
        sum.clone(),
        Div::new(Number::integer(1).as_norm(), Number::integer(2).as_norm()).as_norm(),
    );
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    let expect = Sum {
        items: vec![sum.clone(), Number::integer(2).as_norm()],
    }
    .as_expr();
    assert!(result.cmp_expr(&expect));
}

#[test]
fn test_both_are_divs() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let div = Div {
        top: Div {
            top: Number::integer(1).as_norm(),
            bottom: Number::integer(2).as_norm(),
        }
        .as_norm(),
        bottom: Div {
            top: Number::integer(3).as_norm(),
            bottom: Number::integer(4).as_norm(),
        }
        .as_norm(),
    };
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    let expect = Sum {
        items: vec![
            Div::new(Number::integer(1).as_norm(), Number::integer(2).as_norm())
                .as_norm(),
            Div::new(Number::integer(4).as_norm(), Number::integer(3).as_norm())
                .as_norm(),
        ],
    }
    .as_expr();
    assert!(result.cmp_expr(&expect));
}

#[test]
fn test_top_zero() {
    let shortifier = create();
    let normalizer = MockNormalizer::new();
    let div = Div::new(Number::integer(0).as_norm(), Sum::new(vec![]).as_norm());
    let result = shortifier.shortify(&normalizer, div.as_norm()).unwrap();
    let expect = Number::integer(0).as_expr();
    assert!(result.cmp_expr(&expect));
}

use crate::entities::expression::Operator;
use crate::entities::normalizer::NormalizedBin;
use crate::proto::normalizer::IBinaryReverser;

pub struct BinaryReverser {}

impl BinaryReverser {
    pub fn new() -> BinaryReverser {
        BinaryReverser {}
    }
}

impl IBinaryReverser for BinaryReverser {
    fn reverse(&self, expr: &NormalizedBin) -> Option<NormalizedBin> {
        let value = match expr.operator {
            Operator::Add => NormalizedBin {
                operator: Operator::Add,
                left: expr.right.clone(),
                right: expr.left.clone(),
            },
            Operator::Mul => NormalizedBin {
                operator: Operator::Mul,
                left: expr.right.clone(),
                right: expr.left.clone(),
            },
            _ => return None,
        };
        Some(value)
    }
}

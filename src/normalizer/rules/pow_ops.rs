use std::rc::Rc;

use num_bigint::{BigInt, Sign};
use num_traits::identities::{One, Zero};

use crate::entities::constants::{
    NORM_CONCRETE_PRIORITY, NORM_HIGH_PRIORITY, NORM_WIDE_PRIORITY,
};
use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, Mul, Operator, PNormalized,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};
use num_traits::ToPrimitive;

#[allow(non_camel_case_types)]
pub struct One_Any {}
#[allow(non_camel_case_types)]
pub struct VarRoot_Num {}
#[allow(non_camel_case_types)]
pub struct Mul_Num {}
#[allow(non_camel_case_types)]
pub struct Any_Num {}
#[allow(non_camel_case_types)]
pub struct Zero_Any {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![
        Box::new(One_Any {}),
        Box::new(VarRoot_Num {}),
        Box::new(Mul_Num {}),
        Box::new(Any_Num {}),
        Box::new(Zero_Any {}),
    ]
}

fn num_to_int(num: &Number) -> SolverResult<BigInt> {
    if let Some(value) = num.as_int() {
        Ok(value)
    } else {
        let msg = "exp must be integer".to_string();
        Err(SolverError::InvalidPow(msg))
    }
}

impl INormalizerRuleBinary for VarRoot_Num {
    fn slug(&self) -> String {
        "(var | root) ^ num".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        matches!(a, ExpressionType::Var | ExpressionType::Root)
            && matches!(b, ExpressionType::Number)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let wrapped_num = cast_norm(b).unwrap();
        let num = num_to_int(wrapped_num)?;
        if num.is_zero() {
            return Ok(normalizer.constants().norm_one.clone());
        }
        if num.is_one() {
            return Ok(a.clone());
        }
        let coeff = a.get_coefficient().unwrap().pow(wrapped_num);
        let exp = a.get_exponent().unwrap();
        Ok(a.set_exponent(exp * num).set_coefficient(coeff))
    }
}

impl INormalizerRuleBinary for One_Any {
    fn slug(&self) -> String {
        "1 ^ any".to_string()
    }

    fn priority(&self) -> usize {
        NORM_HIGH_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, cons: &Constants, a: &PNormalized, _: &PNormalized) -> bool {
        a.cmp_expr(&cons.expr_one)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(normalizer.constants().norm_one.clone())
    }
}

impl INormalizerRuleBinary for Zero_Any {
    fn slug(&self) -> String {
        "0 ^ any".to_string()
    }

    fn priority(&self) -> usize {
        NORM_HIGH_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, cons: &Constants, a: &PNormalized, _: &PNormalized) -> bool {
        a.cmp_expr(&cons.expr_zero)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(normalizer.constants().norm_zero.clone())
    }
}

impl INormalizerRuleBinary for Mul_Num {
    fn slug(&self) -> String {
        "mul ^ num".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Mul)
            && matches!(b.get_type(), ExpressionType::Number)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let mul = cast_norm::<Mul>(a).unwrap();
        let wrapped_num = cast_norm(b).unwrap();
        let num = num_to_int(wrapped_num)?;
        if num.is_zero() {
            return Ok(normalizer.constants().norm_one.clone());
        }
        if num.is_one() {
            return Ok(a.clone());
        }
        let coeff = mul.coefficient.pow(wrapped_num);
        let mut items = Vec::new();
        for item in mul.items.iter() {
            let expr = NormalizedBin::new(Operator::Pow, item.clone(), b.clone());
            let item = normalizer.normalize_n_binary(&expr)?;
            items.push(item);
        }
        Ok(Rc::new(Mul::new(coeff, items)))
    }
}

fn pow_norm_num(
    normalizer: &dyn INormalizer,
    a: &PNormalized,
    num: BigInt,
) -> SolverResult<PNormalized> {
    let mut result = a.clone();
    let limit = match num.to_isize() {
        Some(val) => val,
        _ => return Err(SolverError::InvalidPow(format!("any ^ {}", num))),
    };
    for _ in 1..limit {
        let expr = NormalizedBin::new(Operator::Mul, result.clone(), a.clone());
        result = normalizer.normalize_n_binary(&expr)?;
    }
    Ok(result)
}

impl INormalizerRuleBinary for Any_Num {
    fn slug(&self) -> String {
        "any ^ num".to_string()
    }

    fn priority(&self) -> usize {
        NORM_WIDE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, _: &Constants, _: &PNormalized, b: &PNormalized) -> bool {
        if let Some(num) = cast_norm::<Number>(b) {
            num.is_int()
        } else {
            false
        }
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let pow = num_to_int(cast_norm::<Number>(b).unwrap())?;
        if pow.is_zero() {
            return Ok(normalizer.constants().norm_one.clone());
        }
        if matches!(pow.sign(), Sign::Plus) {
            pow_norm_num(normalizer, a, pow)
        } else {
            let val = pow_norm_num(normalizer, a, -pow)?;
            val.apply_div(normalizer.constants())
        }
    }
}

use std::rc::Rc;

use crate::entities::constants::{NORM_NUM_SHORT_PRIORITY, NUM_NUM_PRIORITY};
use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, IExpression, Operator, PNormalized,
};
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

pub struct AddNumNum {}
pub struct MulNumNum {}
pub struct PowZeroNeg {}
pub struct PowNumNum {}

pub struct AddZero {}
pub struct MulZero {}
pub struct MulOne {}
pub struct DivZero {}
pub struct ZeroDiv {}
pub struct MulNum {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![
        Box::new(AddNumNum {}),
        Box::new(MulNumNum {}),
        Box::new(PowZeroNeg {}),
        Box::new(PowNumNum {}),
        Box::new(AddZero {}),
        Box::new(MulZero {}),
        Box::new(MulOne {}),
        Box::new(DivZero {}),
        Box::new(ZeroDiv {}),
        Box::new(MulNum {}),
    ]
}

#[inline(always)]
fn both_are_nums(a: &PNormalized, b: &PNormalized) -> bool {
    a.get_type() == ExpressionType::Number && b.get_type() == ExpressionType::Number
}

impl INormalizerRuleBinary for AddNumNum {
    fn slug(&self) -> String {
        "num + num".to_string()
    }

    fn priority(&self) -> usize {
        NUM_NUM_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        both_are_nums(a, b)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let norm =
            Rc::new(cast_norm::<Number>(a).unwrap() + cast_norm::<Number>(b).unwrap());
        Ok(norm)
    }
}

impl INormalizerRuleBinary for MulNumNum {
    fn slug(&self) -> String {
        "num * num".to_string()
    }

    fn priority(&self) -> usize {
        NUM_NUM_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        both_are_nums(a, b)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let norm =
            Rc::new(cast_norm::<Number>(a).unwrap() * cast_norm::<Number>(b).unwrap());
        Ok(norm)
    }
}

impl INormalizerRuleBinary for PowZeroNeg {
    fn slug(&self) -> String {
        "0 ^ -num".to_string()
    }

    fn priority(&self) -> usize {
        NUM_NUM_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, cons: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        if let Some(num) = cast_norm::<Number>(&b) {
            a.cmp_expr(&cons.expr_zero) && num.is_neg()
        } else {
            false
        }
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Err(SolverError::InvalidPow("0 ^ -num".to_string()))
    }
}

impl INormalizerRuleBinary for PowNumNum {
    fn slug(&self) -> String {
        "num ^ num".to_string()
    }

    fn priority(&self) -> usize {
        NUM_NUM_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Pow
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        both_are_nums(a, b)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let a = cast_norm::<Number>(a).unwrap();
        let b = cast_norm::<Number>(b).unwrap();
        if a.is_neg() && b.is_even_div() {
            let msg = format!("{} ^ {}", a, b);
            return Err(SolverError::NaNFound(msg));
        }
        let val = a.pow(b);
        if val.is_nan() {
            let msg = format!("{} ^ {}", a, b);
            return Err(SolverError::NaNFound(msg));
        }
        Ok(val.as_norm())
    }
}

impl INormalizerRuleBinary for AddZero {
    fn slug(&self) -> String {
        "any + 0".to_string()
    }

    fn priority(&self) -> usize {
        NORM_NUM_SHORT_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, cons: &Constants, _: &PNormalized, b: &PNormalized) -> bool {
        b.cmp_expr(&cons.expr_zero)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(a.clone())
    }
}

impl INormalizerRuleBinary for MulZero {
    fn slug(&self) -> String {
        "any * 0".to_string()
    }

    fn priority(&self) -> usize {
        NORM_NUM_SHORT_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, cons: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        a.cmp_expr(&cons.expr_zero) || b.cmp_expr(&cons.expr_zero)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(normalizer.constants().norm_zero.clone())
    }
}

impl INormalizerRuleBinary for MulOne {
    fn slug(&self) -> String {
        "any * 1".to_string()
    }

    fn priority(&self) -> usize {
        NORM_NUM_SHORT_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, cons: &Constants, _: &PNormalized, b: &PNormalized) -> bool {
        b.cmp_expr(&cons.expr_one)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(a.clone())
    }
}

impl INormalizerRuleBinary for DivZero {
    fn slug(&self) -> String {
        "any / 0".to_string()
    }

    fn priority(&self) -> usize {
        NORM_NUM_SHORT_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Div
    }

    fn match_args(&self, cons: &Constants, _: &PNormalized, b: &PNormalized) -> bool {
        b.cmp_expr(&cons.expr_zero)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let msg = format!("{} / 0", a.to_string());
        Err(SolverError::NaNFound(msg))
    }
}

impl INormalizerRuleBinary for ZeroDiv {
    fn slug(&self) -> String {
        "0 / any".to_string()
    }

    fn priority(&self) -> usize {
        NORM_NUM_SHORT_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Div
    }

    fn match_args(&self, cons: &Constants, a: &PNormalized, _: &PNormalized) -> bool {
        a.cmp_expr(&cons.expr_zero)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(normalizer.constants().norm_zero.clone())
    }
}

impl INormalizerRuleBinary for MulNum {
    fn slug(&self) -> String {
        "any * num".to_string()
    }

    fn priority(&self) -> usize {
        NORM_NUM_SHORT_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, _: &PNormalized, b: &PNormalized) -> bool {
        b.get_type() == ExpressionType::Number
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(a.apply_mul_num(cast_norm::<Number>(b).unwrap()))
    }
}

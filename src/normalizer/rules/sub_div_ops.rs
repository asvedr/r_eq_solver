use crate::entities::constants::{NORM_CONCRETE_PRIORITY, NORM_WIDE_PRIORITY};
use crate::entities::errors::SolverResult;
use crate::entities::expression::{Constants, Operator, PNormalized};
use crate::entities::normalizer::NormalizedBin;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

struct SubSame {}
struct SubOtherOther {}
struct DivSame {}
struct DivOtherOther {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![
        Box::new(SubSame {}),
        Box::new(DivSame {}),
        Box::new(SubOtherOther {}),
        Box::new(DivOtherOther {}),
    ]
}

impl INormalizerRuleBinary for SubSame {
    fn slug(&self) -> String {
        "x - x".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Sub
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        a.cmp_norm(&b)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(normalizer.constants().norm_zero.clone())
    }
}

impl INormalizerRuleBinary for DivSame {
    fn slug(&self) -> String {
        "x / x".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Div
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        a.cmp_norm(&b)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        _: &PNormalized,
        _: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(normalizer.constants().norm_one.clone())
    }
}

impl INormalizerRuleBinary for SubOtherOther {
    fn slug(&self) -> String {
        "any - any".to_string()
    }

    fn priority(&self) -> usize {
        NORM_WIDE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Sub
    }

    fn match_args(&self, _: &Constants, _: &PNormalized, _: &PNormalized) -> bool {
        true
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let expr = NormalizedBin::new(Operator::Add, a.clone(), b.apply_neg());
        normalizer.normalize_n_binary(&expr)
    }
}

impl INormalizerRuleBinary for DivOtherOther {
    fn slug(&self) -> String {
        "any / any".to_string()
    }

    fn priority(&self) -> usize {
        NORM_WIDE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Div
    }

    fn match_args(&self, _: &Constants, _: &PNormalized, _: &PNormalized) -> bool {
        true
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let cons = normalizer.constants();
        let expr = NormalizedBin::new(Operator::Mul, a.clone(), b.apply_div(cons)?);
        normalizer.normalize_n_binary(&expr)
    }
}

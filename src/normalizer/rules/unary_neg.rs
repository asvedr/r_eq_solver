use crate::entities::errors::SolverResult;
use crate::entities::expression::{Constants, Operator, PNormalized};
use crate::proto::normalizer::{INormalizer, INormalizerRuleUnary};

pub struct NormNeg {}

impl INormalizerRuleUnary for NormNeg {
    fn slug(&self) -> String {
        "- any".to_string()
    }

    fn operator(&self) -> Operator {
        Operator::Neg
    }

    fn match_arg(&self, _: &Constants, _: &PNormalized) -> bool {
        true
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        expr: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(expr.apply_neg())
    }
}

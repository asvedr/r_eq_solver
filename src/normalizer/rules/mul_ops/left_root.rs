use std::rc::Rc;

use crate::entities::constants::{NORM_CONCRETE_PRIORITY, NORM_HIGH_PRIORITY};
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, Mul, Operator, PNormalized, Root,
};
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Root_SameRoot {}
#[allow(non_camel_case_types)]
pub struct Root_DiffRoot {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Root_SameRoot {}), Box::new(Root_DiffRoot {})]
}

impl INormalizerRuleBinary for Root_SameRoot {
    fn slug(&self) -> String {
        "root * same root".to_string()
    }

    fn priority(&self) -> usize {
        NORM_HIGH_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        match (cast_norm::<Root>(a), cast_norm::<Root>(b)) {
            (Some(a), Some(b)) => a.root == b.root && a.expr.cmp_norm(&b.expr),
            _ => false,
        }
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let a = cast_norm::<Root>(a).unwrap();
        let b = cast_norm::<Root>(b).unwrap();
        let mut root = a.clone();
        root.exp = &a.exp + &b.exp;
        root.coefficient = &a.coefficient * &b.coefficient;
        Ok(Rc::new(root))
    }
}

impl INormalizerRuleBinary for Root_DiffRoot {
    fn slug(&self) -> String {
        "root * diff root".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Root)
            && matches!(b.get_type(), ExpressionType::Root)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(Rc::new(Mul::two(a.clone(), b.clone())))
    }
}

mod left_div;
mod left_mul;
mod left_root;
mod left_sum;
mod left_var;

use crate::proto::normalizer::INormalizerRuleBinary;

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    let mut rules = left_sum::rules();
    rules.append(&mut left_var::rules());
    rules.append(&mut left_div::rules());
    rules.append(&mut left_mul::rules());
    rules.append(&mut left_root::rules());
    rules
}

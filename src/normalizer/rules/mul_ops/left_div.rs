use std::rc::Rc;

use crate::entities::constants::NORM_CONCRETE_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, Div, ExpressionType, Operator, PNormalized,
};
use crate::entities::normalizer::NormalizedBin;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Div_MulSumRootVar {}
#[allow(non_camel_case_types)]
pub struct Div_Div {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Div_MulSumRootVar {}), Box::new(Div_Div {})]
}

impl INormalizerRuleBinary for Div_MulSumRootVar {
    fn slug(&self) -> String {
        "div * (mul | sum | root | var)".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Div)
            && matches!(
                b.get_type(),
                ExpressionType::Mul
                    | ExpressionType::Sum
                    | ExpressionType::Var
                    | ExpressionType::Root
            )
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let div = cast_norm::<Div>(a).unwrap();
        let expr = NormalizedBin::new(Operator::Mul, b.clone(), div.top.clone());
        let top = normalizer.normalize_n_binary(&expr)?;
        Ok(Rc::new(Div::new(top, div.bottom.clone())))
    }
}

impl INormalizerRuleBinary for Div_Div {
    fn slug(&self) -> String {
        "div * div".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Div)
            && matches!(b.get_type(), ExpressionType::Div)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let a = cast_norm::<Div>(a).unwrap();
        let b = cast_norm::<Div>(b).unwrap();
        let expr = NormalizedBin::new(Operator::Mul, a.top.clone(), b.top.clone());
        let top = normalizer.normalize_n_binary(&expr)?;
        let expr = NormalizedBin::new(Operator::Mul, a.bottom.clone(), b.bottom.clone());
        let bottom = normalizer.normalize_n_binary(&expr)?;
        Ok(Rc::new(Div::new(top, bottom)))
    }
}

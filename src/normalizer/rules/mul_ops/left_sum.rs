use std::rc::Rc;

use crate::entities::constants::NORM_CONCRETE_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, Operator, PNormalized, Sum,
};
use crate::entities::normalizer::NormalizedBin;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Sum_VarMulRoot {}
#[allow(non_camel_case_types)]
pub struct Sum_Sum {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Sum_VarMulRoot {}), Box::new(Sum_Sum {})]
}

impl INormalizerRuleBinary for Sum_VarMulRoot {
    fn slug(&self) -> String {
        "sum * (var | mul | root)".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        matches!(a, ExpressionType::Sum)
            && matches!(
                b,
                ExpressionType::Var | ExpressionType::Mul | ExpressionType::Root
            )
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let sum = cast_norm::<Sum>(a).unwrap();
        let mut items = Vec::new();
        for item in &sum.items {
            let expr = NormalizedBin::new(Operator::Mul, item.clone(), b.clone());
            let normalized = normalizer.normalize_n_binary(&expr)?;
            items.push(normalized);
        }
        Ok(Rc::new(Sum::new(items)))
    }
}

impl INormalizerRuleBinary for Sum_Sum {
    fn slug(&self) -> String {
        "sum * sum".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Sum)
            && matches!(b.get_type(), ExpressionType::Sum)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let sum = cast_norm::<Sum>(a).unwrap();
        let mut items = vec![];
        for item in sum.items.iter() {
            let expr = NormalizedBin::new(Operator::Mul, b.clone(), item.clone());
            let value = normalizer.normalize_n_binary(&expr)?;
            if let Some(item_sum) = cast_norm::<Sum>(&value) {
                items.extend_from_slice(&item_sum.items);
            } else {
                items.push(value)
            }
        }
        Ok(Rc::new(Sum::new(items)))
    }
}

use std::rc::Rc;

use crate::entities::constants::NORM_CONCRETE_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, Mul, Operator, PNormalized,
};
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Mul_VarRoot {}
#[allow(non_camel_case_types)]
pub struct Mul_Mul {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Mul_VarRoot {}), Box::new(Mul_Mul {})]
}

impl INormalizerRuleBinary for Mul_VarRoot {
    fn slug(&self) -> String {
        "mul * (root | var)".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Mul)
            && matches!(b.get_type(), ExpressionType::Var | ExpressionType::Root)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let mut mul = cast_norm::<Mul>(a).unwrap().clone();
        mul.items.push(b.clone());
        Ok(Rc::new(mul))
    }
}

impl INormalizerRuleBinary for Mul_Mul {
    fn slug(&self) -> String {
        "mul * mul".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Mul)
            && matches!(b.get_type(), ExpressionType::Mul)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let a = cast_norm::<Mul>(a).unwrap();
        let b = cast_norm::<Mul>(b).unwrap();
        let mut items = a.items.clone();
        items.extend_from_slice(&b.items);
        let coefficient = &a.coefficient * &b.coefficient;
        Ok(Rc::new(Mul::new(coefficient, items)))
    }
}

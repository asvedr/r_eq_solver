use std::rc::Rc;

use num_traits::identities::Zero;

use crate::entities::constants::{NORM_CONCRETE_PRIORITY, NORM_HIGH_PRIORITY};
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, Mul, Operator, PNormalized, Var,
};
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Var_SameVar {}
#[allow(non_camel_case_types)]
pub struct Var_DiffVarOrRoot {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Var_SameVar {}), Box::new(Var_DiffVarOrRoot {})]
}

impl INormalizerRuleBinary for Var_SameVar {
    fn slug(&self) -> String {
        "var * same var".to_string()
    }

    fn priority(&self) -> usize {
        NORM_HIGH_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        match (cast_norm::<Var>(a), cast_norm::<Var>(b)) {
            (Some(a), Some(b)) => a.name == b.name,
            _ => false,
        }
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let a = cast_norm::<Var>(a).unwrap();
        let b = cast_norm::<Var>(b).unwrap();
        let pow = &a.pow + &b.pow;
        let coefficient = &a.coefficient * &b.coefficient;
        if pow.is_zero() {
            return Ok(Rc::new(coefficient));
        }
        let mut var = a.clone();
        var.coefficient = coefficient;
        var.pow = pow;
        Ok(Rc::new(var))
    }
}

impl INormalizerRuleBinary for Var_DiffVarOrRoot {
    fn slug(&self) -> String {
        "var * (diff var | root)".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Mul
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        matches!(a.get_type(), ExpressionType::Var)
            && matches!(b.get_type(), ExpressionType::Var | ExpressionType::Root)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(Rc::new(Mul::two(a.clone(), b.clone())))
    }
}

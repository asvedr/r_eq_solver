pub mod add_ops;
pub mod mul_ops;
pub mod nan_ops;
pub mod num_ops;
pub mod pow_ops;
pub mod sub_div_ops;
pub mod unary_neg;

use crate::proto::normalizer::{INormalizerRuleBinary, INormalizerRuleUnary};

pub fn unary_rules() -> Vec<Box<dyn INormalizerRuleUnary>> {
    vec![Box::new(unary_neg::NormNeg {})]
}

pub fn binary_rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    let mut result = vec![];
    result.append(&mut nan_ops::rules());
    result.append(&mut num_ops::rules());
    result.append(&mut add_ops::rules());
    result.append(&mut sub_div_ops::rules());
    result.append(&mut mul_ops::rules());
    result.append(&mut pow_ops::rules());
    result
}

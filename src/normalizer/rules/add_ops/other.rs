use std::rc::Rc;

use crate::entities::constants::NORM_WIDE_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{Constants, Operator, PNormalized, Sum};
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Other_Other {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Other_Other {})]
}

impl INormalizerRuleBinary for Other_Other {
    fn slug(&self) -> String {
        "any + any".to_string()
    }

    fn priority(&self) -> usize {
        NORM_WIDE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, _: &PNormalized, _: &PNormalized) -> bool {
        true
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        Ok(Rc::new(Sum::new(vec![a.clone(), b.clone()])))
    }
}

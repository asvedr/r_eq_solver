mod left_div;
mod left_sum;
mod other;

use crate::proto::normalizer::INormalizerRuleBinary;

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    let mut rules = left_sum::rules();
    rules.append(&mut left_div::rules());
    rules.append(&mut other::rules());
    rules
}

use std::rc::Rc;

use crate::entities::constants::NORM_CONCRETE_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, Div, ExpressionType, Operator, PNormalized,
};
use crate::entities::normalizer::NormalizedBin;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Div_Div {}
#[allow(non_camel_case_types)]
pub struct Div_Any {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Div_Div {}), Box::new(Div_Any {})]
}

impl Div_Div {
    fn sum_same_divs(
        &self,
        normalizer: &dyn INormalizer,
        a: &Div,
        b: &Div,
    ) -> SolverResult<PNormalized> {
        let expr = NormalizedBin::new(Operator::Add, a.top.clone(), b.top.clone());
        let top = normalizer.normalize_n_binary(&expr)?;
        Ok(Rc::new(Div::new(top, a.bottom.clone())))
    }

    fn sum_different_divs(
        &self,
        normalizer: &dyn INormalizer,
        a: &Div,
        b: &Div,
    ) -> SolverResult<PNormalized> {
        let expr = NormalizedBin::new(Operator::Mul, a.bottom.clone(), b.bottom.clone());
        let bottom = normalizer.normalize_n_binary(&expr)?;
        let expr = NormalizedBin::new(Operator::Mul, a.top.clone(), b.bottom.clone());
        let top1 = normalizer.normalize_n_binary(&expr)?;
        let expr = NormalizedBin::new(Operator::Mul, b.top.clone(), a.bottom.clone());
        let top2 = normalizer.normalize_n_binary(&expr)?;
        let expr = NormalizedBin::new(Operator::Add, top1.clone(), top2.clone());
        let top = normalizer.normalize_n_binary(&expr)?;
        Ok(Rc::new(Div::new(top, bottom)))
    }

    fn apply_converted(
        &self,
        normalizer: &dyn INormalizer,
        a: &Div,
        b: &Div,
    ) -> SolverResult<PNormalized> {
        if a.bottom.cmp_norm(&b.bottom) {
            self.sum_same_divs(normalizer, a, b)
        } else {
            self.sum_different_divs(normalizer, a, b)
        }
    }
}

impl INormalizerRuleBinary for Div_Div {
    fn slug(&self) -> String {
        "div + div".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        matches!(a, ExpressionType::Div) && matches!(b, ExpressionType::Div)
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        self.apply_converted(normalizer, cast_norm(a).unwrap(), cast_norm(b).unwrap())
    }
}

impl Div_Any {
    fn value_as_div(&self, value: &PNormalized) -> Option<Div> {
        let (top, bottom) = value.as_div()?;
        Some(Div::new(top, bottom))
    }

    fn add_div_value(
        &self,
        normalizer: &dyn INormalizer,
        div: &Div,
        val: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let expr = NormalizedBin::new(Operator::Mul, val.clone(), div.bottom.clone());
        let other_top = normalizer.normalize_n_binary(&expr)?;
        let expr = NormalizedBin::new(Operator::Add, div.top.clone(), other_top);
        let total_top = normalizer.normalize_n_binary(&expr)?;
        Ok(Rc::new(Div::new(total_top, div.bottom.clone())))
    }
}

impl INormalizerRuleBinary for Div_Any {
    fn slug(&self) -> String {
        "div + (sum | var | num | root | mul)".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        matches!(a, ExpressionType::Div)
            && matches!(
                b,
                ExpressionType::Var
                    | ExpressionType::Number
                    | ExpressionType::Sum
                    | ExpressionType::Mul
                    | ExpressionType::Root
            )
    }

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let div_a = cast_norm::<Div>(a).unwrap();
        if let Some(div_b) = self.value_as_div(b) {
            (Div_Div {}).apply_converted(normalizer, div_a, &div_b)
        } else {
            self.add_div_value(normalizer, div_a, b)
        }
    }
}

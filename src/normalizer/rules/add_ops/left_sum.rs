use std::rc::Rc;

use crate::entities::constants::NORM_CONCRETE_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Constants, ExpressionType, Operator, PNormalized, Sum,
};
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct Sum_Sum {}
#[allow(non_camel_case_types)]
pub struct Sum_VarMulRoot {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![Box::new(Sum_VarMulRoot {}), Box::new(Sum_Sum {})]
}

impl INormalizerRuleBinary for Sum_VarMulRoot {
    fn slug(&self) -> String {
        "sum + (var | mul | root)".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        matches!(a, ExpressionType::Sum)
            && matches!(
                b,
                ExpressionType::Var | ExpressionType::Mul | ExpressionType::Root
            )
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let mut sum = cast_norm::<Sum>(a).unwrap().clone();
        sum.items.push(b.clone());
        Ok(Rc::new(sum))
    }
}

impl INormalizerRuleBinary for Sum_Sum {
    fn slug(&self) -> String {
        "sum + sum".to_string()
    }

    fn priority(&self) -> usize {
        NORM_CONCRETE_PRIORITY
    }

    fn operator(&self) -> Operator {
        Operator::Add
    }

    fn match_args(&self, _: &Constants, a: &PNormalized, b: &PNormalized) -> bool {
        let a = a.get_type();
        let b = b.get_type();
        matches!(a, ExpressionType::Sum) && matches!(b, ExpressionType::Sum)
    }

    fn apply(
        &self,
        _: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        let mut sum = cast_norm::<Sum>(a).unwrap().clone();
        sum.items
            .extend_from_slice(&cast_norm::<Sum>(b).unwrap().items);
        Ok(Rc::new(sum))
    }
}

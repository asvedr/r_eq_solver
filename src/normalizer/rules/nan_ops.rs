use crate::entities::constants::NUM_NUM_HIGH_PRIORITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, Constants, Operator, PNormalized};
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, INormalizerRuleBinary};

#[allow(non_camel_case_types)]
pub struct AddNaN {}
#[allow(non_camel_case_types)]
pub struct SubNaN {}
#[allow(non_camel_case_types)]
pub struct MulNaN {}
#[allow(non_camel_case_types)]
pub struct DivNaN {}
#[allow(non_camel_case_types)]
pub struct PowNaN {}

pub fn rules() -> Vec<Box<dyn INormalizerRuleBinary>> {
    vec![
        Box::new(AddNaN {}),
        Box::new(SubNaN {}),
        Box::new(MulNaN {}),
        Box::new(DivNaN {}),
        Box::new(PowNaN {}),
    ]
}

fn is_nan(value: &PNormalized) -> bool {
    if let Some(num) = cast_norm::<Number>(value) {
        num.is_nan()
    } else {
        false
    }
}

fn nan(a: &PNormalized, b: &PNormalized) -> SolverResult<PNormalized> {
    Ok((if is_nan(a) { a } else { b }).clone())
}

macro_rules! impl_rule {
    ($rule:ident, $slug:expr, $operator:ident) => {
        impl INormalizerRuleBinary for $rule {
            fn slug(&self) -> String {
                $slug.to_string()
            }
            fn priority(&self) -> usize {
                NUM_NUM_HIGH_PRIORITY
            }
            fn operator(&self) -> Operator {
                Operator::$operator
            }
            fn match_args(
                &self,
                _: &Constants,
                a: &PNormalized,
                b: &PNormalized,
            ) -> bool {
                is_nan(a) || is_nan(b)
            }
            fn apply(
                &self,
                _: &dyn INormalizer,
                a: &PNormalized,
                b: &PNormalized,
            ) -> SolverResult<PNormalized> {
                nan(a, b)
            }
        }
    };
}

impl_rule!(AddNaN, "nan + _", Add);
impl_rule!(SubNaN, "nan - _", Sub);
impl_rule!(MulNaN, "nan * _", Mul);
impl_rule!(DivNaN, "nan / _", Div);
impl_rule!(PowNaN, "nan ^ _", Pow);

use std::collections::HashMap;

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_expr, BinaryOperation, Constants, ExpressionType, IExpression, Operator,
    PExpression, PNormalized, UnaryOperation,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::{
    IBinaryReverser, IEventLogger, INormalizer, INormalizerRuleBinary,
    INormalizerRuleUnary, IShortifier,
};

pub struct Normalizer {
    unary_rules: HashMap<Operator, Vec<Box<dyn INormalizerRuleUnary>>>,
    binary_rules: HashMap<Operator, Vec<Box<dyn INormalizerRuleBinary>>>,
    constants_: Constants,
    binary_reverser: Box<dyn IBinaryReverser>,
    shortifiers: HashMap<ExpressionType, Box<dyn IShortifier>>,
    pub event_logger: Option<Box<dyn IEventLogger>>,
}

#[inline]
fn append_to<A>(map: &mut HashMap<Operator, Vec<A>>, operator: Operator, rule: A) {
    if let Some(vec) = map.get_mut(&operator) {
        vec.push(rule);
        return;
    }
    map.insert(operator, vec![rule]);
}

#[inline]
fn complete_rules<A>(map: &mut HashMap<Operator, Vec<A>>, operators: Vec<Operator>) {
    for operator in operators {
        let not_found = map.get(&operator).is_none();
        if not_found {
            map.insert(operator, Vec::new());
        }
    }
}

fn find_matched_rule<'a>(
    cons: &Constants,
    rules: &'a [Box<dyn INormalizerRuleBinary>],
    expr: &NormalizedBin,
) -> Option<&'a dyn INormalizerRuleBinary> {
    for rule in rules.iter() {
        if rule.match_args(cons, &expr.left, &expr.right) {
            return Some(&**rule);
        }
    }
    None
}

impl Normalizer {
    pub fn new(
        unary_vec: Vec<Box<dyn INormalizerRuleUnary>>,
        mut binary_vec: Vec<Box<dyn INormalizerRuleBinary>>,
        binary_reverser: Box<dyn IBinaryReverser>,
        shortifiers: Vec<Box<dyn IShortifier>>,
        event_logger: Option<Box<dyn IEventLogger>>,
    ) -> Normalizer {
        binary_vec.sort_by(|a, b| a.priority().partial_cmp(&b.priority()).unwrap());
        let mut unary_rules = HashMap::new();
        let mut binary_rules = HashMap::new();
        for rule in unary_vec {
            let operator = rule.operator();
            append_to(&mut unary_rules, operator, rule);
        }
        for rule in binary_vec {
            let operator = rule.operator();
            append_to(&mut binary_rules, operator, rule);
        }
        complete_rules(&mut unary_rules, Operator::all_unary());
        complete_rules(&mut binary_rules, Operator::all_binary());
        let zero = Number::integer(0);
        let one = Number::integer(1);
        let mut shortifiers_map = HashMap::new();
        for shortifier in shortifiers {
            let key = shortifier.get_type();
            shortifiers_map.insert(key, shortifier);
        }
        Normalizer {
            unary_rules,
            binary_rules,
            binary_reverser,
            shortifiers: shortifiers_map,
            event_logger,
            constants_: Constants {
                expr_zero: zero.as_expr(),
                expr_one: one.as_expr(),
                norm_zero: zero.as_norm(),
                norm_one: one.as_norm(),
            },
        }
    }

    fn apply_bin(
        &self,
        rule: &dyn INormalizerRuleBinary,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized> {
        if let Some(ref logger) = self.event_logger {
            logger.inc();
        }
        let result = rule.apply(self, a, b)?;
        if let Some(ref logger) = self.event_logger {
            logger.dec();
            let event = format!(
                "RULE {}: ({})AND({}) => {}",
                rule.slug(),
                a.to_string(),
                b.to_string(),
                result.to_string()
            );
            logger.log_event(event);
        }
        Ok(result)
    }

    fn apply_un(
        &self,
        rule: &dyn INormalizerRuleUnary,
        a: &PNormalized,
    ) -> SolverResult<PNormalized> {
        if let Some(ref logger) = self.event_logger {
            let event = format!("RULE {}: ({}) => ...", rule.slug(), a.to_string(),);
            logger.log_event(event);
            logger.inc();
        }
        let result = rule.apply(self, a)?;
        if let Some(ref logger) = self.event_logger {
            logger.dec();
            let event = format!("... => {}", result.to_string());
            logger.log_event(event);
        }
        Ok(result)
    }

    fn normalize_binary_without_reverse(
        &self,
        rules: &[Box<dyn INormalizerRuleBinary>],
        expr: &NormalizedBin,
    ) -> SolverResult<PNormalized> {
        let cons = self.constants();
        if let Some(rule) = find_matched_rule(cons, rules, expr) {
            self.apply_bin(rule, &expr.left, &expr.right)
        } else {
            let msg = format!("bin no rev \"{}\"", expr.to_string());
            Err(SolverError::CanNotFindRule(msg))
        }
    }

    fn normalize_binary_with_reverse(
        &self,
        rules_forward: &[Box<dyn INormalizerRuleBinary>],
        expr_forward: &NormalizedBin,
        rules_backward: &[Box<dyn INormalizerRuleBinary>],
        expr_backward: &NormalizedBin,
    ) -> SolverResult<PNormalized> {
        let cons = self.constants();
        let rule_forward = find_matched_rule(cons, rules_forward, expr_forward);
        let rule_backward = find_matched_rule(cons, rules_backward, expr_backward);
        match (rule_forward, rule_backward) {
            (Some(f), Some(b)) => {
                if f.priority() <= b.priority() {
                    self.apply_bin(f, &expr_forward.left, &expr_forward.right)
                } else {
                    self.apply_bin(b, &expr_backward.left, &expr_backward.right)
                }
            }
            (Some(f), None) => self.apply_bin(f, &expr_forward.left, &expr_forward.right),
            (None, Some(b)) => {
                self.apply_bin(b, &expr_backward.left, &expr_backward.right)
            }
            _ => {
                let msg = format!("bin rev \"{}\"", expr_forward.to_string());
                Err(SolverError::CanNotFindRule(msg))
            }
        }
    }

    fn to_norm_bin(&self, expr: &BinaryOperation) -> SolverResult<NormalizedBin> {
        let value = NormalizedBin {
            operator: expr.operator,
            left: self.normalize(&expr.left)?,
            right: self.normalize(&expr.right)?,
        };
        Ok(value)
    }
}

impl INormalizer for Normalizer {
    fn constants(&self) -> &Constants {
        &self.constants_
    }

    fn normalize(&self, expr: &PExpression) -> SolverResult<PNormalized> {
        match expr.get_type() {
            ExpressionType::BinaryOperation => {
                self.normalize_binary(cast_expr::<BinaryOperation>(&expr).unwrap())
            }
            ExpressionType::UnaryOperation => {
                self.normalize_unary(cast_expr::<UnaryOperation>(&expr).unwrap())
            }
            _ => Ok(expr.as_norm()),
        }
    }

    fn normalize_n_binary(&self, forward: &NormalizedBin) -> SolverResult<PNormalized> {
        let rules_forward = self.binary_rules.get(&forward.operator).unwrap();
        let reverse = self.binary_reverser.reverse(&forward);
        let result = if let Some(reversed) = reverse {
            let rules_backward = self.binary_rules.get(&reversed.operator);
            let rules_backward = rules_backward.unwrap();
            self.normalize_binary_with_reverse(
                rules_forward,
                &forward,
                rules_backward,
                &reversed,
            )?
        } else {
            self.normalize_binary_without_reverse(rules_forward, &forward)?
        };
        self.shortify(result)
    }

    fn normalize_binary(&self, expr: &BinaryOperation) -> SolverResult<PNormalized> {
        let expr = self.to_norm_bin(expr)?;
        self.normalize_n_binary(&expr)
    }

    fn normalize_unary(&self, expr: &UnaryOperation) -> SolverResult<PNormalized> {
        let arg = self.normalize(&expr.arg)?;
        let rules = self.unary_rules.get(&expr.operator).unwrap();
        for rule in rules.iter() {
            if rule.match_arg(&self.constants_, &arg) {
                let result = self.apply_un(&**rule, &arg)?;
                return self.shortify(result);
            }
        }
        let msg = format!("unary \"{:?} {}\"", expr.operator, arg.to_string());
        Err(SolverError::CanNotFindRule(msg))
    }

    fn shortify(&self, value: PNormalized) -> SolverResult<PNormalized> {
        match self.shortifiers.get(&value.get_type()) {
            Some(shortifier) => shortifier.shortify(self, value),
            _ => Ok(value),
        }
    }
}

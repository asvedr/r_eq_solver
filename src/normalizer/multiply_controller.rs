use num_bigint::Sign;
use num_traits::identities::Zero;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Mul, PNormalized, Sum,
};
use crate::entities::normalizer::Multipliers;
use crate::proto::normalizer::{IMultiplyController, INormalizer};

pub struct MultiplyController {}

type ShortifyResult = (PNormalized, bool);

macro_rules! not_changed {
    ($val:expr) => {
        return Ok(($val.clone(), false));
    };
}

macro_rules! not_changed_norm {
    ($val:expr) => {
        return Ok(($val.as_norm(), false));
    };
}

impl MultiplyController {
    pub fn new() -> MultiplyController {
        MultiplyController {}
    }

    fn from_var_or_root(&self, result: &mut Multipliers, expr: &PNormalized) {
        if expr.get_exponent().unwrap().sign() != Sign::Plus {
            return;
        }
        result.add_multiplier(expr.clone());
    }

    fn from_div(&self, result: &mut Multipliers, div: &Div) {
        self.from_expr(result, &div.top)
    }

    fn from_mul(&self, result: &mut Multipliers, mul: &Mul) {
        for item in mul.items.iter() {
            self.from_expr(result, item)
        }
    }

    fn from_sum(&self, result: &mut Multipliers, sum: &Sum) {
        let mut total = self.get_multipliers(&sum.items[0]);
        for item in sum.items.iter().skip(1) {
            if total.is_empty() {
                return;
            }
            total = self.get_multipliers(item).cross_multipliers(&total);
        }
        result.append_multipliers(total);
    }

    fn from_expr(&self, result: &mut Multipliers, expr: &PNormalized) {
        match expr.get_type() {
            ExpressionType::Number => (),
            ExpressionType::Var | ExpressionType::Root => {
                self.from_var_or_root(result, expr)
            }
            ExpressionType::Div => self.from_div(result, cast_norm(expr).unwrap()),
            ExpressionType::Mul => self.from_mul(result, cast_norm(expr).unwrap()),
            ExpressionType::Sum => self.from_sum(result, cast_norm(expr).unwrap()),
            _ => unreachable!(),
        }
    }

    fn shortify_var_or_root(
        &self,
        normalizer: &dyn INormalizer,
        multipliers: &mut Multipliers,
        expr: &PNormalized,
    ) -> SolverResult<ShortifyResult> {
        let min_exp = match multipliers.min_multiplier_exp(expr) {
            Some(val) => val,
            _ => not_changed!(expr),
        };
        let expr_exp = expr.get_exponent().unwrap() - &min_exp;
        multipliers.down_multiplier_exp(&expr.set_exponent(min_exp));
        if expr_exp.is_zero() {
            Ok((expr.get_coefficient().unwrap().as_norm(), true))
        } else {
            let expr = normalizer.shortify(expr.set_exponent(expr_exp))?;
            Ok((expr, true))
        }
    }

    fn shortify_mul(
        &self,
        normalizer: &dyn INormalizer,
        multipliers: &mut Multipliers,
        mul: &Mul,
    ) -> SolverResult<ShortifyResult> {
        let mut items = Vec::new();
        let mut changed = false;
        for item in mul.items.iter() {
            let (item, flag) = self.shortify_expr(normalizer, multipliers, item)?;
            items.push(item);
            changed = changed || flag;
        }
        if !changed {
            not_changed_norm!(mul)
        }
        if items.is_empty() {
            return Ok((mul.coefficient.as_norm(), true));
        }
        let new_mul = Mul::new(mul.coefficient.clone(), items);
        let expr = normalizer.shortify(new_mul.as_norm())?;
        Ok((expr, true))
    }

    fn shortify_div(
        &self,
        normalizer: &dyn INormalizer,
        multipliers: &mut Multipliers,
        div: &Div,
    ) -> SolverResult<ShortifyResult> {
        let (top, changed) = self.shortify_expr(normalizer, multipliers, &div.top)?;
        if !changed {
            not_changed_norm!(div)
        }
        let new_div = Div::new(top, div.bottom.clone()).as_norm();
        let expr = normalizer.shortify(new_div)?;
        Ok((expr, true))
    }

    fn shortify_sum(
        &self,
        normalizer: &dyn INormalizer,
        multipliers: &mut Multipliers,
        sum: &Sum,
    ) -> SolverResult<ShortifyResult> {
        let mults = multipliers.cross_multipliers(&self.get_multipliers(&sum.as_norm()));
        if mults.is_empty() {
            not_changed_norm!(sum)
        }
        let mut items = Vec::new();
        for item in sum.items.iter() {
            let mut mults_copy = mults.clone();
            let (item, _) = self.shortify_expr(normalizer, &mut mults_copy, item)?;
            items.push(item)
        }
        for (_, item) in mults.data.iter() {
            multipliers.down_multiplier_exp(item);
        }
        let result = normalizer.shortify(Sum::new(items).as_norm())?;
        Ok((result, true))
    }

    fn shortify_expr(
        &self,
        normalizer: &dyn INormalizer,
        multipliers: &mut Multipliers,
        expr: &PNormalized,
    ) -> SolverResult<ShortifyResult> {
        if multipliers.is_empty() {
            not_changed!(expr)
        }
        match expr.get_type() {
            ExpressionType::Var | ExpressionType::Root => {
                self.shortify_var_or_root(normalizer, multipliers, expr)
            }
            ExpressionType::Mul => {
                self.shortify_mul(normalizer, multipliers, cast_norm(expr).unwrap())
            }
            ExpressionType::Div => {
                self.shortify_div(normalizer, multipliers, cast_norm(expr).unwrap())
            }
            ExpressionType::Sum => {
                self.shortify_sum(normalizer, multipliers, cast_norm(expr).unwrap())
            }
            _ => not_changed!(expr),
        }
    }
}

impl IMultiplyController for MultiplyController {
    fn get_multipliers(&self, expr: &PNormalized) -> Multipliers {
        let mut result = Multipliers::new();
        self.from_expr(&mut result, expr);
        result
    }
    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        expr: &PNormalized,
        mut multipliers: Multipliers,
    ) -> SolverResult<PNormalized> {
        let (val, _) = self.shortify_expr(normalizer, &mut multipliers, expr)?;
        assert!(multipliers.is_empty());
        Ok(val)
    }
}

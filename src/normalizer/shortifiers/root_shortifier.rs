use std::rc::Rc;

use num_bigint::{BigInt, Sign};
use num_traits::identities::Zero;

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Mul, Operator, PNormalized, Root, Var,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, IShortifier};
use num_integer::Integer;

pub struct RootShortifier {
    n1: Number,
    i1: BigInt,
    i2: BigInt,
}

impl RootShortifier {
    pub fn new() -> RootShortifier {
        RootShortifier {
            n1: Number::integer(1),
            i1: (1).into(),
            i2: (2).into(),
        }
    }

    fn try_remove_root(
        &self,
        normalizer: &dyn INormalizer,
        root: &Root,
    ) -> SolverResult<Option<PNormalized>> {
        if root.expr.get_type() == ExpressionType::Number {
            // Only hard processing for numbers
            return Ok(None);
        }
        if root.exp.is_zero() {
            return Ok(Some(normalizer.constants().norm_one.clone()));
        }
        if !(root.exp >= root.root && (&root.exp % &root.root).is_zero()) {
            return Ok(None);
        }
        let exp = &root.exp / &root.root;
        let expr = NormalizedBin::new(
            Operator::Pow,
            root.expr.clone(),
            Rc::new(Number::integer(exp)),
        );
        let value = normalizer.normalize_n_binary(&expr)?;
        let expr = NormalizedBin::new(Operator::Mul, root.coefficient.as_norm(), value);
        let result = normalizer.normalize_n_binary(&expr)?;
        Ok(Some(result))
    }

    fn process_num(&self, root: &Root, num: &Number) -> SolverResult<PNormalized> {
        if root.root.is_even() && num.is_neg() {
            let msg = format!("root[{}]({})", root.root, num);
            return Err(SolverError::NaNFound(msg));
        }
        let e_exp = Number::integer(root.exp.clone());
        let e_root = Number::integer(root.root.clone());
        let result_exp = e_exp / e_root;
        let num = num.pow(&result_exp);
        if num.is_nan() {
            let circumstance = format!("{}^{}", num, result_exp);
            Err(SolverError::NaNFound(circumstance))
        } else {
            Ok(Rc::new(&root.coefficient * &num))
        }
    }

    fn process_inv_var(
        &self,
        normalizer: &dyn INormalizer,
        root: &Root,
        var: &Var,
    ) -> SolverResult<PNormalized> {
        let inv_var = Var::new(-&var.pow, self.n1.clone(), var.name.clone());
        let numerator = self.process_num(&root, &var.coefficient)?;
        let new_root = Root::new(
            root.exp.clone(),
            root.root.clone(),
            self.n1.clone(),
            Rc::new(inv_var),
        );
        let denominator = self.process_root(normalizer, &new_root)?;
        let div = Rc::new(Div::new(numerator, denominator));
        normalizer.shortify(div)
    }

    fn process_var_coeff(&self, root: Root) -> SolverResult<Root> {
        let var = cast_norm::<Var>(&root.expr).unwrap();
        if var.coefficient == self.n1 {
            return Ok(root);
        }
        let coeff;
        if var.coefficient.is_neg() {
            let abs_coeff = -&var.coefficient;
            coeff = self.process_num(&root, &abs_coeff)?;
        } else {
            coeff = self.process_num(&root, &var.coefficient)?;
        }
        let num_coeff = cast_norm::<Number>(&coeff).unwrap();
        let var_coeff;
        if var.coefficient.is_neg() {
            var_coeff = -&self.n1;
        } else {
            var_coeff = self.n1.clone();
        }
        let new_var = Var::new(var.pow.clone(), var_coeff, var.name.clone());
        let new_root = Root::new(
            root.exp.clone(),
            root.root.clone(),
            num_coeff.clone(),
            Rc::new(new_var),
        );
        Ok(new_root)
    }

    fn process_even_var(&self, root: Root) -> Root {
        let var = cast_norm::<Var>(&root.expr).unwrap();
        let exp = &root.exp * &(&var.pow / &self.i2);
        let new_var =
            Var::new(self.i2.clone(), var.coefficient.clone(), var.name.clone());
        Root::new(
            exp,
            root.root.clone(),
            root.coefficient.clone(),
            Rc::new(new_var),
        )
    }

    fn process_odd_var(&self, root: Root) -> Root {
        let var = cast_norm::<Var>(&root.expr).unwrap();
        let exp = &root.exp * &var.pow;
        let new_var =
            Var::new(self.i1.clone(), var.coefficient.clone(), var.name.clone());
        Root::new(
            exp,
            root.root.clone(),
            root.coefficient.clone(),
            Rc::new(new_var),
        )
    }

    fn process_var(
        &self,
        normalizer: &dyn INormalizer,
        mut root: Root,
    ) -> SolverResult<PNormalized> {
        let var = cast_norm::<Var>(&root.expr).unwrap();
        if var.pow.sign() == Sign::Minus {
            return self.process_inv_var(normalizer, &root, var);
        }
        root = self.process_var_coeff(root)?;
        let var = cast_norm::<Var>(&root.expr).unwrap();
        if var.pow.is_even() {
            if var.pow != self.i2 {
                root = self.process_even_var(root)
            }
        } else {
            root = self.process_odd_var(root)
        }
        match self.try_remove_root(normalizer, &root)? {
            Some(val) => Ok(val),
            _ => Ok(Rc::new(root)),
        }
    }

    fn process_mul(
        &self,
        normalizer: &dyn INormalizer,
        root: &Root,
        mul: &Mul,
    ) -> SolverResult<PNormalized> {
        let coeff = self.process_num(root, &mul.coefficient)?;
        let coeff_num = cast_norm::<Number>(&coeff).unwrap().clone();
        let mut items = Vec::new();
        for item in mul.items.iter() {
            let localized = Root::new(
                root.exp.clone(),
                root.root.clone(),
                self.n1.clone(),
                item.clone(),
            );
            items.push(self.process_root(normalizer, &localized)?);
        }
        normalizer.shortify(Rc::new(Mul::new(coeff_num, items)))
    }

    fn process_div(
        &self,
        normalizer: &dyn INormalizer,
        root: &Root,
        div: &Div,
    ) -> SolverResult<PNormalized> {
        let mut numerator = root.clone();
        numerator.expr = div.top.clone();
        let numerator = self.process_root(normalizer, &numerator)?;
        let denominator = Root::new(
            root.exp.clone(),
            root.root.clone(),
            self.n1.clone(),
            div.bottom.clone(),
        );
        let denominator = self.process_root(normalizer, &denominator)?;
        normalizer.shortify(Rc::new(Div::new(numerator, denominator)))
    }

    fn process_sub_root(
        &self,
        normalizer: &dyn INormalizer,
        root: &Root,
        sub_root: &Root,
    ) -> SolverResult<PNormalized> {
        let coeff = self.process_num(root, &sub_root.coefficient)?;
        let coeff_num = cast_norm::<Number>(&coeff).unwrap();
        let value = Root::new(
            &root.exp * &sub_root.exp,
            &root.root * &sub_root.root,
            &root.coefficient * coeff_num,
            sub_root.expr.clone(),
        );
        self.process_root(normalizer, &value)
    }

    fn process_root(
        &self,
        normalizer: &dyn INormalizer,
        root: &Root,
    ) -> SolverResult<PNormalized> {
        if let Some(value) = self.try_remove_root(normalizer, root)? {
            return Ok(value);
        }
        match root.expr.get_type() {
            ExpressionType::Sum => Ok(root.as_norm()),
            ExpressionType::Number => {
                self.process_num(root, cast_norm(&root.expr).unwrap())
            }
            ExpressionType::Var => self.process_var(normalizer, root.clone()),
            ExpressionType::Mul => {
                let mul = cast_norm(&root.expr).unwrap();
                self.process_mul(normalizer, root, mul)
            }
            ExpressionType::Div => {
                let div = cast_norm(&root.expr).unwrap();
                self.process_div(normalizer, root, div)
            }
            ExpressionType::Root => {
                let sub_root = cast_norm(&root.expr).unwrap();
                self.process_sub_root(normalizer, root, sub_root)
            }
            _ => unreachable!(),
        }
    }
}

impl IShortifier for RootShortifier {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Root
    }

    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        value: PNormalized,
    ) -> SolverResult<PNormalized> {
        let root = cast_norm(&value).unwrap();
        self.process_root(normalizer, root)
    }
}

use std::rc::Rc;

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Operator, PNormalized,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::{IMultiplyController, INormalizer, IShortifier};

pub struct DivShortifier {
    multiply_controller: Box<dyn IMultiplyController>,
    one: Number,
}

impl DivShortifier {
    pub fn new(multiply_controller: Box<dyn IMultiplyController>) -> DivShortifier {
        DivShortifier {
            multiply_controller,
            one: Number::integer(1),
        }
    }

    fn case_bottom_is_num(&self, div: &Div) -> SolverResult<PNormalized> {
        let num = cast_norm::<Number>(&div.bottom).unwrap();
        Ok(div.top.apply_mul_num(&(&self.one / num)))
    }

    fn case_top_and_bottom_are_divs(
        &self,
        normalizer: &dyn INormalizer,
        div: &Div,
    ) -> SolverResult<PNormalized> {
        let cons = normalizer.constants();
        let expr = NormalizedBin::new(
            Operator::Mul,
            div.top.clone(),
            div.bottom.apply_div(cons)?,
        );
        normalizer.normalize_n_binary(&expr)
    }

    fn case_top_is_div(
        &self,
        normalizer: &dyn INormalizer,
        div: &Div,
    ) -> SolverResult<PNormalized> {
        let top_div = cast_norm::<Div>(&div.top).unwrap();
        let expr =
            NormalizedBin::new(Operator::Mul, top_div.bottom.clone(), div.bottom.clone());
        let bottom = normalizer.normalize_n_binary(&expr)?;
        let new_div = Div::new(top_div.top.clone(), bottom);
        self.shortify_div(normalizer, &new_div)
    }

    fn case_bottom_is_div(
        &self,
        normalizer: &dyn INormalizer,
        div: &Div,
    ) -> SolverResult<PNormalized> {
        let bottom_div = cast_norm::<Div>(&div.bottom).unwrap();
        let expr =
            NormalizedBin::new(Operator::Mul, div.top.clone(), bottom_div.bottom.clone());
        let top = normalizer.normalize_n_binary(&expr)?;
        let new_div = Div::new(top, bottom_div.top.clone());
        self.shortify_div(normalizer, &new_div)
    }

    fn try_shortify_multipliers(
        &self,
        normalizer: &dyn INormalizer,
        div: &Div,
    ) -> SolverResult<PNormalized> {
        let top_mults = self.multiply_controller.get_multipliers(&div.top);
        if top_mults.is_empty() {
            return Ok(div.as_norm());
        }
        let bottom_mults = self.multiply_controller.get_multipliers(&div.bottom);
        let shared_mults = top_mults.cross_multipliers(&bottom_mults);
        if shared_mults.is_empty() {
            return Ok(div.as_norm());
        }
        let new_top = self.multiply_controller.shortify(
            normalizer,
            &div.top,
            shared_mults.clone(),
        )?;
        let new_bottom =
            self.multiply_controller
                .shortify(normalizer, &div.bottom, shared_mults)?;
        let new_div = Div::new(new_top, new_bottom);
        self.shortify_div(normalizer, &new_div)
    }

    fn convert_params_to_divs(&self, original: &Div) -> Option<Div> {
        let top = original.top.as_div();
        let bottom = original.bottom.as_div();
        if top.is_none() && bottom.is_none() {
            return None;
        }
        let top = match top {
            Some((a, b)) => Rc::new(Div::new(a, b)),
            _ => original.top.clone(),
        };
        let bottom = match bottom {
            Some((c, d)) => Rc::new(Div::new(c, d)),
            _ => original.bottom.clone(),
        };
        Some(Div::new(top, bottom))
    }

    fn shortify_div(
        &self,
        normalizer: &dyn INormalizer,
        div: &Div,
    ) -> SolverResult<PNormalized> {
        if div.bottom.cmp_norm(&normalizer.constants().norm_zero) {
            return Err(SolverError::NaNFound(div.to_string()));
        }
        if div.top.cmp_norm(&normalizer.constants().norm_zero) {
            return Ok(normalizer.constants().norm_zero.clone());
        }
        if div.top.cmp_norm(&div.bottom) {
            return Ok(normalizer.constants().norm_one.clone());
        }
        // "as_div" convert (var, root, or mul) to div
        if let Some(converted) = self.convert_params_to_divs(&div) {
            return self.shortify_div(normalizer, &converted);
        }
        match (div.top.get_type(), div.bottom.get_type()) {
            (_, ExpressionType::Number) => self.case_bottom_is_num(div),
            (ExpressionType::Div, ExpressionType::Div) => {
                self.case_top_and_bottom_are_divs(normalizer, div)
            }
            (ExpressionType::Div, _) => self.case_top_is_div(normalizer, div),
            (_, ExpressionType::Div) => self.case_bottom_is_div(normalizer, div),
            _ => self.try_shortify_multipliers(normalizer, div),
        }
    }

    fn shortify_coefficients(&self, div: &Div) -> Option<PNormalized> {
        let top_coeff = div.top.get_coefficient()?;
        let bottom_coeff = div.bottom.get_coefficient()?;
        if bottom_coeff.eq_int(1) {
            return None;
        }
        let coeff = top_coeff / bottom_coeff;
        let top = div.top.set_coefficient(coeff);
        let bottom = div.bottom.set_coefficient(Number::integer(1));
        Some(Rc::new(Div::new(top, bottom)))
    }
}

impl IShortifier for DivShortifier {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Div
    }

    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        value: PNormalized,
    ) -> SolverResult<PNormalized> {
        let div = cast_norm::<Div>(&value).unwrap();
        let result = self.shortify_div(normalizer, div)?;
        if let Some(div) = cast_norm::<Div>(&result) {
            match self.shortify_coefficients(div) {
                Some(changed) => Ok(changed),
                _ => Ok(result),
            }
        } else {
            Ok(result)
        }
    }
}

use std::collections::BTreeMap;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Operator, PNormalized, Sum,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, IShortifier};

pub struct SumShortifier {}

struct Sorted {
    // Var or Mul or Root
    expressions: BTreeMap<String, (Number, PNormalized)>,
    number: Option<Number>,
    divs: Vec<PNormalized>,
}

impl Sorted {
    fn process_var_root_mul(&mut self, expr: &PNormalized) {
        let without_coeff = expr.drop_coefficient();
        let key = without_coeff.to_string();
        let mut coefficient = expr.get_coefficient().unwrap().clone();
        if let Some((accum, _)) = self.expressions.remove(&key) {
            coefficient = coefficient + accum
        }
        self.expressions.insert(key, (coefficient, without_coeff));
    }

    fn process_num(&mut self, num: &Number) {
        if let Some(ref val) = self.number {
            self.number = Some(val + num);
        } else {
            self.number = Some(num.clone());
        }
    }

    fn process_sum(&mut self, sum: &Sum) {
        for x in sum.items.iter() {
            self.process_item(x)
        }
    }

    fn process_div(&mut self, div: &Div) {
        self.divs.push(div.as_norm())
    }

    fn process_item(&mut self, item: &PNormalized) {
        match item.get_type() {
            ExpressionType::Number => self.process_num(cast_norm(item).unwrap()),
            ExpressionType::Sum => self.process_sum(cast_norm(item).unwrap()),
            ExpressionType::Div => self.process_div(cast_norm(item).unwrap()),
            ExpressionType::Var => self.process_var_root_mul(item),
            ExpressionType::Root => self.process_var_root_mul(item),
            ExpressionType::Mul => self.process_var_root_mul(item),
            _ => unreachable!(),
        }
    }

    fn open_without_div(&self) -> Vec<PNormalized> {
        let mut result = Vec::new();
        for (_, (coefficient, val)) in self.expressions.iter() {
            if !coefficient.eq_int(0) {
                result.push(val.set_coefficient(coefficient.clone()));
            }
        }
        if let Some(ref val) = self.number {
            if !val.eq_int(0) {
                result.push(val.as_norm());
            }
        }
        result
    }
}

impl SumShortifier {
    pub fn new() -> SumShortifier {
        SumShortifier {}
    }

    fn process_sum_with_divs(
        &self,
        normalizer: &dyn INormalizer,
        sorted: Sorted,
    ) -> SolverResult<PNormalized> {
        let items = sorted.open_without_div();
        let mut root = sorted.divs[0].clone();
        for div in sorted.divs.iter().skip(1) {
            let expr = NormalizedBin::new(Operator::Add, root, div.clone());
            root = normalizer.normalize_n_binary(&expr)?;
        }
        for item in items {
            let expr = NormalizedBin::new(Operator::Add, root, item.clone());
            root = normalizer.normalize_n_binary(&expr)?;
        }
        Ok(root)
    }
}

impl IShortifier for SumShortifier {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Sum
    }

    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        value: PNormalized,
    ) -> SolverResult<PNormalized> {
        let sum = cast_norm::<Sum>(&value).unwrap();
        let mut sorted = Sorted {
            expressions: BTreeMap::new(),
            number: None,
            divs: Vec::new(),
        };
        sorted.process_sum(sum);
        if !sorted.divs.is_empty() {
            return self.process_sum_with_divs(normalizer, sorted);
        }
        let items = sorted.open_without_div();
        if items.is_empty() {
            return Ok(normalizer.constants().norm_zero.clone());
        }
        if items.len() == 1 {
            return Ok(items[0].clone());
        }
        Ok(Sum::new(items).as_norm())
    }
}

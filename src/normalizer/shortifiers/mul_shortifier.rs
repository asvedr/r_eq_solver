use std::collections::BTreeMap;
use std::rc::Rc;

use num_traits::Zero;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, Div, ExpressionType, IExpression, Mul, Operator, PNormalized,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::{INormalizer, IShortifier};

pub struct MulShortifier {}

struct Sorted {
    coefficient: Number,
    expressions: BTreeMap<String, PNormalized>,
    sums: Vec<PNormalized>,
    denominators: Vec<PNormalized>,
}

impl Sorted {
    fn new() -> Sorted {
        Sorted {
            coefficient: Number::integer(1),
            expressions: BTreeMap::new(),
            sums: Vec::new(),
            denominators: Vec::new(),
        }
    }

    fn process_num(&mut self, value: PNormalized) {
        let num = cast_norm::<Number>(&value).unwrap();
        let coeff = &self.coefficient * num;
        self.coefficient = coeff;
    }

    fn process_var_or_root(&mut self, mut value: PNormalized) {
        let coeff = value.get_coefficient().unwrap().clone();
        value = value.drop_coefficient();
        if !coeff.eq_int(1) {
            self.coefficient = &self.coefficient * &coeff;
        }
        let key = value.get_key().unwrap();
        if let Some(exists) = self.expressions.remove(&key) {
            let a = exists.get_exponent().unwrap();
            let b = value.get_exponent().unwrap();
            let exp = a + b;
            if exp.is_zero() {
                return;
            }
            value = value.set_exponent(exp);
        }
        self.expressions.insert(key, value);
    }

    fn process_div(&mut self, value: PNormalized) {
        let div = cast_norm::<Div>(&value).unwrap();
        self.process_expression(&div.top);
        self.denominators.push(div.bottom.clone());
    }

    fn process_sum(&mut self, value: PNormalized) {
        self.sums.push(value);
    }

    fn process_mul(&mut self, value: PNormalized) {
        let mul = cast_norm::<Mul>(&value).unwrap();
        let coeff = &self.coefficient * &mul.coefficient;
        self.coefficient = coeff;
        for item in mul.items.iter() {
            self.process_expression(item)
        }
    }

    fn process_expression(&mut self, value: &PNormalized) {
        let copy = value.clone();
        match value.get_type() {
            ExpressionType::Number => self.process_num(copy),
            ExpressionType::Var | ExpressionType::Root => self.process_var_or_root(copy),
            ExpressionType::Sum => self.process_sum(copy),
            ExpressionType::Div => self.process_div(copy),
            ExpressionType::Mul => self.process_mul(copy),
            _ => unreachable!(),
        }
    }

    fn open_single(&self) -> PNormalized {
        let (_, value) = self.expressions.iter().next().unwrap();
        value.set_coefficient(self.coefficient.clone())
    }

    fn open_many(&self) -> PNormalized {
        let items = self
            .expressions
            .iter()
            .map(|(_, e)| e.clone())
            .collect::<Vec<PNormalized>>();
        Rc::new(Mul::new(self.coefficient.clone(), items))
    }

    fn open_expressions(&self) -> PNormalized {
        match self.expressions.len() {
            0 => self.coefficient.as_norm(),
            1 => self.open_single(),
            _ => self.open_many(),
        }
    }
}

impl MulShortifier {
    pub fn new() -> MulShortifier {
        MulShortifier {}
    }

    fn process_denominators(
        &self,
        normalizer: &dyn INormalizer,
        numerator: PNormalized,
        denominators: Vec<PNormalized>,
    ) -> SolverResult<PNormalized> {
        let mut denom_root = denominators[0].clone();
        for value in denominators.iter().skip(1) {
            let expr = NormalizedBin::new(Operator::Mul, denom_root, value.clone());
            denom_root = normalizer.normalize_n_binary(&expr)?;
        }
        let div = Rc::new(Div::new(numerator, denom_root));
        normalizer.shortify(div)
    }

    fn process_sums(
        &self,
        normalizer: &dyn INormalizer,
        mut root: PNormalized,
        sums: Vec<PNormalized>,
    ) -> SolverResult<PNormalized> {
        for value in sums.iter() {
            let expr = NormalizedBin::new(Operator::Mul, root, value.clone());
            root = normalizer.normalize_n_binary(&expr)?;
        }
        Ok(root)
    }
}

impl IShortifier for MulShortifier {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Mul
    }

    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        value: PNormalized,
    ) -> SolverResult<PNormalized> {
        let mut sorted = Sorted::new();
        sorted.process_mul(value);
        if sorted.coefficient.eq_int(0) {
            return Ok(normalizer.constants().norm_zero.clone());
        }
        let mut root = sorted.open_expressions();
        if !sorted.denominators.is_empty() {
            let denominators = sorted.denominators;
            root = self.process_denominators(normalizer, root, denominators)?;
        }
        if !sorted.sums.is_empty() {
            self.process_sums(normalizer, root, sorted.sums)
        } else {
            Ok(root)
        }
    }
}

pub mod div_shortifier;
pub mod mul_shortifier;
pub mod root_shortifier;
pub mod sum_shortifier;

use crate::normalizer::multiply_controller::MultiplyController;
use crate::proto::normalizer::IShortifier;

pub fn create() -> Vec<Box<dyn IShortifier>> {
    let m_ctrl = Box::new(MultiplyController::new());
    vec![
        Box::new(sum_shortifier::SumShortifier::new()),
        Box::new(div_shortifier::DivShortifier::new(m_ctrl)),
        Box::new(mul_shortifier::MulShortifier::new()),
        Box::new(root_shortifier::RootShortifier::new()),
    ]
}

pub mod binary_reverser;
pub mod event_logger;
pub mod multiply_controller;
pub mod normalizer;
pub mod rules;
pub mod shortifiers;
#[cfg(test)]
mod tests;

pub use event_logger::EventLogger;
pub use multiply_controller::MultiplyController;
pub use normalizer::Normalizer;

use crate::proto::normalizer::IEventLogger;

pub fn create(log_rules: bool) -> Normalizer {
    let logger: Option<Box<dyn IEventLogger>> = if log_rules {
        Some(Box::new(EventLogger::new()))
    } else {
        None
    };
    Normalizer::new(
        rules::unary_rules(),
        rules::binary_rules(),
        Box::new(binary_reverser::BinaryReverser::new()),
        shortifiers::create(),
        logger,
    )
}

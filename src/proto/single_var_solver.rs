use std::collections::HashMap;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{ExpressionType, PExpression, PNormalized};
use crate::entities::solver::{Equation, Solution, SolutionOnlyResult};
use crate::number::Number;

pub trait IDerivativer {
    fn derivative(&self, fun: &PNormalized) -> PNormalized;

    fn all_derivatives(&self, fun: &PNormalized) -> Vec<PNormalized>;
}

pub type VarExprMap = HashMap<String, PExpression>;
pub type VarNormMap = HashMap<String, PNormalized>;

pub trait IReplacer {
    fn replace_vars_in_expr_with_flag(
        &self,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool);

    fn replace_vars_in_norm_with_flag(
        &self,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)>;

    fn replace_vars_in_expr(&self, expr: &PExpression, vars: &VarExprMap) -> PExpression {
        let (result, _) = self.replace_vars_in_expr_with_flag(expr, vars);
        result
    }

    fn replace_var_in_expr(
        &self,
        expr: &PExpression,
        var: &str,
        value: PExpression,
    ) -> PExpression {
        let mut vars = HashMap::new();
        vars.insert(var.to_string(), value);
        let (result, _) = self.replace_vars_in_expr_with_flag(expr, &vars);
        result
    }

    fn replace_vars_in_norm(
        &self,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<PNormalized> {
        let (result, _) = self.replace_vars_in_norm_with_flag(expr, vars)?;
        Ok(result)
    }
}

pub trait IReplacerRule {
    fn get_type(&self) -> ExpressionType;

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool);

    fn replace_norm(
        &self,
        _replacer: &dyn IReplacer,
        _expr: &PNormalized,
        _vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        unimplemented!("replace_norm is not implemented for {:?}", self.get_type())
    }
}

pub trait IDerivativeSolver {
    fn solve_at(&self, left_part: &PNormalized, from: Number, to: Number) -> Vec<Number>;
}

pub trait ICalculator {
    // Replace var with number and calculate
    // Works only for one variable
    fn calculate(&self, expr: &PNormalized, value: &Number) -> Number;
}

pub trait IPreprocessor {
    fn process(&self, fun: PNormalized) -> SolverResult<PNormalized>;
}

pub trait IValidator {
    fn validate_norm_eq(
        &self,
        equation: &Equation<PNormalized>,
        vars: &VarNormMap,
    ) -> SolverResult<bool>;

    fn validate_expr_eq(
        &self,
        equation: &Equation<PExpression>,
        vars: &VarExprMap,
    ) -> SolverResult<bool>;
}

pub trait ISolver {
    fn solve_expr(&self, equation: &PExpression) -> SolverResult<Solution>;

    fn solve_norm(
        &self,
        equation: &Equation<PNormalized>,
    ) -> SolverResult<SolutionOnlyResult>;
}

pub trait IConcreteEquationSolver {
    fn slug(&self) -> String;

    // right part is always 0
    fn match_equation_type(&self, fun: &PNormalized) -> bool;

    fn solve(
        &self,
        solver: &dyn ISolver,
        fun: &PNormalized,
    ) -> SolverResult<(Vec<Number>, Vec<String>, usize)>;
}

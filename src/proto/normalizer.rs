use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    BinaryOperation, Constants, ExpressionType, Operator, PExpression, PNormalized,
    UnaryOperation,
};
use crate::entities::normalizer::{Multipliers, NormalizedBin};

pub trait INormalizer {
    fn normalize(&self, expression: &PExpression) -> SolverResult<PNormalized>;

    fn constants(&self) -> &Constants;

    fn normalize_n_binary(&self, expr: &NormalizedBin) -> SolverResult<PNormalized>;

    fn normalize_binary(&self, expr: &BinaryOperation) -> SolverResult<PNormalized>;

    fn normalize_unary(&self, expr: &UnaryOperation) -> SolverResult<PNormalized>;

    fn shortify(&self, expr: PNormalized) -> SolverResult<PNormalized>;
}

pub trait INormalizerRuleUnary {
    fn slug(&self) -> String;

    fn operator(&self) -> Operator;

    fn match_arg(&self, cons: &Constants, expr: &PNormalized) -> bool;

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        expr: &PNormalized,
    ) -> SolverResult<PNormalized>;
}

pub trait INormalizerRuleBinary {
    fn slug(&self) -> String;

    fn priority(&self) -> usize;

    fn operator(&self) -> Operator;

    fn match_args(&self, cons: &Constants, a: &PNormalized, b: &PNormalized) -> bool;

    fn apply(
        &self,
        normalizer: &dyn INormalizer,
        a: &PNormalized,
        b: &PNormalized,
    ) -> SolverResult<PNormalized>;
}

pub trait IBinaryReverser {
    fn reverse(&self, expr: &NormalizedBin) -> Option<NormalizedBin>;
}

pub trait IMultiplyController {
    fn get_multipliers(&self, expr: &PNormalized) -> Multipliers;
    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        expr: &PNormalized,
        multipliers: Multipliers,
    ) -> SolverResult<PNormalized>;
}

pub trait IShortifier {
    fn get_type(&self) -> ExpressionType;
    fn shortify(
        &self,
        normalizer: &dyn INormalizer,
        value: PNormalized,
    ) -> SolverResult<PNormalized>;
}

pub trait IEventLogger {
    fn inc(&self);
    fn dec(&self);
    fn last_level(&self) -> Option<usize>;
    fn log_event(&self, event: String);
    fn get_log(&self) -> Vec<(usize, String)>;
    fn format_log(&self) -> String;
}

use crate::entities::errors::UIError;
use crate::entities::ui::Command;

pub trait ICommandsParser {
    fn get_command(&self) -> Result<Command, UIError>;
}

pub trait ICommandParser {
    fn command(&self) -> String;
    fn help_head(&self) -> String;
    fn help_body(&self) -> String;
    fn parse(&self, line: &[String]) -> Result<Command, UIError>;
}

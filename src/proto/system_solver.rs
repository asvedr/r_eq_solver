use crate::entities::errors::SolverResult;
use crate::entities::expression::{ExpressionType, PExpression, PNormalized};
use crate::entities::solver::{
    Equation, EquationComplexity, EquationExtractorRuleResult, ExtractedVar,
    PreparedEquations, SystemSolutionVariant,
};
use crate::number::Number;
use std::collections::{HashMap, HashSet};

pub type NEquation = Equation<PNormalized>;

pub trait IPolynomVarExtractor {
    fn extract(&self, var: &str, polynom: PNormalized) -> SolverResult<Vec<PNormalized>>;

    // WARNING: target var MUST NOT be in the right part of equation
    fn extract_equation(
        &self,
        var: &str,
        equation: Equation<PNormalized>,
    ) -> SolverResult<Vec<PNormalized>>;
}

/*
    x + y = 2
    x = 2 - y
    return (2 - y)
*/
pub trait IEquationVarExtractorRule {
    fn get_type(&self) -> ExpressionType;
    fn apply(
        &self,
        extractor: &dyn IPolynomVarExtractor,
        var: &str,
        equation: NEquation,
    ) -> SolverResult<EquationExtractorRuleResult>;
}

pub trait IComplexityEstimator {
    fn estimate(&self, expression: &PNormalized) -> EquationComplexity;
    fn estimate_vec(&self, expressions: &[PNormalized]) -> EquationComplexity;
}

pub trait ISystemValidator {
    // Check that full solution completely works on all the equations
    fn validate_full(
        &self,
        prepared: &PreparedEquations,
        vars: &HashMap<String, Number>,
    ) -> SolverResult<bool>;

    // May be used when `vars` is incompleted. Just check that current solution
    // doesn't make NaN situation
    fn validate_partial(
        &self,
        prepared: &PreparedEquations,
        vars: &HashMap<String, Number>,
    ) -> SolverResult<bool>;
}
/*
    x + y - 2 = 0, x - y - 1 = 0
    A = x + y - 2
    B = x - y - 1
    extract("x", A, B) => [A + B*1]; extract(x) => Some(3/2)
    It's mean that "x = 3/2"
*/
pub trait IAdditionalExtractor {
    fn extract(
        &self,
        var: &str,
        a: &PNormalized,
        b: &PNormalized,
        var_include: &HashMap<String, HashSet<String>>,
    ) -> SolverResult<ExtractedVar<PNormalized>>;
}

pub trait IResearcher {
    fn research(
        &self,
        prepared: &PreparedEquations,
    ) -> SolverResult<Vec<SystemSolutionVariant>>;
}

pub trait ISystemSolver {
    fn solve(
        &self,
        equations: Vec<PExpression>,
    ) -> SolverResult<Vec<SystemSolutionVariant>>;
}

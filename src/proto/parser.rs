use crate::entities::errors::ParserError;
use crate::entities::expression::PExpression;
use crate::entities::parser::{State, Token, TokenType};

pub trait IStateMachineRule {
    fn token_type(&self) -> TokenType;
    fn apply(&self, state: State, symbol: char) -> State;
}

pub trait ILexer {
    fn next_token(
        &self,
        text: &[char],
        begin: usize,
    ) -> Result<Option<(usize, Token)>, ParserError>;
}

pub trait IParser {
    fn parse(&self, text: &str) -> Result<PExpression, ParserError>;
}

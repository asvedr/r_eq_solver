mod linear;
mod separator;
mod square;
mod var_eq_val;

pub use crate::proto::single_var_solver::IConcreteEquationSolver;

pub fn solvers() -> Vec<Box<dyn IConcreteEquationSolver>> {
    vec![
        Box::new(linear::Solver::new()),
        Box::new(var_eq_val::Solver::new()),
        Box::new(square::Solver::new()),
        Box::new(separator::Solver::new()),
    ]
}

use num_bigint::BigInt;

use crate::entities::constants::SQUARE_COMPLEXITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, PNormalized, Sum, Var};
use crate::number::Number;
use crate::proto::single_var_solver::{IConcreteEquationSolver, ISolver};
use crate::single_var_solver::utils::get_pow_values_of_sum;

pub struct Solver {}

impl Solver {
    pub fn new() -> Solver {
        Solver {}
    }

    fn full_solution(&self, a: &Var, b: &Var, c: &Number) -> Vec<Number> {
        let two = Number::integer(2);
        let four = Number::integer(4);
        let d = (&b.coefficient * &b.coefficient) - (four * (&a.coefficient * c));
        // -b +- sqrt(d) / 2a
        if d.is_neg() {
            return vec![];
        }
        let sqrt_d = d.pow(&Number::fraction(1, 2));
        let x1 = (&sqrt_d - &b.coefficient) / (&two * &a.coefficient);
        let x2 = (-(&b.coefficient + &sqrt_d)) / (&two * &a.coefficient);
        if x1 == x2 {
            vec![x1]
        } else {
            vec![x1, x2]
        }
    }

    fn simple_solution(&self, a: &Var, c: &Number) -> Vec<Number> {
        let num = -(c / &a.coefficient);
        if num.is_neg() {
            return vec![];
        }
        let mut roots = vec![num.pow(&Number::fraction(1, 2))];
        if !roots[0].eq_int(0) {
            let another = -&roots[0];
            roots.push(another)
        }
        roots
    }

    fn get_var<'a>(&self, items: &'a [PNormalized], exp: isize) -> Option<&'a Var> {
        let exp: BigInt = exp.into();
        for item in items {
            if let Some(var) = cast_norm::<Var>(item) {
                if var.pow == exp {
                    return Some(var);
                }
            }
        }
        None
    }

    fn get_num(&self, items: &[PNormalized]) -> Number {
        for item in items {
            if let Some(num) = cast_norm::<Number>(item) {
                return num.clone();
            }
        }
        Number::integer(0)
    }
}

impl IConcreteEquationSolver for Solver {
    fn slug(&self) -> String {
        "square".to_string()
    }

    fn match_equation_type(&self, left_part: &PNormalized) -> bool {
        if let Some(sum) = cast_norm::<Sum>(left_part) {
            let pow_values = get_pow_values_of_sum(sum);
            pow_values.iter().max() == Some(&(2).into())
        } else {
            false
        }
    }

    fn solve(
        &self,
        _: &dyn ISolver,
        fun: &PNormalized,
    ) -> SolverResult<(Vec<Number>, Vec<String>, usize)> {
        let sum = cast_norm::<Sum>(fun).unwrap();
        let a = self.get_var(&sum.items, 2).unwrap();
        let c = self.get_num(&sum.items);
        let roots = if let Some(b) = self.get_var(&sum.items, 1) {
            self.full_solution(a, b, &c)
        } else {
            self.simple_solution(a, &c)
        };
        Ok((roots, vec![self.slug()], SQUARE_COMPLEXITY))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entities::expression::{IExpression, PExpression, Sum};
    use crate::entities::solver::{Equation, Solution, SolutionOnlyResult};

    struct MockSolver {}
    impl ISolver for MockSolver {
        fn solve_expr(&self, _: &PExpression) -> SolverResult<Solution> {
            panic!()
        }
        fn solve_norm(
            &self,
            _: &Equation<PNormalized>,
        ) -> SolverResult<SolutionOnlyResult> {
            panic!()
        }
    }

    #[test]
    fn test_square_detect_and_solve_a_c() {
        let solver = Solver::new();
        let mock = MockSolver {};
        let left = Sum::new(vec![
            Var::new(2, Number::integer(2), format!("x")).as_norm(),
            Number::integer(-50).as_norm(),
        ])
        .as_norm();
        assert!(solver.match_equation_type(&left));
        let (val, _, _) = solver.solve(&mock, &left).unwrap();
        assert_eq!(val.len(), 2);
        assert!(val[0].eq_int(5));
        assert!(val[1].eq_int(-5));
    }

    #[test]
    fn test_square_detect_and_solve_a_b_c() {
        let solver = Solver::new();
        let mock = MockSolver {};
        let left = Sum::new(vec![
            Var::new(1, Number::integer(-5), format!("x")).as_norm(),
            Var::new(2, Number::integer(1), format!("x")).as_norm(),
            Number::fraction(225, 100).as_norm(),
        ])
        .as_norm();
        assert!(solver.match_equation_type(&left));
        let (val, _, _) = solver.solve(&mock, &left).unwrap();
        assert_eq!(val.len(), 2);
        assert!(val[0] == Number::fraction(45, 10));
        assert!(val[1] == Number::fraction(1, 2));
    }

    #[test]
    fn test_square_detect_and_solve_a_b() {
        let solver = Solver::new();
        let mock = MockSolver {};
        let left = Sum::new(vec![
            Var::new(1, Number::integer(2), format!("x")).as_norm(),
            Var::new(2, Number::integer(1), format!("x")).as_norm(),
        ])
        .as_norm();

        assert!(solver.match_equation_type(&left));
        let (val, _, _) = solver.solve(&mock, &left).unwrap();
        assert_eq!(val.len(), 2);
        assert!(val[1].eq_int(-2));
        assert!(val[0].eq_int(0));
    }

    #[test]
    fn test_square_detect_false() {
        let solver = Solver::new();
        let left = Sum::new(vec![
            Var::new(1, Number::integer(2), format!("x")).as_norm(),
            Number::integer(12).as_norm(),
        ])
        .as_norm();
        assert!(!solver.match_equation_type(&left));

        let left = Sum::new(vec![
            Var::new(-1, Number::integer(2), format!("x")).as_norm(),
            Number::integer(12).as_norm(),
        ])
        .as_norm();
        assert!(!solver.match_equation_type(&left));
    }
}

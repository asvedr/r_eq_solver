use num_bigint::{BigInt, Sign};
use num_integer::Integer;

use crate::entities::constants::LINEAR_COMPLEXITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, ExpressionType, PNormalized, Sum, Var};
use crate::number::Number;
use crate::proto::single_var_solver::{IConcreteEquationSolver, ISolver};

pub struct Solver {}

impl Solver {
    pub fn new() -> Solver {
        Solver {}
    }

    fn solution_even(&self, exp: BigInt, num: Number) -> Vec<Number> {
        if num < Number::integer(0) {
            return vec![];
        }
        let a = num.pow(&Number::fraction(1, exp));
        let b = -&a;
        vec![a, b]
    }

    fn solution_odd(&self, exp: BigInt, num: Number) -> Vec<Number> {
        vec![num.pow(&Number::fraction(1, exp))]
    }
}

impl IConcreteEquationSolver for Solver {
    fn slug(&self) -> String {
        "var=val".to_string()
    }

    fn match_equation_type(&self, left_part: &PNormalized) -> bool {
        let sum = match cast_norm::<Sum>(left_part) {
            Some(sum) => sum,
            _ => return false,
        };
        if sum.items.len() != 2 {
            return false;
        }
        matches!(
            (sum.items[0].get_type(), sum.items[1].get_type()),
            (ExpressionType::Var, ExpressionType::Number)
        )
    }

    fn solve(
        &self,
        _: &dyn ISolver,
        fun: &PNormalized,
    ) -> SolverResult<(Vec<Number>, Vec<String>, usize)> {
        let sum = cast_norm::<Sum>(fun).unwrap();
        let var = cast_norm::<Var>(&sum.items[0]).unwrap();
        let mut num = -cast_norm::<Number>(&sum.items[1]).unwrap();
        num = &num / &var.coefficient;
        let mut exp: BigInt = var.pow.clone();
        if exp.sign() == Sign::Minus {
            num = Number::integer(1) / num;
            exp = -exp;
        }
        let solution;
        if exp.is_even() {
            solution = self.solution_even(exp, num)
        } else {
            solution = self.solution_odd(exp, num)
        }
        Ok((solution, vec![self.slug()], LINEAR_COMPLEXITY))
    }
}

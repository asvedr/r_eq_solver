use num_bigint::{BigInt, Sign};

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, IExpression, PExpression, PNormalized, Sum, Var,
};
use crate::number::Number;
use crate::proto::single_var_solver::{IConcreteEquationSolver, ISolver};
use crate::single_var_solver::utils::get_pow_values_of_sum;
use crate::single_var_solver::utils::{add, eq};

pub struct Solver {
    zero: PExpression,
}

impl Solver {
    pub fn new() -> Solver {
        Solver {
            zero: Number::integer(0).as_expr(),
        }
    }

    fn as_eq(&self, left: PExpression) -> PExpression {
        eq(left, self.zero.clone())
    }
}

impl IConcreteEquationSolver for Solver {
    fn slug(&self) -> String {
        "separate".to_string()
    }

    fn match_equation_type(&self, left_part: &PNormalized) -> bool {
        if let Some(sum) = cast_norm::<Sum>(left_part) {
            if let Some(val) = get_pow_values_of_sum(sum).iter().min() {
                return matches!(val.sign(), Sign::Plus);
            }
        }
        false
    }

    fn solve(
        &self,
        solver: &dyn ISolver,
        fun: &PNormalized,
    ) -> SolverResult<(Vec<Number>, Vec<String>, usize)> {
        let sum = cast_norm::<Sum>(fun).unwrap();
        let min_pow: BigInt = get_pow_values_of_sum(sum).iter().min().unwrap().clone();
        let mut b = self.zero.clone();
        for item in sum.items.iter() {
            let var = cast_norm::<Var>(item).unwrap();
            if var.pow == min_pow {
                b = add(b, var.coefficient.as_expr());
            } else {
                let new_var = Var::new(
                    &var.pow - &min_pow,
                    var.coefficient.clone(),
                    var.name.clone(),
                );
                b = add(b, new_var.as_expr());
            }
        }
        // var name doesn't matter anything
        let a = Var::new(min_pow, Number::integer(1), "x".to_string()).as_expr();
        let solution_a = solver.solve_expr(&self.as_eq(a))?;
        let solution_b = solver.solve_expr(&self.as_eq(b))?;
        let mut methods = vec![self.slug()];
        methods.extend(solution_a.used_methods);
        methods.extend(solution_b.used_methods);
        let complexity = solution_a.complexity.max(solution_b.complexity);
        let mut roots = solution_a.roots;
        'toploop: for x1 in solution_b.roots.iter() {
            for x2 in roots.iter() {
                if x1 == x2 {
                    continue 'toploop;
                }
            }
            roots.push(x1.clone())
        }
        Ok((roots, methods, complexity))
    }
}

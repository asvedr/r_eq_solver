use num_bigint::{BigInt, Sign};
use num_traits::identities::{One, Zero};

use crate::entities::constants::LINEAR_COMPLEXITY;
use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, ExpressionType, PNormalized, Sum, Var};
use crate::number::Number;
use crate::proto::single_var_solver::{IConcreteEquationSolver, ISolver};
use crate::single_var_solver::utils::get_pow_values_of_sum;

pub struct Solver {}

impl Solver {
    pub fn new() -> Solver {
        Solver {}
    }

    fn solution_var(&self, var: &Var) -> Vec<Number> {
        if matches!(var.pow.sign(), Sign::Minus) {
            vec![]
        } else {
            vec![Number::integer(0)]
        }
    }

    fn solution_var_eq_num(&self, var: &Var, num: &Number) -> Vec<Number> {
        vec![&(-num) / &var.coefficient]
    }
}

impl IConcreteEquationSolver for Solver {
    fn slug(&self) -> String {
        "linear".to_string()
    }

    fn match_equation_type(&self, left_part: &PNormalized) -> bool {
        if let Some(sum) = cast_norm::<Sum>(left_part) {
            let pow_values = get_pow_values_of_sum(sum);
            pow_values == [BigInt::one(), BigInt::zero()]
        } else {
            left_part.get_type() == ExpressionType::Var
        }
    }

    fn solve(
        &self,
        _: &dyn ISolver,
        fun: &PNormalized,
    ) -> SolverResult<(Vec<Number>, Vec<String>, usize)> {
        let roots = if let Some(var) = cast_norm::<Var>(fun) {
            self.solution_var(var)
        } else {
            let sum = cast_norm::<Sum>(fun).unwrap();
            self.solution_var_eq_num(
                cast_norm::<Var>(&sum.items[0]).unwrap(),
                cast_norm::<Number>(&sum.items[1]).unwrap(),
            )
        };
        Ok((roots, vec![self.slug()], LINEAR_COMPLEXITY))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entities::expression::{IExpression, PExpression, Sum};
    use crate::entities::solver::{Equation, Solution, SolutionOnlyResult};

    struct MockSolver {}
    impl ISolver for MockSolver {
        fn solve_expr(&self, _: &PExpression) -> SolverResult<Solution> {
            panic!()
        }
        fn solve_norm(
            &self,
            _: &Equation<PNormalized>,
        ) -> SolverResult<SolutionOnlyResult> {
            panic!()
        }
    }

    #[test]
    fn test_linear_detect_and_solve() {
        let solver = Solver::new();
        let mock = MockSolver {};
        let left = Sum {
            items: vec![
                Var::new(1, Number::integer(2), format!("x")).as_norm(),
                Number::integer(12).as_norm(),
            ],
        }
        .as_norm();
        assert!(solver.match_equation_type(&left));
        let (val, _, _) = solver.solve(&mock, &left).unwrap();
        assert_eq!(val.len(), 1);
        assert!(val[0].eq_int(-6));

        let left = Var::new(3, Number::integer(2), format!("x")).as_norm();
        assert!(solver.match_equation_type(&left));
        let (val, _, _) = solver.solve(&mock, &left).unwrap();
        assert_eq!(val.len(), 1);
        assert!(val[0].eq_int(0));

        let left = Var::new(-3, Number::integer(2), format!("x")).as_norm();
        assert!(solver.match_equation_type(&left));
        let (val, _, _) = solver.solve(&mock, &left).unwrap();
        assert_eq!(val.len(), 0);
    }

    #[test]
    fn test_linear_detect_false() {
        let solver = Solver::new();

        let left = Sum {
            items: vec![
                Var::new(2, Number::integer(2), format!("x")).as_norm(),
                Number::integer(12).as_norm(),
            ],
        }
        .as_norm();
        assert!(!solver.match_equation_type(&left));

        let left = Sum {
            items: vec![
                Var::new(-1, Number::integer(2), format!("x")).as_norm(),
                Number::integer(12).as_norm(),
            ],
        }
        .as_norm();
        assert!(!solver.match_equation_type(&left));
    }
}

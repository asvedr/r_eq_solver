use std::rc::Rc;

use num_bigint::{BigInt, Sign};
use num_traits::identities::One;

use crate::entities::expression::{
    cast_norm, ExpressionType, IExpression, PNormalized, Sum, Var,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::IDerivativer;
use crate::single_var_solver::utils::{add, get_pow_values_of_sum, mul, pow};

pub struct Derivativer {
    normalizer: Rc<dyn INormalizer>,
}

impl Derivativer {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Derivativer {
        Derivativer { normalizer }
    }

    fn process_sum(&self, sum: &Sum) -> PNormalized {
        let mut root = self.normalizer.constants().expr_zero.clone();
        for item in &sum.items {
            let d = self.derivative(item);
            root = add(root, d.as_expr());
        }
        self.normalizer.normalize(&root).unwrap()
    }

    fn process_var(&self, var: &Var) -> PNormalized {
        let coeff = mul(
            var.coefficient.as_expr(),
            Number::integer(var.pow.clone()).as_expr(),
        );
        let one = BigInt::one();
        let pure_var =
            Var::new(one.clone(), Number::integer(1), var.name.clone()).as_expr();
        let power = Number::integer(&var.pow - one).as_expr();
        let expr = mul(coeff, pow(pure_var, power));
        self.normalizer.normalize(&expr).unwrap()
    }

    fn validate_finite_derivative_sequence(&self, fun: &PNormalized) {
        let sum = if let Some(sum) = cast_norm::<Sum>(fun) {
            sum
        } else {
            return;
        };
        let values = get_pow_values_of_sum(sum);
        for i in values {
            if matches!(i.sign(), Sign::Minus) {
                panic!("can not build derivative sequence if pow is {}", i)
            }
        }
    }
}

impl IDerivativer for Derivativer {
    fn derivative(&self, fun: &PNormalized) -> PNormalized {
        match fun.get_type() {
            ExpressionType::Sum => self.process_sum(cast_norm::<Sum>(fun).unwrap()),
            ExpressionType::Var => self.process_var(cast_norm::<Var>(fun).unwrap()),
            ExpressionType::Number => self.normalizer.constants().norm_zero.clone(),
            bad_type => panic!("derivative for {:?} is not implemented", bad_type),
        }
    }

    fn all_derivatives(&self, fun: &PNormalized) -> Vec<PNormalized> {
        self.validate_finite_derivative_sequence(fun);
        let mut result = Vec::new();
        let mut current = fun.clone();
        loop {
            let derivative = self.derivative(&current);
            result.push(current);
            if derivative.get_type() == ExpressionType::Number {
                break;
            }
            current = derivative;
        }
        result
    }
}

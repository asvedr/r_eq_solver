use std::rc::Rc;

use crate::entities::expression::{IExpression, Sum, Var};
use crate::normalizer;
use crate::number::Number;
use crate::proto::single_var_solver::IDerivativer;
use crate::single_var_solver::derivativer::Derivativer;

#[test]
fn test_single_derivative() {
    let fun = Sum {
        items: vec![
            Var::new(1, Number::integer(1), format!("x")).as_norm(),
            Var::new(2, Number::integer(2), format!("x")).as_norm(),
            Var::new(3, Number::integer(1), format!("x")).as_norm(),
        ],
    }
    .as_norm();
    let derivativer = Derivativer::new(Rc::new(normalizer::create(true)));
    let d = derivativer.derivative(&fun);
    assert_eq!(d.format(), format!("(4x + 3x^2 + 1)"));
}

#[test]
fn test_all_derivatives() {
    let fun = Sum {
        items: vec![
            Var::new(1, Number::integer(1), format!("x")).as_norm(),
            Var::new(2, Number::integer(2), format!("x")).as_norm(),
            Var::new(3, Number::integer(1), format!("x")).as_norm(),
        ],
    }
    .as_norm();
    let derivativer = Derivativer::new(Rc::new(normalizer::create(true)));
    let derivatives = derivativer.all_derivatives(&fun);
    let texts = derivatives
        .iter()
        .map(|d| d.format())
        .collect::<Vec<String>>();
    assert_eq!(
        texts,
        vec![
            format!("(x + 2x^2 + x^3)"),
            format!("(4x + 3x^2 + 1)"),
            format!("(6x + 4)"),
        ],
    );
}

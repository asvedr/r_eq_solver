use std::rc::Rc;

use crate::entities::expression::{
    BinaryOperation, Div, IExpression, Mul, Operator, Root, Sum, UnaryOperation, Var,
};
use crate::normalizer;
use crate::number::Number;
use crate::proto::single_var_solver::IReplacer;
use crate::single_var_solver::replacer::Replacer;
use crate::single_var_solver::replacer_rules;
use std::collections::HashMap;

fn create() -> Replacer {
    let normalizer = Rc::new(normalizer::create(false));
    Replacer::new(replacer_rules::rules(normalizer))
}

#[test]
fn test_replace_num() {
    let replacer = create();
    let a = Number::integer(3).as_expr();
    let result = replacer.replace_var_in_expr(&a, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), a.to_string());
}

#[test]
fn test_replace_var_yes() {
    let replacer = create();
    let a = Var::new(2, Number::integer(3), format!("x")).as_expr();
    let result = replacer.replace_var_in_expr(&a, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "(3 Mul (1 Pow 2))");
}

#[test]
fn test_replace_var_no() {
    let replacer = create();
    let a = Var::new(2, Number::integer(3), format!("y")).as_expr();
    let result = replacer.replace_var_in_expr(&a, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "3y^2");
}

#[test]
fn test_replace_bin_yes() {
    let replacer = create();
    let expr = BinaryOperation::new(
        Operator::Add,
        Var::new(2, Number::integer(3), format!("x")).as_expr(),
        Var::new(2, Number::integer(3), format!("y")).as_expr(),
    )
    .as_expr();
    let result = replacer.replace_var_in_expr(&expr, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "((3 Mul (1 Pow 2)) Add 3y^2)");
}

#[test]
fn test_replace_un_yes() {
    let replacer = create();
    let expr = UnaryOperation::new(
        Operator::Neg,
        Var::new(2, Number::integer(3), format!("x")).as_expr(),
    )
    .as_expr();
    let result = replacer.replace_var_in_expr(&expr, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "(Neg (3 Mul (1 Pow 2)))");
}

#[test]
fn test_replace_div_no() {
    let replacer = create();
    let expr = Div {
        top: Var::new(2, Number::integer(3), format!("x")).as_norm(),
        bottom: Var::new(2, Number::integer(3), format!("y")).as_norm(),
    }
    .as_expr();
    let result = replacer.replace_var_in_expr(&expr, "z", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "DIV[3x^2,3y^2]");
}

#[test]
fn test_replace_div_yes() {
    let replacer = create();
    let expr = Div {
        top: Var::new(2, Number::integer(3), format!("x")).as_norm(),
        bottom: Var::new(2, Number::integer(3), format!("y")).as_norm(),
    }
    .as_expr();
    let result = replacer.replace_var_in_expr(&expr, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "((3 Mul (1 Pow 2)) Div 3y^2)");
}

#[test]
fn test_replace_sum_no() {
    let replacer = create();
    let expr = Sum {
        items: vec![
            Var::new(2, Number::integer(3), format!("x")).as_norm(),
            Var::new(2, Number::integer(3), format!("y")).as_norm(),
        ],
    }
    .as_expr();
    let result = replacer.replace_var_in_expr(&expr, "z", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "SUM[3x^2,3y^2,]");
}

#[test]
fn test_replace_sum_yes() {
    let replacer = create();
    let expr = Sum {
        items: vec![
            Var::new(2, Number::integer(3), format!("x")).as_norm(),
            Var::new(2, Number::integer(3), format!("y")).as_norm(),
        ],
    }
    .as_expr();
    let result = replacer.replace_var_in_expr(&expr, "x", Number::integer(1).as_expr());
    assert_eq!(result.to_string(), "((3 Mul (1 Pow 2)) Add 3y^2)");
}

#[test]
fn test_replace_in_norm() {
    //  root(x)*root(z) + 3y^2 + 2                     2*root(z) + 77
    // ----------------------------  => (x=4, y=5) => ----------------
    //       root(z + 7)                                  root(z+7)

    //  root(x)*root(z) + 3y^2 + 2                         6+77
    // ----------------------------  => (x=4, y=5, z=9) => ---- => 20(3/4)
    //       root(z + 7)                                    4
    let replacer = create();
    let one = Number::integer(1);
    let x = Var::new(1, one.clone(), "x".to_string()).as_norm();
    let z = Var::new(1, one.clone(), "z".to_string()).as_norm();
    let rx = Root::new(1, 2, one.clone(), x.clone()).as_norm();
    let rz = Root::new(1, 2, one.clone(), z.clone()).as_norm();
    let rmul = Mul::two(rx, rz).as_norm();
    let y32 = Var::new(2, Number::integer(3), "y".to_string()).as_norm();
    let sum_top = Sum::new(vec![rmul, y32, Number::integer(2).as_norm()]).as_norm();
    let seven = Number::integer(7);
    let sum_under_root = Sum::new(vec![z, seven.as_norm()]).as_norm();
    let bottom = Root::new(1, 2, one, sum_under_root).as_norm();
    let div = Div::new(sum_top, bottom).as_norm();

    let mut vars = HashMap::new();
    vars.insert("x".to_string(), Number::integer(4).as_norm());
    vars.insert("y".to_string(), Number::integer(5).as_norm());
    let result = replacer.replace_vars_in_norm(&div, &vars).unwrap();
    assert_eq!(result.format(), "((2*[z]^(1/2) + 77) / [(z + 7)]^(1/2))");

    vars.insert("z".to_string(), Number::integer(9).as_norm());
    let result = replacer.replace_vars_in_norm(&div, &vars).unwrap();
    assert_eq!(result.format(), "20(3/4)");
}

use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::constants::{GIANT_NUMBERS, LINEAR_COMPLEXITY, SQUARE_COMPLEXITY};
use crate::entities::expression::{
    BinaryOperation, IExponented, IExpression, Operator, PNormalized, Root, Sum, Var,
};
use crate::entities::solver::Equation;
use crate::normalizer;
use crate::number::Number;
use crate::proto::single_var_solver::{IDerivativeSolver, ISolver};
use crate::single_var_solver::replacer_rules;
use crate::single_var_solver::solver::Solver;
use crate::single_var_solver::validator::Validator;
use crate::single_var_solver::{concrete_solvers, preprocessors, replacer};

struct DerivativeSolver {}

impl IDerivativeSolver for DerivativeSolver {
    fn solve_at(&self, _: &PNormalized, _: Number, _: Number) -> Vec<Number> {
        panic!()
    }
}

fn create(log_rules: bool) -> Solver {
    let norm = Rc::new(normalizer::create(false));
    let repl_rules = replacer_rules::rules(norm.clone());
    let repl = replacer::Replacer::new(repl_rules);
    let validator = Validator::new(norm.clone(), Box::new(repl));
    Solver::new(
        Rc::new(Config::new()),
        Rc::new(normalizer::create(log_rules)),
        concrete_solvers::solvers(),
        preprocessors::create(norm.clone()),
        Box::new(DerivativeSolver {}),
        Box::new(validator),
    )
}

#[test]
fn test_detect_and_solve_linear() {
    let left = BinaryOperation::new(
        Operator::Mul,
        Number::integer(2).as_expr(),
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
    )
    .as_expr();
    let right = Number::integer(8).as_expr();
    let source = BinaryOperation::new(Operator::Eql, left, right).as_expr();
    let solver = create(true);
    let solution = solver.solve_expr(&source).unwrap();
    assert_eq!(solution.complexity, LINEAR_COMPLEXITY);
    assert_eq!(
        format!(
            "{} = {}",
            solution.normalized_equation.left.format(),
            solution.normalized_equation.right.format()
        ),
        "(2x + -8) = 0",
    );
    assert_eq!(solution.used_methods, vec![format!("linear")]);
    assert_eq!(solution.roots, vec![Number::integer(4)]);
}

#[test]
fn test_detect_and_solve_square() {
    let left = BinaryOperation::new(
        Operator::Sub,
        BinaryOperation::new(
            Operator::Mul,
            Var::new(1, Number::integer(1), format!("x")).as_expr(),
            Var::new(1, Number::integer(1), format!("x")).as_expr(),
        )
        .as_expr(),
        BinaryOperation::new(
            Operator::Mul,
            Number::integer(3).as_expr(),
            Var::new(1, Number::integer(1), format!("x")).as_expr(),
        )
        .as_expr(),
    )
    .as_expr();
    let right = Number::integer(-2).as_expr();
    let eq = BinaryOperation::new(Operator::Eql, left, right).as_expr();
    let solver = create(true);
    let solution = solver.solve_expr(&eq).unwrap();
    assert_eq!(solution.complexity, SQUARE_COMPLEXITY);
    assert_eq!(
        format!(
            "{} = {}",
            solution.normalized_equation.left.format(),
            solution.normalized_equation.right.format()
        ),
        "(-3x + x^2 + 2) = 0",
    );
    assert_eq!(solution.used_methods, vec![format!("square")]);
    assert_eq!(solution.roots, vec![Number::integer(2), Number::integer(1)]);
}

#[test]
fn test_separate_solver() {
    // "x^3 + 2*x^2 + x = 0"
    let left = BinaryOperation::new(
        Operator::Add,
        BinaryOperation::new(
            Operator::Add,
            Var::new(3, Number::integer(1), format!("x")).as_expr(),
            Var::new(2, Number::integer(2), format!("x")).as_expr(),
        )
        .as_expr(),
        Var::new(1, Number::integer(1), format!("x")).as_expr(),
    )
    .as_expr();
    let zero = Number::integer(0).as_expr();
    let eq = BinaryOperation::new(Operator::Eql, left, zero).as_expr();
    let solver = create(true);
    let solution = solver.solve_expr(&eq).unwrap();
    assert_eq!(solution.complexity, SQUARE_COMPLEXITY);
    assert_eq!(
        format!(
            "{} = {}",
            solution.normalized_equation.left.format(),
            solution.normalized_equation.right.format()
        ),
        "(x + 2x^2 + x^3) = 0",
    );
    assert_eq!(
        solution.used_methods,
        vec![format!("separate"), format!("linear"), format!("square")]
    );
    assert_eq!(
        solution.roots,
        vec![Number::integer(0), Number::integer(-1)]
    );
}

#[test]
fn test_simple_solution_with_root() {
    // (x^(1/2))^4 - 2^4 = 0 => x = 4
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let r = Root::new(4, 2, Number::integer(1), x.as_norm());
    let items = vec![r.as_norm(), Number::integer(-16).as_norm()];
    let sum = Sum::new(items).as_norm();
    let eq = Equation {
        left: sum,
        right: Number::integer(0).as_norm(),
    };
    let solver = create(true);
    let solution = solver.solve_norm(&eq).unwrap();
    assert_eq!(solution.complexity, LINEAR_COMPLEXITY);
    assert_eq!(solution.roots, vec![Number::integer(4)]);
}

#[test]
fn test_simple_solution_irrational() {
    // x^2 = 7
    let x = Var::new(2, Number::integer(1), "x".to_string());
    let items = vec![x.as_norm(), Number::integer(-7).as_norm()];
    let sum = Sum::new(items).as_norm();
    let eq = Equation {
        left: sum,
        right: Number::integer(0).as_norm(),
    };
    let solver = create(true);
    let solution = solver.solve_norm(&eq).unwrap();
    assert_eq!(solution.complexity, GIANT_NUMBERS);
    let val = Number::fraction(2645751311_u64, 1000000000_u64);
    assert_eq!(solution.roots, vec![val.clone(), -&val]);
}

#[test]
fn test_shortify_root_rule() {
    // (x ^ 1/2) ^ 2 = 5 => x = 5
    // (x ^ 1/2) ^ 2 = 5 => no solutions
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let root = Root::new(2, 2, Number::integer(1), x.as_norm()).as_norm();
    let eq = Equation {
        left: root.clone(),
        right: Number::integer(5).as_norm(),
    };
    let solver = create(true);
    let solution = solver.solve_norm(&eq).unwrap();
    assert_eq!(solution.roots, vec![Number::integer(5)]);
    let eq = Equation {
        left: root,
        right: Number::integer(-5).as_norm(),
    };
    let solver = create(true);
    let solution = solver.solve_norm(&eq).unwrap();
    assert_eq!(solution.roots, vec![]);
}

#[test]
fn test_squad_with_root() {
    // x^2 + x + 3 = 5 => [1,-2]
    // x^2 + (x ^ 1/2) ^ 2 + 3 = 5 => [1]
    let x = Var::new(1, Number::integer(1), "x".to_string());
    let root = Root::new(2, 2, Number::integer(1), x.as_norm()).as_norm();
    let sum_items = vec![
        x.set_exponent((2).into()),
        root,
        Number::integer(3).as_norm(),
    ];
    let eq = Equation {
        left: Sum::new(sum_items).as_norm(),
        right: Number::integer(5).as_norm(),
    };
    let solver = create(true);
    let solution = solver.solve_norm(&eq).unwrap();
    assert_eq!(solution.roots, vec![Number::integer(1)]);
}

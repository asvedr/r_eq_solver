use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::expression::{BinaryOperation, IExpression, Operator, Sum, Var};
use crate::normalizer;
use crate::number::Number;
use crate::proto::single_var_solver::{IDerivativeSolver, ISolver};
use crate::single_var_solver::replacer_rules;
use crate::single_var_solver::validator::Validator;
use crate::single_var_solver::{
    calculator, derivative_solver, derivativer, preprocessors,
};
use crate::single_var_solver::{concrete_solvers, replacer, solver};

fn create() -> derivative_solver::DerivativeSolver {
    let normalizer = Rc::new(normalizer::create(false));
    let derivativer = derivativer::Derivativer::new(normalizer);
    derivative_solver::DerivativeSolver::new(
        Box::new(derivativer),
        Box::new(calculator::Calculator::new()),
    )
}

fn create_full(config: Config) -> solver::Solver {
    let normalizer = Rc::new(normalizer::create(false));
    let repl_rules = replacer_rules::rules(normalizer.clone());
    let repl = replacer::Replacer::new(repl_rules);
    let validator = Validator::new(normalizer.clone(), Box::new(repl));
    solver::Solver::new(
        Rc::new(config),
        normalizer.clone(),
        concrete_solvers::solvers(),
        preprocessors::create(normalizer.clone()),
        Box::new(create()),
        Box::new(validator),
    )
}

/*
    x^3 + 8x^2 - 5 = 0 interval (-7; 2)
    -8 < x1 < -7.6
    -1 < x2 < -0.5
    0.5 < x3 < 1
    Тут и первая производная и функция будут иметь положительные знаки
    на обоих концах отрезка. Хотя на отрезке есть корни
*/
#[test]
fn test_inverval_with_plus_f_and_fi() {
    let solver = create();
    let fun = Sum::new(vec![
        Var::new(2, Number::integer(8), format!("x")).as_norm(),
        Var::new(3, Number::integer(1), format!("x")).as_norm(),
        Number::integer(-5).as_norm(),
    ])
    .as_norm();
    let roots = solver.solve_at(&fun, Number::integer(-7), Number::integer(2));
    assert_eq!(roots.len(), 2);
    assert!(roots[0] > Number::integer(-1) && roots[0] < Number::rational(-0.7));
    assert!(roots[1] > Number::rational(0.6) && roots[1] < Number::rational(0.9));

    let roots = solver.solve_at(&fun, Number::integer(-100), Number::integer(100));
    assert_eq!(roots.len(), 3);
    assert!(roots[0] > Number::integer(-8) && roots[0] < Number::rational(-7.6));
    assert!(roots[1] > Number::integer(-1) && roots[1] < Number::rational(-0.7));
    assert!(roots[2] > Number::rational(0.6) && roots[2] < Number::rational(0.9));
}

/*
    x^3 + 8x^2 + 2 = 0 interval (-9; 2)
    x1 ~= -8.1
    Тут два из трех корней комплексные
*/
#[test]
fn test_inverval_with_complex_roots() {
    let solver = create();
    let fun = Sum::new(vec![
        Var::new(2, Number::integer(8), format!("x")).as_norm(),
        Var::new(3, Number::integer(1), format!("x")).as_norm(),
        Number::integer(2).as_norm(),
    ])
    .as_norm();
    let roots = solver.solve_at(&fun, Number::rational(-9.0), Number::integer(2));
    assert_eq!(roots.len(), 1);
    assert!(roots[0] > Number::rational(-8.2) && roots[0] < Number::integer(-8));

    let roots = solver.solve_at(&fun, Number::integer(-7), Number::integer(2));
    assert_eq!(roots.len(), 0);
}

#[test]
fn test_solve_and_validate() {
    // panic on
    // "x^4 - 8*x^3 + x^2 + 2 = 0" -100000 100000
    let interval = (Number::integer(-100000), Number::integer(100000));
    let solver = create_full(Config::new().set_interval(Some(interval)));
    let sum = Sum::new(vec![
        Var::new(2, Number::integer(1), format!("x")).as_norm(),
        Var::new(3, Number::integer(-8), format!("x")).as_norm(),
        Var::new(4, Number::integer(1), format!("x")).as_norm(),
        Number::integer(2).as_norm(),
    ])
    .as_expr();
    let eq =
        BinaryOperation::new(Operator::Eql, sum, Number::integer(0).as_expr()).as_expr();
    solver.solve_expr(&eq).unwrap();
}

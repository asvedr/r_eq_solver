mod calculator;
mod concrete_solvers;
mod derivative_solver;
mod derivativer;
mod preprocessors;
mod replacer;
mod replacer_rules;
mod solver;
#[cfg(test)]
mod tests;
pub mod utils;
mod validator;

use std::rc::Rc;

use crate::proto::normalizer::INormalizer;

use crate::entities::config::Config;
use crate::single_var_solver::replacer::Replacer;
use crate::single_var_solver::validator::Validator;
pub use solver::Solver;

pub fn create_replacer(normalizer: Rc<dyn INormalizer>) -> Replacer {
    let replacer_rules = replacer_rules::rules(normalizer);
    Replacer::new(replacer_rules)
}

pub fn create_validator(normalizer: Rc<dyn INormalizer>) -> Validator {
    let replacer = create_replacer(normalizer.clone());
    Validator::new(normalizer, Box::new(replacer))
}

pub fn create(config: Rc<Config>, normalizer: Rc<dyn INormalizer>) -> Solver {
    let derivativer = derivativer::Derivativer::new(normalizer.clone());
    let derivative_solver = derivative_solver::DerivativeSolver::new(
        Box::new(derivativer),
        Box::new(calculator::Calculator::new()),
    );
    let validator = create_validator(normalizer.clone());
    Solver::new(
        config,
        normalizer.clone(),
        concrete_solvers::solvers(),
        preprocessors::create(normalizer),
        Box::new(derivative_solver),
        Box::new(validator),
    )
}

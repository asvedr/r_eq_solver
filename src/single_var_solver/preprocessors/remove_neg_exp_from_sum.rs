use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, Div, ExpressionType, PNormalized, Sum};
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::IPreprocessor;

pub struct Preprocessor {
    normalizer: Rc<dyn INormalizer>,
}

impl Preprocessor {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Preprocessor {
        Preprocessor { normalizer }
    }
}

impl IPreprocessor for Preprocessor {
    fn process(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        if fun.get_type() != ExpressionType::Sum {
            return Ok(fun);
        }
        let mut items: Vec<PNormalized> = Vec::new();
        let mut div_var_found = false;
        let mut not_div_var_found = false;
        for item in cast_norm::<Sum>(&fun).unwrap().items.iter() {
            if item.get_type() != ExpressionType::Var {
                items.push(item.clone());
                continue;
            }
            if let Some((top, bottom)) = item.as_div() {
                items.push(Rc::new(Div::new(top, bottom)));
                div_var_found = true;
            } else {
                items.push(item.clone());
                not_div_var_found = true;
            }
        }
        if !(div_var_found && not_div_var_found) {
            return Ok(fun);
        }
        let sum = Rc::new(Sum::new(items));
        let result = self.normalizer.shortify(sum)?;
        Ok(result)
    }
}

use crate::entities::errors::SolverResult;
use crate::entities::expression::{cast_norm, Div, ExpressionType, PNormalized};
use crate::proto::single_var_solver::IPreprocessor;

pub struct Preprocessor {}

impl Preprocessor {
    pub fn new() -> Preprocessor {
        Preprocessor {}
    }
}

impl IPreprocessor for Preprocessor {
    fn process(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        if fun.get_type() != ExpressionType::Div {
            return Ok(fun);
        }
        Ok(cast_norm::<Div>(&fun).unwrap().top.clone())
    }
}

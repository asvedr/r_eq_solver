use std::rc::Rc;

use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_norm, contain_root, get_lcm_of_roots, ExpressionType, Mul, PNormalized, Root,
    Sum,
};
use crate::entities::solver::Equation;
use crate::proto::normalizer::{IMultiplyController, INormalizer};
use crate::proto::single_var_solver::IPreprocessor;
use crate::single_var_solver::utils::pow_norm;
use num_bigint::BigInt;

pub struct Preprocessor {
    normalizer: Rc<dyn INormalizer>,
    m_controller: Box<dyn IMultiplyController>,
}

impl Preprocessor {
    pub fn new(
        normalizer: Rc<dyn INormalizer>,
        m_controller: Box<dyn IMultiplyController>,
    ) -> Preprocessor {
        Preprocessor {
            normalizer,
            m_controller,
        }
    }

    fn get_target_exp(&self, fun: &PNormalized) -> BigInt {
        match fun.get_type() {
            ExpressionType::Root => cast_norm::<Root>(fun).unwrap().exp.clone(),
            ExpressionType::Mul => {
                let mul = cast_norm::<Mul>(fun).unwrap();
                get_lcm_of_roots(&mul.items)
            }
            _ => unreachable!(),
        }
    }

    fn pow_fun(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        let exp = self.get_target_exp(&fun);
        pow_norm(&*self.normalizer, fun, exp)
    }

    fn separate_sum_by_roots(&self, sum: &Sum) -> SolverResult<Equation<PNormalized>> {
        let mut left = vec![];
        let mut right = vec![];
        for item in sum.items.iter() {
            if contain_root(item) {
                left.push(item.clone())
            } else {
                right.push(item.clone())
            }
        }
        let left = self.normalizer.shortify(Rc::new(Sum::new(left)))?;
        let right = self.normalizer.shortify(Rc::new(Sum::new(right)))?;
        Ok(Equation {
            left,
            right: right.apply_neg(),
        })
    }

    fn process_sum(&self, sum: PNormalized) -> SolverResult<PNormalized> {
        let eq = self.separate_sum_by_roots(cast_norm(&sum).unwrap())?;
        let mut mults = self.m_controller.get_multipliers(&eq.left);
        mults.only_roots();
        if mults.is_empty() {
            return Ok(sum);
        }
        let a = mults.as_mul();
        let b = self
            .m_controller
            .shortify(&*self.normalizer, &eq.left, mults)?;
        if contain_root(&b) {
            // We can not do anything about this so just return
            return Ok(sum);
        }
        let exp = self.get_target_exp(&a);
        let new_a = pow_norm(&*self.normalizer, a, exp.clone())?;
        let new_b = pow_norm(&*self.normalizer, b, exp.clone())?;
        let right = pow_norm(&*self.normalizer, eq.right, exp)?;
        let mul = Mul::two(new_a, new_b);
        let short_mul = self.normalizer.shortify(Rc::new(mul))?;
        let new_sum = Sum::new(vec![short_mul, right.apply_neg()]);
        let result = self.normalizer.shortify(Rc::new(new_sum))?;
        if contain_root(&result) {
            let formatted = sum.format();
            eprintln!("open_roots> {}", formatted);
            return Err(SolverError::ExpressionIsVeryComplex(formatted));
        }
        Ok(result)
    }
}

impl IPreprocessor for Preprocessor {
    // fun(x) == 0
    fn process(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        if !contain_root(&fun) {
            return Ok(fun);
        }
        match fun.get_type() {
            ExpressionType::Root => {
                let root = cast_norm::<Root>(&fun).unwrap();
                Ok(root.expr.clone())
            }
            ExpressionType::Mul => self.pow_fun(fun),
            ExpressionType::Sum => self.process_sum(fun),
            _ => Ok(fun),
        }
    }
}

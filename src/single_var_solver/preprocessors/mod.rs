mod open_roots;
mod remove_division;
mod remove_neg_exp_from_sum;
mod shortify_roots;

use std::rc::Rc;

use crate::normalizer::MultiplyController;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::IPreprocessor;

pub fn create(normalizer: Rc<dyn INormalizer>) -> Vec<Box<dyn IPreprocessor>> {
    let m_controller = Box::new(MultiplyController::new());
    vec![
        Box::new(remove_division::Preprocessor::new()),
        Box::new(remove_neg_exp_from_sum::Preprocessor::new(
            normalizer.clone(),
        )),
        Box::new(shortify_roots::Preprocessor::new(normalizer.clone())),
        Box::new(open_roots::Preprocessor::new(normalizer, m_controller)),
    ]
}

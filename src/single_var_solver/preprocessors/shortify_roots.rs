use std::rc::Rc;

use num_traits::Zero;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, contain_root, ExpressionType, IExpression, Mul, Operator, PNormalized,
    Root, Sum,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::IPreprocessor;

pub struct Preprocessor {
    normalizer: Rc<dyn INormalizer>,
}

impl Preprocessor {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Preprocessor {
        Preprocessor { normalizer }
    }

    fn process_root(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        let root = cast_norm::<Root>(&fun).unwrap();
        if root.exp == root.root {
            let mult = Mul::two(root.expr.clone(), root.coefficient.as_norm());
            return self.normalizer.shortify(Rc::new(mult));
        }
        if (&root.exp % &root.root).is_zero() {
            let exp = &root.exp / &root.root;
            let call = NormalizedBin::new(
                Operator::Pow,
                root.expr.clone(),
                Rc::new(Number::integer(exp)),
            );
            let expr = self.normalizer.normalize_n_binary(&call)?;
            let mult = Mul::two(expr, root.coefficient.as_norm());
            return self.normalizer.shortify(Rc::new(mult));
        }
        Ok(fun)
    }

    fn process_mul(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        let mul = cast_norm::<Mul>(&fun).unwrap();
        let mut items = vec![];
        for item in mul.items.iter() {
            items.push(self.process(item.clone())?);
        }
        let result = Mul::new(mul.coefficient.clone(), items);
        self.normalizer.shortify(Rc::new(result))
    }

    fn process_sum(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        let sum = cast_norm::<Sum>(&fun).unwrap();
        let mut items = vec![];
        for item in sum.items.iter() {
            items.push(self.process(item.clone())?);
        }
        let result = Sum::new(items);
        self.normalizer.shortify(Rc::new(result))
    }
}

impl IPreprocessor for Preprocessor {
    // fun(x) == 0
    fn process(&self, fun: PNormalized) -> SolverResult<PNormalized> {
        if !contain_root(&fun) {
            return Ok(fun);
        }
        match fun.get_type() {
            ExpressionType::Root => self.process_root(fun),
            ExpressionType::Mul => self.process_mul(fun),
            ExpressionType::Sum => self.process_sum(fun),
            _ => Ok(fun),
        }
    }
}

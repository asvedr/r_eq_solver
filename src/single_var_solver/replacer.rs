use crate::entities::errors::SolverResult;
use crate::entities::expression::{ExpressionType, PExpression, PNormalized};
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};
use std::collections::HashMap;

pub struct Replacer {
    rules: HashMap<ExpressionType, Box<dyn IReplacerRule>>,
}

impl Replacer {
    pub fn new(rules_vec: Vec<Box<dyn IReplacerRule>>) -> Replacer {
        let rules = rules_vec
            .into_iter()
            .map(|rule| (rule.get_type(), rule))
            .collect::<HashMap<ExpressionType, Box<dyn IReplacerRule>>>();
        Replacer { rules }
    }
}

impl IReplacer for Replacer {
    fn replace_vars_in_expr_with_flag(
        &self,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let mut total_changed = false;
        let mut expr: PExpression = expr.clone();
        loop {
            let rule = self.rules.get(&expr.get_type()).unwrap();
            let (result, changed) = rule.replace_expr(self, &expr, vars);
            if !changed {
                return (expr, total_changed);
            }
            total_changed = true;
            expr = result;
        }
    }

    fn replace_vars_in_norm_with_flag(
        &self,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        let mut total_changed = false;
        let mut expr: PNormalized = expr.clone();
        loop {
            let rule = self.rules.get(&expr.get_type()).unwrap();
            let (result, changed) = rule.replace_norm(self, &expr, vars)?;
            if !changed {
                return Ok((expr, total_changed));
            }
            total_changed = true;
            expr = result;
        }
    }
}

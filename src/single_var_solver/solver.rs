use std::rc::Rc;

use crate::entities::config::Config;
use crate::entities::constants::{DERIVATIVE_COMPLEXITY, GIANT_NUMBERS};
use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{
    cast_expr, cast_norm, contain_root, expr_to_equation, BinaryOperation, Div,
    IExpression, Operator, PExpression, PNormalized, Sum, Var,
};
use crate::entities::solver::{Equation, Solution, SolutionOnlyResult};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{
    IConcreteEquationSolver, IDerivativeSolver, IPreprocessor, ISolver, IValidator,
};
use std::collections::HashMap;

type EquationN = Equation<PNormalized>;
type EquationE = Equation<PExpression>;
type ConcreteSolution = (Vec<Number>, Vec<String>, usize);

pub struct Solver {
    config: Rc<Config>,
    normalizer: Rc<dyn INormalizer>,
    concrete_solvers: Vec<Box<dyn IConcreteEquationSolver>>,
    preprocessors: Vec<Box<dyn IPreprocessor>>,
    derivative_solver: Box<dyn IDerivativeSolver>,
    validator: Box<dyn IValidator>,
}

fn collect_var_name(found: &mut Option<String>, var: Option<String>) -> SolverResult<()> {
    let new_name = match var {
        Some(val) => val,
        _ => return Ok(()),
    };
    if let Some(old_name) = found {
        if new_name != *old_name {
            let vars = format!("{}, {}", old_name, new_name);
            return Err(SolverError::TooManyVars(vars));
        }
    } else {
        *found = Some(new_name)
    }
    Ok(())
}

impl Solver {
    pub fn new(
        config: Rc<Config>,
        normalizer: Rc<dyn INormalizer>,
        concrete_solvers: Vec<Box<dyn IConcreteEquationSolver>>,
        preprocessors: Vec<Box<dyn IPreprocessor>>,
        derivative_solver: Box<dyn IDerivativeSolver>,
        validator: Box<dyn IValidator>,
    ) -> Solver {
        Solver {
            config,
            normalizer,
            concrete_solvers,
            derivative_solver,
            preprocessors,
            validator,
        }
    }

    fn apply_preprocessors(&self, mut fun: PNormalized) -> SolverResult<PNormalized> {
        loop {
            let original = fun.clone();
            for preprocessor in self.preprocessors.iter() {
                fun = preprocessor.process(fun)?;
            }
            if fun.cmp_norm(&original) {
                return Ok(fun);
            }
        }
    }

    fn pre_check(&self, fun: &PNormalized) -> SolverResult<()> {
        if contain_root(&fun) {
            let msg = format!("{} = 0", fun.format());
            return Err(SolverError::EquationContainsRoot(msg));
        }
        Ok(())
    }

    fn make_equation(
        &self,
        source: &PExpression,
    ) -> SolverResult<(EquationE, EquationN)> {
        let original = expr_to_equation(source)?;
        let expr = BinaryOperation::new(
            Operator::Sub,
            original.left.clone(),
            original.right.clone(),
        )
        .as_expr();
        let expr = self.normalizer.normalize(&expr)?;
        let expr = self.apply_preprocessors(expr)?;
        self.pre_check(&expr)?;
        let normalized = Equation {
            left: expr,
            right: self.normalizer.constants().norm_zero.clone(),
        };
        Ok((original, normalized))
    }

    fn prepare_norm_equation(&self, source: &EquationN) -> SolverResult<EquationN> {
        let fun = Sum::new(vec![source.left.clone(), source.right.apply_neg()]);
        let fun = self.normalizer.shortify(Rc::new(fun))?;
        let fun = self.apply_preprocessors(fun)?;
        self.pre_check(&fun)?;
        let result = Equation {
            left: fun,
            right: self.normalizer.constants().norm_zero.clone(),
        };
        Ok(result)
    }

    fn concrete_solution(
        &self,
        equation: &Equation<PNormalized>,
    ) -> SolverResult<Option<ConcreteSolution>> {
        for rule in self.concrete_solvers.iter() {
            if rule.match_equation_type(&equation.left) {
                let solution = rule.solve(self, &equation.left)?;
                return Ok(Some(solution));
            }
        }
        Ok(None)
    }

    fn validate_results(
        &self,
        original_equation: &Equation<PExpression>,
        var: &str,
        solutions: Vec<Number>,
    ) -> SolverResult<Vec<Number>> {
        let mut result = vec![];
        for solution in solutions {
            let mut rounded_map = HashMap::new();
            rounded_map.insert(var.to_string(), solution.round().as_expr());
            let valid = self
                .validator
                .validate_expr_eq(original_equation, &rounded_map)?;
            if valid {
                result.push(solution.round());
                continue;
            }
            // rounding has failed
            // let map = vec![(var.to_string(), solution.clone())];
            let mut map = HashMap::new();
            map.insert(var.to_string(), solution.as_expr());
            let valid = self.validator.validate_expr_eq(original_equation, &map)?;
            if valid {
                result.push(solution)
            }
        }
        Ok(result)
    }

    fn extract_var_name_expr(
        &self,
        expression: &PExpression,
    ) -> SolverResult<Option<String>> {
        if let Some(var) = cast_expr::<Var>(expression) {
            return Ok(Some(var.name.clone()));
        }
        let mut found = None;
        for child in expression.children().iter() {
            let var = self.extract_var_name_expr(child)?;
            collect_var_name(&mut found, var)?;
        }
        Ok(found)
    }

    fn extract_var_name_norm(
        &self,
        expression: &PNormalized,
    ) -> SolverResult<Option<String>> {
        if let Some(var) = cast_norm::<Var>(expression) {
            return Ok(Some(var.name.clone()));
        }
        let mut found = None;
        for child in expression.norm_children().iter() {
            let var = self.extract_var_name_norm(child)?;
            collect_var_name(&mut found, var)?;
        }
        Ok(found)
    }

    fn derivative_solution(
        &self,
        equation: &Equation<PNormalized>,
        interval: &(Number, Number),
    ) -> SolverResult<Vec<Number>> {
        let (ref from, ref to) = interval;
        let result =
            self.derivative_solver
                .solve_at(&equation.left, from.clone(), to.clone());
        Ok(result)
    }

    fn get_roots_complexity(&self, roots: &[Number]) -> usize {
        for root in roots {
            if root.is_difficult() {
                return GIANT_NUMBERS;
            }
        }
        0
    }

    fn get_solution(
        &self,
        var: &str,
        to_solve: &EquationN,
        to_check: &EquationE,
    ) -> SolverResult<(Vec<Number>, Vec<String>, usize)> {
        let concrete_solution = self.concrete_solution(&to_solve)?;
        if let Some((roots, methods, mut complexity)) = concrete_solution {
            let roots = self.validate_results(&to_check, var, roots)?;
            complexity += self.get_roots_complexity(&roots);
            return Ok((roots, methods, complexity));
        }
        if let Some(ref interval) = self.config.interval {
            let roots = self.derivative_solution(&to_solve, interval)?;
            let roots = self.validate_results(&to_check, var, roots)?;
            let methods = vec!["derivative".to_string()];
            return Ok((roots, methods, DERIVATIVE_COMPLEXITY));
        }
        Err(SolverError::SolutionNotFound)
    }
}

impl ISolver for Solver {
    fn solve_expr(&self, source: &PExpression) -> SolverResult<Solution> {
        let var = match self.extract_var_name_expr(source)? {
            Some(var) => var,
            None => return Err(SolverError::VarNotFound),
        };
        let (original, equation) = self.make_equation(source)?;
        let (roots, methods, complexity) =
            self.get_solution(&var, &equation, &original)?;
        let solution = Solution {
            source_equation: original,
            normalized_equation: equation,
            used_methods: methods,
            complexity,
            roots,
        };
        Ok(solution)
    }

    fn solve_norm(
        &self,
        equation: &Equation<PNormalized>,
    ) -> SolverResult<SolutionOnlyResult> {
        // use this div for recursive search for var names
        let div = Div::new(equation.left.clone(), equation.right.clone());
        let var = match self.extract_var_name_norm(&div.as_norm())? {
            Some(var) => var,
            None => return Err(SolverError::VarNotFound),
        };
        let prepared = self.prepare_norm_equation(equation)?;
        let eq_for_check = Equation {
            left: equation.left.as_expr(),
            right: equation.right.as_expr(),
        };
        let (roots, methods, complexity) =
            self.get_solution(&var, &prepared, &eq_for_check)?;
        let solution = SolutionOnlyResult {
            used_methods: methods,
            complexity,
            roots,
        };
        Ok(solution)
    }
}

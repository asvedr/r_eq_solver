use std::rc::Rc;

use crate::entities::constants::MAX_INACCURACY_DENOMINATOR;
use crate::entities::errors::{SolverError, SolverResult};
use crate::entities::expression::{cast_norm, PExpression, PNormalized};
use crate::entities::solver::Equation;
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{IReplacer, IValidator, VarExprMap, VarNormMap};

pub struct Validator {
    normalizer: Rc<dyn INormalizer>,
    replacer: Box<dyn IReplacer>,
    max_inaccuracy: Number,
}

impl Validator {
    pub fn new(
        normalizer: Rc<dyn INormalizer>,
        replacer: Box<dyn IReplacer>,
    ) -> Validator {
        let max_inaccuracy = Number::fraction(1, MAX_INACCURACY_DENOMINATOR);
        Validator {
            normalizer,
            replacer,
            max_inaccuracy,
        }
    }

    fn check_eql_numbers(
        &self,
        a_norm: &PNormalized,
        b_norm: &PNormalized,
    ) -> SolverResult<bool> {
        if let (Some(a), Some(b)) = (cast_norm::<Number>(a_norm), cast_norm(b_norm)) {
            Ok((a - b).abs() < self.max_inaccuracy)
        } else {
            let msg = format!("{} = {}", a_norm.format(), b_norm.format());
            Err(SolverError::ValidatorValueIsNotANumber(msg))
        }
    }
}

impl IValidator for Validator {
    fn validate_norm_eq(
        &self,
        equation: &Equation<PNormalized>,
        vars: &VarNormMap,
    ) -> SolverResult<bool> {
        let left = try_convert_ret!(
            self.replacer.replace_vars_in_norm(&equation.left, vars),
            false
        );
        let right_wrapped = self.replacer.replace_vars_in_norm(&equation.right, vars);
        let right = try_convert_ret!(right_wrapped, false);
        self.check_eql_numbers(&left, &right)
    }

    fn validate_expr_eq(
        &self,
        equation: &Equation<PExpression>,
        vars: &VarExprMap,
    ) -> SolverResult<bool> {
        let left = self.replacer.replace_vars_in_expr(&equation.left, vars);
        let left = try_convert_ret!(self.normalizer.normalize(&left), false);
        let right = self.replacer.replace_vars_in_expr(&equation.right, vars);
        let right = try_convert_ret!(self.normalizer.normalize(&right), false);
        self.check_eql_numbers(&left, &right)
    }
}

use num_bigint::BigInt;
use num_traits::identities::Zero;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_norm, BinaryOperation, ExpressionType, IExpression, Operator, PExpression,
    PNormalized, Sum, Var,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::INormalizer;

pub fn get_pow_values_of_sum(sum: &Sum) -> Vec<BigInt> {
    sum.items
        .iter()
        .map(|item| {
            if let Some(var) = cast_norm::<Var>(item) {
                var.pow.clone()
            } else if item.get_type() == ExpressionType::Number {
                BigInt::zero()
            } else {
                panic!("sum items must be num or var at this point")
            }
        })
        .collect::<Vec<BigInt>>()
}

pub fn add(a: PExpression, b: PExpression) -> PExpression {
    BinaryOperation::new(Operator::Add, a.as_expr(), b.as_expr()).as_expr()
}

pub fn mul(a: PExpression, b: PExpression) -> PExpression {
    BinaryOperation::new(Operator::Mul, a.as_expr(), b.as_expr()).as_expr()
}

pub fn pow(a: PExpression, b: PExpression) -> PExpression {
    BinaryOperation::new(Operator::Pow, a.as_expr(), b.as_expr()).as_expr()
}

pub fn pow_norm(
    norm: &dyn INormalizer,
    expr: PNormalized,
    exp: BigInt,
) -> SolverResult<PNormalized> {
    let exp = Number::integer(exp).as_norm();
    let bin = NormalizedBin::new(Operator::Pow, expr, exp);
    norm.normalize_n_binary(&bin)
}

pub fn eq(a: PExpression, b: PExpression) -> PExpression {
    BinaryOperation::new(Operator::Eql, a.as_expr(), b.as_expr()).as_expr()
}

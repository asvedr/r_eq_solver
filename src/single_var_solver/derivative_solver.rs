use crate::entities::constants::{MAX_INACCURACY_DENOMINATOR, MINIMAL_STEP_DENOMINATOR};
use crate::entities::expression::PNormalized;
use crate::number::Number;
use crate::proto::single_var_solver::{ICalculator, IDerivativeSolver, IDerivativer};

pub struct DerivativeSolver {
    derivativer: Box<dyn IDerivativer>,
    calculator: Box<dyn ICalculator>,
    minimal_step: Number,
    max_inaccuracy: Number,
    two: Number,
}

struct Functions {
    original: PNormalized,
    derivatives: Vec<PNormalized>,
}

macro_rules! check_end {
    ($to:expr, $from:expr, $ret:expr) => {
        if $to == $from {
            return $ret;
        }
    };
}

fn add_to_roots(roots: &mut Vec<Number>, root: &Number) {
    for x in roots.iter() {
        if root == x {
            return;
        }
    }
    roots.push(root.clone())
}

impl DerivativeSolver {
    pub fn new(
        derivativer: Box<dyn IDerivativer>,
        calculator: Box<dyn ICalculator>,
    ) -> DerivativeSolver {
        let minimal_step = Number::fraction(1, MINIMAL_STEP_DENOMINATOR);
        let max_inaccuracy = Number::fraction(1, MAX_INACCURACY_DENOMINATOR);
        let two = Number::integer(2);
        DerivativeSolver {
            derivativer,
            calculator,
            minimal_step,
            max_inaccuracy,
            two,
        }
    }

    fn is_null_on(&self, expr: &PNormalized, value: &Number) -> bool {
        let num = self.calculator.calculate(expr, value);
        !num.is_nan() && num.abs() < self.max_inaccuracy
    }

    fn shift_to_not_null(
        &self,
        expr: &PNormalized,
        value: &Number,
        limit: &Number,
    ) -> Number {
        let step = &(limit - value).signum() * &self.minimal_step;
        let mut current: Number = value.clone();
        while current != *limit {
            if !self.is_null_on(expr, &current) {
                return current;
            }
            current = &current + &step;
        }
        current
    }

    fn sign_changed(&self, fun: &PNormalized, a: &Number, b: &Number) -> bool {
        let on_a = self.calculator.calculate(fun, a).signum();
        let on_b = self.calculator.calculate(fun, b).signum();
        on_a != on_b
    }

    fn derivative_sign_changes(
        &self,
        functions: &Functions,
        a: &Number,
        b: &Number,
    ) -> bool {
        for fun in &functions.derivatives {
            if self.sign_changed(fun, a, b) {
                return true;
            }
        }
        false
    }

    fn bisect(
        &self,
        functions: &Functions,
        mut from: Number,
        mut to: Number,
    ) -> Vec<Number> {
        check_end!(from, to, Vec::new());
        let mut roots = vec![];
        if self.is_null_on(&functions.original, &from) {
            add_to_roots(&mut roots, &from);
            from = self.shift_to_not_null(&functions.original, &from, &to);
        }
        check_end!(from, to, roots);
        if self.is_null_on(&functions.original, &to) {
            add_to_roots(&mut roots, &to);
            to = self.shift_to_not_null(&functions.original, &to, &from);
        }
        check_end!(from, to, roots);
        if !self.derivative_sign_changes(functions, &from, &to) {
            return roots;
        }
        if (&to - &from) < (&self.minimal_step * &Number::integer(2)) {
            if let Some(root) = self.finalize(functions, &from, &to) {
                add_to_roots(&mut roots, &root);
            }
        } else {
            let mid = (&from + &to) / Number::integer(2);
            for root in self.bisect(functions, from, mid.clone()) {
                add_to_roots(&mut roots, &root)
            }
            for root in self.bisect(functions, mid, to) {
                add_to_roots(&mut roots, &root)
            }
        }
        roots
    }

    // bisect in interval where the function changes it's sign
    // continue bisect till will be small enought
    fn finalize(
        &self,
        functions: &Functions,
        from: &Number,
        to: &Number,
    ) -> Option<Number> {
        if !self.sign_changed(&functions.original, from, to) {
            return None;
        }
        let mid = &(from + to) / &self.two;
        if self.is_null_on(&functions.original, &mid) {
            Some(mid)
        } else if let Some(val) = self.finalize(functions, from, &mid) {
            Some(val)
        } else {
            self.finalize(functions, &mid, to)
        }
    }
}

impl IDerivativeSolver for DerivativeSolver {
    fn solve_at(&self, left_part: &PNormalized, from: Number, to: Number) -> Vec<Number> {
        let derivatives = self.derivativer.all_derivatives(left_part);
        let functions = Functions {
            original: left_part.clone(),
            derivatives,
        };
        self.bisect(&functions, from, to)
    }
}

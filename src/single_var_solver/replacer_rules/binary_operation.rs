use std::rc::Rc;

use crate::entities::expression::{
    cast_expr, BinaryOperation, ExpressionType, PExpression,
};
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap};

pub struct Rule {}

impl Rule {
    pub fn new() -> Rule {
        Rule {}
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::BinaryOperation
    }

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let bin = cast_expr::<BinaryOperation>(expr).unwrap();
        let (left, changed_left) =
            replacer.replace_vars_in_expr_with_flag(&bin.left, vars);
        let (right, changed_right) =
            replacer.replace_vars_in_expr_with_flag(&bin.right, vars);
        if !(changed_left || changed_right) {
            return (expr.clone(), false);
        }
        let new_bin = BinaryOperation::new(bin.operator, left, right);
        (Rc::new(new_bin), true)
    }
}

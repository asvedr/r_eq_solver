use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_expr, cast_norm, BinaryOperation, ExpressionType, IExpression, Mul, Operator,
    PExpression, PNormalized, Var,
};
use crate::entities::normalizer::NormalizedBin;
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Var
    }

    fn replace_expr(
        &self,
        _replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let var: &Var = cast_expr(expr).unwrap();
        let value = match vars.get(&var.name) {
            Some(value) => value,
            _ => return (expr.clone(), false),
        };
        let pow = BinaryOperation::new(
            Operator::Pow,
            value.clone(),
            Rc::new(Number::integer(var.pow.clone())),
        );
        let result =
            BinaryOperation::new(Operator::Mul, var.coefficient.as_expr(), Rc::new(pow));
        (Rc::new(result), true)
    }

    fn replace_norm(
        &self,
        _replacer: &dyn IReplacer,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        let var: &Var = cast_norm(expr).unwrap();
        let value = match vars.get(&var.name) {
            Some(value) => value,
            _ => return Ok((expr.clone(), false)),
        };
        let pow = NormalizedBin::new(
            Operator::Pow,
            value.clone(),
            Rc::new(Number::integer(var.pow.clone())),
        );
        let exp = self.normalizer.normalize_n_binary(&pow)?;
        let mul = Mul::two(var.coefficient.as_norm(), exp);
        let result = self.normalizer.shortify(Rc::new(mul))?;
        Ok((result, true))
    }
}

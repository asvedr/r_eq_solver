use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_expr, cast_norm, BinaryOperation, ExpressionType, IExpression, Operator,
    PExpression, PNormalized, Sum,
};
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Sum
    }

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let sum = cast_expr::<Sum>(expr).unwrap();
        let mut items: Vec<PExpression> = vec![];
        let mut changed = false;
        for item in &sum.items {
            let (val, flag) =
                replacer.replace_vars_in_expr_with_flag(&item.as_expr(), vars);
            changed = flag || changed;
            items.push(val);
        }
        if !changed {
            return (expr.clone(), false);
        }
        let mut root = items[0].clone();
        for item in items.iter().skip(1) {
            root = BinaryOperation::new(Operator::Add, root, item.clone()).as_expr();
        }
        (root, true)
    }

    fn replace_norm(
        &self,
        replacer: &dyn IReplacer,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        let sum = cast_norm::<Sum>(expr).unwrap();
        let mut items: Vec<PNormalized> = vec![];
        let mut changed = false;
        for item in &sum.items {
            let (val, flag) = replacer.replace_vars_in_norm_with_flag(&item, vars)?;
            changed = flag || changed;
            items.push(val);
        }
        if !changed {
            return Ok((expr.clone(), false));
        }
        let result = self.normalizer.shortify(Rc::new(Sum::new(items)))?;
        Ok((result, true))
    }
}

use std::rc::Rc;

use crate::entities::expression::{
    cast_expr, ExpressionType, PExpression, UnaryOperation,
};
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap};

pub struct Rule {}

impl Rule {
    pub fn new() -> Rule {
        Rule {}
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::UnaryOperation
    }

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let opr = cast_expr::<UnaryOperation>(expr).unwrap();
        let (child, changed) = replacer.replace_vars_in_expr_with_flag(&opr.arg, vars);
        if !changed {
            return (expr.clone(), false);
        }
        let new_opr = UnaryOperation::new(opr.operator, child);
        (Rc::new(new_opr), true)
    }
}

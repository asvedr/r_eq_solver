mod binary_operation;
mod div;
mod mul;
mod number;
mod root;
mod sum;
mod unary_operation;
mod var;

use std::rc::Rc;

use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::IReplacerRule;

pub fn rules(normalizer: Rc<dyn INormalizer>) -> Vec<Box<dyn IReplacerRule>> {
    vec![
        Box::new(var::Rule::new(normalizer.clone())),
        Box::new(sum::Rule::new(normalizer.clone())),
        Box::new(div::Rule::new(normalizer.clone())),
        Box::new(mul::Rule::new(normalizer.clone())),
        Box::new(root::Rule::new(normalizer)),
        Box::new(number::Rule::new()),
        Box::new(binary_operation::Rule::new()),
        Box::new(unary_operation::Rule::new()),
    ]
}

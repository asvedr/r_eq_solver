use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_expr, cast_norm, BinaryOperation, ExpressionType, IExpression, Mul, Operator,
    PExpression, PNormalized,
};
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Mul
    }

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let mul = cast_expr::<Mul>(expr).unwrap();
        let mut items: Vec<PExpression> = vec![];
        let mut changed = false;
        for item in &mul.items {
            let (val, flag) =
                replacer.replace_vars_in_expr_with_flag(&item.as_expr(), vars);
            changed = flag || changed;
            items.push(val);
        }
        if !changed {
            return (expr.clone(), false);
        }
        let mut root = mul.coefficient.as_expr();
        for item in items.iter() {
            root = BinaryOperation::new(Operator::Mul, root, item.clone()).as_expr();
        }
        (root, true)
    }

    fn replace_norm(
        &self,
        replacer: &dyn IReplacer,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        let mul = cast_norm::<Mul>(expr).unwrap();
        let mut items: Vec<PNormalized> = vec![];
        let mut changed = false;
        for item in &mul.items {
            let (val, flag) = replacer.replace_vars_in_norm_with_flag(&item, vars)?;
            changed = flag || changed;
            items.push(val);
        }
        if !changed {
            return Ok((expr.clone(), false));
        }
        let new_mul = Mul::new(mul.coefficient.clone(), items);
        let result = self.normalizer.shortify(Rc::new(new_mul))?;
        Ok((result, true))
    }
}

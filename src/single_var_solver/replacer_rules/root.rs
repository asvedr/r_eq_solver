use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_expr, cast_norm, BinaryOperation, ExpressionType, IExpression, Operator,
    PExpression, PNormalized, Root,
};
use crate::number::Number;
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Root
    }

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let root = cast_expr::<Root>(expr).unwrap();
        let (child, changed) =
            replacer.replace_vars_in_expr_with_flag(&root.expr.as_expr(), vars);
        if !changed {
            return (expr.clone(), false);
        }
        let exp_child =
            (Number::integer(1) / Number::integer(root.root.clone())).as_expr();
        let exp_super = Number::integer(root.exp.clone()).as_expr();
        let child = BinaryOperation::new(Operator::Pow, child, exp_child);
        let child = BinaryOperation::new(Operator::Pow, child.as_expr(), exp_super);
        let expr = BinaryOperation::new(
            Operator::Mul,
            root.coefficient.as_expr(),
            child.as_expr(),
        );
        (expr.as_expr(), true)
    }

    fn replace_norm(
        &self,
        replacer: &dyn IReplacer,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        let root = cast_norm::<Root>(expr).unwrap();
        let (replaced, changed) =
            replacer.replace_vars_in_norm_with_flag(&root.expr, vars)?;
        if !changed {
            return Ok((expr.clone(), false));
        }
        let new_expr = Root::new(
            root.exp.clone(),
            root.root.clone(),
            root.coefficient.clone(),
            replaced,
        );
        let result = self.normalizer.shortify(Rc::new(new_expr))?;
        Ok((result, true))
    }
}

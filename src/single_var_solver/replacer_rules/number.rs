use crate::entities::errors::SolverResult;
use crate::entities::expression::{ExpressionType, PExpression, PNormalized};
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};

pub struct Rule {}

impl Rule {
    pub fn new() -> Rule {
        Rule {}
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Number
    }

    fn replace_expr(
        &self,
        _replacer: &dyn IReplacer,
        expr: &PExpression,
        _vars: &VarExprMap,
    ) -> (PExpression, bool) {
        (expr.clone(), false)
    }

    fn replace_norm(
        &self,
        _replacer: &dyn IReplacer,
        expr: &PNormalized,
        _vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        Ok((expr.clone(), false))
    }
}

use std::rc::Rc;

use crate::entities::errors::SolverResult;
use crate::entities::expression::{
    cast_expr, cast_norm, BinaryOperation, Div, ExpressionType, Operator, PExpression,
    PNormalized,
};
use crate::proto::normalizer::INormalizer;
use crate::proto::single_var_solver::{IReplacer, IReplacerRule, VarExprMap, VarNormMap};

pub struct Rule {
    normalizer: Rc<dyn INormalizer>,
}

impl Rule {
    pub fn new(normalizer: Rc<dyn INormalizer>) -> Rule {
        Rule { normalizer }
    }
}

impl IReplacerRule for Rule {
    fn get_type(&self) -> ExpressionType {
        ExpressionType::Div
    }

    fn replace_expr(
        &self,
        replacer: &dyn IReplacer,
        expr: &PExpression,
        vars: &VarExprMap,
    ) -> (PExpression, bool) {
        let div = cast_expr::<Div>(expr).unwrap();
        let (top, top_flag) =
            replacer.replace_vars_in_expr_with_flag(&div.top.as_expr(), vars);
        let (bot, bot_flag) =
            replacer.replace_vars_in_expr_with_flag(&div.bottom.as_expr(), vars);
        if !(top_flag || bot_flag) {
            return (expr.clone(), false);
        }
        let result = BinaryOperation::new(Operator::Div, top, bot);
        (Rc::new(result), true)
    }

    fn replace_norm(
        &self,
        replacer: &dyn IReplacer,
        expr: &PNormalized,
        vars: &VarNormMap,
    ) -> SolverResult<(PNormalized, bool)> {
        let div = cast_norm::<Div>(expr).unwrap();
        let (top, top_flag) = replacer.replace_vars_in_norm_with_flag(&div.top, vars)?;
        let (bot, bot_flag) =
            replacer.replace_vars_in_norm_with_flag(&div.bottom, vars)?;
        if !(top_flag || bot_flag) {
            return Ok((expr.clone(), false));
        }
        let result = self.normalizer.shortify(Rc::new(Div::new(top, bot)))?;
        Ok((result, true))
    }
}

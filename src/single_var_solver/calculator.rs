use crate::entities::expression::{
    cast_norm, Div, ExpressionType, PNormalized, Sum, Var,
};
use crate::number::Number;
use crate::proto::single_var_solver::ICalculator;

pub struct Calculator {}

impl Calculator {
    pub fn new() -> Calculator {
        Calculator {}
    }

    fn process_var(&self, var: &Var, value: &Number) -> Number {
        &var.coefficient * &value.pow(&Number::integer(var.pow.clone()))
    }

    fn process_sum(&self, sum: &Sum, value: &Number) -> Number {
        let mut result = Number::integer(0);
        for item in &sum.items {
            result = result + self.calculate(item, value)
        }
        result
    }

    fn process_div(&self, div: &Div, value: &Number) -> Number {
        self.calculate(&div.top, value) / self.calculate(&div.bottom, value)
    }
}

impl ICalculator for Calculator {
    fn calculate(&self, expr: &PNormalized, value: &Number) -> Number {
        match expr.get_type() {
            ExpressionType::Number => cast_norm::<Number>(expr).unwrap().clone(),
            ExpressionType::Var => {
                self.process_var(cast_norm::<Var>(expr).unwrap(), value)
            }
            ExpressionType::Sum => {
                self.process_sum(cast_norm::<Sum>(expr).unwrap(), value)
            }
            ExpressionType::Div => {
                self.process_div(cast_norm::<Div>(expr).unwrap(), value)
            }
            _ => unreachable!(),
        }
    }
}

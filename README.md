# Equation Solver

## What is it
Simple software to find solutions for equation like
`ax^n + bx^(n-1) + cx^(n-2) + ... + const = 0` or system of those equations. The software can simplify source equation like `x * 2 * x + 3 ^ (x - x) = 1` will be simplified to
`2*x^2 = 0`. Linear and square equations can be solved via exact equations but for more complex equations the software can find only approximated solution. The software uses long integer math instead of native integers and fractions instead of float type for more accurate calculations.

## How to use
`./rsolver help` - show full manual\
`./rsolver help solve_equation` - get help for command\
`./rsolver solve_equation "2*x^2 = 0"` - find exact solution for equation\
`./rsolver solve_equation "2*x^2 = 0" -i -100,100` - find solution. If exact solution can not be found use approximated method on interval [-100 .. 100]\
`./rsolver solve_system "x^2 + y^2 = 2" "y = x*2"` - find exact solutions for system\
`./rsolver solve_system "(x^2 - z) / y = 0" "x + y^2 - z^3 = -53" "y^2 + x*y - y*z = y" -i -10,10` - find approximated solutions for system

## How to build
  1. Install rust environment from https://www.rust-lang.org/
  2. Run `sh build.sh`
  3. Your solution is `./rsolver`

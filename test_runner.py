import json
import os
import subprocess
import sys

test_dir = 'integration_tests'
solver = './rsolver'
suites = [os.path.join(test_dir, path) for path in os.listdir(test_dir)]


def process_suite(suite, schema):
    for (key, value) in schema.items():
        output = subprocess.check_output([solver] + value['args']).strip()
        expected = '\n'.join(value['out']).strip()
        if output != expected:
            print('Case failed: %s::%s' % (suite, key))
            print('>>>> EXPECTED\n%s\n====\n%s\n<<<< FOUND' % (expected, output))
            sys.exit(1)
        print('Case %s::%s passed' % (suite, key))


for suite in suites:
    with open(suite) as handler:
        schema = json.load(handler)
        process_suite(suite, schema)
